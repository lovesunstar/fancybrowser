;(function() {
  if(window["K_UCKUIWebViewVideoFinder"] != undefined) return;
  
  function isFrameAccessible(frame) {
  if(frame.stAccessible === true) {
  if(!frame.contentWindow || !frame.contentDocument) {
  frame.stAccessible = false;
  }
  }
  return (frame.stAccessible === true);
  }
  
  function trimQueryStringForUrl(aUrl) {
  if(aUrl && aUrl.length > 0) {
  var idx = aUrl.indexOf("?");
  if(idx > -1) {
  aUrl = aUrl.substring(0, idx);
  }
  }
  return aUrl;
  }
  
  function hookFrameVideos(docObj, frameEle) {
  if (docObj == undefined) {
		return 0;
  }
  var videoList = docObj.getElementsByTagName("video");
  var len = videoList.length;
  for (var i = 0; i < len; ++i) {
  UC_VIDEO_Hook_Play(videoList[i], frameEle);
  }
  }
  
  function getVideoNodeFromDoc(docObj, frameEle) {
  if (docObj == undefined) {
		return 0;
  }
  var videoList = docObj.getElementsByTagName("video");
  var len = videoList.length;
  for (var i = 0; i < len; ++i) {
  UC_VIDEO_Hook_Play(videoList[i], frameEle);
  }
  
  var hiddenVideo = 0;
  for (var j = 0; j < len; ++j) {
		var videoNode = videoList[j];
  var vSrc = UC_VIDEO_getSrcFromVideoNode(videoNode);
  if(vSrc && vSrc.length > 0) {
  var subCmptStyle = docObj.defaultView.getComputedStyle(videoNode);
  if(subCmptStyle.display == "none") {
  if(!hiddenVideo) { hiddenVideo = videoNode; }
  continue;
  }
  return videoNode;
  }
  }
  return hiddenVideo;
  }
  
  
  function UC_VIDEO_findVideoInfoObject() {
  var resultInfo = {};
  if(s_lastDetectedVideoInfo.videoUrl != undefined && s_lastDetectedVideoInfo.videoUrl.length > 0) {
  resultInfo = s_lastDetectedVideoInfo;
  } else {
  resultInfo = UC_VIDEO_findNewVideoInfo();
  }
  return resultInfo;
  }
  
  function UC_VIDEO_findVideoURL() {
  return JSON.stringify(UC_VIDEO_findVideoInfoObject());
  }
  
  function UC_VIDEO_updateErrorCode(videoNode) {
  var video = videoNode.oriHookVideo ? videoNode.oriHookVideo : videoNode;
  if (video.error && video.error.code) {
  s_lastDetectedVideoInfo.errorCode = (video.ucSrcReset === true) ? 0 : video.error.code;
  } else {
  s_lastDetectedVideoInfo.errorCode = 0;
  }
  }
  
  function UC_VIDEO_checkVideoSupported() {
  // MEDIA_ERR_SRC_NOT_SUPPORTED: 4
  return (s_lastDetectedVideoInfo.errorCode != 4);
  }
  
/*获取当前页面中<video>标签*/
  function UC_VIDEO_findNewVideoInfo() {
  var videoInfo = {videoUrl:"", posterUrl:""};
  var videoNode = UC_VIDEO_findVideoNode();
  if (videoNode) {
  UC_VIDEO_updateErrorCode(videoNode);
  if(UC_VIDEO_checkVideoSupported()) {
  videoInfo.videoUrl = UC_VIDEO_getSrcFromVideoNode(videoNode);
  videoInfo.posterUrl = UC_VIDEO_getPosterUrlForVideoNode(videoNode);
  } else {
  videoNode.src = ""; // 如果不支持播放，则置空url，禁止点击
  }
  }
  return videoInfo;
  }
  
/*获取当前页面中<video>标签*/
  function UC_VIDEO_findVideoNode() {
  ///< 先尝试主document*/
  var videoNode = getVideoNodeFromDoc(window.document, 0);
  if(videoNode) {
		return videoNode;
  }
  
  ///< 再尝试所有的iFrame
  var frameList = document.getElementsByTagName("iframe");
  for (var i = 0; i < frameList.length; ++i) {
  var frame = frameList[i];
  if (isFrameAccessible(frame))
  {
  var frameWindow = frame.contentWindow;
  /// 还没找到video，就尝试找出第一个包含video的frame；已经找到过了，继续查找剩余iframe以hook其它video
  if(!videoNode)
  {
  videoNode = getVideoNodeFromDoc(frameWindow.document, frame);
  }
  else
  {
  hookFrameVideos(frameWindow.document, frame);
  }
  }
  }
  return videoNode;
  }
  
  function UC_VIDEO_tryPlayWithInfo(videoInfo) {
  if (window.bridge) {
  window.bridge.post('video.play', videoInfo)
  }
  }
  
  function UC_VIDEO_tryLoadAtURL(videoURL) {
  if (window.bridge) {
  window.bridge.post('video.load', {'url': videoURL})
  }
  }
  
  function UC_VIDEO_getSrcFromVideoNode(vNode) {
  ///当src没有设置任何值时，vNode.src会返回当前页面url（不带锚点的部分）
  //var vsrc = (window.location.href != vNode.src) ? vNode.src : "";
  var vsrc = vNode.getAttribute("src");
  if(vsrc && vsrc.length > 0)
  {
  /*预防有些页面设置的是相对路径，.src属性能返回完整路径*/
  vsrc = vNode.src;
  }
  else
  {
  /*走到这里说明有video标签，但是没有src的地址，其实url还有可能藏在video标签的子节点中*/
  var childList = vNode.getElementsByTagName("source");
  for (var i = 0; i < childList.length; ++i)
  {
  var childNode = childList[i];
  var childNodeSrc = childNode.getAttribute("src");
  if (childNodeSrc && childNodeSrc.length > 0)
  {
  vsrc = childNode.src;
  break;
  }
  }
  }
  
  return vsrc ? vsrc : "";
  }
  
  function getExchangeNodeForVideoNode(oriHookVideo)
  {
  var foundNode = null;
  var videoList = document.getElementsByTagName("video");
  var videoLen = videoList.length;
  for (var i = 0; i < videoLen; ++i)
  {
  if(videoList[i].oriHookVideo === oriHookVideo) { return videoList[i]; }
  }
  
  ///< 再尝试所有的iFrame
  var frameList = document.getElementsByTagName("iframe");
  var frameLen = frameList.length;
  for (var j = 0; j < frameLen; ++j)
  {
  var frame = frameList[j];
  if (isFrameAccessible(frame))
  {
  videoList = frame.contentDocument.getElementsByTagName("video");
  videoLen = videoList.length;
  for (var i = 0; i < videoLen; ++i)
  {
  if(videoList[i].oriHookVideo === oriHookVideo) { return videoList[i]; }
  }
  }
  }
  
  return null;
  }
  
  function UC_VIDEO_getPosterUrlForVideoNode(vNode)
  {
  var posterUrl = vNode.getAttribute("poster");
  if(posterUrl && posterUrl.length > 0)
  {
  /*预防有些页面设置的是相对路径，.poster属性能返回完整路径*/
  posterUrl = vNode.poster;
  }
  else
  {
  var externalGetter = window["UCVPosterUrlGetter"];
  if(typeof externalGetter === "function")
  {
  if(vNode.parentNode === null)
  {//父节点为空，说明是被接管了的video节点，要找到替换它
  vNode = getExchangeNodeForVideoNode(vNode);
  }
  
  if(vNode) { posterUrl = externalGetter(vNode); }
  }
  }
  
  return posterUrl ? posterUrl : "";
  }
  
  function UC_VIDEO_Cancel_SystemPlayer(v)
  {
  var canTakeOver = false;
  if(v && typeof v.webkitExitFullScreen === "function")
  {
  v.pause();
  v.webkitExitFullScreen();
  canTakeOver = true;
  }
  
  return canTakeOver;
  }
  
  function UC_VIDEO_tryDelayPlayVideoNative(videoInfo)
  {
  if(s_timeoutPlay)
  {
  clearTimeout(s_timeoutPlay);
  }
  
  s_timeoutPlay = window.setTimeout(function(){
                                    s_timeoutPlay = null;
                                    UC_VIDEO_tryPlayWithInfo(videoInfo);
                                    }, 100);
  }
  
  
  function isExpectedEvent(evt)
  {
  return evt && (evt.type == "click" || evt.type == "tap" ||evt.type.indexOf("touch") == 0);// || evt.type == "load"
  }
  
  function getCurrentEvent()
  {
  if(window.event)
  return window.event;
  
  var frameList = document.getElementsByTagName("iframe");
  for (var i = 0; i < frameList.length; ++i)
  {
  var frameWindow = frameList[i].contentWindow;
  if(frameWindow && frameWindow.event)
  {
  return frameWindow.event;
  }
  }
  
  return null;
  }
  
  
  function playVideoWithUC()
  {
  var evt = getCurrentEvent();
  //  var p = this.parentNode;
  //  var pname = (p ? p.nodeName : "null");
  //  window.console.log("target:" + this + "(" + pname + "." + this.nodeName + ") event:" + (evt ? evt.type : "undefined") + ", mainframe video.play: " + this.src);
  if (isExpectedEvent(evt) || (typeof this.ucRetryLeft == "number" && this.ucRetryLeft > 0))
  {
  var curVideoUrl = UC_VIDEO_getSrcFromVideoNode(this);
  if(curVideoUrl)
  {
  var videoInfo = {"videoUrl":curVideoUrl, "posterUrl":UC_VIDEO_getPosterUrlForVideoNode(this)};
  UC_VIDEO_tryPlayWithInfo(videoInfo);
  this.ucRetryLeft = null;
  }
  else
  {
  /// 某些网站会延时设置src，延时尝试几次。
  this.ucRetryLeft = ((typeof this.ucRetryLeft == "number") ? (this.ucRetryLeft - 1) : 3);
  if(this.ucRetryLeft > 0) window.setTimeout(playVideoWithUC, 10);
  }
  }
  }
  
  function loadVideoWithUC() {
  if (isExpectedEvent(getCurrentEvent())) {
  UC_VIDEO_tryLoadAtURL(UC_VIDEO_getSrcFromVideoNode(this));
  }
  }
  
  function canCustomizeSystemPlay(vNode, videoURL) {
  //videoURL与window.location相同，说明是从地址栏输入了一个媒体播放地址
  return (vNode.ucVCusPlayDisabled !== 1) && (videoURL !== window.location.href);
  }
  
  function canObserveVideoNodeInserted()
  {
  return (window["videoInsertObserveEnabled"] === 1);
  }
  
  function dispatchVideoEvent(vNode, eventName, isDispatchToOriHook)
  {
  var dstNode = null;
  switch(s_videoEventReceiver)
  {
  case "live": //发给当前在DOM树中得节点
  dstNode = vNode;
  break;
  case "dis":
  dstNode = null;
  break;
  case "ori":
  default:
  dstNode = vNode.oriHookVideo ? vNode.oriHookVideo : vNode;
  break;
  }
  
  if(dstNode)
  {
  var videoEvent = document.createEvent("HTMLEvents");
  videoEvent.initEvent(eventName, false, true);
  videoEvent.isUCEvent = true;
  dstNode.dispatchEvent(videoEvent);
  }
  }
  
  function hookVideoFullScreen(vNode)
  {
  vNode.webkitEnterFullscreen = function(e){};
  vNode.webkitEnterFullScreen = function(e){};
  vNode.webkitRequestFullScreen = function(e){};
  vNode.webkitRequestFullscreen = function(e){};
  vNode.setAttribute("webkit-playsinline", "");
  vNode.controls = true;
  }
  
  function safeAddEventListener(node, evtName, cb, cap)
  {
  node.removeEventListener(evtName, cb, cap);
  node.addEventListener(evtName, cb, cap);
  }
  
  function addVideoEventListeners(vNode, needsHookPlay)
  {
  if(needsHookPlay === true)
  {
  if(!vNode.ucOriPlay) vNode.ucOriPlay = HTMLMediaElement.prototype.play;
  if(!vNode.ucOriLoad) vNode.ucOriLoad = HTMLMediaElement.prototype.load;
  }
  
  safeAddEventListener(vNode, "play", onSystemPlayed, false);
  safeAddEventListener(vNode, "ended", onSystemPlayEnded, false);
  safeAddEventListener(vNode, "error", onSystemPlayError, false);
  safeAddEventListener(vNode, "abort", onSystemPlayAbort, false);
  safeAddEventListener(vNode, "canplaythrough", onSystemCanPlayThrough, false);
  safeAddEventListener(vNode, "webkitendfullscreen", onSystemPlayExitFullScreen, false);
  }
  
  function clearVideoEventListeners(vNode)
  {
  vNode.removeEventListener("play", onSystemPlayed, false);
  vNode.removeEventListener("ended", onSystemPlayEnded, false);
  vNode.removeEventListener("error", onSystemPlayError, false);
  vNode.removeEventListener("abort", onSystemPlayAbort, false);
  vNode.removeEventListener("canplaythrough", onSystemCanPlayThrough, false);
  vNode.removeEventListener("webkitendfullscreen", onSystemPlayExitFullScreen, false);
  }
  
  function replaceVideoNode(vNode, videoURL)
  {
  var parentContainer = vNode.parentNode;
  if(!parentContainer)
  {
  return;
  }
  
  var rpNode = vNode.cloneNode(true);
  rpNode.ucVCusPlayDisabled = vNode.ucVCusPlayDisabled;
  if(videoURL && videoURL.length > 0)
  {
  rpNode.src = videoURL;
  }
  
  if(vNode.oriHookVideo)
  {
  clearVideoEventListeners(vNode);
  rpNode.oriHookVideo = vNode.oriHookVideo;
  vNode.oriHookVideo = null;
  }
  else
  {
  rpNode.oriHookVideo = vNode;
  vNode.ucLastCheckSrc = videoURL;
  vNode.ucSkipError = true;
  }
  
  rpNode.ucSkipError = true;
  rpNode.ucListened = true;
  //  rpNode.setAttribute("preload", "none");
  hookVideoFullScreen(rpNode);
  addVideoEventListeners(rpNode, true);
  rpNode.onclick = onVideoTagClicked;
  parentContainer.replaceChild(rpNode, vNode);
  
  return rpNode;
  }
  
  function onVideoTagClicked(evt)
  {
  if(evt && evt.target)
  {
  var vNode = evt.target;
  if(vNode.nodeName.toLowerCase() == "video")
  {
  vNode.play();
  }
  }
  }
  
  function notifySystemVideoEventToNative(videoUrl, evtData)
  {
  var params = [];
  switch(evtData.vEvent)
  {
  case "play":
  params.push(evtData.vEvent);
  params.push(evtData.isNextPlay);
  break;
  
  case "error":
  params.push(evtData.vEvent);
  break;
  
  case "succ":
  params.push(evtData.vEvent);
  params.push(evtData.loadTime);
  break;
  
  default:
  break;
  }
  
  if(params.length > 0)
  {
  var videoInfo = {"cmd":"ucvsys:"};
  videoInfo.params = params;
  videoInfo.videoUrl = videoUrl;//播放事件无需上报posterUrl
  UC_VIDEO_tryPlayWithInfo(videoInfo);
  }
  }
  
  function onSystemPlayed(evt)
  {
  if(evt.isUCEvent) return;
  
  var vNode = evt.target;
  var nodeName = vNode.nodeName.toLowerCase();
  if(nodeName == "video")
  {
  var videoInfo = {videoUrl:UC_VIDEO_getSrcFromVideoNode(vNode)};
  var canTakeOver = (canCustomizeSystemPlay(vNode, videoInfo.videoUrl) && UC_VIDEO_Cancel_SystemPlayer(vNode));
  if(canTakeOver) {
  videoInfo.posterUrl = UC_VIDEO_getPosterUrlForVideoNode(vNode);
  
  vNode.src = "";
  if(typeof vNode.ucOriLoad === "function")
  {
  vNode.ucOriLoad();
  }
  else
  {
  vNode.load();
  }
  replaceVideoNode(vNode, videoInfo.videoUrl);
  
  ///< 有些网站可能会强行requestFullscreen，所以还是延时发出通知，给系统时间去退出全屏
  UC_VIDEO_tryDelayPlayVideoNative(videoInfo);
  }
  else
  {
  //window.console.log("onSystemPlayed(" + vNode.ucIsPlaying + "," + vNode.ucIsPlayerShown + ") : " + window.event.type);
  if(vNode.ucIsPlaying !== true)
  {//开始新的播放才记录，忽略暂停后点继续
  var evtData = {vEvent:"play", isNextPlay:(vNode.ucIsPlayerShown === true)};
  notifySystemVideoEventToNative(videoInfo.videoUrl, evtData);
  vNode.ucIsPlaying = true;
  vNode.ucIsPlayerShown = true;
  
  var d = new Date();
  vNode.ucStartLoadTime = d.getTime();
  
  if(vNode.ucFirstLoaded === true)
  {///已经提前加载完了，可直接播放的
  evtData = {vEvent:"succ", loadTime:1};
  notifySystemVideoEventToNative(videoInfo.videoUrl, evtData);
  }
  }
  }
  }
  }
  
  function onSystemPlayError(evt)
  {
  if(evt.isUCEvent) return;
  
  var vNode = evt.target;
  vNode.ucIsPlaying = false;
  vNode.ucFirstLoaded = false;
  
  if(vNode.ucSkipError !== true)
  {
  var videoURL = UC_VIDEO_getSrcFromVideoNode(vNode);
  if(videoURL && videoURL.length > 0)
  {
  notifySystemVideoEventToNative(videoURL, {vEvent:"error"});
  }
  }
  
  vNode.ucFirstLoaded = false;
  vNode.ucStartLoadTime = 0;
  }
  
  function onSystemPlayEnded(evt)
  {
  if(evt.isUCEvent) return;
  
  var vNode = evt.target;
  vNode.ucIsPlaying = false;
  vNode.ucFirstLoaded = false;
  vNode.ucFirstLoaded = false;
  vNode.ucStartLoadTime = 0;
  }
  
  function onSystemPlayAbort(evt)
  {
  if(evt.isUCEvent) return;
  
  var vNode = evt.target;
  vNode.ucIsPlaying = false;
  vNode.ucFirstLoaded = false;
  vNode.ucFirstLoaded = false;
  vNode.ucStartLoadTime = 0;
  }
  
  function onSystemCanPlayThrough(evt)
  {
  if(evt.isUCEvent) return;
  
  var vNode = evt.target;
  if(vNode.ucFirstLoaded !== true)
  {
  vNode.ucFirstLoaded = true;
  if(vNode.ucStartLoadTime > 0)
  {
  var d = new Date();
  var evtData = {vEvent:"succ", loadTime:d.getTime() - vNode.ucStartLoadTime};
  notifySystemVideoEventToNative(UC_VIDEO_getSrcFromVideoNode(vNode), evtData)
  }
  }
  }
  
  function onSystemPlayExitFullScreen(evt)
  {
  if(evt.isUCEvent) return;
  
  var vNode = evt.target;
  vNode.ucIsPlaying = false;
  vNode.ucIsPlayerShown = false;
  }
  
  function UC_VIDEO_Hook_Play(v, frame)
  {
  // hook video play/load method
  // origin play/load method is define in HTMLMediaElement.prototype
  var curWindow = frame ? frame.contentWindow : window;
  var vProtoType = curWindow.HTMLVideoElement.prototype;
  if(vProtoType.play !== playVideoWithUC)
  {
  vProtoType.play = playVideoWithUC;
  vProtoType.load = loadVideoWithUC;
  }
  
  if(v.ucListened !== true)
  {
  v.ucListened = true;
  v.removeAttribute("autoplay");
  v.ucVCusPlayDisabled = frame ? frame.ucVCusPlayDisabled : window["videoCustomPlayDisabled"];
  
  var videoURL = UC_VIDEO_getSrcFromVideoNode(v);
  addVideoEventListeners(v, (videoURL !== window.location.href));
  if(canCustomizeSystemPlay(v, videoURL))
  {
  v.pause();
  hookVideoFullScreen(v);
  /// 替换掉原有videoTag,避免接管系统播放器时设置 v.src="" 发出error事件
  replaceVideoNode(v, videoURL);
  //        v.src = "";
  //        v.ucOriLoad();
  }
  
  var reqReceiver = window["UCVVideoEvtReceiver"];
  s_videoEventReceiver = (reqReceiver && reqReceiver.length > 0) ? reqReceiver : "ori";
  }
  
  if(v.oriHookVideo)
  {
  var oriUrl = UC_VIDEO_getSrcFromVideoNode(v.oriHookVideo);
  if(oriUrl && oriUrl.length > 0 && oriUrl != v.oriHookVideo.ucLastCheckSrc)
  {
  v.src = oriUrl;
  v.oriHookVideo.ucLastCheckSrc = oriUrl;
  v.oriHookVideo.ucSrcReset = true;
  }
  else
  {
  var curSrc = UC_VIDEO_getSrcFromVideoNode(v);
  if(curSrc && curSrc.length > 0 && curSrc != v.oriHookVideo.ucLastCheckSrc)
  {
  v.oriHookVideo.ucLastCheckSrc = curSrc;
  v.oriHookVideo.ucSrcReset = true;
  v.oriHookVideo.src = curSrc;
  }
  }
  }
  }
  
/* 跨域视频检测相关 */
  function UC_VIDEO_onReceiveCrossdomainMsg(e)
  {
  var evt = e || window.event;
  var data = evt.data;
  if(!data)
  {
  return;
  }
  
  var msgName = data["UC_MSG_NAME"];
  if(!msgName || msgName.length == 0)
  {
  return
  }
  
  var videoUrl = data["UC_VIDEO_URL"];
  var videoInfo = {"videoUrl":videoUrl?videoUrl:"", "posterUrl":(data.extInfo && data.extInfo.posterUrl) ? data.extInfo.posterUrl : ""};
  switch(msgName) {
  case "UCVideoURLGot":
  onRecvVideoUrlFromFrameWindow(videoInfo, evt.source, data.extInfo);
  break;
  
  case "UCPlayVideo":
  UC_VIDEO_tryPlayWithInfo(videoInfo);
  break;
  
  case "UCLoadVideo":
  UC_VIDEO_tryLoadAtURL(videoInfo.videoUrl);
  break;
  
  case "UCCusSysPlayVideo":
  UC_VIDEO_tryDelayPlayVideoNative(videoInfo);
  break;
  
  case "UCOnSysVEvent":
  notifySystemVideoEventToNative(videoInfo.videoUrl, data.extInfo);
  break;
  
  case "UCSeekVideo":
  notifySeekTimeToNative(data.extInfo.seekTime, videoInfo);
  break;
  default:
  break;
  }
  }
  
  function isFrameLocationMacthed(matchList, frame)
  {
  if((matchList && matchList.length > 0) && (frame.src && frame.src.length > 0))
  {
  try
  {
  var trimedSrc = trimQueryStringForUrl(frame.src);
  for(var i = 0; i < matchList.length; ++i)
  {
  var curHost = matchList[i];
  if(trimedSrc.indexOf(curHost) > -1)
  {
  return true;
  }
  }
  }
  catch(e)
  {
  }
  }
  return false;
  }
  
  function UC_VIDEO_postMessageToCrossdomainIFrame(msg, extInfo)
  {
  var frameList = document.getElementsByTagName("iframe");
  for (var i = 0; i < frameList.length; ++i)
  {
  var frame = frameList[i];
  if (!isFrameAccessible(frame))
  {
  postMessageToIFrame(msg, frame, extInfo);
  }
  }
  }
  
  function postMessageToIFrame(msg, frame, extInfo)
  {
  if(frame.ucNotRecvMsg == true)
  {
  return;
  }
  
  if(frame.ucVideoFlag != true)
  {
  if(frame.ucPostRetryCount === undefined)
  {
  frame.ucPostRetryCount = 0;
  }
  else if(frame.ucPostRetryCount >= 30)
  {
  return;
  }
  
  frame.ucPostRetryCount += 1;
  }
  
  var data = {UC_MSG_NAME:msg};
  if(extInfo)
  {
  data["extInfo"] = extInfo;
  }
  if(frame.ucVCusPlayDisabled === 1)
  {
  data["disCusSysPlay"] = 1;
  }
  
  if(frame.ucVInsertObserveEnabled === 1)
  {
  data["obsVIn"] = 1;
  }
  
  var frameWindow = frame.contentWindow;
  frameWindow.postMessage(data, '*');
  }
  
  function onRecvVideoUrlFromFrameWindow(videoInfo, srcWindow, extInfo)
  {
  var foundFrame = 0;
  var frameList = document.getElementsByTagName("iframe");
  for (var i = 0; i < frameList.length; ++i)
  {
  var frame = frameList[i];
  if(frame.contentWindow == srcWindow)
  {
  frame.ucVideoFlag = true;
  foundFrame = frame;
  break;
  }
  }
  
  if(foundFrame)
  {
  s_crossVideoFrame = foundFrame;
  if (extInfo && extInfo.errorcode)
  {
  s_lastDetectedVideoInfo.errorCode = extInfo.errorcode;
  }
  
  if(videoInfo.posterUrl.length > 0)
  {
  s_lastDetectedVideoInfo.posterUrl = videoInfo.posterUrl;
  }
  
  UC_VIDEO_notifyToNative(videoInfo.videoUrl, videoInfo.posterUrl, (extInfo && extInfo.changed === true));
  }
  }
  
  function UC_VIDEO_notifyToNative(videoUrl, posterUrl, forceNotify) {
  if (forceNotify || videoUrl != s_lastDetectedVideoInfo.videoUrl || s_videoNotifyCounter < 1)
  {
  s_lastDetectedVideoInfo.videoUrl = videoUrl;
  s_lastDetectedVideoInfo.posterUrl = posterUrl;
  
  if(s_isVideoTagNotifyEnanbled !== true) {
  return;
  }
  
  if (videoUrl.length) {
  ++s_videoNotifyCounter;
  }
  if (window.bridge) {
  window.bridge.post('video.detected', s_lastDetectedVideoInfo)
  }
  }
  }
  
  function UC_VIDEO_onDOMNodeInserted(evt)
  {
  var comingNode = evt.target;
  var vNode;
  if(comingNode.nodeName.toLowerCase() == "video")
  {
  vNode = comingNode;
  }
  else if(comingNode.childElementCount > 0)
  {
  var vList = comingNode.getElementsByTagName("video");
  if(vList.length > 0) vNode = vList[0];
  }
  
  if(vNode)
  {
  UC_VIDEO_Hook_Play(vNode, 0);
  }
  }
  
  function extractFrameControls(frameList)
  {
  if(window.disCusSysPlaySites)
  {
  s_customSysPlayDisableList = window.disCusSysPlaySites.split(",");
  window.disCusSysPlaySites = undefined;
  }
  
  if(window.vInsertObserveSites)
  {
  s_videoInsertObserveList = window.vInsertObserveSites.split(",");
  window.vInsertObserveSites = undefined;
  }
  
  for (var i = 0; i < frameList.length; ++i)
  {
  var frame = frameList[i];
  if(frame.ucLastCheckSrc != frame.src)
  {
  /// 设置是否屏蔽接管系统播放器
  frame.ucVCusPlayDisabled = isFrameLocationMacthed(s_customSysPlayDisableList, frame) ? 1 : 0;
  /// 设置是否监测video节点插入
  frame.ucVInsertObserveEnabled = isFrameLocationMacthed(s_videoInsertObserveList, frame) ? 1 : 0;
  ///检测frame DOM的可访问性
  frame.stAccessible = ((!frame.contentWindow || !frame.contentDocument) ? false : true);
  /// 设置是否禁止向frame发消息
  frame.ucNotRecvMsg = (window.ucNotRecvMsgFrames) ? isFrameLocationMacthed(window.ucNotRecvMsgFrames, frame) : false;
  frame.ucPostRetryCount = 0;
  frame.ucLastCheckSrc = frame.src;
  }
  }
  }
  
  function reHookVideos()
  {
  var toReplaces = new Array();
  
  if(window["videoCustomPlayDisabled"] !== 1)
  {
  var videoList = document.getElementsByTagName("video");
  for(var i = 0; i < videoList.length; ++i)
  {
  var vNode = videoList[i];
  if(vNode.ucListened === true) toReplaces.push(vNode);
  }
  }
  
  ///< 再尝试所有的iFrame
  var frameList = document.getElementsByTagName("iframe");
  for (var fi = 0; fi < frameList.length; ++fi)
  {
  var frame = frameList[fi];
  if (isFrameAccessible(frame) && frame.ucVCusPlayDisabled !== 1)
  {
  var videoList = frame.contentDocument.getElementsByTagName("video");
  for (var vi = 0; vi < videoList.length; ++vi)
  {
  var vNode = videoList[vi];
  if(vNode.ucListened === true) toReplaces.push(vNode);
  }
  }
  }
  
  for(var i = 0; i < toReplaces.length; ++i)
  {
  var vNode = toReplaces[i];
  replaceVideoNode(vNode, UC_VIDEO_getSrcFromVideoNode(vNode));
  }
  
  UC_VIDEO_postMessageToCrossdomainIFrame("UCBROWSER_VIDEO_BACKFORWARD", null);
  }
  
  function UC_VIDEO_checkVideo()
  {
  var frameList = document.getElementsByTagName("iframe");
  extractFrameControls(frameList);
  
  if(s_needsResetWhenUpdate == 1)
  {
  s_needsResetWhenUpdate = 0;
  s_lastDetectedVideoInfo.videoUrl = "";
  s_lastDetectedVideoInfo.posterUrl = "";
  s_lastDetectedVideoInfo.errorcode = 0;
  
  reHookVideos();
  }
  
  var forceNotify = false;
  //  // 检查Hash变化、检查State变化
  if(window.location.href !== s_lastLocationHref)
  {
  s_lastLocationHref = window.location.href;
  s_videoNotifyCounter = 0;
  }
  
  var videoInfo = UC_VIDEO_findNewVideoInfo();
  if(videoInfo.videoUrl.length > 0)
  {
  s_crossVideoFrame = 0;
  s_lastDetectedVideoInfo.posterUrl = videoInfo.posterUrl;
  UC_VIDEO_notifyToNative(videoInfo.videoUrl, videoInfo.posterUrl, forceNotify);
  }
  else
  {
  if(s_lastDetectedVideoInfo.videoUrl && s_lastDetectedVideoInfo.videoUrl.length > 0)
  {
  if(s_crossVideoFrame)
  {//之前是从iframe取到的url
  var isCrossFrameExist = false;
  for(var i = 0; i < frameList.length; ++i)
  {
  if(frameList[i] === s_crossVideoFrame)
  {
  isCrossFrameExist = true;
  break;
  }
  }
  
  if(isCrossFrameExist !== true)
  {//原来找到视频url的frame不存在了，需要重置下
  s_crossVideoFrame = 0;
  UC_VIDEO_notifyToNative("", "", true);
  }
  else
  {//原来找到视频url，但可能还没成功通知到native，需再次尝试
  UC_VIDEO_notifyToNative(s_lastDetectedVideoInfo.videoUrl, s_lastDetectedVideoInfo.posterUrl, false);
  }
  }
  else
  {//之前是从mainframe直接取到的url，说明video没有了或地址变空了
  UC_VIDEO_notifyToNative("", "", true);
  }
  }
  
  //询问各跨域iframe
  UC_VIDEO_postMessageToCrossdomainIFrame("UCBROWSER_GET_VIDEO_URL", null);
  }
  }
  
  function UC_VIDEO_checkVideo_repeatly()
  {
  UC_VIDEO_checkVideo();
  
  var delay = 1000;
  if (s_initCheckCounter++ < 5) // 前5次时间短一点，较快时间找到video
  {
  if(s_lastDetectedVideoInfo.videoUrl.length > 0)
  {
  s_initCheckCounter = 5; // 如果已经找到了，更改次数为5，避免后续再进入这个逻辑
  }
  else
  {
  delay = 200;
  }
  }
  
  window.setTimeout(UC_VIDEO_checkVideo_repeatly, delay);
  }
  
  function UC_VIDEO_Monitor_Video()
  {
  // hook video play/load method
  // origin play/load method is define in HTMLMediaElement.prototype
  HTMLVideoElement.prototype.play = playVideoWithUC;
  HTMLVideoElement.prototype.load = loadVideoWithUC;
  
  window.setTimeout(UC_VIDEO_Start_Monitor_Video, 1);
  }
  
  function UC_VIDEO_Start_Monitor_Video()
  {
  window.setTimeout(UC_VIDEO_checkVideo, 100);
  
  if (s_videoCheckerStarted === 0)
  {
  s_videoCheckerStarted = 1;
  window.setTimeout(UC_VIDEO_checkVideo_repeatly, 0);
  window.addEventListener("message", UC_VIDEO_onReceiveCrossdomainMsg, false);
  
  if(canObserveVideoNodeInserted())
  {
  document.addEventListener("DOMNodeInserted", UC_VIDEO_onDOMNodeInserted, false);
  }
  }
  }
  
  function notifySeekTimeToNative(t, oriInfo)
  {
  var videoInfo = JSON.parse(JSON.stringify(oriInfo));
  videoInfo.cmd = "webseek:";
  var params = [t];
  videoInfo.params = params;
  UC_VIDEO_tryPlayWithInfo(videoInfo);
  }
  
  function internalGetDuration()
  {
  return this.ucDuration ? this.ucDuration : 0;
  }
  
  function internalGetCurrentTime()
  {
  return this.ucCurTime ? this.ucCurTime : 0;
  }
  
  function internalSetCurrentTime(t)
  {
  if(s_skipWebVideoSeek === true)
  {
  return;
  }
  
  this.ucCurTime = t;
  notifySeekTimeToNative(t, s_lastDetectedVideoInfo);
  }
  
  var TimeRanges = {
  length:1,
  duration:0,
  start:function(idx) { return 0; },
  end:function(idx) { return this.duration; },
  };
  
  function checkReplaceVideoProperties(videoNode)
  {
  if(s_skipDefineCustomVideoProp === true)
  {///保护开关，当发现某个站点接管有问题时，可以下发这个开关屏蔽接管这些property
  return;
  }
  
  if(videoNode.ucVideoPropertiesHooked !== true)
  {
  try{
  videoNode.ucVideoPropertiesHooked = true;
  Object.defineProperty(videoNode, "duration", {get:internalGetDuration});
  Object.defineProperty(videoNode, "currentTime", {
                        get:internalGetCurrentTime,
                        set:internalSetCurrentTime
                        });
  Object.defineProperty(videoNode, "seekable", {
                        get:function() { return TimeRanges; }
                        });
  }
  catch(e){
  }
  }
  
  if(videoNode.oriHookVideo)
  {
  checkReplaceVideoProperties(videoNode.oriHookVideo);
  }
  }
  
  
  function fillVideoDuration(videoNode, duration)
  {
  TimeRanges.duration = duration;
  videoNode.ucDuration = parseInt(duration, 10);
  var oriVideo = videoNode.oriHookVideo;
  if(oriVideo)
  {
  oriVideo.ucDuration = videoNode.ucDuration;
  }
  
  checkReplaceVideoProperties(videoNode);
  }
  
  function fillVideoCurrentTime(videoNode, currentTime)
  {
  videoNode.ucCurTime = parseFloat(currentTime);
  
  var oriVideo = videoNode.oriHookVideo;
  if(oriVideo)
  {
  oriVideo.ucCurTime = videoNode.ucCurTime;
  }
  
  checkReplaceVideoProperties(videoNode);
  }
  
  function UC_VIDEO_Playback_Event(evt)
  {
  function setVideoIsWaitPlaying(videoNode, isWaiting)
  {
  videoNode.ucWaitPlaying = isWaiting;
  if(videoNode.oriHookVideo) videoNode.oriHookVideo.ucWaitPlaying = isWaiting;
  }
  
  window.setTimeout(function(){
                    var videoNode = UC_VIDEO_findVideoNode();
                    if (videoNode)
                    {
                    //window.console.log("mainframe recv natvie playback event: " + evt.vEvent)
                    switch(evt.vEvent) {
                    case "durationchange":
                    fillVideoDuration(videoNode, evt.duration);
                    break;
                    case "timeupdate":
                    fillVideoCurrentTime(videoNode, evt.currentTime);
                    break;
                    case "play":
                    setVideoIsWaitPlaying(videoNode, true);
                    break;
                    case "playing":
                    setVideoIsWaitPlaying(videoNode, false);
                    break;
                    case "pause":
                    if(videoNode.ucWaitPlaying === true && evt.stop == "1")
                    {/// 补发一个开始播放事件，避免某些页面在收到"play"后会等待“playing”来结束转圈等ui。
                    dispatchVideoEvent(videoNode, "playing", true);
                    }
                    break;
                    default:
                    break;
                    }
                    
                    dispatchVideoEvent(videoNode, evt.vEvent, true);
                    }
                    else
                    {
                    //window.console.log("try in iframe " + evt.vEvent)
                    UC_VIDEO_postMessageToCrossdomainIFrame("UCBROWSER_VIDEO_PLAYBACK_EVENTS", evt);
                    }
                    }, 1);
  }
  
  function UC_VIDEO_onBackForward()
  {
  s_needsResetWhenUpdate = 1;
  s_initCheckCounter = 0;
  }
  
  function UC_VIDEO_isFirstDetected()
  {
  return (s_videoNotifyCounter <= 1);
  }
  
  function UC_VIDEO_sniffVideoUrl(forceEnabled)
  {
  var videoInfo = UC_VIDEO_findVideoInfoObject();
  var videoURL = videoInfo.videoUrl;
  if(videoURL.length > 0 || forceEnabled)
  {///及时找到视频了，或者上层允许直接接管了
  s_isVideoTagNotifyEnanbled = true;
  }
  
  return videoURL;
  }
  
  var s_videoCheckerStarted = 0;
  var s_lastLocationHref = "";
  
  var s_crossVideoFrame = 0;
  
  var s_timeoutPlay;
  var s_needsResetWhenUpdate = 0;
  
  var s_customSysPlayDisableList;
  var s_videoInsertObserveList;
  
  var s_lastDetectedVideoInfo = {videoUrl:"", posterUrl:"", errorCode:0};
  
  var s_initCheckCounter = 0;
  var s_videoNotifyCounter = 0;
  var s_isVideoTagNotifyEnanbled = true;
  
  var s_videoEventReceiver = "ori"; ///default to origin video node
  var s_skipWebVideoSeek = window["ucvSkipVideoPropDefine"];
  var s_skipDefineCustomVideoProp = window["ucvSkipVideoCustomProp"];
  
  
  window["UC_VIDEO_isFirstDetected"]=UC_VIDEO_isFirstDetected;
  window["UC_VIDEO_checkVideoSupported"]=UC_VIDEO_checkVideoSupported;
  window["UC_VIDEO_findVideoURL"]=UC_VIDEO_findVideoURL;
  window["UC_VIDEO_sniffVideoUrl"]=UC_VIDEO_sniffVideoUrl;
  window["UC_VIDEO_Playback_Event"]=UC_VIDEO_Playback_Event;
  window["UC_VIDEO_onBackForward"]=UC_VIDEO_onBackForward;
  window["K_UCKUIWebViewVideoFinder"]=0;
  window.videoInsertObserveEnabled = 1;
  
  ///< 对于需要监听video标签插入的站点，通过额外下发JS设置 window.videoInsertObserveEnabled=1。
  ///< 通过下发JS设置 window.videoCustomPlayDisabled=1来屏蔽接管系统播放器，不下发表示不屏蔽。
  
  
  UC_VIDEO_Monitor_Video();
  
  })();
