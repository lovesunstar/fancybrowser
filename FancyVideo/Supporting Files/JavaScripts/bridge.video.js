(function() {
 if (!window.bridge) {
    return;
 }
 var videoBridge = {
    play: function(videoInfo) {
        window.bridge.post('video.play', {videoInfo: videoInfo})
    },
    load: function(videoInfo) {
        window.bridge.post('video.load', {videoInfo: videoInfo})
    },
    detected: function(videoInfo) {
        window.bridge.post('video.detected', {videoInfo: videoInfo})
    }
 };
 
 window.bridge.video = videoBridge
}());
