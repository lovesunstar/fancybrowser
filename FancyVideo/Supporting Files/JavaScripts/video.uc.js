
(function () {
 function t(a) {
 !0 !== a.ucAccessible || a.contentWindow && a.contentDocument || (a.ucAccessible = !1);
 return !0 === a.ucAccessible
 }
 function G(a, c) {
 if (void 0 == a) return 0;
 for (var b = a.getElementsByTagName("video"), d = b.length, e = 0; e < d; ++e) x(b[e], c);
 for (var l = e = 0; l < d; ++l) {
 var f = b[l],
 g = h(f);
 if (g && 0 < g.length) if ("none" == a.defaultView.getComputedStyle(f).display) e || (e = f);
 else return f
 }
 return e
 }
 function I() {
 var a = {};
 return a = void 0 != f.videoUrl && 0 < f.videoUrl.length ? f : J()
 }
 function la() {
 return I()
 }
 
 function K() {
 return 4 != f.errorCode
 }
 function J() {
 var a = {
 videoUrl: "",
 posterUrl: ""
 },
 c = L();
 if (c) {
 var b = c.oriHookVideo ? c.oriHookVideo : c;
 f.errorCode = b.error && b.error.code ? !0 === b.ucSrcReset ? 0 : b.error.code : 0;
 K() ? (a.videoUrl = h(c), a.posterUrl = y(c)) : c.src = ""
 }
 return a
 }
 function L() {
 var a = G(window.document, 0);
 if (a) return a;
 for (var c = document.getElementsByTagName("iframe"), b = 0; b < c.length; ++b) {
 var d = c[b];
 if (t(d)) {
 var e = d.contentWindow;
 if (a) {
 if (e = e.document, void 0 != e) for (var e = e.getElementsByTagName("video"), l = e.length, f = 0; f < l; ++f) x(e[f], d)
 } else a = G(e.document, d)
 }
 }
 return a
 }
 function n(a) {
 window.bridge.video.play(a)
 }
 function M(a) {
 window.bridge.video.load(a)
 }
 
 function h(a) {
 var c = a.getAttribute("src");
 if (c && 0 < c.length) c = a.src;
 else {
 a = a.getElementsByTagName("source");
 for (var b = 0; b < a.length; ++b) {
 var d = a[b],
 e = d.getAttribute("src");
 if (e && 0 < e.length) {
 c = d.src;
 break
 }
 }
 }
 return c ? c : ""
 }
 function y(a) {
 var c = a.getAttribute("poster");
 if (c && 0 < c.length) c = a.poster;
 else {
 var b = window.UCVPosterUrlGetter;
 if ("function" === typeof b) {
 if (null === a.parentNode) a: {
 for (var d = document.getElementsByTagName("video"), e = d.length, f = 0; f < e; ++f) if (d[f].oriHookVideo === a) {
 a = d[f];
 break a
 }
 for (var H =
      document.getElementsByTagName("iframe"), h = H.length, g = 0; g < h; ++g) if (d = H[g], t(d)) for (d = d.contentDocument.getElementsByTagName("video"), e = d.length, f = 0; f < e; ++f) if (d[f].oriHookVideo === a) {
 a = d[f];
 break a
 }
 a = null
 }
 a && (c = b(a))
 }
 }
 return c ? c : ""
 }
 function N(a) {
 u && clearTimeout(u);
 u = g(function () {
       u = null;
       n(a)
       }, 100)
 }
 function O(a) {
 return a && ("click" == a.type || "tap" == a.type || 0 == a.type.indexOf("touch"))
 }
 function P() {
 if (window.event) return window.event;
 for (var a = document.getElementsByTagName("iframe"), c = 0; c < a.length; ++c) {
 var b =
 a[c].contentWindow;
 if (b && b.event) return b.event
 }
 return null
 }
 function v() {
 var a = P();
 if (void 0 != window.UC_VIDEO_SHOULD_AUTO_PLAY || O(a) || "number" == typeof this.ucRetryLeft && 0 < this.ucRetryLeft)(a = h(this)) ? (a = {
                                                                                                                                        videoUrl: a,
                                                                                                                                        posterUrl: y(this)
                                                                                                                                        }, n(a), this.ucRetryLeft = null) : (this.ucRetryLeft = "number" == typeof this.ucRetryLeft ? this.ucRetryLeft - 1 : 3, 0 < this.ucRetryLeft && g(v, 10))
 }
 function Q() {
 O(P()) && M(h(this))
 }
 function R(a, c, b) {
 b = null;
 switch (ma) {
 case "live":
 b = a;
 break;
 case "dis":
 b = null;
 break;
 default:
 b = a.oriHookVideo ? a.oriHookVideo : a
 }
 b && (a = document.createEvent("HTMLEvents"), a.initEvent(c, !1, !0), a.isUCEvent = !0, b.dispatchEvent(a))
 }
 function S(a) {
 a.webkitEnterFullscreen = function (a) {};
 a.webkitEnterFullScreen = function (a) {};
 a.webkitRequestFullScreen = function (a) {};
 a.webkitRequestFullscreen = function (a) {};
 a.setAttribute("webkit-playsinline", "");
 a.setAttribute("playsinline", "");
 a.controls = !0
 }
 function k(a, c, b, d) {
 a.removeEventListener(c, b, d);
 a.addEventListener(c, b, d)
 }
 function T(a, c) {
 k(a, "play", U, !1);
 k(a, "ended", V, !1);
 k(a, "error", W, !1);
 k(a, "abort", X, !1);
 k(a, "canplaythrough", Y, !1);
 k(a, "webkitendfullscreen", Z, !1)
 }
 function z(a, c) {
 var b = a.parentNode;
 if (b) {
 var d = a.cloneNode(!0);
 d.ucVCusPlayDisabled = a.ucVCusPlayDisabled;
 c && 0 < c.length && (d.src = c);
 a.oriHookVideo ? (a.removeEventListener("play", U, !1), a.removeEventListener("ended", V, !1), a.removeEventListener("error", W, !1), a.removeEventListener("abort", X, !1), a.removeEventListener("canplaythrough", Y, !1), a.removeEventListener("webkitendfullscreen", Z, !1), d.oriHookVideo = a.oriHookVideo, a.oriHookVideo = null) : (d.oriHookVideo = a, a.ucLastCheckSrc = c, a.ucSkipError = !0);
 d.ucSkipError = !0;
 d.ucListened = !0;
 S(d);
 T(d, !0);
 d.onclick = na;
 b.replaceChild(d, a);
 return d
 }
 }
 function na(a) {
 a && a.target && (a = a.target, "video" == a.nodeName.toLowerCase() && a.play())
 }
 function p(a, c) {
 var b = [];
 switch (c.vEvent) {
 case "play":
 b.push(c.vEvent);
 b.push(c.isNextPlay);
 break;
 case "error":
 b.push(c.vEvent);
 break;
 case "succ":
 b.push(c.vEvent),
 b.push(c.loadTime)
 }
 if (0 < b.length) {
 var d = {
 cmd: "ucvsys:"
 };
 d.params = b;
 d.videoUrl = a;
 n(d)
 }
 }
 function U(a) {
 if (!a.isUCEvent && (a = a.target, "video" == a.nodeName.toLowerCase())) {
 var c = {
 videoUrl: h(a)
 },
 b;
 if (b = !m && 1 !== a.ucVCusPlayDisabled && c.videoUrl !== window.location.href) b = !1,
 a && "function" === typeof a.webkitExitFullScreen && (a.pause(), a.webkitExitFullScreen(), b = !0);
 b ? (c.posterUrl = y(a), a.src = "", "function" === typeof a.ucOriLoad ? a.ucOriLoad() : a.load(), z(a, c.videoUrl), N(c)) : !0 !== a.ucIsPlaying && (b = {
                                                                                                                                                       vEvent: "play",
                                                                                                                                                       isNextPlay: !0 === a.ucIsPlayerShown
                                                                                                                                                       }, p(c.videoUrl, b), a.ucIsPlaying = !0, a.ucIsPlayerShown = !0, a.ucStartLoadTime = (new Date).getTime(), !0 === a.ucFirstLoaded && (b = {
                                                                                                                                                                                                                                                                                             vEvent: "succ",
                                                                                                                                                                                                                                                                                             loadTime: 1
                                                                                                                                                                                                                                                                                             }, p(c.videoUrl, b)))
 }
 }
 function W(a) {
 if (!a.isUCEvent) {
 a = a.target;
 a.ucIsPlaying = !1;
 if (!0 !== a.ucSkipError) {
 var c = h(a);
 c && 0 < c.length && p(c, {
                        vEvent: "error"
                        })
 }
 a.ucFirstLoaded = !1;
 a.ucStartLoadTime = 0
 }
 }
 function V(a) {
 a.isUCEvent || (a = a.target, a.ucIsPlaying = !1, a.ucFirstLoaded = !1, a.ucStartLoadTime = 0)
 }
 function X(a) {
 a.isUCEvent || (a = a.target, a.ucIsPlaying = !1, a.ucFirstLoaded = !1, a.ucStartLoadTime = 0)
 }
 function Y(a) {
 if (!a.isUCEvent && (a = a.target, !0 !== a.ucFirstLoaded && (a.ucFirstLoaded = !0, 0 < a.ucStartLoadTime))) {
 var c = {
 vEvent: "succ",
 loadTime: (new Date).getTime() - a.ucStartLoadTime
 };
 p(h(a), c)
 }
 }
 function Z(a) {
 a.isUCEvent || (a = a.target, a.ucIsPlaying = !1, a.ucIsPlayerShown = !1)
 }
 function x(a, c) {
 if (!m) {
 var b = (c ? c.contentWindow : window).HTMLVideoElement.prototype;
 b.play !== v && (b.play = v, b.load = Q)
 }!0 !== a.ucListened && (a.ucListened = !0, b = h(a), T(a, b !== window.location.href), m || (a.removeAttribute("autoplay"), a.ucVCusPlayDisabled = c ? c.ucVCusPlayDisabled : aa, 1 !== a.ucVCusPlayDisabled && b !== window.location.href && (a.pause(), S(a), z(a, b))));
 a.oriHookVideo && ((b = h(a.oriHookVideo)) && 0 < b.length && b != a.oriHookVideo.ucLastCheckSrc ? (a.src = b, a.oriHookVideo.ucLastCheckSrc = b, a.oriHookVideo.ucSrcReset = !0) : (b = h(a)) && 0 < b.length && b != a.oriHookVideo.ucLastCheckSrc && (a.oriHookVideo.ucLastCheckSrc = b, a.oriHookVideo.ucSrcReset = !0, a.oriHookVideo.src = b))
 }
 function oa(a) {
 var c = a || window.event,
 b = c.data;
 if (b) {
 var d = b.UC_MSG_NAME;
 if (d && 0 != d.length) switch (a = b.UC_VIDEO_URL, a = {
                                 videoUrl: a ? a : "",
                                 posterUrl: b.extInfo && b.extInfo.posterUrl ? b.extInfo.posterUrl : ""
                                 }, d) {
 case "UCVideoURLGot":
 for (var c = c.source, b = b.extInfo, d = 0, e = document.getElementsByTagName("iframe"), l = 0; l < e.length; ++l) {
 var g = e[l];
 if (g.contentWindow == c) {
 g.ucVideoFlag = !0;
 d = g;
 break
 }
 }
 d && (q = d, b && b.errorcode && (f.errorCode = b.errorcode), 0 < a.posterUrl.length && (f.posterUrl = a.posterUrl), r(a.videoUrl, a.posterUrl, b && !0 === b.changed));
 break;
 case "UCPlayVideo":
 n(a);
 break;
 case "UCLoadVideo":
 M(a.videoUrl);
 break;
 case "UCCusSysPlayVideo":
 N(a);
 break;
 case "UCOnSysVEvent":
 p(a.videoUrl, b.extInfo);
 break;
 case "UCSeekVideo":
 ba(b.extInfo.seekTime, a)
 }
 }
 }
 function A(a, c) {
 if (a && 0 < a.length && c.src && 0 < c.src.length) try {
 var b, d = c.src;
 if (d && 0 < d.length) {
 var e = d.indexOf("?"); - 1 < e && (d = d.substring(0, e))
 }
 b = d;
 for (d = 0; d < a.length; ++d) if (-1 < b.indexOf(a[d])) return !0
 } catch (f) {}
 return !1
 }
 function B(a, c) {
 for (var b = document.getElementsByTagName("iframe"), d = 0; d < b.length; ++d) {
 var e = b[d];
 if (!t(e)) a: {
 var f = a,
 g = c;
 if (1 != e.ucNotRecvMsg) {
 if (1 != e.ucVideoFlag) {
 if (void 0 === e.ucPostRetryCount) e.ucPostRetryCount = 0;
 else if (30 <= e.ucPostRetryCount) break a;
 e.ucPostRetryCount += 1
 }
 f = {
 UC_MSG_NAME: f
 };
 g && (f.extInfo = g);
 1 === e.ucVCusPlayDisabled && (f.disCusSysPlay = 1);
 1 === e.ucVInsertObserveEnabled && (f.obsVIn = 1);
 (e = e.contentWindow) && e.postMessage(f, "*")
 }
 }
 }
 }
 function r(a, c, b) {
 !m && (b || a != f.videoUrl || 1 > w) && (f.videoUrl = a, f.posterUrl = c, !0 === ca && (a.length && ++w, window.bridge.video.detected(f)))
 }
 function pa(a) {
 a = a.target;
 var c;
 "video" == a.nodeName.toLowerCase() ? c = a : 0 < a.childElementCount && (a = a.getElementsByTagName("video"), 0 < a.length && (c = a[0]));
 c && x(c, 0)
 }
 function da() {
 var a = document.getElementsByTagName("iframe");
 window.disCusSysPlaySites && (ea = window.disCusSysPlaySites.split(","), window.disCusSysPlaySites = void 0);
 window.vInsertObserveSites && (fa = window.vInsertObserveSites.split(","), window.vInsertObserveSites = void 0);
 for (var c = 0; c < a.length; ++c) {
 var b = a[c];
 b.ucLastCheckSrc != b.src && (b.ucVCusPlayDisabled = A(ea, b) ? 1 : 0, b.ucVInsertObserveEnabled = A(fa, b) ? 1 : 0, b.ucAccessible = b.contentWindow && b.contentDocument ? !0 : !1, b.ucNotRecvMsg = window.ucNotRecvMsgFrames ? A(window.ucNotRecvMsgFrames, b) : !1, b.ucPostRetryCount = 0, b.ucLastCheckSrc =
                               b.src)
 }
 if (1 == C && (C = 0, f.videoUrl = "", f.posterUrl = "", f.errorcode = 0, !m)) {
 c = [];
 if (1 !== aa) for (var b = document.getElementsByTagName("video"), d = 0; d < b.length; ++d) {
 var e = b[d];
 !0 === e.ucListened && c.push(e)
 }
 for (var d = document.getElementsByTagName("iframe"), g = 0; g < d.length; ++g) if (b = d[g], t(b) && 1 !== b.ucVCusPlayDisabled) for (var b = b.contentDocument.getElementsByTagName("video"), k = 0; k < b.length; ++k) e = b[k],
 !0 === e.ucListened && c.push(e);
 for (d = 0; d < c.length; ++d) e = c[d],
 z(e, h(e));
 B("UCBROWSER_VIDEO_BACKFORWARD", null)
 }
 window.location.href !== ga && (ga = window.location.href, w = 0);
 c = J();
 if (0 < c.videoUrl.length) q = 0,
 f.posterUrl = c.posterUrl,
 r(c.videoUrl, c.posterUrl, !1);
 else {
 if (f.videoUrl && 0 < f.videoUrl.length) if (q) {
 c = !1;
 for (b = 0; b < a.length; ++b) if (a[b] === q) {
 c = !0;
 break
 }!0 !== c ? (q = 0, r("", "", !0)) : r(f.videoUrl, f.posterUrl, !1)
 } else r("", "", !0);
 B("UCBROWSER_GET_VIDEO_URL", null)
 }
 }
 function ha() {
 da();
 var a = 1E3;
 5 > D++ && (0 < f.videoUrl.length ? D = 5 : a = 200);
 g(ha, a)
 }
 function qa() {
 g(da, 100);
 0 === ia && (ia = 1, g(ha, 0), window.addEventListener("message", oa, !1), 1 === window.videoInsertObserveEnabled && document.addEventListener("DOMNodeInserted", pa, !1))
 }
 function ba(a, c) {
 var b = JSON.parse(JSON.stringify(c));
 b.cmd = "webseek:";
 b.params = [a];
 n(b)
 }
 function ra() {
 return this.ucDuration ? this.ucDuration : 0
 }
 function sa() {
 return this.ucCurTime ? this.ucCurTime : 0
 }
 function ta(a) {
 !0 !== ua && (this.ucCurTime = a, ba(a, f))
 }
 function E(a) {
 if (!0 !== va && !m) {
 if (!0 !== a.ucVideoPropertiesHooked) try {
 a.ucVideoPropertiesHooked = !0,
 Object.defineProperty(a, "duration", {
                       get: ra
                       }),
 Object.defineProperty(a, "currentTime", {
                       get: sa,
                       set: ta
                       }),
 Object.defineProperty(a, "seekable", {
                       get: function () {
                       return ja
                       }
                       })
 } catch (c) {}
 a.oriHookVideo && E(a.oriHookVideo)
 }
 }
 function wa(a) {
 g(function () {
   var c = L();
   if (c) {
   switch (a.vEvent) {
   case "durationchange":
   var b = a.duration;
   ja.duration = b;
   c.ucDuration = parseInt(b, 10);
   if (b = c.oriHookVideo) b.ucDuration = c.ucDuration;
   E(c);
   break;
   case "timeupdate":
   c.ucCurTime = parseFloat(a.currentTime);
   if (b = c.oriHookVideo) b.ucCurTime = c.ucCurTime;
   E(c);
   break;
   case "play":
   c.ucWaitPlaying = !0;
   c.oriHookVideo && (c.oriHookVideo.ucWaitPlaying = !0);
   break;
   case "playing":
   c.ucWaitPlaying = !1;
   c.oriHookVideo && (c.oriHookVideo.ucWaitPlaying = !1);
   break;
   case "pause":
   !0 === c.ucWaitPlaying && "1" == a.stop && R(c, "playing", !0)
   }
   R(c, a.vEvent, !0)
   } else B("UCBROWSER_VIDEO_PLAYBACK_EVENTS", a)
   }, 1)
 }
 function xa() {
 C = 1;
 D = 0
 }
 function ya() {
 return 1 >= w
 }
 function ka() {
 var a = I();
 ca = !0;
 return a.videoUrl
 }
 function za(a) {
 a && window.ucbrowser_readmode_detect || g(ka, 1)
 }
 function g(a, c) {
 window.setTimeout(a, c)
 }
 if (void 0 == window.K_UCKUIWebViewVideoFinder) {
    window.ucVideoAutoHookDisabled = false;
    window.videoCustomPlayDisabled = false;
 var ja = {
 length: 1,
 duration: 0,
 start: function (a) {
 return 0
 },
 end: function (a) {
 return this.duration
 }
 },
 ia = 0,
 ga = "",
 q = 0,
 u, C = 0,
 ea, fa, f = {
 videoUrl: "",
 posterUrl: "",
 errorCode: 0
 },
 D = 0,
 w = 0,
 ca = !1,
 F = window.UCVVideoEvtReceiver,
 ma = F && 0 < F.length ? F : "ori",
 ua = window.ucvSkipVideoPropDefine,
 va = window.ucvSkipVideoCustomProp,
 aa = window.videoCustomPlayDisabled,
 m = window.ucVideoAutoHookDisabled;
 window.UCV_accept_flvscheme = 0;
 window.UC_VIDEO_isFirstDetected = ya;
 window.UC_VIDEO_checkVideoSupported = K;
 window.UC_VIDEO_findVideoURL = la;
 window.UC_VIDEO_sniffVideoUrl = ka;
 window.UC_VIDEO_enableAutoSniff = za;
 window.UC_VIDEO_Playback_Event = wa;
 window.UC_VIDEO_onBackForward = xa;
 window.K_UCKUIWebViewVideoFinder = 0;
 window.ucbrowser_readmode_detect = 1;
 m || (HTMLVideoElement.prototype.play = v, HTMLVideoElement.prototype.load = Q);
 g(qa, 1)
 }
 })();

(function () {
    function f(a) {
        var b = a.getAttribute("src");
        if (b && 0 < b.length) b = a.src;
        else {
            a = a.getElementsByTagName("source");
            for (var c = 0; c < a.length; ++c) {
                var d = a[c];
                if (void 0 != d.src && 0 < d.src.length) {
                    b = d.src;
                    break
                }
            }
        }
        return b ? b : ""
    }
    function l(a) {
        var b = a.getAttribute("poster");
        if (b && 0 < b.length) b = a.poster;
        else {
            var c = window.UCVPosterUrlGetter;
            if ("function" === typeof c) {
                if (null === a.parentNode) a: {
                    for (var d = document.getElementsByTagName("video"), f = d.length, e = 0; e < f; ++e) if (d[e].oriHookVideo === a) {
                        a = d[e];
                        break a
                    }
                    a = null
                }
                a && (b = c(a))
            }
        }
        return b ? b : ""
    }
    function e(a, b, c) {
        window.parent && "undefined" != typeof window.parent.postMessage && (a = {
            UC_MSG_NAME: a,
            UC_VIDEO_URL: b
        }, c && (a.extInfo = c), window.parent.postMessage(a, "*"))
    }
    function q(a, b, c) {
        a = a.oriHookVideo ? a.oriHookVideo : a;
        c = document.createEvent("HTMLEvents");
        c.initEvent(b, !1, !0);
        c.isUCEvent = !0;
        a.dispatchEvent(c)
    }
    function r(a) {
        return void 0 != a && ("click" == a.type || "tap" == a.type || 0 == a.type.indexOf("touch"))
    }
    function t() {
        if (r(window.event) || "number" == typeof this.ucRetryLeft && 0 < this.ucRetryLeft) {
            var a =
            f(this);
            if (a) {
                var b = {
                    posterUrl: l(this)
                };
                e("UCPlayVideo", a, b);
                this.ucRetryLeft = null
            } else this.ucRetryLeft = "number" == typeof this.ucRetryLeft ? this.ucRetryLeft - 1 : 3,
            0 < this.ucRetryLeft && window.setTimeout(t, 10)
        }
    }
    function G() {
        r(window.event) && e("UCLoadVideo", f(this), 0)
    }
    function u(a) {
        a.webkitEnterFullscreen = function (a) {};
        a.webkitEnterFullScreen = function (a) {};
        a.webkitRequestFullScreen = function (a) {};
        a.webkitRequestFullscreen = function (a) {};
        a.setAttribute("webkit-playsinline", "");
        a.controls = !0
    }
    function h(a, b, c, d) {
        a.removeEventListener(b, c, d);
        a.addEventListener(b, c, d)
    }
    function v(a) {
        a.play = t;
        a.load = G;
        h(a, "play", w, !1);
        h(a, "ended", x, !1);
        h(a, "error", y, !1);
        h(a, "abort", z, !1);
        h(a, "canplaythrough", A, !1);
        h(a, "webkitendfullscreen", B, !1)
    }
    function m(a, b) {
        var c = a.parentNode;
        if (c) {
            var d = a.cloneNode(!0);
            d.ucVCusPlayDisabled = a.ucVCusPlayDisabled;
            b && 0 < b.length && (d.src = b);
            a.oriHookVideo ? (a.removeEventListener("play", w, !1), a.removeEventListener("ended", x, !1), a.removeEventListener("error", y, !1), a.removeEventListener("abort", z, !1), a.removeEventListener("canplaythrough", A, !1), a.removeEventListener("webkitendfullscreen", B, !1), d.oriHookVideo = a.oriHookVideo, a.oriHookVideo = null) : (d.oriHookVideo = a, a.ucLastCheckSrc = b, a.ucSkipError = !0);
            d.ucSkipError = !0;
            d.ucListened = !0;
            d.setAttribute("preload", "none");
            u(d);
            v(d);
            d.onclick = H;
            c.replaceChild(d, a);
            return d
        }
    }
    function H(a) {
        a && a.target && (a = a.target, "video" == a.nodeName.toLowerCase() && a.play())
    }
    function w(a) {
        if (!a.isUCEvent) {
            a = a.target;
            var b = {};
            if ("video" == a.nodeName.toLowerCase()) {
                var c =
                f(a),
                    d;
                if (d = 1 != a.ucVCusPlayDisabled) d = !1,
                a && "function" === typeof a.webkitExitFullScreen && (a.pause(), a.webkitExitFullScreen(), d = !0);
                d ? (b.posterUrl = l(a), a.src = "", a.load(), m(a, c), e("UCCusSysPlayVideo", c, b)) : !0 !== a.ucIsPlaying && (b.vEvent = "play", b.isNextPlay = !0 === a.ucIsPlayerShown, e("UCOnSysVEvent", c, b), a.ucIsPlaying = !0, a.ucIsPlayerShown = !0, a.ucStartLoadTime = (new Date).getTime(), !0 === a.ucFirstLoaded && (b = {
                        vEvent: "succ",
                        loadTime: 1
                    }, e("UCOnSysVEvent", c, b)))
            }
        }
    }
    function y(a) {
        if (!a.isUCEvent) {
            a = a.target;
            a.ucIsPlaying = !1;
            if (!0 !== a.ucSkipError) {
                var b = f(a);
                b && 0 < b.length && e("UCOnSysVEvent", b, {
                    vEvent: "error"
                })
            }
            a.ucFirstLoaded = !1;
            a.ucStartLoadTime = 0
        }
    }
    function x(a) {
        a.isUCEvent || (a = a.target, a.ucIsPlaying = !1, a.ucFirstLoaded = !1, a.ucStartLoadTime = 0)
    }
    function z(a) {
        a.isUCEvent || (a = a.target, a.ucIsPlaying = !1, a.ucFirstLoaded = !1, a.ucStartLoadTime = 0)
    }
    function A(a) {
        if (!a.isUCEvent && (a = a.target, !0 !== a.ucFirstLoaded && (a.ucFirstLoaded = !0, 0 < a.ucStartLoadTime))) {
            var b = {
                vEvent: "succ",
                loadTime: (new Date).getTime() - a.ucStartLoadTime
            };
            e("UCOnSysVEvent", f(a), b)
        }
    }
    function B(a) {
        a.isUCEvent || (a = a.target, a.ucIsPlaying = !1, a.ucIsPlayerShown = !1)
    }
    function C(a) {
        !0 !== a.ucListened && (a.ucListened = !0, a.removeAttribute("autoplay"), a.ucVCusPlayDisabled = n, v(a), 1 != a.ucVCusPlayDisabled && (a.pause(), u(a), m(a, f(a))));
        if (a.oriHookVideo) {
            var b = f(a.oriHookVideo);
            b && 0 < b.length && b != a.oriHookVideo.ucLastCheckSrc ? (a.src = b, a.oriHookVideo.ucLastCheckSrc = b, a.oriHookVideo.ucSrcReset = !0) : (b = f(a)) && 0 < b.length && b != a.oriHookVideo.ucLastCheckSrc && (a.oriHookVideo.ucLastCheckSrc = b, a.oriHookVideo.ucSrcReset = !0, a.oriHookVideo.src = b)
        }
    }
    function D() {
        for (var a = document.getElementsByTagName("video"), b = 0; b < a.length; ++b) C(a[b]);
        for (var c = 0, b = 0; b < a.length; ++b) {
            var d = a[b],
                e = f(d);
            if (e && 0 < e.length) if ("none" == document.defaultView.getComputedStyle(d).display) c || (c = d);
            else return d
        }
        return c
    }
    function I() {
        return this.ucDuration ? this.ucDuration : 0
    }
    function J() {
        return this.ucCurTime ? this.ucCurTime : 0
    }
    function K(a) {
        !0 !== L && (this.ucCurTime = a, e("UCSeekVideo", g, {
            seekTime: a
        }))
    }
    function p(a) {
        if (!0 !== M) {
            if (!0 !== a.ucVideoPropertiesHooked) try {
                a.ucVideoPropertiesHooked = !0,
                Object.defineProperty(a, "duration", {
                    get: I
                }),
                Object.defineProperty(a, "currentTime", {
                    get: J,
                    set: K
                }),
                Object.defineProperty(a, "seekable", {
                    get: function () {
                        return E
                    }
                })
            } catch (b) {}
            a.oriHookVideo && p(a.oriHookVideo)
        }
    }
    function N(a) {
        a = a.target;
        var b;
        "video" == a.nodeName.toLowerCase() ? b = a : 0 < a.childElementCount && (a = a.getElementsByTagName("video"), 0 < a.length && (b = a[0]));
        b && C(b)
    }
    var E = {
        length: 1,
        duration: 0,
        start: function (a) {
            return 0
        },
        end: function (a) {
            return this.duration
        }
    },
        g = "",
        k = 0,
        F = 0,
        n = 0,
        L = window.ucvSkipVideoPropDefine,
        M = window.ucvSkipVideoCustomProp;
    window.addEventListener("message", function (a) {
            if (a = (a || window.event).data) switch (a.UC_MSG_NAME) {
            case "UCBROWSER_GET_VIDEO_URL":
                1 === a.disCusSysPlay && (n = 1);
                1 === a.obsVIn && 1 !== F && (F = 1, document.addEventListener("DOMNodeInserted", N, !1));
                a = "";
                var b = D();
                if (b) {
                    var c = b.oriHookVideo ? b.oriHookVideo : b;
                    k = c.error && c.error.code ? !0 === c.ucSrcReset ? 0 : c.error.code : 0;
                    4 != k ? a = f(b) : b.src = ""
                }
                a && 0 < a.length ? a != g && (g = a, a = {
                    errorcode: k,
                    posterUrl: l(b)
                }, e("UCVideoURLGot", g, a)) : g && 0 < g.length && (g = "", a = {
                    changed: !0,
                    errorcode: k,
                    posterUrl: ""
                }, e("UCVideoURLGot", g, a));
                break;
            case "UCBROWSER_VIDEO_PLAYBACK_EVENTS":
                if ((a = a.extInfo) && a.vEvent && (b = D())) {
                    switch (a.vEvent) {
                    case "durationchange":
                        c = a.duration;
                        E.duration = c;
                        b.ucDuration = parseInt(c, 10);
                        if (c = b.oriHookVideo) c.ucDuration = b.ucDuration;
                        p(b);
                        break;
                    case "timeupdate":
                        b.ucCurTime = parseFloat(a.currentTime);
                        if (c = b.oriHookVideo) c.ucCurTime = b.ucCurTime;
                        p(b);
                        break;
                    case "play":
                        b.ucWaitPlaying = !0;
                        b.oriHookVideo && (b.oriHookVideo.ucWaitPlaying = !0);
                        break;
                    case "playing":
                        b.ucWaitPlaying = !1;
                        b.oriHookVideo && (b.oriHookVideo.ucWaitPlaying = !1);
                        break;
                    case "pause":
                        !0 === b.ucWaitPlaying && "1" == a.stop && q(b, "playing", !0)
                    }
                    q(b, a.vEvent, !0)
                }
                break;
            case "UCBROWSER_CLEAR_VIDEO_CACHE":
                g = "";
                break;
            case "UCBROWSER_VIDEO_BACKFORWARD":
                if (g = "", 1 !== n) {
                    a = [];
                    b = document.getElementsByTagName("video");
                    for (c = 0; c < b.length; ++c) {
                        var d = b[c];
                        !0 === d.ucListened && a.push(d)
                    }
                    for (c = 0; c < a.length; ++c) d = a[c],
                    m(d, f(d))
                }
            }
        }, !1)
})();
