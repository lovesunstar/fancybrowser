//
//  UIKit.swift
//  FancyVideo
//
//  Created by SunJiangting on 16/10/22.
//  Copyright © 2016年 Suen. All rights reserved.
//

import UIKit
import WebKit
import GiantShoulder

let barTintColor = UIColor.white.withAlphaComponent(0.73)

public extension UIColor {
    
    public convenience init(rgb: Int, alpha: CGFloat = 1.0) {
        self.init(red: CGFloat((rgb & 0xFF0000) >> 16) / 255.0, green: CGFloat((rgb & 0x00FF00) >> 8) / 255.0, blue: CGFloat((rgb & 0x0000FF)) / 255.0, alpha: alpha)
    }
}

extension UIResponder {
    func nextResponder<T>(_: T.Type) -> T? {
        var nextResponder:UIResponder? = self
        while (nextResponder != nil) && !(nextResponder is T) {
            nextResponder = nextResponder?.next
        }
        if nextResponder is T {
            return nextResponder as? T
        }
        return nil
    }
}


struct FancyVideo {
    
    static let errorDomain: String = "com.fancyVideo.error"
    
    class Button: UIButton {
        var touchInsets: UIEdgeInsets = .zero
        override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
            if touchInsets == .zero {
                return super.point(inside: point, with: event)
            }
            var bounds = self.bounds
            bounds.origin.x += touchInsets.left
            bounds.origin.y += touchInsets.top
            bounds.size.width -= (touchInsets.left + touchInsets.right)
            bounds.size.height -= (touchInsets.top + touchInsets.bottom)
            return self.isUserInteractionEnabled && self.isEnabled && self.alpha > 0.01 && bounds.contains(point)
        }
    }
}

extension WKWebView {
    func dispatchDocumentEvent(_ event: String, parameters: [String: Any], completionHandler: ((Any?, Error?) -> Swift.Void)? = nil) {
        let eventBody: [String: Any] = parameters
        let jsString: String
        if
            let _data = try? JSONSerialization.data(withJSONObject: eventBody, options: JSONSerialization.WritingOptions()),
            let eventString = String(data: _data, encoding: .utf8) {
            
            jsString = "(function() { var event = new CustomEvent('\(event)', \(eventString)); document.dispatchEvent(event)}());"
        } else {
            jsString = "(function() { var event = new CustomEvent('\(event)', {}); document.dispatchEvent(event)}());"
        }
        evaluateJavaScript(jsString, completionHandler: completionHandler)
    }
}

func updateStatusBar() {
    let player = VideoCenter.shared.player
    if player.window == nil {
        STGetStatusBarWindow()?.alpha = 1
        if let bvc = topmostViewController() as? BaseViewController {
            bvc.temporaryStatusBarStyle = nil
            bvc.setNeedsStatusBarAppearanceUpdate()
        }
        return
    }
    // Hide
    if player.isFullscreen {
        if Device.isIPhoneX {
            STGetStatusBarWindow()?.alpha = 0.0
        } else {
            STGetStatusBarWindow()?.alpha = !player.controlView.isTitleViewHidden ? 1.0 : 0
        }
        if let bvc = topmostViewController() as? BaseViewController {
            bvc.temporaryStatusBarStyle = UIStatusBarStyle.lightContent
            bvc.setNeedsStatusBarAppearanceUpdate()
        }
    } else {
        if let bvc = topmostViewController() as? BaseViewController {
            bvc.temporaryStatusBarStyle = nil
            bvc.setNeedsStatusBarAppearanceUpdate()
        }
        if NiubilityPlayer.isIntersectsWithStatusBar {
            STGetStatusBarWindow()?.alpha = NiubilityPlayer.preferredStatusBarHidden ? 0.0 : 1.0
        } else {
            STGetStatusBarWindow()?.alpha = 1.0
        }
    }
}

func topmostViewController() -> UIViewController? {
    guard let rootVC = UIApplication.shared.delegate?.window??.rootViewController else {
        return nil
    }
    guard let vc = rootVC as? SafariViewController, let visibleVC = vc.visibleViewController else {
        return visibleViewController(for: rootVC)
    }
    return visibleViewController(for: visibleVC)
}

func visibleViewController(for vc: UIViewController) -> UIViewController {
    if let nvc = vc as? UINavigationController {
        if let nvcv = nvc.visibleViewController {
            return visibleViewController(for: nvcv)
        }
    }
    if let snvc = vc as? STNavigationController {
        if let nvcv = snvc.visibleViewController {
            return visibleViewController(for: nvcv)
        }
    }
    if let tvc = vc as? UITabBarController {
        if let svc = tvc.selectedViewController {
            return visibleViewController(for: svc)
        }
    }
    if let stvc = vc as? STTabBarController {
        if let svc = stvc.selectedViewController {
            return visibleViewController(for: svc)
        }
    }
    if let pvc = vc.presentedViewController {
        return visibleViewController(for: pvc)
    }
    return vc
}
