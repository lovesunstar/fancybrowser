//
//  TableViewCell.swift
//  FancyVideo
//
//  Created by SunJiangting on 2018/2/26.
//  Copyright © 2018年 Samaritan. All rights reserved.
//

import UIKit
import GiantShoulder

class TableViewCell: UITableViewCell {
    
    var selectedEnabled = true {
        didSet {
            if !selectedEnabled {
                selectedView.isHidden = true
            }
        }
    }
    
    var selectedView = UIView(frame: .zero)
    var separatorView = UIView(frame: .zero)
    var topSeparatorView = UIView(frame: .zero)
    
    var separatorViewInset: UIEdgeInsets = .zero {
        didSet {
            setNeedsLayout()
        }
    }
    
    var bkgView = UIView(frame: .zero)
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        loadSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadSubviews()
    }
    
    func loadSubviews() {
        
        self.selectionStyle = .none
        self.backgroundView = bkgView
        
        selectedView.frame = self.contentView.bounds
        selectedView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        selectedView.backgroundColor = UIColor(rgb: 0xDDDDDD, alpha: 0.5)
        selectedView.isHidden = true
        addSubview(selectedView)
        sendSubview(toBack: selectedView)
        
        separatorView.backgroundColor = UIColor(rgb: 0xDDDDDD)
        addSubview(separatorView)
        
        topSeparatorView.backgroundColor = UIColor(rgb: 0xDDDDDD)
        topSeparatorView.isHidden = true
        addSubview(topSeparatorView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        separatorView.frame = CGRect(x: separatorViewInset.left, y: self.bounds.height - STOnePixel(), width: self.bounds.width - (separatorViewInset.left + separatorViewInset.right), height: STOnePixel())
        
        topSeparatorView.frame = CGRect(x: self.separatorViewInset.left, y: 0, width: self.width - (separatorViewInset.left + separatorViewInset.right), height: STOnePixel())
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        if selectedEnabled {
            selectedView.isHidden = false
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        if selectedEnabled {
            selectedView.isHidden = true
        }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
        if selectedEnabled {
            selectedView.isHidden = true
        }
    }
}
