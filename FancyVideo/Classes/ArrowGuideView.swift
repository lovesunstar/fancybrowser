//
//  ArrowGuideView.swift
//  FancyVideo
//
//  Created by SunJiangting on 2018/5/6.
//  Copyright © 2018年 Samaritan. All rights reserved.
//

import UIKit
import GiantShoulder

enum UIGuideType {
    case videoMoreAction
    case browserMoreAction
}

extension UIGuideType {
    var persistenceKey: String {
        switch self {
        case .videoMoreAction:
            return "video-more-action"
        case .browserMoreAction:
            return "browser-more-action"
        }
    }
}

class UIGuideManager: NSObject {
    
    static let shared = UIGuideManager()
    
    let persistence = STPersistence(named: "UIGuideRecord")
    
    func hasShow(_ guide: UIGuideType) -> Bool {
        return (persistence.value(forKey: guide.persistenceKey) as? NSNumber)?.boolValue ?? false
    }
    
    func setShow(_ show: Bool, for guide: UIGuideType) {
        persistence.setValue(show, forKey: guide.persistenceKey)
    }
    
    func hasShow(_ guide: UIGuideType, extra: String) -> Bool {
        let key = "\(guide.persistenceKey)-\(extra)"
        return (persistence.value(forKey: key) as? NSNumber)?.boolValue ?? false
    }
    
    func setShow(_ show: Bool, for guide: UIGuideType, extra: String) {
        let key = "\(guide.persistenceKey)-\(extra)"
        persistence.setValue(show, forKey: key)
    }
    
    @objc func reset() {
        persistence.removeAllCachedValues()
    }
    
    func setGuideTimes(_ times: Int, for guide: UIGuideType) {
        persistence.setValue(times, forKey: guide.persistenceKey)
    }
    
    func guideTimes(for guide: UIGuideType) -> Int {
        return (persistence.value(forKey: guide.persistenceKey) as? NSNumber)?.intValue ?? 0
    }
}


class ArrowGuideView: UIView {
    enum Direction: Int {
        case up
        case down
        
        var arrowImageName: String {
            switch self {
            case .up:
                return "arrow_guide_up"
            case .down:
                return "arrow_guide_down"
            }
        }
    }
    
    enum UI {
        static let arrowSize = CGSize(width: 15, height: 6)
        static let arrowBkgIntersection: CGFloat = 1
    }
    
    @objc var duration = 5.0
    private let bkgView = UIView(frame: .zero)
    private let mkView = UIView(frame: .zero)
    private let arrowView = UIImageView(frame: .zero)
    private let direction: Direction
    private let shapeLayer = CAShapeLayer()
    
    var highlightedArea: CGRect? = nil
    @objc var overlayBackgroundColor = UIColor.clear
    
    var contentInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
    var contentMargin = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
    
    deinit {
        mkView.removeFromSuperview()
    }
    
    init(direction: Direction) {
        self.direction = direction
        super.init(frame: .zero)
        
        bkgView.backgroundColor = UIColor(rgb: 0xffed43)
        bkgView.layer.borderColor = UIColor.white.cgColor
        bkgView.layer.borderWidth = 1
        bkgView.layer.cornerRadius = 6
        addSubview(bkgView)
        
        mkView.isUserInteractionEnabled = false
        mkView.backgroundColor = UIColor.black.withAlphaComponent(0.1)
        
        arrowView.image = UIImage(named: direction.arrowImageName)
        arrowView.sizeToFit()
        addSubview(arrowView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc(showInView:center:) public func show(in view: UIView, center: CGPoint) {
        mkView.frame = view.bounds
        view.addSubview(mkView)
        shapeLayer.path = {
            let path = UIBezierPath(roundedRect: mkView.bounds, cornerRadius: 0)
            if let area = self.highlightedArea {
                path.append(UIBezierPath(roundedRect: area, cornerRadius: 0))
            }
            return path.cgPath
        }()
        shapeLayer.fillRule = kCAFillRuleEvenOdd
        shapeLayer.fillColor = overlayBackgroundColor.cgColor
        mkView.layer.addSublayer(shapeLayer)
        
        let contentView = self.contentView
        bkgView.addSubview(contentView)
        let contentConstraintedSize = CGSize(width: view.bounds.width - contentMargin.width - contentInset.width, height: view.bounds.height - contentMargin.height - contentInset.height)
        let newContentSize = contentSize(constraintedTo: contentConstraintedSize)
        
        var bkgWidth  = newContentSize.width + contentInset.width
        let bkgHeight = newContentSize.height + contentInset.height
        var viewWidth: CGFloat = bkgWidth
        let viewHeight: CGFloat = bkgHeight + UI.arrowSize.height - UI.arrowBkgIntersection
        
        if viewWidth > view.bounds.width - contentMargin.width {
            viewWidth = view.bounds.width - contentMargin.width
            bkgWidth = viewWidth
        }
        view.addSubview(self)
        
        var viewX = center.x - viewWidth * 0.5
        if viewX < contentMargin.left {
            viewX = contentMargin.left
        } else if (viewX + viewWidth) > (view.bounds.width - contentMargin.right) {
            viewX = (view.bounds.width - contentMargin.right) - viewWidth
        }
        let viewY, bkgY, arrowY: CGFloat
        if direction == .up {
            viewY = center.y
            bkgY = UI.arrowSize.height - UI.arrowBkgIntersection
            arrowY = 0
        } else {
            viewY = center.y - viewHeight
            bkgY = 0
            arrowY = bkgHeight - UI.arrowBkgIntersection
        }
        self.frame = CGRect(x: viewX, y: viewY, width: viewWidth, height: viewHeight)
        bkgView.frame = CGRect(x: 0, y: bkgY, width: self.bounds.width, height: bkgHeight)
        contentView.frame = CGRect(x: contentInset.left, y: contentInset.top, width: bkgView.bounds.width - contentInset.width, height: bkgView.bounds.height - contentInset.height)
        // viewX + arrowX = centerX
        arrowView.frame = CGRect(x: center.x - UI.arrowSize.width * 0.5 - viewX, y: arrowY, width: UI.arrowSize.width, height: UI.arrowSize.height)
        
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.y")
        let delta: CGFloat = 3
        animation.duration = 1.6
        animation.repeatCount = Float.greatestFiniteMagnitude
        animation.values = [-delta, delta]
        animation.autoreverses = true
        animation.isRemovedOnCompletion = false
        self.layer.add(animation, forKey: "shakeAnimation")
        perform(#selector(removeFromSuperview), with: nil, afterDelay: duration)
    }
    
    var contentView: UIView {
        return UIView()
    }
    
    func contentSize(constraintedTo size: CGSize) -> CGSize {
        return CGSize(width: 100, height: 50)
    }
    
    @objc private func handleTap(_ gesture: UITapGestureRecognizer) {
        removeFromSuperview()
    }
    
    override func removeFromSuperview() {
        super.removeFromSuperview()
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        mkView.removeFromSuperview()
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        removeFromSuperview()
        return nil
    }
}

class TextArrowGuideView: ArrowGuideView {
    
    private let contentLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = UIColor(rgb: 0x282828)
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    init(direction: Direction, text: String) {
        super.init(direction: direction)
        contentLabel.text = text
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var contentView: UIView {
        return contentLabel
    }
    override func contentSize(constraintedTo size: CGSize) -> CGSize {
        return contentLabel.sizeThatFits(size)
    }
}
