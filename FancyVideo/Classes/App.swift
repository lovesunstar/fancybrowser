//
//  App.swift
//  VEnjoy
//
//  Created by SunJiangting on 2017/1/2.
//  Copyright © 2017年 Suen. All rights reserved.
//

import Foundation
import GiantShoulder
import AdSupport

struct App {
    
    static let isDebug = false
    
    static let version = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String ?? "1.0"
    static let buildVersion = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as? String ?? "1.0"
    static let identifier = Bundle.main.object(forInfoDictionaryKey: "AppIdentifier") as? String ?? "1000"
    static let bundleIdentifier = Bundle.main.bundleIdentifier ?? "Unknown"
    static let channel = Bundle.main.object(forInfoDictionaryKey: "AppChannel") ?? "App Store"
    static let launchTimesKey = "AppLaunchTimesKey"
    static let versionLaunchTimesKey = "AppLaunchTimesKey\(version)"
    
    static let registerTimeKey = "AppRegisterTimeKey\(version)"
    static let activeCountKey = "AppActiveCountKey"
    static let versionActiveCountKey = "AppActiveCountKey\(version)"
    
    static let displayName: String = {
        if let name = Bundle.main.object(forInfoDictionaryKey: "CFBundleDisplayName") as? String {
            return name
        }
        if let name = Bundle.main.object(forInfoDictionaryKey: "CFBundleName") as? String {
            return name
        }
        return "FancyBrowser"
    }()
    
    /**
     Read the IDFV using following steps
     - Read from userDefaults
     - If not exists, Read from Keychain
     - If not exists, read from device and store to keychain
     */
    static let idfv: String = {
        let userDefaults = UserDefaults.standard
        if let storedVendorId = userDefaults.string(forKey: "STVendorID") {
            return storedVendorId
        }
        if let keychainId = STKeychainManager.shared.value(forKey: "STVendorID") as? String {
            userDefaults.setValue(keychainId, forKey: "STVendorID")
            userDefaults.synchronize()
            return keychainId
        }
        if let vendorId = device.identifierForVendor {
            let value = vendorId.uuidString
            userDefaults.set(value, forKey: "STVendorID")
            userDefaults.synchronize()
            STKeychainManager.shared.setValue(value, forKey: "STVendorID")
            return value
        }
        return ""
    }()
    
    static let idfa: String = {
        return ASIdentifierManager.shared().advertisingIdentifier.uuidString
    }()
    
    static let locale = (language: "zh", region: "CN", script: "")
    
    static let appStoreIdentifier = AppKeys.appStoreID
    
    private static let calendar = Calendar.current
    private static let systemLocale = Locale.current
    private static let device = UIDevice.current
    
    static let acceptLanguage: String = {
        let userLanguage = App.locale.language.lowercased(), localeLanguage = Device.locale.language.lowercased()
        if userLanguage == localeLanguage {
            return "\(userLanguage)"
        }
        return ""
    }()
    
    static let userAgent: String = {
        let ntName = Device.nt.type?.name ?? "Unknown"
        let deviceName = (Device.name as NSString).st_stringByURLEncoded()
        let systemName = (Device.system.name as NSString).st_stringByURLEncoded()
        var results = "\(App.identifier)/\(App.version) (\(deviceName)); \(systemName) \(Device.system.version); \(Device.locale.language); \(ntName)"
        let carrier = Device.nt.carrier
        if carrier.utf16.count > 0 {
            results += "; \((carrier as NSString).st_stringByURLEncoded())"
        }
        let radio = Device.nt.radio
        if let r = radio, r.utf16.count > 0 {
            results += "; \((r as NSString).st_stringByURLEncoded())"
        }
        results += ")"
        return results
    }()
    
    
    static let eventSessionParameters: [String : Any] = {
        var results = [String : Any]()
        results["Device Model"] = Device.model
        results["Screen Width"] = Device.screen.width
        results["Screen Height"] = Device.screen.height
        results["OS"] = "iOS"
        results["OS Version"] = Device.system.version
        results["iOS Jailbroken"] = Device.isJailBroken
        results["App ID"] = App.identifier
        results["App Version"] = App.version
        results["App Channel"] = App.channel
        results["Timezone Name"] = Device.timeZone.name
        results["Timezone Offset"] = Device.timeZone.offset
        return results
    }()
    
    static let localeBasedEventParameters: [String : Any] = {
        return ["Language" : Device.locale.language, "Region" : Device.locale.region, "User Language" : App.locale.language, "User Region" : App.locale.region]
    }()
    
    
    static let localeBasedParameters: [String : Any] = {
        var results = [String : Any]()
        results["tz_offset"] = NSNumber(value: Device.timeZone.offset)
        results["tz_name"] = Device.timeZone.name
        
        results["language"] = Device.locale.language
        results["region"] = Device.locale.region
        results["user_language"] = App.locale.language
        results["user_region"] = App.locale.region
        return results
    }()
    
    static let httpCommonParameters: [String : Any] = {
        var results = [String : Any]()
        results["aid"] = App.identifier
        results["version"] = App.version
        results["channel"] = App.channel
        results["os"] = Device.osName
        results["os_version"] = Device.system.version
        results["device_model"] = Device.model
        results["device_platform"] = Device.platform
        results["vid"] = App.idfv
        results["idfa"] = App.idfa
        results += localeBasedParameters
        return results
    }()
    
    static var isDevEnvironment: Bool {
        return false
    }
}
