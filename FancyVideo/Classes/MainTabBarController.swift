//
//  MainTabBarController.swift
//  FancyVideo
//
//  Created by SunJiangting on 2017/6/8.
//  Copyright © 2017年 Samaritan. All rights reserved.
//

import UIKit
import GiantShoulder

protocol MainTabBarRefreshable {
    func refresh()
}

class MainTabBarController: STTabBarController {

    enum UI {
        fileprivate static let tabBarSeparatorColor = UIColor(rgb: 0xDDDDDD)
    }
    
    private lazy var tabBarView = TabBar(frame: .zero)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBar.separatorView.backgroundColor = UI.tabBarSeparatorColor
        tabBarView.frame = self.tabBar.bounds
        self.tabBar.addSubview(tabBarView)
        self.tabBar.bringSubview(toFront: self.tabBar.separatorView)
        
        let browseViewController = HomeViewController()
        let browseNavigationController = BaseNavigationController(rootViewController: browseViewController)
        
        let settingViewController = SettingViewController()
        let settingNavigationController = BaseNavigationController(rootViewController: settingViewController)
        
        self.viewControllers = [browseNavigationController, settingNavigationController]
        
        updateTabBarItems()
        
    }

    override var selectedIndex: UInt {
        didSet {
            updateTabBarItems()
            if oldValue == selectedIndex {
                if let refreshable = self.selectedViewController as? MainTabBarRefreshable {
                    refreshable.refresh()
                }
            }
        }
    }
    
    private func updateTabBarItems() {
        switch selectedIndex {
        case 0:
            tabBarView.browseButton.isSelected = true
            tabBarView.localButton.isSelected = false
        case 1:
            tabBarView.browseButton.isSelected = false
            tabBarView.localButton.isSelected = true
        default:
            break
        }
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var prefersStatusBarHidden: Bool {
        return selectedViewController?.prefersStatusBarHidden ?? false
    }
    
    @available(iOS 11.0, *)
    override func viewSafeAreaInsetsDidChange() {
        super.viewSafeAreaInsetsDidChange()
        self.view.setNeedsLayout()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let safeAreaBottom: CGFloat
        if #available(iOS 11, *) {
            safeAreaBottom = self.view.safeAreaInsets.bottom
        } else {
            safeAreaBottom = 0
        }
        tabBarView.frame = CGRect(x: 0, y: 0, width: self.tabBar.bounds.width, height: self.tabBar.bounds.height - safeAreaBottom)
    }
    
    @available(iOS 11.0, *)
    override func prefersHomeIndicatorAutoHidden() -> Bool {
        return selectedViewController?.prefersHomeIndicatorAutoHidden() ?? false
    }
    
    private class TabBar: UIView {
        
        let contentView = UIView(frame: .zero)
        
        fileprivate let separatorView = UIImageView(frame: .zero)
        
        fileprivate let browseButton: UIButton = Button(frame: .zero)
        fileprivate let localButton: UIButton = Button(frame: .zero)
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            func setImage(name: String, selectedImage: String, for button: UIButton) {
                let image = UIImage(named: name), selectedImage = UIImage(named: selectedImage)
                button.setImage(image, for: .normal)
                button.setImage(image, for: .highlighted)
                button.setImage(selectedImage, for: .selected)
                button.setImage(selectedImage, for: [.selected, .highlighted])
            }
            contentView.frame = self.bounds
            contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            addSubview(contentView)
            
            separatorView.backgroundColor = MainTabBarController.UI.tabBarSeparatorColor
            contentView.addSubview(separatorView)
            
            setImage(name: "tabbar_browser_normal", selectedImage: "tabbar_browser_selected", for: browseButton)
            browseButton.setTitle("TabBarBrowse".localized(), for: .normal)
            contentView.addSubview(browseButton)
            
            setImage(name: "tabbar_setting_normal", selectedImage: "tabbar_setting_selected", for: localButton)
            localButton.setTitle("TabBarSetting".localized(), for: .normal)
            contentView.addSubview(localButton)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            
            separatorView.frame = CGRect(x: self.bounds.width * 0.5, y: 10, width: STOnePixel(), height: self.bounds.height - 20)
            browseButton.frame = CGRect(x: 0, y: 0, width: self.bounds.width * 0.5, height: self.bounds.height)
            localButton.frame = CGRect(x: self.bounds.width * 0.5, y: 0, width: self.bounds.width * 0.5, height: self.bounds.height)
        }
        
        override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
            return nil
        }
        
        class Button: UIButton {
            
            enum UI {
                static let titleFont = UIFont.systemFont(ofSize: 10)
                
                static let titleHeight: CGFloat = 10
                static let titleImageMargin: CGFloat = 2
                static let imageSize = CGSize(width: 26, height: 26)
            }
            
            override init(frame: CGRect) {
                super.init(frame: frame)
                self.titleLabel?.font = UI.titleFont
                self.titleLabel?.textAlignment = .center
                setTitleColor(UIColor(rgb: 0xBCC1CB), for: .normal)
                setTitleColor(UIColor(rgb: 0x1787fb), for: .selected)
            }
            
            required init?(coder aDecoder: NSCoder) {
                fatalError()
            }
            
            override func titleRect(forContentRect contentRect: CGRect) -> CGRect {
                let topMargin: CGFloat = (contentRect.height - UI.titleHeight - UI.titleImageMargin - UI.imageSize.height) * 0.5
                return CGRect(x: 0, y: topMargin + UI.imageSize.height + UI.titleImageMargin, width: contentRect.width, height: UI.titleHeight)
            }
            
            override func imageRect(forContentRect contentRect: CGRect) -> CGRect {
                let topMargin: CGFloat = (contentRect.height - UI.titleHeight - UI.titleImageMargin - UI.imageSize.height) * 0.5
                return CGRect(x: (contentRect.size.width - UI.imageSize.width) * 0.5, y: topMargin, width: UI.imageSize.width, height: UI.imageSize.height)
            }
        }
    }

}

extension STNavigationController: MainTabBarRefreshable {
    
    func refresh() {
        if let refreshable = self.topViewController as? MainTabBarRefreshable {
            refreshable.refresh()
        }
    }
}
