//
//  URLString.swift
//  VEnjoy
//
//  Created by SunJiangting on 2016/11/26.
//  Copyright © 2016年 Suen. All rights reserved.
//

import UIKit
import GiantShoulder
import Network

extension App {
    
    static let domainPrefixUserDefaultKey = "DomainPrefix"
    static let fetchDomainTimeIntervalUserDefaultKey = "FetchDomainTimeInterval"
    
    private static let minimumFetchDomainInterval: TimeInterval = 6 * 3600
    private static var isFetchingDomains = false
    static public func fetchDomains() {
        if isFetchingDomains {
            return
        }
        let shouldSync: Bool
        if let previousTime = (STPersistence.standard.value(forKey: fetchDomainTimeIntervalUserDefaultKey) as? NSNumber)?.doubleValue, (Date().timeIntervalSince1970 - previousTime) < minimumFetchDomainInterval {
            shouldSync = false
        } else {
            shouldSync = true
        }
        if shouldSync {
            isFetchingDomains = true
            Network.request(.domains).build()?.fv_response(completion: { (_, _, results: Request.APIResult<[String: Any]>) in
                switch results {
                case .value(let dict):
                    if let results = dict, let prefix = results["prefix"] as? String, prefix.lowercased().hasPrefix("http") {
                        STPersistence.standard.setValue(prefix, forKey: domainPrefixUserDefaultKey)
                        STPersistence.standard.setValue(Date().timeIntervalSince1970, forKey: fetchDomainTimeIntervalUserDefaultKey)
                    }
                case .error(_):
                    break
                }
                isFetchingDomains = false
            })
        }
    }

}
struct URLString: RawRepresentable {
    
    typealias RawValue = String
    
    private(set) var rawValue: String
    
    private static let prefix: String = {
        if let prefix = STPersistence.standard.value(forKey: App.domainPrefixUserDefaultKey) as? String, prefix.lowercased().hasPrefix("http") {
            return prefix
        }
        return "\(URLString.scheme)://\(URLString.host)"
    }()
    // private let prefix = "http://127.0.0.1:8000"
    
    init(rawValue: URLString.RawValue) {
        if let url = URL(string: rawValue), !(url.scheme?.isEmpty ?? true) {
            self.rawValue = rawValue
        } else {
            if rawValue.hasPrefix("/") {
                self.rawValue = URLString.prefix.appending(rawValue)
            } else {
                self.rawValue = URLString.prefix.appending("/").appending(rawValue)
            }
        }
    }
    
    
    public static var scheme: String {
        return "https"
    }
    
    public static var host: String {
        return "fancybrowser.herokuapp.com"
    }
    
    public static let login = URLString(rawValue: "/api/v1/user/login")
    public static let accountLogin = URLString(rawValue: "/api/v1/user/account/login")
    public static let suggestionWebsite = URLString(rawValue: "/api/v2/website/suggestion")
    
    public static let register = URLString(rawValue: "/api/v1/user/register")
    public static let logout = URLString(rawValue: "/api/v1/user/logout")
    public static let me = URLString(rawValue: "/api/v1/user/me")
    
    public static let follow = URLString(rawValue: "/api/v1/user/follow")
    public static let unfollow = URLString(rawValue: "/api/v1/user/unfollow")
    public static let popular = URLString(rawValue: "/api/v1/feed/popular")
    
    public static let config = URLString(rawValue: "/api/v1/config")
    public static let submitFeedback = URLString(rawValue: "/api/v1/feedback/submit")
    public static let about = URLString(rawValue: "/web/about.html")
    
    public static let domains = URLString(rawValue: "/api/v1/config/domains/")
    
    public static let privacy = URLString(rawValue: "/web/privacy.html")
    public static let openSource = URLString(rawValue: "/web/open_source.html")
    public static let publishPolicy = URLString(rawValue: "/web/publish/policy.html")
    public static let userAgreement = URLString(rawValue: "/web/user/agreement.html")
    
    public static let rate = URLString(rawValue: "https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=\(AppKeys.appStoreID)&pageNumber=0&sortOrdering=2&type=Purple+Software&mt=8")
}

extension URLString {
    var url: URL? {
        return URL(string: self.rawValue)
    }
}
