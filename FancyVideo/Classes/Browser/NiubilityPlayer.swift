//
//  NiubilityPlayer.swift
//  BiuTube
//
//  Created by SunJiangting on 2017/5/21.
//  Copyright © 2017年 Suen. All rights reserved.
//

import UIKit

class NiubilityPlayer: NSObject {
    
    static let defaultVideoRatio: CGFloat = 9.0 / 16.0
    
    static let shared = NiubilityPlayer()
    
    static var isIntersectsWithStatusBar: Bool {
        return (VideoCenter.shared.player.nextResponder(VideoEmbeddable.self) as? UIView == sharedView) && !sharedView.isMinimize
    }
    
    static var preferredStatusBarStyle: UIStatusBarStyle? {
        if Device.isIPhoneX {
            return sharedView.isMinimize ? nil : UIStatusBarStyle.lightContent
        }
        return nil
    }
    
    static var preferredStatusBarHidden: Bool {
        if Device.isIPhoneX {
            return false
        }
        return !sharedView.isMinimize
    }
    
    private static let sharedView = InnerView(frame: CGRect(x: 0, y: InnerView.UI.topMargin, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width * NiubilityPlayer.defaultVideoRatio))
    
    private override init() {
        super.init()
    }
    
    typealias Video = (url: URL, title: String?)
    
    func present(video: Video, animated: Bool) {
        VideoCenter.shared.play(video.url, title: video.title)
        VideoCenter.shared.embed(in: NiubilityPlayer.sharedView)
        NiubilityPlayer.sharedView.present(animated: animated)
        updateStatusBar()
    }
    
    func dismiss() {
        NiubilityPlayer.sharedView.dismiss()
    }
    
    private class InnerView: UIView, VideoEmbeddable, UIGestureRecognizerDelegate {
        
        enum UI {
            
            static let topMargin: CGFloat = Device.isIPhoneX ? 44 : 0
            static let minimumHorizontalMargin: CGFloat = 10
            static let minimumBottomMargin: CGFloat = 44 + (Device.isIPhoneX ? 44 : 10)
            static let minimumTopMargin: CGFloat = max(20, UI.topMargin)
            static let minimumWidth: CGFloat = UIScreen.main.bounds.width * 0.45
            static let minimumHeight: CGFloat = UI.minimumWidth * NiubilityPlayer.defaultVideoRatio
            
            static func completion(at y: CGFloat) -> CGFloat {
                // y= topMargin, completion = 1
                // y = (UIScreen.main.bounds.height - UI.minimumBottomMargin - UI.minimumHeight) completion = 0
                let completionPercent = 1.0 - ((y - UI.topMargin) / (UIScreen.main.bounds.height - UI.minimumBottomMargin - UI.minimumHeight - UI.topMargin))
                return max(0.0, min(1.0, completionPercent))
            }
            
            static let maximumFrame = CGRect(x: 0, y: UI.topMargin, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width * NiubilityPlayer.defaultVideoRatio)
            
            static let minimumFrame = CGRect(x: (UIScreen.main.bounds.width - UI.minimumWidth - UI.minimumHorizontalMargin), y: UIScreen.main.bounds.height - UI.minimumHeight - UI.minimumBottomMargin, width: UI.minimumWidth, height: UI.minimumHeight)
            
            static func newFrame(at y: CGFloat) -> CGRect {
                let completion = UI.completion(at: y)
                if completion >= 1.0 {
                    return UI.maximumFrame
                }
                if completion <= 0.0 {
                    return UI.minimumFrame
                }
                let widthAtY: CGFloat = (UIScreen.main.bounds.width - UI.minimumWidth) * completion + UI.minimumWidth
                let heightAtY: CGFloat = widthAtY * NiubilityPlayer.defaultVideoRatio
                var newFrame = CGRect(x: 0, y: y, width: widthAtY, height: heightAtY)
                if newFrame.origin.y < UI.topMargin {
                    newFrame.origin.y = UI.topMargin
                }
                if newFrame.origin.y + heightAtY > (UIScreen.main.bounds.height - UI.minimumBottomMargin) {
                    newFrame.origin.y = (UIScreen.main.bounds.height - UI.minimumBottomMargin) - heightAtY
                }
                let rightMarginAffectThreshold: CGFloat = 0.9
                if completion > rightMarginAffectThreshold {
                    newFrame.origin.x = (UIScreen.main.bounds.width - widthAtY)
                } else {
                    newFrame.origin.x = (UIScreen.main.bounds.width - widthAtY) - ((rightMarginAffectThreshold - completion) / rightMarginAffectThreshold * UI.minimumHorizontalMargin)
                }
                return newFrame
            }
        }
        
        private let contentView = UIView(frame: .zero)
        private let collapseButton = UIButton(frame: .zero)
        private let collapseGesture = UIPanGestureRecognizer()
        private let moveGesture = UIPanGestureRecognizer()
        private let expandGesture = UITapGestureRecognizer()
        private let closeButton = UIButton(frame: .zero)
        private var previousCollapseFrame: CGRect?
        fileprivate var isMinimize = false

        private var gestureStartPoint: CGPoint?
        private var viewStartFrame = CGRect.zero
        
        private var frameWhenKeyboardShow: CGRect?
        
        deinit {
            NotificationCenter.default.removeObserver(self)
        }
        
        fileprivate override init(frame: CGRect) {
            super.init(frame: frame)
            self.backgroundColor = UIColor.white
            
            contentView.backgroundColor = UIColor.black
            addSubview(contentView)
            
            collapseButton.setImage(UIImage(named: "niubility_video_collapse"), for: .normal)
            collapseButton.contentEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 2, right: 16)
            collapseButton.addTarget(self, action: #selector(handleCollapse), for: .touchUpInside)
            contentView.addSubview(collapseButton)
            collapseButton.frame = CGRect(x: 0, y: 0, width: 60, height: 46)
            
            closeButton.setImage(UIImage(named: "niubility_video_close"), for: .normal)
            closeButton.addTarget(self, action: #selector(handleDismiss), for: .touchUpInside)
            closeButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 8, right: 8)
            closeButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
            contentView.addSubview(closeButton)
            closeButton.isHidden = true
            
            collapseGesture.delegate = self
            collapseGesture.addTarget(self, action: #selector(handleCollapseGesture(_:)))
            contentView.addGestureRecognizer(collapseGesture)
            
            expandGesture.delegate = self
            expandGesture.numberOfTapsRequired = 1
            expandGesture.numberOfTouchesRequired = 1
            expandGesture.addTarget(self, action: #selector(handleExpand))
            contentView.addGestureRecognizer(expandGesture)
            
            moveGesture.delegate = self
            moveGesture.addTarget(self, action: #selector(handleMoveGesture(_:)))
            contentView.addGestureRecognizer(moveGesture)
            
            NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardShow(_:)), name: .UIKeyboardWillShow, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardHide(_:)), name: .UIKeyboardWillHide, object: nil)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func present(animated: Bool) {
            if self.superview == nil {
                UIApplication.shared.delegate?.window??.addSubview(self)
                collapse(animated: false)
                expand(animated: true)
                return
            }
            if isMinimize {
                expand(animated: true)
            }
        }
        
        func dismiss() {
            collapse(animated: false)
            removeFromSuperview()
            previousCollapseFrame = nil
            updateStatusBar()
        }
        
        func embed(_ player: UIView) {
            VideoCenter.shared.isTitleVisibleWhenNotFullscreen = false
            player.frame = contentView.bounds
            player.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            contentView.insertSubview(player, at: 0)
        }
        
        func didRemovePlayer() {
            removeFromSuperview()
            updateStatusBar()
        }
        
        @objc private func handleKeyboardShow(_ notification: Notification) {
            guard isMinimize else {
                frameWhenKeyboardShow = nil
                return
            }
            guard
                let userInfo = notification.userInfo,
                let endKeyboardFrame = userInfo[UIKeyboardFrameEndUserInfoKey] as? CGRect,
                let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as? TimeInterval,
                let options = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? UInt else {
                    return
            }
            var newFrame = self.frame
            let newY = endKeyboardFrame.origin.y - newFrame.size.height - 10
            if newY > self.frame.maxY {
                frameWhenKeyboardShow = nil
                return
            }
            frameWhenKeyboardShow = self.frame
            newFrame.origin.y = newY
            UIView.animate(withDuration: duration,
                           delay: 0,
                           options: UIViewAnimationOptions(rawValue: options << 16),
                           animations: {
                            self.frame = newFrame
            }) { (finished) in
                self.frame = newFrame
            }
        }
        
        @objc private func handleKeyboardHide(_ notification: Notification) {
            guard isMinimize, let oldFrame = frameWhenKeyboardShow else {
                frameWhenKeyboardShow = nil
                return
            }
            frameWhenKeyboardShow = nil
            guard
                let userInfo = notification.userInfo,
                let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as? TimeInterval,
                let options = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? UInt else {
                    return
            }
            UIView.animate(withDuration: duration,
                           delay: 0,
                           options: UIViewAnimationOptions(rawValue: options << 16),
                           animations: {
                            self.frame = oldFrame
            }) { (finished) in
                self.frame = oldFrame
            }
        }
        
        override func removeFromSuperview() {
            super.removeFromSuperview()
            if VideoCenter.shared.player.nextResponder(VideoEmbeddable.self) as? UIView == self {
                VideoCenter.shared.stop()
                VideoCenter.shared.player.removeFromSuperview()
            }
        }
        
        @objc private func handleCollapseGesture(_ gesture: UIPanGestureRecognizer) {
            guard let superview = self.superview else {
                gestureStartPoint = nil
                return
            }
            let currentPoint = gesture.location(in: superview)
            switch gesture.state {
            case .possible:
                break
            case .failed:
                self.gestureStartPoint = nil
            case .began:
                gestureStartPoint = currentPoint
                viewStartFrame = self.frame
                collapseButton.isHidden = true
                VideoCenter.shared.player.hidesContolForMinimumView = true
            case .changed:
                guard let sp = gestureStartPoint else { return }
                let deltaY = (currentPoint.y - sp.y)
                let newY = viewStartFrame.origin.y + deltaY
                self.frame = UI.newFrame(at: newY)
            case .ended, .cancelled:
                // rollback
                commitShrinkGesture(velocityY: gesture.velocity(in: self).y)
            }
        }
        
        @objc private func commitShrinkGesture(velocityY: CGFloat) {
            let newTargetY = min(max(velocityY * 0.2 + self.frame.origin.y, UI.maximumFrame.origin.y), UI.minimumFrame.origin.y)
            let completion = UI.completion(at: newTargetY)
            // expand or collapse
            if completion > 0.5 {
                expand(animated: true)
            } else {
                collapse(animated: true)
                previousCollapseFrame = UI.minimumFrame
            }
        }
        
        @objc private func handleExpand() {
            expand(animated: true)
        }
        
        private func expand(animated: Bool) {
            let completion: ((Bool)->Void) = { (_) in
                self.frame = UI.maximumFrame
                self.collapseButton.isHidden = false
                self.closeButton.isHidden = true
                self.isMinimize = false
                self.isUserInteractionEnabled = true
                self.layer.borderWidth = 0
                self.layer.borderColor = nil
                VideoCenter.shared.player.hidesContolForMinimumView = false
                VideoCenter.shared.player.isUserInteractionEnabled = true
                updateStatusBar()
            }
            
            if animated {
                self.closeButton.isHidden = true
                self.isUserInteractionEnabled = false
                UIView.animate(withDuration: 0.25, delay: 0.0, options: [.layoutSubviews], animations: {
                    self.frame = UI.maximumFrame
                }, completion: completion)
            } else {
                completion(true)
            }
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            contentView.frame = self.bounds
        }
        
        fileprivate var shouldHideStatusBar: Bool {
            return !isMinimize
        }
        
        @objc private func handleDismiss() {
            dismiss()
        }
        
        @objc private func handleCollapse() {
            collapse(animated: true, targetFrame: previousCollapseFrame ?? UI.minimumFrame)
        }
        
        @objc private func collapse(animated: Bool, targetFrame: CGRect = UI.minimumFrame) {
            let completion: (Bool)->Void = { (_) in
                self.frame = targetFrame
                self.collapseButton.isHidden = true
                self.closeButton.isHidden = false
                self.isMinimize = true
                self.isUserInteractionEnabled = true
                self.layer.borderWidth = 1
                self.layer.borderColor = UIColor.white.cgColor
                VideoCenter.shared.player.hidesContolForMinimumView = true
                VideoCenter.shared.player.isUserInteractionEnabled = false
                updateStatusBar()
            }
            if animated {
                self.isUserInteractionEnabled = false
                self.collapseButton.isHidden = true
                UIView.animate(withDuration: 0.25, delay: 0.0, options: [.layoutSubviews], animations: {
                    self.frame = targetFrame
                }, completion: completion)
            } else {
                completion(true)
            }
            
        }
        
        @objc private func handleMoveGesture(_ gesture: UIPanGestureRecognizer) {
            guard isMinimize else {
                gesture.isEnabled = false
                gesture.isEnabled = true
                return
            }
            guard let superview = self.superview else {
                gestureStartPoint = nil
                return
            }
            let currentPoint = gesture.location(in: superview)
            switch gesture.state {
            case .possible:
                break
            case .failed:
                self.gestureStartPoint = nil
            case .began:
                gestureStartPoint = currentPoint
                viewStartFrame = self.frame
            case .changed:
                guard let sp = gestureStartPoint else { return }
                let deltaX = (currentPoint.x - sp.x)
                let deltaY = currentPoint.y - sp.y
                var newX = viewStartFrame.origin.x + deltaX
                var newY = viewStartFrame.origin.y + deltaY
                let width = self.bounds.size.width
                let height = self.bounds.size.height
                if newX < UI.minimumHorizontalMargin {
                    newX = UI.minimumHorizontalMargin
                }
                if (newX + width) > (UIScreen.main.bounds.width - UI.minimumHorizontalMargin) {
                    newX = UIScreen.main.bounds.width - UI.minimumHorizontalMargin - width
                }
                if newY < UI.minimumTopMargin {
                    newY = UI.minimumTopMargin
                }
                if (newY + height) > (UIScreen.main.bounds.height - UI.minimumBottomMargin) {
                    newY = UIScreen.main.bounds.height - UI.minimumBottomMargin - height
                }
                self.frame = CGRect(x: newX, y: newY, width: width, height: height)
            case .ended, .cancelled:
                // rollback
                self.gestureStartPoint = nil
                // 贴边
                commitMoveGesture()
            }
        }
        
        private func commitMoveGesture() {
            let midX = self.frame.midX
            var newFrame = self.frame
            if midX > UIScreen.main.bounds.width * 0.5 {
                // 贴在右边
                newFrame.origin.x = UIScreen.main.bounds.width - UI.minimumHorizontalMargin - self.bounds.size.width
            } else {
                newFrame.origin.x = UI.minimumHorizontalMargin
            }
            self.isUserInteractionEnabled = false
            UIView.animate(withDuration: 0.25, animations: {
                self.frame = newFrame
            }) { (_) in
                self.frame = newFrame
                self.isUserInteractionEnabled = true
                self.previousCollapseFrame = newFrame
            }
        }
        
        func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
            if [collapseGesture, moveGesture, expandGesture].contains(gestureRecognizer) {
                if gestureRecognizer == collapseGesture {
                    return !isMinimize
                }
                return isMinimize
            }
            return true
        }
    }
}
