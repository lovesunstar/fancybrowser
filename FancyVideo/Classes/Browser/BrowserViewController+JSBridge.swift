//
//  BrowserViewController+JSBridge.swift
//  FancyVideo
//
//  Created by SunJiangting on 2017/6/9.
//  Copyright © 2017年 Samaritan. All rights reserved.
//

import Foundation
import WebKit

extension BrowserViewController {
    
    static let bridgeUserScript: WKUserScript = {
        let bridgeScriptString = try! String(contentsOf: Bundle.main.url(forResource: "bridge.min", withExtension: "js")!, encoding: .utf8)
        return WKUserScript(source: bridgeScriptString, injectionTime: .atDocumentEnd, forMainFrameOnly: false)
    }()
    
    static let videoBridgeUserScript: WKUserScript = {
        let videoBridgeUserScriptString = try! String(contentsOf: Bundle.main.url(forResource: "bridge.video", withExtension: "js")!, encoding: .utf8)
        return WKUserScript(source: videoBridgeUserScriptString, injectionTime: .atDocumentEnd, forMainFrameOnly: false)
    }()
    
    static let videoUserScript: WKUserScript = {
        let videoScriptString = try! String(contentsOf: Bundle.main.url(forResource: "video.uc", withExtension: "js")!, encoding: .utf8)
        return WKUserScript(source: videoScriptString, injectionTime: .atDocumentEnd, forMainFrameOnly: false)
    }()
}
