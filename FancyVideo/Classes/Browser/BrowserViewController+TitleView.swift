//
//  BrowserViewController+TitleView.swift
//  FancyVideo
//
//  Created by SunJiangting on 2017/6/9.
//  Copyright © 2017年 Samaritan. All rights reserved.
//

import UIKit

protocol BrowserTitleViewDelegate: NSObjectProtocol {
    
    func browserTitleViewDidBeginEditing(_ titleView: BrowserViewController.TitleView)
    func browserTitleViewDidEndEditing(_ titleView: BrowserViewController.TitleView)
    func browserTitleViewDidReturn(_ titleView: BrowserViewController.TitleView, with textField: UITextField)
}

extension BrowserViewController {
    
    class TitleView: UIView {
        
        enum UI {
            fileprivate static let displayFont = UIFont.systemFont(ofSize: 16)
            fileprivate static let displayTextColor = UIColor(rgb: 0x222222)
        }
        
        weak var delegate: BrowserTitleViewDelegate?
        
        fileprivate let contentView = UIView(frame: .zero)
        fileprivate let displayBar = DisplayBar(frame: .zero)
        fileprivate let addressBar = AddressBar(frame: .zero)
        
        var isLoading = false {
            didSet {
                if !editing {
                    stopLoadingButton.isHidden = !isLoading
                    reloadButton.isHidden = isLoading
                }
            }
        }
        
        let stopLoadingButton = FancyVideo.Button(frame: .zero)
        let reloadButton = FancyVideo.Button(frame: .zero)
        
        fileprivate(set) var cancelButton = FancyVideo.Button(frame: .zero)
        fileprivate let tapGesture: UITapGestureRecognizer = {
            let tapGesture = UITapGestureRecognizer()
            tapGesture.numberOfTapsRequired = 1
            tapGesture.numberOfTouchesRequired = 1
            return tapGesture
        }()
        
        fileprivate var editing = false
        
        var showsTitleView = true {
            didSet {
                contentView.isHidden = !showsTitleView
            }
        }
        
        var editable: Bool = true {
            didSet {
                tapGesture.isEnabled = editable
            }
        }
        
        var hasOnlySecureContent: Bool = false {
            didSet {
                displayBar.hasOnlySecureContent = hasOnlySecureContent
            }
        }
        
        var isSniffButtonHidden: Bool {
            set {
                displayBar.sniffButton.isHidden = newValue
                displayBar.setNeedsLayout()
            }
            get {
                return displayBar.sniffButton.isHidden
            }
        }
        
        init() {
            super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 44))
            loadSubviews()
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        fileprivate func loadSubviews() {
           
            contentView.backgroundColor = UIColor.white
            contentView.layer.cornerRadius = 4
            contentView.layer.masksToBounds = true
            addSubview(contentView)
            
            tapGesture.addTarget(self, action: #selector(handle(tap:)))
            displayBar.addGestureRecognizer(tapGesture)
            contentView.addSubview(displayBar)
            
            addressBar.textField.delegate = self
            addressBar.textField.addTarget(self, action: #selector(textDidChange(_:)), for: .editingChanged)
            addressBar.textField.addTarget(self, action: #selector(textDidReturn(_:)), for: .editingDidEndOnExit)
            
            addressBar.closeButton.addTarget(self, action: #selector(handle(clearText:)), for: .touchUpInside)
            contentView.addSubview(addressBar)
            
            stopLoadingButton.setImage(UIImage(named: "browser_cancel"), for: UIControlState())
            stopLoadingButton.isHidden = true
            stopLoadingButton.touchInsets = UIEdgeInsets(top: -10, left: -15, bottom: -10, right: -15)
            contentView.addSubview(stopLoadingButton)
            
            reloadButton.isHidden = true
            reloadButton.setImage(UIImage(named: "browser_reload"), for: UIControlState())
            reloadButton.touchInsets = UIEdgeInsets(top: -10, left: -15, bottom: -10, right: -15)
            contentView.addSubview(reloadButton)
            
            cancelButton.setTitle("Cancel".localized(), for: UIControlState())
            cancelButton.setTitleColor(UIColor(rgb: 0x2a90d7), for: .normal)
            cancelButton.sizeToFit()
            var cancelButtonFrame = cancelButton.frame
            cancelButtonFrame.size.width = max(cancelButton.frame.width, 60)
            cancelButton.frame = cancelButtonFrame
            cancelButton.addTarget(self, action: #selector(handle(cancel:)), for: .touchUpInside)
            addSubview(cancelButton)
            
            editable = true
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            
            if editing {
                stopLoadingButton.isHidden = true
                reloadButton.isHidden = true
                
                displayBar.isHidden = true
                addressBar.isHidden = false
                cancelButton.isHidden = false
                cancelButton.sizeToFit()
                
                let cancelButtonWidth = cancelButton.frame.width + 15
                let contentViewWidth = self.bounds.width - cancelButtonWidth - 20
                
                let contentViewHeight: CGFloat = 29
                contentView.frame = CGRect(x: 10, y: (self.bounds.height - contentViewHeight) / 2, width: contentViewWidth, height: contentViewHeight)
                cancelButton.frame = CGRect(x: contentView.frame.maxX, y: 0, width: cancelButtonWidth, height: self.bounds.height)
                addressBar.frame = contentView.bounds
            } else {
                stopLoadingButton.isHidden = !isLoading
                reloadButton.isHidden = isLoading
                
                let contentViewLeft: CGFloat = 10
                displayBar.isHidden = false
                addressBar.isHidden = true
                cancelButton.isHidden = true
                displayBar.frame = .zero
                let contentViewHeight: CGFloat = 29
                let contentViewWidth = self.bounds.width - contentViewLeft - 10
                contentView.frame = CGRect(x: contentViewLeft, y: (self.bounds.height - contentViewHeight) / 2, width: contentViewWidth, height: contentViewHeight)
                displayBar.frame = contentView.bounds
            }
            stopLoadingButton.frame = CGRect(x: contentView.frame.width - 25, y: (contentView.frame.height - 20) / 2, width: 20, height: 20)
            reloadButton.frame = stopLoadingButton.frame
        }
        
        func addSniffTarget(_ target: Any?, action: Selector) {
            displayBar.sniffButton.addTarget(target, action: action, for: .touchUpInside)
        }
        
        func setEditing(_ editing: Bool, animated: Bool) {
            if editing == self.editing {
                return
            }
            self.editing = editing
            setNeedsLayout()
            if editing {
                addressBar.textField.becomeFirstResponder()
            } else {
                addressBar.textField.resignFirstResponder()
            }
        }
        
        @objc fileprivate func handle(cancel cancelButton: UIButton) {
            setEditing(false, animated: true)
        }
        
        @objc fileprivate func handle(tap tapGesture: UITapGestureRecognizer) {
            setEditing(true, animated: true)
        }
        
        @objc fileprivate func handle(clearText: UIButton) {
            addressBar.textField.text = ""
            textDidChange(addressBar.textField)
        }
        
        @objc fileprivate func textDidReturn(_ textField: UITextField) {
            // delegate?.webViewHeaderDidReturn?(self, withTextField: self.textField)
        }
        
        @objc fileprivate func textDidChange(_ textField: UITextField) {
            addressBar.closeButton.isHidden = textField.text?.isEmpty ?? true
        }
    }
}


extension BrowserViewController.TitleView: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        delegate?.browserTitleViewDidBeginEditing(self)
        addressBar.closeButton.isHidden = textField.text?.isEmpty ?? true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        delegate?.browserTitleViewDidEndEditing(self)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "\n" {
            delegate?.browserTitleViewDidReturn(self, with: textField)
            setEditing(false, animated: true)
            return false
        }
        return true
    }
}

extension BrowserViewController.TitleView {
    
    var textField: UITextField {
        return addressBar.textField
    }
    
    fileprivate class AddressBar: UIView {
        
        fileprivate var textFieldBkg = UIView(frame: .zero)
        
        fileprivate var textField = UITextField(frame: .zero)
        
        fileprivate var closeButton = FancyVideo.Button(frame: CGRect(x: 0, y: 0, width: 25, height: 29))
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            loadSubviews()
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        fileprivate func loadSubviews() {
            addSubview(textFieldBkg)
            
            textField.returnKeyType = .go
            textField.font = UI.displayFont
            textField.enablesReturnKeyAutomatically = true
            textField.autocapitalizationType = .none
            textField.autocorrectionType = .no
            textField.textColor = UI.displayTextColor
            textField.tintColor = UIColor(rgb: 0x2a90d7)
            textField.clipsToBounds = true
            textField.autocapitalizationType = .none
            textField.autocorrectionType = .no
            textFieldBkg.addSubview(textField)
            
            closeButton.setImage(UIImage(named: "search_bar_clear"), for: UIControlState())
            addSubview(closeButton)
        }
        
        fileprivate override func layoutSubviews() {
            super.layoutSubviews()
            
            let padding: CGFloat = 10, closeButtonWidth: CGFloat = 30, closeButtonHeight = self.bounds.height
            closeButton.imageEdgeInsets = UIEdgeInsets(top: floor(closeButtonHeight * 0.5 - 10), left: closeButtonWidth - 5 - 20, bottom: ceil(closeButtonHeight * 0.5 - 10), right: 5)
            textFieldBkg.frame = CGRect(x: 10, y: 0, width: self.bounds.width - padding - closeButtonWidth, height: self.bounds.height)
            textField.frame = textFieldBkg.bounds
            closeButton.frame = CGRect(x: textFieldBkg.frame.maxX, y: 0, width: 30, height: closeButtonHeight)
        }
    }
    
    var title: String? {
        set {
            displayBar.titleLabel.text = newValue
            displayBar.setNeedsLayout()
        }
        get {
            return displayBar.titleLabel.text
        }
    }
    
    var titleLabel: UILabel {
        return displayBar.titleLabel
    }
    
    fileprivate class DisplayBar: UIView {
        
        enum DUI {
            static let secureSize: CGFloat = 12
            static let padding: CGFloat = 30
            static let secureTitleMargin: CGFloat = 1.5
        }
        let sniffButton = FancyVideo.Button(frame: .zero)

        let titleLabel = UILabel(frame: .zero)
        
        let securityView = UIImageView(frame: .zero)
        
        var hasOnlySecureContent: Bool = false {
            didSet {
                if hasOnlySecureContent {
                    addSubview(securityView)
                    titleLabel.textColor = UIColor(rgb: 0x169e41)
                } else {
                    titleLabel.textColor = UI.displayTextColor
                    securityView.removeFromSuperview()
                }
                
                setNeedsLayout()
            }
        }
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            loadSubviews()
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        fileprivate func loadSubviews() {
            
            sniffButton.setImage(UIImage(named: "browser_video_sniff"), for: UIControlState())
            sniffButton.touchInsets = UIEdgeInsets(top: -10, left: -10, bottom: -10, right: -10)
            sniffButton.isHidden = true
            addSubview(sniffButton)
            
            securityView.image = UIImage(named: "browser_security")
            
            titleLabel.isUserInteractionEnabled = false
            titleLabel.textAlignment = .left
            titleLabel.font = UI.displayFont
            titleLabel.textColor = UI.displayTextColor
            addSubview(titleLabel)
        }
        
        fileprivate override func layoutSubviews() {
            super.layoutSubviews()
            sniffButton.frame = CGRect(x: 0, y: (self.frame.height - 22) / 2, width: 20, height: 22)
            let leftMargin: CGFloat
            if sniffButton.isHidden {
                leftMargin = 8
            } else {
                leftMargin = DUI.padding
            }
            
            titleLabel.sizeToFit()
            if securityView.superview == nil {
                let totalWidth: CGFloat = titleLabel.bounds.width
                if totalWidth > (self.bounds.width - (leftMargin + DUI.padding))  {
                    titleLabel.frame = CGRect(x: leftMargin, y: 0, width: self.bounds.width - (leftMargin + DUI.padding), height: self.bounds.height)
                } else {
                    titleLabel.frame = CGRect(x: leftMargin + (self.bounds.width - (leftMargin + DUI.padding) - titleLabel.bounds.width) * 0.5, y: 0, width: titleLabel.bounds.width, height: self.bounds.height)
                }
                return
            }
            let totalWidth: CGFloat = titleLabel.bounds.width + DUI.secureSize + DUI.secureTitleMargin
            let securityTopOffset: CGFloat = 1.5
            if totalWidth > (self.bounds.width - (leftMargin + DUI.padding))  {
                securityView.frame = CGRect(x: leftMargin, y: (self.bounds.height - DUI.secureSize) * 0.5 + securityTopOffset, width: DUI.secureSize, height: DUI.secureSize)
            } else {
                let leftPadding: CGFloat = leftMargin + (self.bounds.width - DUI.padding - leftMargin - totalWidth) * 0.5
                securityView.frame = CGRect(x: leftPadding, y: (self.bounds.height - DUI.secureSize) * 0.5 + securityTopOffset, width: DUI.secureSize, height: DUI.secureSize)
            }
            let titleLabelLeft = securityView.frame.maxX + DUI.secureTitleMargin
            titleLabel.frame = CGRect(x: titleLabelLeft, y: 0, width: self.bounds.width - DUI.padding - titleLabelLeft, height: self.bounds.height)
            
        }
    }
}
