//
//  BrowserViewController.swift
//  FancyVideo
//
//  Created by SunJiangting on 2017/6/9.
//  Copyright © 2017年 Samaritan. All rights reserved.
//

import UIKit
import WebKit
import GiantShoulder
import WKBridge

class FavoriteTextFieldDelegation: NSObject, UITextFieldDelegate {
    static let shared = FavoriteTextFieldDelegation()
    
    static let maximumFavoriteName: Int = 30
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.selectedTextRange = textField.textRange(from: textField.beginningOfDocument, to: textField.endOfDocument)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        return (newLength <= FavoriteTextFieldDelegation.maximumFavoriteName) || newLength < text.count
    }
}

class BrowserViewController: BaseViewController {
    
    enum Configuration {
        static let timeoutInterval: TimeInterval = 30
        
        static let clearContentURLString = "about:blank?clear_by_fancy=1"
        
        static let ignoreWebKitErrorCodes: [Int] = [102, 204]
    }
    
    fileprivate(set) lazy var webView: WKWebView = {
        let configuration = WKWebViewConfiguration()
        configuration.userContentController.addUserScript(BrowserViewController.bridgeUserScript)
        configuration.userContentController.addUserScript(BrowserViewController.videoBridgeUserScript)
        configuration.userContentController.addUserScript(BrowserViewController.videoUserScript)
        configuration.allowsInlineMediaPlayback = false
        configuration.mediaTypesRequiringUserActionForPlayback = .video
        let webView = WKWebView(frame: .zero, configuration: configuration)
        webView.allowsBackForwardNavigationGestures = true
        webView.allowsLinkPreview = true
        webView.scrollView.clipsToBounds = false
        if #available(iOS 11.0, *) {
            webView.scrollView.contentInsetAdjustmentBehavior = .never
        }
        return webView
    }()
    
    fileprivate lazy var titleBar = STNavigationBar()
    fileprivate(set) lazy var titleView = TitleView()
    fileprivate lazy var toolbar = Toolbar(frame: .zero)
    fileprivate lazy var progressView = UIView()
    
    fileprivate let originURL: URL
    fileprivate var sniffVideoURL: URL?
    fileprivate var playingURL: URL?
    
    fileprivate lazy var placeholderView = PlaceholderView()
    
    var isBeingPreviewed = false
    fileprivate var observerContext = 0
    fileprivate weak var loadingNavigation: WKNavigation?
    
    deinit {
        if isViewLoaded {
            webView.removeObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress))
            webView.removeObserver(self, forKeyPath: #keyPath(WKWebView.url))
            webView.removeObserver(self, forKeyPath: #keyPath(WKWebView.title))
            webView.removeObserver(self, forKeyPath: #keyPath(WKWebView.isLoading))
            webView.removeObserver(self, forKeyPath: #keyPath(WKWebView.hasOnlySecureContent))
            webView.removeObserver(self, forKeyPath: #keyPath(WKWebView.backForwardList))
            webView.removeObserver(self, forKeyPath: #keyPath(WKWebView.canGoBack))
            webView.removeObserver(self, forKeyPath: #keyPath(WKWebView.canGoForward))
            webView.scrollView.removeObserver(self, forKeyPath: #keyPath(UIScrollView.contentInset))
        }
        NotificationCenter.default.removeObserver(self)
    }
    
    init(url: URL) {
        originURL = url
        super.init(nibName: nil, bundle: nil)
        self.hidesBottomBarWhenPushed = true
        self.extendedLayoutIncludesOpaqueBars = false
        self.edgesForExtendedLayout = UIRectEdge(rawValue: 0)
        self.automaticallyAdjustsScrollViewInsets = false
        self.st_automaticallyDisableBarBlurEffect = false
        self.st_navigationBarHidden = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.clipsToBounds = true
        
        track("Open URL", properties: ["URL": originURL.absoluteString, "URL Host": originURL.host ?? originURL.absoluteString])
        
        titleBar.barTintColor = UIColor.white.withAlphaComponent(0.88)
        titleBar.blurEffect = UIBlurEffect(style: .extraLight)
        titleBar.separatorView.backgroundColor = UIColor.st_color(rgb: 0xDDDDDD)
        titleBar.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 44)
        self.view.addSubview(titleBar)
        
        titleView.delegate = self
        titleView.addSniffTarget(self, action: #selector(handleSniffPlay))
        titleView.stopLoadingButton.addTarget(self, action: #selector(handleStopLoading), for: .touchUpInside)
        titleView.reloadButton.addTarget(self, action: #selector(handleReload), for: .touchUpInside)
        titleView.frame = titleBar.contentView.bounds
        titleView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        titleBar.contentView.addSubview(titleView)
        
        webView.uiDelegate = self
        webView.navigationDelegate = self
        self.view.addSubview(webView)
        
        placeholderView.backgroundColor = UIColor.white
        placeholderView.contentOffset = CGPoint(x: 0, y: -40)
        
        toolbar.frame = CGRect(x: 0, y: self.view.bounds.height - Toolbar.UI.height, width: self.view.bounds.width, height: Toolbar.UI.height)
        toolbar.backButton.addTarget(self, action: #selector(handleWebViewBack), for: .touchUpInside)
        let backLongPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleBackLongPress(_:)))
        toolbar.backButton.addGestureRecognizer(backLongPressGesture)
        
        toolbar.forwardButton.addTarget(self, action: #selector(handleWebViewForward), for: .touchUpInside)
        let forwardLongPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleForwardLongPress(_:)))
        toolbar.forwardButton.addGestureRecognizer(forwardLongPressGesture)
        
        toolbar.snapshotButton.addTarget(self, action: #selector(handleSnapshot), for: .touchUpInside)
        toolbar.favoriteButton.addTarget(self, action: #selector(handleFavorite), for: .touchUpInside)
        toolbar.shareButton.addTarget(self, action: #selector(handleMoreAction), for: .touchUpInside)
        toolbar.exposeButton.addTarget(self, action: #selector(handleExpose), for: .touchUpInside)
        self.view.addSubview(toolbar)
        self.view.bringSubview(toFront: titleBar)

        var webViewFrame = self.view.bounds
        webViewFrame.origin.y = titleBar.frame.maxY
        webViewFrame.size.height = self.view.bounds.height - toolbar.frame.size.height - titleBar.frame.maxY
        webView.frame = webViewFrame
        
        progressView.frame = CGRect(x: 0, y: 0, width: webView.bounds.width, height: 2)
        progressView.backgroundColor = UIColor.red
        webView.addSubview(progressView)
        
        webView.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: [.new], context: &observerContext)
        webView.addObserver(self, forKeyPath: #keyPath(WKWebView.url), options: [.new], context: &observerContext)
        webView.addObserver(self, forKeyPath: #keyPath(WKWebView.title), options: [.new], context: &observerContext)
        webView.addObserver(self, forKeyPath: #keyPath(WKWebView.isLoading), options: [.new], context: &observerContext)
        webView.addObserver(self, forKeyPath: #keyPath(WKWebView.hasOnlySecureContent), options: [.new], context: &observerContext)
        webView.addObserver(self, forKeyPath: #keyPath(WKWebView.backForwardList), options: [.new], context: &observerContext)
        webView.addObserver(self, forKeyPath: #keyPath(WKWebView.canGoBack), options: [.new], context: &observerContext)
        webView.addObserver(self, forKeyPath: #keyPath(WKWebView.canGoForward), options: [.new], context: &observerContext)
        webView.scrollView.addObserver(self, forKeyPath: #keyPath(UIScrollView.contentInset), options: [.new], context: &observerContext)
        
        webView.load(URLRequest(url: originURL))
        updateTitle(with: originURL, title: nil)
        
        webView.bridge.register({ [weak self] (parameters, _) in
            guard let strongSelf = self else { return }
            guard let parameters = parameters else { return }
            print(parameters)
            guard let url = parseVideoURL(parameters) else {
                return
            }
            strongSelf.playVideo(url)
        }, for: "video.play")
        
        webView.bridge.register({ [weak self] (parameters, _) in
            guard let strongSelf = self else { return }
            guard let parameters = parameters else { return }
            print(parameters)
            guard let url = parseVideoURL(parameters) else {
                return
            }
            strongSelf.playVideo(url)
        }, for: "video.load")
        
        webView.bridge.register({ [weak self] (parameters, _) in
            guard let strongSelf = self else { return }
            guard let parameters = parameters else {
                return
            }
            print(parameters)
            guard let url = parseVideoURL(parameters) else {
                strongSelf.titleView.isSniffButtonHidden = true
                return
            }
            strongSelf.sniffVideoURL = url
            strongSelf.titleView.isSniffButtonHidden = false
        }, for: "video.detected")
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleVideoEnd(_:)), name: .videoPlayerDidFinishPlay, object: nil)
        DispatchQueue.main.asyncAfter(delay: 5.0) {
            self.showMoreGuideIfNeeded()
        }
    }
    
    func stopLoading() {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(handleTimeOut), object: nil)
        webView.stopLoading()
    }
    
    fileprivate func updateTitle(with url: URL, title: String?) {
        if url.isBlankURL {
            titleView.title = ""
            titleView.textField.text = ""
            return
        }
        titleView.title = (title?.isEmpty ?? true) ? url.host : title
        if !titleView.textField.isFirstResponder {
            titleView.textField.text = url.absoluteString
        }
    }
    
    @objc private func handleBackLongPress(_ longPressGesture: UILongPressGestureRecognizer) {
        guard longPressGesture.state == .changed else {
            return
        }
        longPressGesture.isEnabled = false
        longPressGesture.isEnabled = true
        handleBackForwardItems(webView.backForwardList.backList.reversed())
    }
    
    @objc private func handleForwardLongPress(_ longPressGesture: UILongPressGestureRecognizer) {
        guard longPressGesture.state == .changed else {
            return
        }
        longPressGesture.isEnabled = false
        longPressGesture.isEnabled = true
        handleBackForwardItems(webView.backForwardList.forwardList)
    }
    
    private func handleBackForwardItems(_ items: [WKBackForwardListItem]) {
        guard !items.isEmpty else {
            return
        }
        let vc = BrowserBackForwadListViewController(items: items) { [weak self] (_, item) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.webView.go(to: item)
        }
        present(BaseNavigationController(rootViewController: vc), animated: true, completion: nil)
    }
    
    fileprivate func reloadBackForwardState() {
        toolbar.backButton.isEnabled = webView.canGoBack
        toolbar.forwardButton.isEnabled = webView.canGoForward
        
        if webView.canGoBack {
            self.st_maximumInteractivePopEdgeDistance = 0
        } else {
            self.st_maximumInteractivePopEdgeDistance = 20
        }
    }
    
    fileprivate func reloadToolbarState() {
        if let url = webView.url, !url.isBlankURL {
            updateTitle(with: url, title: webView.title)
            toolbar.favoriteButton.isSelected = LocalURLStorage.favorite.exists(url)
            toolbar.favoriteButton.isEnabled = true
        } else {
            toolbar.favoriteButton.isSelected = false
            toolbar.favoriteButton.isEnabled = false
        }
        webView.evaluateJavaScript("window.UC_VIDEO_findVideoURL()") { [weak self] (parameters, error) in
            guard let strongSelf = self else {
                return
            }
            guard let parameters = parameters as? [String: Any] else {
                return
            }
            guard let url = parseVideoURL(parameters) else {
                strongSelf.sniffVideoURL = nil
                strongSelf.titleView.isSniffButtonHidden = true
                return
            }
            strongSelf.sniffVideoURL = url
            strongSelf.titleView.isSniffButtonHidden = false
            if url.absoluteString == strongSelf.webView.url?.absoluteString {
                // 如果播放的就是本身的视频，则进行一些处理
                strongSelf.webView.stopLoading()
                if strongSelf.webView.canGoBack {
                   strongSelf.webView.goBack()
                } else {
                    strongSelf.closeTap()
                }
                strongSelf.playVideo(url)
            }
        }
    }
    
    @objc private func handleSnapshot() {
        guard !webView.isCapturing else {
            TopTipView.showTip("WebViewIsCapturingNow".localized(), in: self.view, duration: 2.0, offsetY: 1)
            return
        }
        toolbar.snapshotButton.startAnimating()
        toolbar.snapshotButton.isEnabled = false
        webView.fv_takeSnapshot { [weak self] (image) in
            guard let strongSelf = self else { return }
            strongSelf.toolbar.snapshotButton.stopAnimating()
            strongSelf.toolbar.snapshotButton.isEnabled = true
            guard let image = image else {
                TopTipView.showTip("WebViewCaptureFailed".localized(), in: strongSelf.view, duration: 2.0, offsetY: 1)
                return
            }
            let previewViewContoller = ImagePreviewViewController(image: image)
            strongSelf.present(BaseNavigationController(rootViewController: previewViewContoller), animated: true, completion: nil)
        }
    }
    
    private func showMoreGuideIfNeeded() {
        guard let moreSuperview = toolbar.shareButton.superview else {
            return
        }
        if UIGuideManager.shared.hasShow(.browserMoreAction) {
            return
        }
        UIGuideManager.shared.setShow(true, for: .browserMoreAction)
        let guideView = TextArrowGuideView(direction: .down, text: "GuideTipBrowserAddToTop".localized())
        var pointCenter = moreSuperview.convert(toolbar.shareButton.center, to: self.view)
        pointCenter.y -= (toolbar.shareButton.frame.height * 0.5 + 5)
        guideView.show(in: self.view, center: pointCenter)
    }
    
    @objc private func handleFavorite() {
        guard let url = webView.url else {
            return
        }
        if toolbar.favoriteButton.isSelected {
            if LocalURLStorage.favorite.exists(url) {
                // 取消
                let alertController = UIAlertController(title: "WebSiteRemoveFavoriteTitle".localized(), message: nil, preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: nil))
                let confirmAction = UIAlertAction(title: "WebSiteRemoveFavoriteOK".localized(), style: .default, handler: { [weak self] (_) in
                    guard let strongSelf = self else {
                         return
                    }
                    LocalURLStorage.favorite.remove(url)
                    strongSelf.toolbar.favoriteButton.isSelected = false
                })
                alertController.addAction(confirmAction)
                present(alertController, animated: true, completion: nil)
            } else {
                toolbar.favoriteButton.isSelected = false
            }
        } else {
            if !LocalURLStorage.favorite.exists(url) {
                let alertController = UIAlertController(title: "WebSiteAddFavoriteTitle".localized(), message: nil, preferredStyle: .alert)
                alertController.addTextField(configurationHandler: { [weak self] (textField) in
                    textField.text = self?.webView.title
                    textField.delegate = FavoriteTextFieldDelegation.shared
                })
                alertController.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: nil))
                let confirmAction = UIAlertAction(title: "WebSiteAddFavoriteOK".localized(), style: .default, handler: { [weak self] (_) in
                    guard let strongSelf = self else {
                        return
                    }
                    guard let textField = alertController.textFields?.first else {
                        return
                    }
                    let name = textField.text ?? strongSelf.webView.title ?? "未命名"
                    LocalURLStorage.favorite.save(url, name: name)
                    track("Add To Favorite", properties: ["URL": url.absoluteString, "Name": name, "URL Host": url.host ?? url.absoluteString])
                    strongSelf.toolbar.favoriteButton.isSelected = true
                })
                alertController.addAction(confirmAction)
                present(alertController, animated: true, completion: nil)
            } else {
                toolbar.favoriteButton.isSelected = true
            }
        }
    }
    
    @objc private func handleExpose() {
        self.st_safariController?.expose(animated: true)
    }
    
    @objc private func handleMoreAction() {
        guard let url = webView.url else {
            handleShare()
            return
        }
        
        let alertController = UIAlertController(title: "BrowserMoreActionTitle".localized(), message: nil, preferredStyle: .actionSheet)
        
        let placeAtTopAction = UIAlertAction(title: "PlaceAtTop".localized(), style: .destructive, handler: { [weak self] (_) in
            self?.placeURLAtTop(url)
        })
        alertController.addAction(placeAtTopAction)
        
        let openInSafariAction = UIAlertAction(title: "OpenInSafari".localized(), style: .default, handler: { (_) in
            UIApplication.shared.open(url, options: [:]) { (_) in
                
            }
        })
        alertController.addAction(openInSafariAction)
        
        let shareAction = UIAlertAction(title: "ShareUsingSystem".localized(), style: .default, handler: { [weak self] (_) in
            self?.handleShare()
        })
        alertController.addAction(shareAction)
        
        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
        
    @objc private func handleShare() {
        var items = [Any]()
        var appActivities = [UIActivity]()
        if let title = webView.title {
            items.append(title)
        }
        if let url = webView.url {
            items.append(url)
            if !LocalURLStorage.top.exists(url) {
                let placeAtTopActivity = PlaceAtTopActivity { [weak self] (_) in
                    self?.placeURLAtTop(url)
                }
                appActivities.append(placeAtTopActivity)
            }
            appActivities.append(OpenInSafariActivity(url: url))
        }
        if items.isEmpty {
            // 稍后分享
            return
        }
        
        let activityContoller = UIActivityViewController(activityItems: items, applicationActivities: appActivities)
        present(activityContoller, animated: true, completion: nil)
    }
    
    fileprivate func playVideo(_ url: URL) {
        self.playingURL = url
        NiubilityPlayer.shared.present(video: (url: url, title: webView.title), animated: true)
    }
    
    @objc private func handleSniffPlay() {
        guard let url = sniffVideoURL else {
            return
        }
        playVideo(url)
    }
    
    @objc private func handleReload() {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(handleTimeOut), object: nil)
        if let url = webView.url {
            if !url.isBlankURL {
                webView.reload()
            } else {
                
            }
        } else {
            webView.load(URLRequest(url: originURL))
        }
    }
    
    @objc private func handleStopLoading() {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(handleTimeOut), object: nil)
        webView.stopLoading()
    }
    
    @objc private func handleWebViewBack() {
        if webView.canGoBack {
            webView.goBack()
        }
    }
    
    @objc private func handleWebViewForward() {
        if webView.canGoForward {
            webView.goForward()
        }
    }
    
    private func clearWebViewContent() {
        webView.evaluateJavaScript("$('html').remove()", completionHandler: nil)
    }
    
    @objc fileprivate func handleTimeOut() {
        let urlString = webView.url?.absoluteString ?? originURL.absoluteString
        handleError(NSError(domain: FancyVideo.errorDomain, code: URLError.Code.timedOut.rawValue, errorDescription: "CannotOpenThisURL".localized([urlString])))
    }
    
    private func handleError(_ error: Error) {
        let nsError = error as NSError
        if nsError.code == URLError.cancelled.rawValue {
            return
        }
        if nsError.domain == "WebKitErrorDomain" && (Configuration.ignoreWebKitErrorCodes.contains(nsError.code)) {
            return
        }
        clearWebViewContent()
        var progressFrame = progressView.frame
        progressFrame.size.width = 0
        progressView.frame = progressFrame
        placeholderView.state = .case(.error, error.displayDescription ?? error.localizedDescription, nil)
        webView.addSubview(placeholderView)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let topSafeArea: CGFloat
        let bottomSafeArea: CGFloat
        if isSafariZoomed || isBeingPreviewed {
            topSafeArea = 0
            bottomSafeArea = 0
        } else {
            if #available(iOS 11.0, *) {
                let safeAreaInsets = (self.view.window ?? self.view).safeAreaInsets
                topSafeArea = max(safeAreaInsets.top, 20)
                bottomSafeArea = safeAreaInsets.bottom
            } else {
                topSafeArea = 20
                bottomSafeArea = 0
            }
        }
        titleBar.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 44 + topSafeArea)
        toolbar.frame = CGRect(x: 0, y: self.view.bounds.height - Toolbar.UI.height - bottomSafeArea, width: self.view.bounds.width, height: Toolbar.UI.height + bottomSafeArea)
        var webViewFrame = self.view.bounds
        webViewFrame.origin.y = titleBar.frame.maxY
        webViewFrame.size.height = self.view.bounds.height - toolbar.frame.size.height - titleBar.frame.maxY
        webView.frame = webViewFrame
        placeholderView.frame = webView.bounds
    }
    
    @available(iOS 11.0, *)
    override func viewSafeAreaInsetsDidChange() {
        super.viewSafeAreaInsetsDidChange()
        self.view.setNeedsLayout()
    }
    
    @objc private func handleVideoEnd(_ notification: Notification) {
        guard let url = notification.userInfo?["url"] as? URL else {
            return
        }
        guard url == self.playingURL else {
            return
        }
        //
        webView.evaluateJavaScript("window.UC_VIDEO_Playback_Event({'vEvent':'ended'})", completionHandler: nil)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if context == &observerContext {
            guard let keyPath = keyPath else {
                return
            }
            if keyPath == #keyPath(WKWebView.estimatedProgress) {
                UIView.animate(withDuration: 0.1, animations: {
                    var progressFrame = self.progressView.frame
                    progressFrame.origin.y = 0
                    progressFrame.size.width = CGFloat(Double(self.webView.bounds.width) * self.webView.estimatedProgress)
                    self.progressView.frame = progressFrame
                })
                progressView.isHidden = (webView.estimatedProgress == 1.0)
                if progressView.superview == self.webView {
                    webView.bringSubview(toFront: progressView)
                }
            } else if let scrollView = object as? UIScrollView, keyPath == #keyPath(WKWebView.scrollView.contentInset) && scrollView == webView.scrollView {
                if progressView.superview == self.webView {
                    let progressFrame = progressView.frame
                    self.progressView.frame = progressFrame
                }
            } else if keyPath == #keyPath(WKWebView.url), let url = webView.url {
                updateTitle(with: url, title: webView.title)
            } else if keyPath == #keyPath(WKWebView.title) {
                if let url = webView.url, let title = webView.title, !url.isBlankURL {
                    LocalURLStorage.history.save(url, name: title)
                    updateTitle(with: url, title: title)
                }
            } else if keyPath == #keyPath(WKWebView.hasOnlySecureContent) {
                titleView.hasOnlySecureContent = webView.hasOnlySecureContent
            } else if keyPath == #keyPath(WKWebView.backForwardList) || keyPath == #keyPath(WKWebView.canGoBack) || keyPath == #keyPath(WKWebView.canGoForward) {
                reloadBackForwardState()
            } else if keyPath == #keyPath(WKWebView.isLoading) {
                titleView.isLoading = webView.isLoading
            }
        } else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }
    
    override var previewActionItems: [UIPreviewActionItem] {
        return [UIPreviewAction.init(title: "OpenInSafari".localized(), style: .default, handler: { [weak self] (_, _) in
            guard let strongSelf = self else {
                return
            }
            UIApplication.shared.open(strongSelf.originURL, options: [:], completionHandler: nil)
        })]
    }
    
    fileprivate func presentOpenAppAlertIfNeeded(_ url: URL) {
        
        let alertController = UIAlertController(title: "WebViewOpenAppAlertTitle".localized(), message: "WebViewOpenAppAlertMessage".localized(), preferredStyle: .alert)
        
        let continueAction = UIAlertAction(title: "WebViewOpenAppConfirm".localized(), style: .default, handler: { (_) in
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        })
        alertController.addAction(continueAction)
        
        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    private func placeURLAtTop(_ url: URL) {
        let alertController = UIAlertController(title: "WebSitePlaceAtTopTitle".localized(), message: nil, preferredStyle: .alert)
        alertController.addTextField(configurationHandler: { [weak self] (textField) in
            textField.text = self?.webView.title
            textField.delegate = FavoriteTextFieldDelegation.shared
        })
        alertController.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: nil))
        let confirmAction = UIAlertAction(title: "WebSitePlaceAtConfirm".localized(), style: .default, handler: { [weak self] (_) in
            guard let strongSelf = self else {
                return
            }
            guard let textField = alertController.textFields?.first else {
                return
            }
            let name = textField.text ?? strongSelf.webView.title ?? "未命名"
            track("Place At Top", properties: ["URL": url.absoluteString, "Name": name, "URL Host": url.host ?? url.absoluteString])
            LocalURLStorage.top.save(url, name: name)
        })
        alertController.addAction(confirmAction)
        present(alertController, animated: true, completion: nil)
    }
    
    fileprivate func openNewTap(_ url: URL) {
        let viewController = BrowserViewController(url: url)
        self.st_safariController?.show(viewController, animated: true)
    }
    
    fileprivate func closeTap() {
        self.st_safariController?.close(self, animated: true)
    }
}

extension BrowserViewController: BrowserTitleViewDelegate {
    
    func browserTitleViewDidBeginEditing(_ titleView: BrowserViewController.TitleView) {
        let textField = titleView.textField
        if !(textField.text?.isEmpty ?? true) {
            textField.selectAll(nil)
        }
    }
    
    func browserTitleViewDidEndEditing(_ titleView: BrowserViewController.TitleView) {
        
    }
    
    func browserTitleViewDidReturn(_ titleView: BrowserViewController.TitleView, with textField: UITextField) {
        guard let text = textField.text, let url = makeFancyURL(text) else {
            TopTipView.showTip("InvalidInputURL".localized(), in: self.view, duration: 2.0, offsetY: (self.st_navigationBar?.frame.maxY ?? 20) + 1)
            return
        }
        textField.resignFirstResponder()
        webView.load(URLRequest(url: url))
    }
}

extension BrowserViewController: WKUIDelegate {
    
    func webView(_ webView: WKWebView, shouldPreviewElement elementInfo: WKPreviewElementInfo) -> Bool {
        guard let url = elementInfo.linkURL else {
            return false
        }
        return !url.isBlankURL && url.webViewCanOpen
    }
    
    func webView(_ webView: WKWebView, commitPreviewingViewController previewingViewController: UIViewController) {
        if let bvc = previewingViewController as? BrowserViewController {
            bvc.isBeingPreviewed = false
            bvc.view.setNeedsLayout()
        }
        self.st_safariController?.show(previewingViewController, animated: false)
    }
    
    func webView(_ webView: WKWebView, previewingViewControllerForElement elementInfo: WKPreviewElementInfo, defaultActions previewActions: [WKPreviewActionItem]) -> UIViewController? {
        guard let url = elementInfo.linkURL, url.webViewCanOpen else {
            return nil
        }
        let vc = BrowserViewController(url: url)
        vc.isBeingPreviewed = true
        return vc
    }
    
    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping () -> Void) {
        let alertController = UIAlertController(title: webView.title, message: message, preferredStyle: .alert)
        let confirmAction = UIAlertAction(title: "OK".localized(), style: .default) { (action) in
            completionHandler()
        }
        alertController.addAction(confirmAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func webView(_ webView: WKWebView, runJavaScriptConfirmPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (Bool) -> Void) {
        let alertController = UIAlertController(title: webView.title, message: message, preferredStyle: .alert)
        let confirmAction = UIAlertAction(title: "OK".localized(), style: .default) { (_) in
            completionHandler(true)
        }
        alertController.addAction(confirmAction)
        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .cancel) { (_) in
            completionHandler(false)
        }
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func webView(_ webView: WKWebView, runJavaScriptTextInputPanelWithPrompt prompt: String, defaultText: String?, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (String?) -> Void) {
        let alertController = UIAlertController(title: webView.title, message: prompt, preferredStyle: .alert)
        
        alertController.addTextField { (textField) in
            textField.text = defaultText
        }
        let confirmAction = UIAlertAction(title: "OK".localized(), style: .default) { (_) in
            completionHandler(alertController.textFields?.first?.text)
        }
        alertController.addAction(confirmAction)
        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .cancel) { (_) in
            completionHandler(nil)
        }
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        guard let url = navigationAction.request.url else {
            return nil
        }
        if url.isBlankURL {
            return nil
        }
        if !url.webViewCanOpen {
            // Alert
            presentOpenAppAlertIfNeeded(url)
            return nil
        }
        openNewTap(url)
        return nil
    }
    
    func webViewDidClose(_ webView: WKWebView) {
        closeTap()
    }
}

extension BrowserViewController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        guard let url = navigationAction.request.url else {
            decisionHandler(.cancel)
            return
        }
        if url.isBlankURL {
            if url.absoluteString.lowercased().contains(Configuration.clearContentURLString) {
                decisionHandler(.allow)
                return
            }
            decisionHandler(.cancel)
            return
        }
        if !url.webViewCanOpen {
            presentOpenAppAlertIfNeeded(url)
            decisionHandler(.cancel)
            return
        }
        if navigationAction.targetFrame == nil {
            decisionHandler(.cancel)
            if url.scheme?.lowercased().hasPrefix("http") ?? false {
                openNewTap(url)
                return
            }
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            decisionHandler(.allow)
        }
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        if let URL = webView.url, let response = navigationResponse.response as? HTTPURLResponse {
            if let allHeaderFields = response.allHeaderFields as? [String: String] {
                let cookies = HTTPCookie.cookies(withResponseHeaderFields: allHeaderFields, for: URL)
                cookies.forEach({HTTPCookieStorage.shared.setCookie($0)})
            }
        }
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(handleTimeOut), object: nil)
        decisionHandler(.allow)
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        if let url = webView.url, url.isBlankURL {
            toolbar.snapshotButton.isEnabled = false
            return
        }
        webView.bringSubview(toFront: progressView)
        loadingNavigation = navigation
        perform(#selector(handleTimeOut), with: nil, afterDelay: Configuration.timeoutInterval)
        placeholderView.removeFromSuperview()
        toolbar.snapshotButton.isEnabled = false
    }
    
    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
        // navigation?.numberOfRedirectTimes += 1
        // let times = navigation?.numberOfRedirectTimes ?? 0
        // if times > 10 {
            // 10次重定向
        //    print("The url has too many redirects")
        // }
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(handleTimeOut), object: nil)
        handleError(error)
        reloadToolbarState()
    }
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webView.scrollView.contentInset = .zero
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(handleTimeOut), object: nil)
        webView.isOpaque = true
        reloadToolbarState()
        if let url = webView.url {
            if let title = webView.title, !url.isBlankURL {
                LocalURLStorage.history.save(url, name: title)
            }
            toolbar.snapshotButton.isEnabled = !url.isBlankURL
        }
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(handleTimeOut), object: nil)
        if webView.scrollView.contentSize.height == 0 && !webView.isLoading && webView.estimatedProgress == 1 {
            handleError(error)
        }
        reloadToolbarState()
    }
    
    func webView(_ webView: WKWebView, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        if let serverTrust = challenge.protectionSpace.serverTrust {
            completionHandler(.useCredential, URLCredential(trust: serverTrust))
        } else {
            completionHandler(.performDefaultHandling, nil)
        }
    }
    
    /*! @abstract Invoked when the web view's web content process is terminated.
     @param webView The web view whose underlying web content process was terminated.
     */
    @available(iOS 9.0, *)
    func webViewWebContentProcessDidTerminate(_ webView: WKWebView) {
        webView.reload()
    }
}

extension WKNavigation {
    
    fileprivate struct STPrivateAssociatedKey {
        static var numberOfRedirectTimesKey = "numberOfRedirectTimesKey"
    }
    
    var numberOfRedirectTimes: Int {
        set {
            objc_setAssociatedObject(self, &STPrivateAssociatedKey.numberOfRedirectTimesKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
        get {
            return objc_getAssociatedObject(self, &STPrivateAssociatedKey.numberOfRedirectTimesKey) as? Int ?? 0
        }
    }
}
