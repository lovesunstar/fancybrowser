//
//  LocalURLStorage.swift
//  FancyVideo
//
//  Created by SunJiangting on 2018/2/28.
//  Copyright © 2018年 Samaritan. All rights reserved.
//

import UIKit
import GiantShoulder

class URLStorageItem {
    
    let url: URL
    let name: String
    let date: Date
    let extra: [String: Any]?
    
    init?(dictionary: [String: Any]) {
        guard
            let urlString = dictionary["url"] as? String, !urlString.isEmpty, let url = URL(string: urlString),
            let name = dictionary["name"] as? String else {
                return nil
        }
        self.url = url
        self.name = name
        if let timeInterval = (dictionary["date"] as? NSNumber)?.doubleValue, timeInterval > 0 {
            self.date = Date(timeIntervalSince1970: timeInterval)
        } else {
            self.date = Date()
        }
        extra = dictionary["extra"] as? [String: Any] ?? dictionary
    }
    
    init(url: URL, name: String) {
        self.url = url
        self.name = name
        self.date = Date()
        self.extra = nil
    }
    
    var dictValue: [String: Any] {
        var dict: [String: Any] = ["url": url.absoluteString, "name": name, "date": date.timeIntervalSince1970]
        dict["extra"] = extra
        return dict
    }
}

class LocalURLStorage: NSObject {
    
    static let history = LocalURLStorage(name: "History")
    static let top = LocalURLStorage(name: "Top")
    static let favorite = LocalURLStorage(name: "Favorite")
    static let suggestion = LocalURLStorage(name: "Suggestion")
    
    private(set) var allItems: [URLStorageItem]
    
    init(name: String) {
        persistence = STPersistence(named: name)
        allItems = (persistence.value(forKey: "Items") as? [[String: Any]])?.compactMap({URLStorageItem(dictionary: $0)}) ?? []
        super.init()
    }
    
    func save(_ url: URL, name: String) {
        let item = URLStorageItem(url: url, name: name)
        // if exists
        if let idx = allItems.index(where: {$0.url.absoluteString == url.absoluteString}) {
            allItems.remove(at: idx)
        }
        allItems.insert(item, at: 0)
        persistence.setValue(allItems.map({$0.dictValue}), forKey: "Items")
    }
    
    func remove(_ url: URL) {
            // if exists
        if let idx = allItems.index(where: {$0.url.absoluteString == url.absoluteString}) {
            allItems.remove(at: idx)
        }
        persistence.setValue(allItems.map({$0.dictValue}), forKey: "Items")
    }
    
    func exists(_ url: URL) -> Bool {
        return allItems.contains(where: {$0.url.absoluteString == url.absoluteString})
    }
    
    func removeAll() {
        allItems.removeAll(keepingCapacity: true)
        persistence.setValue([], forKey: "Items")
    }
    
    func replaceAll(with items: [URLStorageItem]) {
        allItems.removeAll(keepingCapacity: true)
        allItems.append(contentsOf: items)
        persistence.setValue(allItems.map({$0.dictValue}), forKey: "Items")
    }
    
    private let persistence: STPersistence
}
