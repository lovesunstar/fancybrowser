//
//  BrowserViewController+Toolbar.swift
//  FancyVideo
//
//  Created by SunJiangting on 2017/6/9.
//  Copyright © 2017年 Samaritan. All rights reserved.
//

import Foundation
import UIKit
import GiantShoulder

extension BrowserViewController {

    class Toolbar: UIView {
        
        class Button: UIButton {
            
            private lazy var indicator = UIActivityIndicatorView(activityIndicatorStyle: .white)
            
            override init(frame: CGRect) {
                super.init(frame: frame)
               
                addSubview(indicator)
            }
            
            required init?(coder aDecoder: NSCoder) {
                fatalError()
            }
            
            override var isEnabled: Bool {
                didSet {
                    self.alpha = isEnabled ? 1.0 : 0.5
                }
            }
            
            func startAnimating() {
                isAnimating = true
                indicator.isHidden = false
                indicator.startAnimating()
            }
            
            func stopAnimating() {
                isAnimating = false
                indicator.stopAnimating()
                indicator.isHidden = true
            }
            
            private(set) var isAnimating: Bool = false
            
            override func layoutSubviews() {
                super.layoutSubviews()
                indicator.center = CGPoint(x: self.bounds.midX, y: self.bounds.midY)
            }
        }
        
        class FavoriteButton: Button {
            
            override init(frame: CGRect) {
                super.init(frame: frame)

                setImage(UIImage(named: "browser_favorite"), for: .normal)
                setImage(UIImage(named: "browser_favorite"), for: [.normal, .highlighted])
                setImage(UIImage(named: "browser_favorited"), for: [.selected])
                setImage(UIImage(named: "browser_favorited"), for: [.selected, .highlighted])
            }
            
            required init?(coder aDecoder: NSCoder) {
                fatalError()
            }
        }
        
        enum UI {
            static let height: CGFloat = 44 + STOnePixel()
            
            static let itemMargin: CGFloat = appropriate(size: 5, iphone5: 5, iphone6: 10, iphone6p: 10)
        }
        
        fileprivate let separatorView = UIView(frame: .zero)
        let backButton = Button(frame: .zero)
        let forwardButton = Button(frame: .zero)
        let snapshotButton = Button(frame: .zero)
        let favoriteButton = FavoriteButton(frame: .zero)
        let shareButton: UIButton = Button(frame: .zero)
        let exposeButton: UIButton = Button(frame: .zero)
        private let blurView = UIVisualEffectView(effect: UIBlurEffect(style: .light))
        
        fileprivate var videoItems = [String]()
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            loadSubviews()
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func loadSubviews() {
            
            addSubview(blurView)
            blurView.backgroundColor = UIColor.white.withAlphaComponent(0.88)
            
            backButton.setImage(UIImage(named: "browser_back_normal"), for: .normal)
            backButton.isEnabled = false
            addSubview(backButton)
            
            forwardButton.setImage(UIImage(named: "browser_forward_normal"), for: .normal)
            forwardButton.isEnabled = false
            addSubview(forwardButton)
            
            snapshotButton.setImage(UIImage(named: "browser_snapshot"), for: .normal)
            addSubview(snapshotButton)
            
            addSubview(favoriteButton)
            
            shareButton.setImage(UIImage(named: "browser_share"), for: .normal)
            addSubview(shareButton)
            
            exposeButton.setImage(UIImage(named: "browser_expose"), for: .normal)
            addSubview(exposeButton)
            
            separatorView.backgroundColor = UIColor(rgb: 0xdddddd)
            addSubview(separatorView)
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            blurView.frame = self.bounds
            
            let itemWidth: CGFloat = self.bounds.width / 6.0 - UI.itemMargin
            
            let buttons: [UIView] = [backButton, forwardButton, snapshotButton, shareButton, favoriteButton, exposeButton]
            for (idx, button) in buttons.enumerated() {
                button.frame = CGRect(x: CGFloat(idx) * (itemWidth + UI.itemMargin), y: 0, width: itemWidth, height: UI.height)
            }
            separatorView.frame = CGRect(x: 0, y: 0, width: self.bounds.width, height: STOnePixel())
        }
    }
}
