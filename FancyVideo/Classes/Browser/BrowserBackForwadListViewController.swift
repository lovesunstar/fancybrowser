//
//  BrowserBackForwadListViewController.swift
//  FancyVideo
//
//  Created by SunJiangting on 2018/3/13.
//  Copyright © 2018年 Samaritan. All rights reserved.
//

import UIKit
import WebKit
import GiantShoulder

class BrowserBackForwadListViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    fileprivate lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.separatorStyle = .none
        tableView.backgroundView = nil
        tableView.backgroundColor = nil
        tableView.tableFooterView = UIView(frame: .zero)
        if #available(iOS 11.0, *) {
            tableView.contentInsetAdjustmentBehavior = .never
        }
        return tableView
    }()
    
    private let items: [WKBackForwardListItem]
    private let selectionHandler: ((BrowserBackForwadListViewController, WKBackForwardListItem)->Void)
    
    fileprivate lazy var placeholderView = PlaceholderView()

    init(items: [WKBackForwardListItem], selectionHandler: @escaping (BrowserBackForwadListViewController, WKBackForwardListItem)->Void) {
        self.items = items
        self.selectionHandler = selectionHandler
        super.init(nibName: nil, bundle: nil)
        self.hidesBottomBarWhenPushed = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = "BackForwardListTitle".localized()
        
        let backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 44))
        backButton.setImage(UIImage(named: "navigation_dismiss"), for: .normal)
        backButton.imageEdgeInsets = UIEdgeInsetsMake(11, 15, 11, 0)
        backButton.contentHorizontalAlignment = .left
        backButton.addTarget(self, action: #selector(handleDismissAction), for: .touchUpInside)
        let backBarButtonItem = UIBarButtonItem(customView: backButton)
        self.navigationItem.leftBarButtonItem = backBarButtonItem
        
        placeholderView.contentOffset = CGPoint(x: 0, y: -40)
        tableView.frame = self.view.bounds
        tableView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        tableView.dataSource = self
        tableView.delegate = self
        self.view.addSubview(tableView)
        tableView.register(Cell.self, forCellReuseIdentifier: "Identifier")
    
        updatePlaceholderView()
    }
    
    private func updatePlaceholderView() {
        if items.isEmpty {
            placeholderView.state = .case(.empty, "BackForwardEmpty".localized(), nil)
            placeholderView.frame = tableView.bounds
            tableView.addSubview(placeholderView)
        } else {
            placeholderView.state = .normal
            placeholderView.removeFromSuperview()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let item = items[indexPath.row]
        self.st_navigationController?.dismiss(animated: true, completion: nil)
        selectionHandler(self, item)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Identifier") as! Cell
        cell.item = items[indexPath.row]
        return cell
    }
    
    @objc private func handleDismissAction() {
        self.st_navigationController?.dismiss(animated: true, completion: nil)
    }
    
    class Cell: TableViewCell {
        
        var item: WKBackForwardListItem? {
            didSet {
                nameLabel.text = item?.title
                urlLabel.text = item?.url.absoluteString
            }
        }
        
        private let nameLabel = UILabel(frame: .zero)
        private let urlLabel = UILabel(frame: .zero)
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            
            nameLabel.textColor = UIColor(rgb: 0x555555)
            nameLabel.font = UIFont.boldSystemFont(ofSize: 16)
            self.contentView.addSubview(nameLabel)
            
            urlLabel.textColor = UIColor(rgb: 0x717171)
            urlLabel.font = UIFont.systemFont(ofSize: 14)
            self.contentView.addSubview(urlLabel)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            nameLabel.frame = CGRect(x: 12, y: 8, width: self.contentView.bounds.width - 24, height: 20)
            urlLabel.frame = CGRect(x: 12, y: 36, width: self.contentView.bounds.width - 24, height: 16)
        }
    }
}
