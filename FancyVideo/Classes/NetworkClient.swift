//
//  NetworkClient.swift
//  VEnjoy
//
//  Created by SunJiangting on 2017/1/2.
//  Copyright © 2017年 Suen. All rights reserved.
//

import UIKit
import GiantShoulder
import Network

class NetworkClient: NSObject, NetworkClientProtocol {

    static var commonParameters: [String: Any]? {
        var dynamicParameters = [String: Any]()
        dynamicParameters["network"] = Device.nt.type?.name
        dynamicParameters["radio"] = Device.nt.radio
        dynamicParameters["carrier"] = Device.nt.carrier
        return App.httpCommonParameters + dynamicParameters
    }
    static var requestHeaders: [String: String]? {
        return ["User-Agent": App.userAgent, "Accept-Language": App.acceptLanguage]
    }
    
    /**
     - returns: compressed
     */
    static func compressDataUsingGZip(_ data: inout Data) -> Bool {
        if let compressedData = try? (data as NSData).st_gzipCompressedData(), compressedData.count > 0 {
            data = compressedData
            return true
        }
        return false
    }
    
    static func willProcessRequestWithURL(_ URLString: inout String, headers: inout [String: String], parameters: inout [String: Any]?) {
        
    }
    
    static func willProcessResponseWithRequest(_ request: URLRequest, timestamp: TimeInterval, duration: TimeInterval, responseData: Any?, error: Error?, URLResponse: HTTPURLResponse?) {
        
    }
}
