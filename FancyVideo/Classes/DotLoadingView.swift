//
//  DotLoadingView.swift
//  FancyVideo
//
//  Created by SunJiangting on 2018/1/31.
//  Copyright © 2018年 Samaritan. All rights reserved.
//

import UIKit

class DotLoadingView: UIView {
    
    private static let animationImages: [UIImage] = {
        var images = [UIImage]()
        for i in 1...21 {
            let name = "dot_loading_\(i)"
            if let img = UIImage(named: name) {
                images.append(img)
            }
        }
        return images
    }()
    
    private static let imageScale: CGFloat = 105.0 / 18.0
    
    private let imageView = UIImageView(frame: .zero)
    
    private let duration: TimeInterval = 1.0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        imageView.frame = self.bounds
        imageView.contentMode = .scaleAspectFit
        imageView.animationImages = DotLoadingView.animationImages
        imageView.animationDuration = duration
        addSubview(imageView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        guard self.width * self.height > 0 else {
            return
        }
        let scale = self.width / self.height
        let imageWidth, imageHeight: CGFloat
        if scale > DotLoadingView.imageScale {
            imageHeight = self.height
            // imagwi/ imagehei = i
            imageWidth = imageHeight * DotLoadingView.imageScale
        } else {
            imageWidth = self.width
            imageHeight = imageWidth / DotLoadingView.imageScale
        }
        imageView.frame = CGRect(x: (self.width - imageWidth) / 2, y: (self.height - imageHeight) / 2, width: imageWidth, height: imageHeight)
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        return CGSize(width: 50, height: 9)
    }
    
    func startAnimating() {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(stopAfterDelay), object: nil)
        imageView.startAnimating()
        startDate = Date()
        isAnimating = true
    }
    
    func stopAnimating() {
        isAnimating = false
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(stopAfterDelay), object: nil)
        // 由于转一圈需要1s。如果刷新维持了0.5s，就没有完成转一圈。这里强制延时保证整数圈
        if let date = startDate {
            let duration = Date().timeIntervalSince(date)
            
            let delay = self.duration - (duration - floor(duration))
            perform(#selector(stopAfterDelay), with: nil, afterDelay: delay)
        } else {
            imageView.stopAnimating()
        }
        self.startDate = nil
    }
    
    @objc fileprivate func stopAfterDelay() {
        self.imageView.stopAnimating()
    }
    
    override func willMove(toWindow newWindow: UIWindow?) {
        super.willMove(toWindow: newWindow)
        if (newWindow != nil) {
            if (isAnimating) {
                startAnimating()
            }
        }
    }
    
    override func removeFromSuperview() {
        super.removeFromSuperview()
        stopAnimating()
    }
    
    var isAnimating = false
    private var startDate: Date?
}
