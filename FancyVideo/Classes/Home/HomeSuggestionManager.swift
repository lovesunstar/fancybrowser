//
//  HomeSuggestionManager.swift
//  BiuTube
//
//  Created by SunJiangting on 2017/5/21.
//  Copyright © 2017年 Suen. All rights reserved.
//

import Foundation
import GiantShoulder
import Network

class HomeSuggestionManager: NSObject {
    
    static let didChangeSuggestion = Notification.Name("com.suen.SuggestionWebSiteDidChange")
    static let requestInterval: TimeInterval = 600
    
    static let shared = HomeSuggestionManager()
    
    fileprivate(set) var items: [URLStorageItem] = []
    
    private var retryTimes: Int = 0
    
    private var previousRequestDate = Date()
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    fileprivate override init() {
        self.items = LocalURLStorage.suggestion.allItems
        super.init()
        NotificationCenter.default.addObserver(self, selector: #selector(applicationWillEnterForeground), name: .UIApplicationWillEnterForeground, object: nil)
        update()
        previousRequestDate = Date()
    }
    
    @objc private func applicationWillEnterForeground() {
        if Date().timeIntervalSince(previousRequestDate) > HomeSuggestionManager.requestInterval {
            update()
            previousRequestDate = Date()
        }
    }
    
    @objc private func update() {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(update), object: nil)
        if !STIsNetworkConnected() {
            perform(#selector(update), with: nil, afterDelay: 5.0)
            return
        }
        Network.request(.suggestionWebsite).build()?.fv_response(completion: { (_, _, result: Request.APIResult<[[String: Any]]>) in
            switch result {
            case .value(let r):
                guard let r = r else {
                    return
                }
                let items = r.compactMap({URLStorageItem(dictionary: $0)})
                LocalURLStorage.suggestion.replaceAll(with: items)
                self.items = items
                NotificationCenter.default.post(name: HomeSuggestionManager.didChangeSuggestion, object: nil)
                self.retryTimes = 0
            case .error(_):
                if self.retryTimes < 15 {
                    self.perform(#selector(HomeSuggestionManager.update), with: nil, afterDelay: TimeInterval(arc4random() % 15))
                    self.retryTimes += 1
                }
            }
        })
    }
}
