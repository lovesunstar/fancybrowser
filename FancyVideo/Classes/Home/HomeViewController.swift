//
//  HomeViewController.swift
//  FancyVideo
//
//  Created by SunJiangting on 2017/6/9.
//  Copyright © 2017年 Samaritan. All rights reserved.
//

import UIKit
import SDWebImage
import GoogleMobileAds

class HomeViewController: BaseViewController {
    
    private lazy var collectionView: UICollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .vertical
        flowLayout.minimumLineSpacing = 20
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        collectionView.backgroundView = nil
        collectionView.backgroundColor = nil
        collectionView.alwaysBounceHorizontal = false
        collectionView.alwaysBounceVertical = true
        if #available(iOS 11.0, *) {
            collectionView.contentInsetAdjustmentBehavior = .never
        }
        return collectionView
    }()
    
    fileprivate var items: [URLStorageItem] = []
    
    fileprivate var adBannerView: GADBannerView?
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        items = (HomeSuggestionManager.shared.items + LocalURLStorage.top.allItems)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        items = (HomeSuggestionManager.shared.items + LocalURLStorage.top.allItems)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let searchBar = SearchBar(frame: self.st_navigationBar?.contentView.bounds ??  CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 44))
        searchBar.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        searchBar.exposeButton.addTarget(self, action: #selector(handleExpose), for: .touchUpInside)
        self.st_navigationBar?.contentView.addSubview(searchBar)
        searchBar.textFieldCompletionHandler = { [weak self] (_, textField) in
            guard let strongSelf = self else { return }
            guard let text = textField.text, let url = makeFancyURL(text) else {
                TopTipView.showTip("InvalidInputURL".localized(), in: strongSelf.view, duration: 2.0, offsetY: (strongSelf.st_navigationBar?.frame.maxY ?? 20) + 1)
                return
            }
            textField.resignFirstResponder()
            searchBar.setEditing(false, animated: false)
            strongSelf.openURL(url)
        }
        
        self.view.backgroundColor = UIColor(rgb: 0xf5f6f7)
        
        collectionView.frame = self.view.bounds
        collectionView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        collectionView.contentInset = UIEdgeInsets(top: 15, left: 15, bottom: 60, right: 15)
        collectionView.delegate = self
        collectionView.dataSource = self
        self.view.addSubview(collectionView)
        
        collectionView.register(Cell.self, forCellWithReuseIdentifier: "Identifier")
        
        if let homeBannerADConfig = ConfigManager.shared.adConfig.homeBanner, homeBannerADConfig.display {
           let adBannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
            adBannerView.adUnitID = homeBannerADConfig.unitID
            adBannerView.rootViewController = self
            adBannerView.delegate = self
            self.view.addSubview(adBannerView)
            let adRequest = GADRequest()
            if App.isDebug {
                if Device.isSimulator {
                    adRequest.testDevices = [kDFPSimulatorID]
                } else {
                    adRequest.testDevices = ["0785945b6784e62c085cdc49c98f7f72"]
                }
            }
            adBannerView.load(adRequest)
            self.adBannerView = adBannerView
        }
        
        track("AD Request", properties: ["Type": "Home Banner"])
        
        NotificationCenter.default.addObserver(self, selector: #selector(didChangeSuggestion), name: HomeSuggestionManager.didChangeSuggestion, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        items = (HomeSuggestionManager.shared.items + LocalURLStorage.top.allItems)
        collectionView.reloadData()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if let adBannerView = self.adBannerView {
            let adBannerMinY = (self.st_tabBarController?.tabBar.frame.height ?? 0)
            adBannerView.frame = CGRect(x: 0, y: self.view.bounds.height - adBannerView.frame.height - adBannerMinY, width: adBannerView.frame.width, height: adBannerView.frame.height)
            collectionView.contentInset = UIEdgeInsets(top: (self.st_navigationBar?.frame.height ?? 20) + 15, left: 15, bottom: adBannerMinY, right: 15)
        }
    }
    
    @available(iOS 11.0, *)
    override func viewSafeAreaInsetsDidChange() {
        super.viewSafeAreaInsetsDidChange()
        self.view.setNeedsLayout()
    }
    
    @objc private func handleExpose() {
        self.st_safariController?.expose(animated: true)
    }
    
    @objc private func didChangeSuggestion() {
        items = (HomeSuggestionManager.shared.items + LocalURLStorage.top.allItems)
        collectionView.reloadData()
    }
    
    fileprivate func openURL(_ url: URL) {
        let browserVC = BrowserViewController(url: url)
        self.st_safariController?.show(browserVC, animated: true, at: 0)
    }
}


extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Identifier", for: indexPath) as! Cell
        cell.item = items[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        let item = items[indexPath.row]
        self.view.endEditing(true)
        openURL(item.url)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let item = items[indexPath.row]
        return Cell.size(for: item, constrainedTo: collectionView.frame.size)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return Cell.UI.itemBottomSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return Cell.UI.itemSpacing
    }
}

extension HomeViewController {
    
    class Cell: UICollectionViewCell {
        
        struct UI {
            static let itemSpacing: CGFloat = 20
            static let itemMargin: CGFloat = 15
            static let itemWidth: CGFloat = (UIScreen.main.bounds.width - itemMargin * 2 - itemSpacing * 3) / 4
            static let itemHeight: CGFloat = itemWidth + 10
            
            static let itemBottomSpacing: CGFloat = 5
            
            static let imagePadding: CGFloat = 10
        }
        
        
        fileprivate let iconView = UIView(frame: .zero)
        
        fileprivate let iconLabel = UILabel(frame: .zero)
        fileprivate let nameLabel = UILabel(frame: .zero)
        
        fileprivate let iconImageView = UIImageView(frame: .zero)
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            loadSubviews()
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError()
        }
        
        private func loadSubviews() {
            
            self.contentView.backgroundColor = nil
            
            iconView.backgroundColor = UIColor.white
            iconView.layer.cornerRadius = 4
            iconView.layer.masksToBounds = true
            iconView.contentMode = .scaleAspectFill
            self.contentView.addSubview(iconView)
            
            iconLabel.textAlignment = .center
            iconLabel.adjustsFontSizeToFitWidth = true
            iconLabel.font = UIFont.systemFont(ofSize: UI.itemWidth - 30)
            iconLabel.shadowColor = nil
            iconLabel.textColor = UIColor.white
            iconLabel.backgroundColor = UIColor(rgb: 0xbbbbbb)
            iconView.addSubview(iconLabel)
            
            iconImageView.contentMode = .scaleAspectFill
            iconView.addSubview(iconImageView)
            
            nameLabel.textAlignment = .center
            nameLabel.font = UIFont.systemFont(ofSize: 11)
            nameLabel.textColor = UIColor.lightGray
            self.contentView.addSubview(nameLabel)
        }
        
        var item: URLStorageItem? {
            didSet {
                if let item = item {
                    iconLabel.isHidden = false
                    if let n = item.name.first {
                        iconLabel.text = String(describing: n)
                    } else {
                        iconLabel.text = "收藏"
                    }
                    iconImageView.sd_cancelCurrentImageLoad()
                    if let extra = item.extra, let _iconURLString = extra["icon_url"] as? String, !_iconURLString.isEmpty, let iconURL = URL(string: _iconURLString) {
                        iconImageView.sd_setImage(with: iconURL, completed: { [weak self] (image, error, _, _) in
                            guard let strongSelf = self else {
                                return
                            }
                            guard image != nil, error == nil else {
                                strongSelf.iconImageView.isHidden = true
                                strongSelf.iconLabel.isHidden = false
                                return
                            }
                            strongSelf.iconImageView.isHidden = false
                            strongSelf.iconLabel.isHidden = true
                            
                        })
                    } else {
                        iconImageView.isHidden = true
                    }
                    nameLabel.text = item.name
                } else {
                    iconLabel.text = nil
                    nameLabel.text = nil
                    iconImageView.image = nil
                    iconImageView.sd_cancelCurrentImageLoad()
                    iconLabel.isHidden = true
                }
            }
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            iconView.frame = CGRect(x: UI.imagePadding, y: UI.imagePadding, width: self.bounds.width - UI.imagePadding * 2, height: self.bounds.width - UI.imagePadding * 2)
            iconLabel.frame = iconView.bounds
            iconImageView.frame = iconView.bounds
            nameLabel.frame = CGRect(x: 0, y: iconView.frame.maxY, width: self.bounds.width, height: self.bounds.height - iconView.frame.maxY)
        }
        
        class func size(for item: URLStorageItem, constrainedTo size: CGSize) -> CGSize {
            let itemWidth: CGFloat = (size.width - UI.itemMargin * 2 - UI.itemSpacing * 3) / 4
            let itemHeight: CGFloat = itemWidth + 10
            return CGSize(width: itemWidth, height: itemHeight)
        }
    }
}

extension HomeViewController: GADBannerViewDelegate {
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        track("AD Show", properties: ["Type": "Home Banner"])
        track("AD Request Success", properties: ["Type": "Home Banner"])
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
        self.view.setNeedsLayout()
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        track("AD Request Failure", properties: ["Type": "Home Banner"])
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
}

extension HomeViewController {
    
    fileprivate class SearchBar: UIView, UITextFieldDelegate {
        
        enum UI {
            static let textLeftPadding: CGFloat = 10
        }
        
        fileprivate let contentView = UIView(frame: .zero)
        fileprivate let addressBar = AddressBar(frame: .zero)
        fileprivate(set) var cancelButton = UIButton(frame: .zero)
        fileprivate let tapGesture: UITapGestureRecognizer = {
            let tapGesture = UITapGestureRecognizer()
            tapGesture.numberOfTapsRequired = 1
            tapGesture.numberOfTouchesRequired = 1
            return tapGesture
        }()
        
        let exposeButton = UIButton(frame: .zero)
        
        fileprivate var isEditing = false
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            loadSubviews()
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        var textFieldCompletionHandler: ((SearchBar, UITextField) -> Void)?
        
        private func loadSubviews() {
            
            contentView.layer.cornerRadius = 4
            contentView.layer.masksToBounds = true
            addSubview(contentView)
            
            addressBar.textField.delegate = self
            addressBar.textField.addTarget(self, action: #selector(textDidChange(_:)), for: .editingChanged)
            
            addressBar.clearButton.addTarget(self, action: #selector(handleClearText(_:)), for: .touchUpInside)
            contentView.addSubview(addressBar)
            
            cancelButton.setTitle("Cancel".localized(), for: .normal)
            cancelButton.setTitleColor(UIColor(rgb: 0x2a90d7), for: .normal)
            cancelButton.sizeToFit()
            var cancelButtonFrame = cancelButton.frame
            cancelButtonFrame.size.width = max(cancelButton.frame.width, 60)
            cancelButton.frame = cancelButtonFrame
            cancelButton.addTarget(self, action: #selector(handleCancel(_:)), for: .touchUpInside)
            addSubview(cancelButton)
            
            exposeButton.setImage(UIImage(named: "navigation_browser_expose"), for: .normal)
            exposeButton.contentHorizontalAlignment = .right
            exposeButton.contentEdgeInsets = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 10)
            addSubview(exposeButton)
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            if isEditing {
                cancelButton.isHidden = false
                exposeButton.isHidden = true
                cancelButton.sizeToFit()
                let cancelButtonWidth = cancelButton.frame.width + 15
                let contentViewWidth = self.bounds.width - cancelButtonWidth - 10
                let contentViewHeight: CGFloat = 28
                contentView.frame = CGRect(x: 10, y: (self.bounds.height - contentViewHeight) / 2, width: contentViewWidth, height: contentViewHeight)
                cancelButton.frame = CGRect(x: contentView.frame.maxX, y: 0, width: cancelButtonWidth, height: self.bounds.height)
                addressBar.frame = contentView.bounds
            } else {
                cancelButton.isHidden = true
                exposeButton.isHidden = false
                let exposeButtonWidth = self.bounds.height + 10
                exposeButton.frame = CGRect(x: self.bounds.width - exposeButtonWidth, y: 0, width: exposeButtonWidth, height: self.bounds.height)
                
                let contentViewHeight: CGFloat = 28
                
                let contentViewWidth = exposeButton.frame.minX
                contentView.frame = CGRect(x: 10, y: (self.bounds.height - contentViewHeight) / 2, width: contentViewWidth, height: contentViewHeight)
                addressBar.frame = contentView.bounds
            }
            if !isEditing && (addressBar.textField.text?.isEmpty ?? true) {
                var frame = addressBar.placeholder.frame
                frame.origin.x = (addressBar.textField.bounds.width - frame.width) * 0.5
                frame.origin.y = (addressBar.textField.bounds.height - frame.height) * 0.5
                addressBar.placeholder.frame = frame
            }
        }
        
        func setEditing(_ editing: Bool, animated: Bool) {
            if editing == self.isEditing {
                return
            }
            self.isEditing = editing
            setNeedsLayout()
            if editing {
                addressBar.textField.becomeFirstResponder()
            } else {
                addressBar.textField.resignFirstResponder()
            }
        }
        
        @objc fileprivate func handleCancel(_ cancelButton: UIButton) {
            setEditing(false, animated: true)
        }
        
        @objc fileprivate func handleClearText(_ button: UIButton) {
            clearText()
            setEditing(true, animated: true)
        }
        
        fileprivate func clearText() {
            addressBar.textField.text = ""
            textDidChange(addressBar.textField)
        }
        
        fileprivate func updateTextFieldAccessory() {
            let textFieldIsEmpty = (addressBar.textField.text?.isEmpty ?? true)
            addressBar.placeholder.textLabel.isHidden = !textFieldIsEmpty
            addressBar.clearButton.isHidden = textFieldIsEmpty
        }
        
        @objc fileprivate func textDidChange(_ textField: UITextField) {
            updateTextFieldAccessory()
        }
        
        fileprivate func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
            setEditing(true, animated: true)
            return true
        }
        
        func textFieldDidBeginEditing(_ textField: UITextField) {
            addressBar.clearButton.isHidden = textField.text?.isEmpty ?? true
            var frame = addressBar.placeholder.frame
            frame.origin.x = UI.textLeftPadding
            frame.origin.y = (addressBar.textField.height - frame.size.height) * 0.5
            addressBar.placeholder.frame = frame
            if !(textField.text?.isEmpty ?? true) {
                textField.selectAll(nil)
            }
        }
        
        func textFieldDidEndEditing(_ textField: UITextField) {
            var frame = addressBar.placeholder.frame
            if (addressBar.textField.text?.isEmpty ?? true) {
                frame.origin.x = (addressBar.textField.width - frame.width) * 0.5
                frame.origin.y = (addressBar.textField.height - frame.height) * 0.5
            } else {
                frame.origin.x = UI.textLeftPadding
                frame.origin.y = (addressBar.textField.height - frame.height) * 0.5
            }
            addressBar.placeholder.frame = frame
        }
        
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            if string == "\n" {
                textFieldCompletionHandler?(self, textField)
                return false
            }
            return true
        }
    }
    
    fileprivate class AddressBar: UIView {
        
        fileprivate var textField = UITextField(frame: .zero)
        fileprivate var placeholder = PlaceholderView(frame: .zero)

        fileprivate var clearButton = FancyVideo.Button(frame: CGRect(x: 0, y: 0, width: 19, height: 19))
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            loadSubviews()
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        fileprivate func loadSubviews() {
            textField.frame = self.bounds
            textField.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            textField.returnKeyType = .search
            textField.backgroundColor = UIColor.white
            textField.enablesReturnKeyAutomatically = true
            textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 38, height: 1))
            textField.leftViewMode = .always
            textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 27, height: 1))
            textField.rightViewMode = .always
            textField.autocapitalizationType = .none
            textField.autocorrectionType = .no
            textField.layer.cornerRadius = 4
            textField.layer.masksToBounds = true
            textField.textColor = UIColor(rgb: 0x222222)
            textField.tintColor = UIColor(rgb: 0x2a90d7)
            textField.font = UIFont.systemFont(ofSize: 16)
            addSubview(textField)
            
            clearButton.frame = CGRect(x: 0, y: 0, width: 19, height: 19)
            clearButton.setImage(UIImage(named: "search_bar_clear"), for: .normal)
            clearButton.touchInsets = UIEdgeInsets(top: -10, left: -10, bottom: -10, right: -10)
            clearButton.isHidden = true
            textField.addSubview(clearButton)
            
            placeholder.textLabel.text = "InputURLPlaceholder".localized()
            placeholder.sizeToFit()
            placeholder.isUserInteractionEnabled = false
            textField.addSubview(placeholder)
        }
        
        fileprivate override func layoutSubviews() {
            super.layoutSubviews()
            clearButton.frame = CGRect(x: textField.bounds.width - 27, y: (self.textField.bounds.height - 19) * 0.5, width: 19, height: 19)
        }
        
        class PlaceholderView: UIView {
            
            let iconView = UIImageView(frame: .zero)
            let textLabel = UILabel(frame: .zero)
            
            override init(frame: CGRect) {
                super.init(frame: frame)
                iconView.image = UIImage(named: "search_bar_icon")
                addSubview(iconView)
                textLabel.font = UIFont.systemFont(ofSize: 15)
                textLabel.textColor = UIColor(rgb: 0x9c9c9c)
                addSubview(textLabel)
            }
            
            required init?(coder aDecoder: NSCoder) {
                fatalError()
            }
            
            override func sizeThatFits(_ size: CGSize) -> CGSize {
                iconView.sizeToFit()
                textLabel.sizeToFit()
                let width = iconView.frame.width > 0 ? (iconView.frame.width + 9 + textLabel.frame.width) : textLabel.frame.width
                return CGSize(width: width, height: 28)
            }
            
            override func layoutSubviews() {
                super.layoutSubviews()
                if iconView.frame.width > 0 {
                    iconView.frame = CGRect(x: 0, y: (self.bounds.height - iconView.frame.width) * 0.5, width: iconView.frame.width, height: iconView.frame.height)
                    textLabel.frame = CGRect(x: iconView.frame.maxX + 9, y: (self.bounds.height - textLabel.frame.height) * 0.5, width: textLabel.frame.width, height: textLabel.frame.height)
                } else {
                    textLabel.frame = self.bounds
                }
            }
        }
    }
}
