//
//  Device.swift
//  VEnjoy
//
//  Created by SunJiangting on 2017/1/2.
//  Copyright © 2017年 Suen. All rights reserved.
//

import Foundation
import GiantShoulder

struct Device {
    
    private static let calendar = Calendar.current
    private static let systemLocale = Locale.current
    private static let device = UIDevice.current
    
    static let isSimulator: Bool = {
        var isSim = false
        #if arch(i386) || arch(x86_64)
            isSim = true
        #endif
        return isSim
    }()
    
    static let isJailBroken: Bool = {
        let fileManager = FileManager.default
        return fileManager.fileExists(atPath: "/Applications/Cydia.app") || fileManager.fileExists(atPath: "/private/var/lib/apt")
    }()
    
    static let isIPhoneX: Bool = {
        return STIsIPhoneX()
    }()
    
    static let platform: String = (device.userInterfaceIdiom == .pad) ? "ipad" : "iphone"
    
    static let osName = "iOS"
    
    static let name = device.name
    static let system = (name: device.systemName, version: device.systemVersion)
    
    static let model: String = STGetMachineID()
    
    static let timeZone: (offset: Int, name: String) = (
        offset: Device.calendar.timeZone.secondsFromGMT(),
        name: Device.calendar.timeZone.abbreviation() ?? ""
    )
    
    static let locale: (language: String, region: String, script: String) = (
        language: {
            if let language = Device.systemLocale.languageCode {
                let script = Device.systemLocale.scriptCode ?? ""
                if language.contains("-") || script.isEmpty {
                    return language
                }
                return "\(language)-\(script)"
            }
            return "jp"
        }(),
        region: {
            return Device.systemLocale.regionCode ?? "JP"
        }(),
        script: {
            return Device.systemLocale.scriptCode ?? ""
        }()
    )
    
    static let screen: CGSize = UIScreen.main.bounds.size
    
    static let isShortSizedScreen = UIScreen.main.bounds.height == 480
    
    static let is667SizedScreen = UIScreen.main.bounds.height == 667
    
    static let is736SizedScreen = UIScreen.main.bounds.height == 736
    
    static let isLargeScreen = UIScreen.main.bounds.height >= 667
}

import CoreTelephony

extension Device {
    
    struct nt {
        static var type: NTType? {
            return Network.Reachability.shared.nt
        }
        
        static var radio: String? {
            return Network.Reachability.shared.currentRadio
        }
        
        static var carrier: String {
            return Network.Reachability.shared.carrierName()
        }
        
        static func isConnected() -> Bool {
            return Network.Reachability.shared.nt != nil
        }
        
        static func isWIFI() -> Bool {
            return Network.Reachability.shared.nt == .wifi
        }
    }
    
    enum NTType {
        case gprs   // 2G second generation
        case cdma   // 3G third generation
        case lte    // 4G forth generation
        case wifi   // wifi
        
        var name: String {
            switch self {
            case .wifi:
                return "WIFI"
            case .lte:
                return "4G"
            case .cdma:
                return "3G"
            case .gprs:
                return "2G"
            }
        }
    }
}

extension Device {
    
    fileprivate struct Network {
        
        fileprivate class Reachability: NSObject {
            
            static let shared = Reachability()
            
            var reachability = STReachability(host: "samaritan.herukuapp.com")
            private var status: STNetworkStatus = .reachNone
            
            var telephonyNetworkInfo = CTTelephonyNetworkInfo()
            fileprivate var currentRadio: String?
            
            private var gprsRadios = [CTRadioAccessTechnologyGPRS, CTRadioAccessTechnologyEdge]
            private var cdmaRadios = [CTRadioAccessTechnologyWCDMA, CTRadioAccessTechnologyHSDPA, CTRadioAccessTechnologyHSUPA, CTRadioAccessTechnologyCDMA1x, CTRadioAccessTechnologyCDMAEVDORev0, CTRadioAccessTechnologyCDMAEVDORevA, CTRadioAccessTechnologyCDMAEVDORevB, CTRadioAccessTechnologyeHRPD]
            private var lteRadios = [CTRadioAccessTechnologyLTE]
            
            var observer1: AnyObject?
            var observer2: AnyObject?
            
            deinit {
                if let observer1 = observer1 {
                    NotificationCenter.default.removeObserver(observer1)
                }
                if let observer2 = observer2 {
                    NotificationCenter.default.removeObserver(observer2)
                }
            }
            
            private override init() {
                super.init()
                self.reachability.startNotification()
                self.status = self.reachability.reachabilityStatus
                self.observer1 = NotificationCenter.default.addObserver(forName: NSNotification.Name.STReachabilityDidChanged, object: self.reachability, queue: nil) { [weak self] (notification) -> Void in
                    if let strongSelf = self {
                        strongSelf.status = strongSelf.reachability.reachabilityStatus
                    }
                }
                self.currentRadio = self.telephonyNetworkInfo.currentRadioAccessTechnology
                self.observer2 = NotificationCenter.default.addObserver(forName: NSNotification.Name.CTRadioAccessTechnologyDidChange, object: self.telephonyNetworkInfo, queue: nil) { [weak self] (notification) -> Void in
                    if let strongSelf = self {
                        strongSelf.currentRadio = strongSelf.telephonyNetworkInfo.currentRadioAccessTechnology
                    }
                }
            }
            
            var nt: NTType? {
                if status == .reachNone {
                    return nil
                }
                if status == .reachWIFI {
                    return .wifi
                }
                if self.is2GConnected() {
                    return .gprs
                }
                if self.is3GConnected() {
                    return .cdma
                }
                return .lte
            }
            
            func carrierName() -> String {
                return telephonyNetworkInfo.subscriberCellularProvider?.carrierName ?? ""
            }
            
            private func is2GConnected() -> Bool {
                return hits(radios: self.gprsRadios)
            }
            
            private func is3GConnected() -> Bool {
                return hits(radios: self.cdmaRadios)
            }
            
            private func is4GConnected() -> Bool {
                return hits(radios: self.lteRadios)
            }
            
            private func hits(radios: [String]) -> Bool {
                if let radio = self.currentRadio {
                    return radios.contains(radio)
                }
                return false
            }
        }
   
    }
}
