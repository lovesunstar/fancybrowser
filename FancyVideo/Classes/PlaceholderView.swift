//
//  PlaceholderView.swift
//  DokiDoki
//
//  Created by SunJiangting on 2016/12/11.
//  Copyright © 2016年 Suen. All rights reserved.
//

import UIKit

final class PlaceholderView: UIView {
    
    static let defaultActionTag: Int = 999999
    
    enum UI {
        static let horizontalPadding: CGFloat = appropriate(size: 40, iphone5: 40, iphone6: 60, iphone6p: 62)
        static let titleTop: CGFloat = 40
        static let actionButtonTop: CGFloat = 34
    }
    
    struct ImageName: RawRepresentable {
        typealias RawValue = String
        private(set) var rawValue: ImageName.RawValue
        init(rawValue: ImageName.RawValue) {
            self.rawValue = rawValue
        }
    }
    
    enum Action {
        case custom(tag: Int, title: String)
        case `default`
    }
    
    enum State {
        case normal
        case loading
        case `case`(ImageName, String, Action?)    // (let imageName, let message, let showRetryButton)
        case custom(UIView)
    }
    
    private var loadingView = LoadingView()
    private var stateView = StateView()
    
    var stateTitleTopMargin: CGFloat? {
        didSet {
            stateView.titleTopMargin = stateTitleTopMargin
        }
    }
    
    var maximumTitleNumberOfLines: Int = 0 {
        didSet {
            stateView.titleLabel.numberOfLines = maximumTitleNumberOfLines
        }
    }
    
    var stateActionButtonTopMargin: CGFloat? {
        didSet {
            stateView.actionButtonTopMargin = stateActionButtonTopMargin
        }
    }
    
    var titleColor: UIColor? = nil {
        didSet {
            stateView.titleLabel.textColor = titleColor
        }
    }
    
    var titleFont: UIFont? = nil {
        didSet {
            stateView.titleLabel.font = titleFont
        }
    }
    
    private weak var customView: UIView?
    
    var contentOffset: CGPoint = .zero
    
    var actionButton: UIButton {
        return stateView.retryButton
    }
    
    var state: State = .normal {
        didSet {
            switch state {
            case .normal:
                loadingView.indicator.stopAnimating()
                loadingView.removeFromSuperview()
                stateView.removeFromSuperview()
                customView?.removeFromSuperview()
                customView = nil
            case .loading:
                loadingView.indicator.startAnimating()
                loadingView.frame = self.bounds
                addSubview(loadingView)
                stateView.removeFromSuperview()
                customView?.removeFromSuperview()
                customView = nil
            case .case(let name, let title, let action):
                loadingView.indicator.stopAnimating()
                loadingView.removeFromSuperview()
                
                if let action = action {
                    switch action {
                    case .custom(let tag, let title):
                        stateView.retryButton.tag = tag
                        stateView.retryButton.setTitle(title, for: .normal)
                    case .default:
                        stateView.retryButton.tag = PlaceholderView.defaultActionTag
                        stateView.retryButton.setTitle("Common_Retry".localized(), for: .normal)
                    }
                    stateView.update(image: name.rawValue, title: title, shouldRetry: true)
                } else {
                    stateView.update(image: name.rawValue, title: title, shouldRetry: false)
                }
                stateView.frame = self.bounds
                addSubview(stateView)
                customView?.removeFromSuperview()
                customView = nil
            case .custom(let view):
                loadingView.indicator.stopAnimating()
                loadingView.removeFromSuperview()
                stateView.removeFromSuperview()
                view.frame = self.bounds
                view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
                addSubview(view)
                customView = view
            }
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        loadingView.frame = self.bounds.offsetBy(dx: contentOffset.x, dy: contentOffset.y)
        stateView.frame = self.bounds.offsetBy(dx: contentOffset.x, dy: contentOffset.y)
        customView?.frame = self.bounds.offsetBy(dx: contentOffset.x, dy: contentOffset.y)
    }
    
    private class LoadingView: UIView {
        
        fileprivate(set) var indicator = UIActivityIndicatorView(activityIndicatorStyle: .white)
        
        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            loadSubviews()
        }
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            loadSubviews()
        }
        
        func loadSubviews() {
            indicator.color = UIColor.red
            addSubview(indicator)
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            indicator.center = CGPoint(x: self.bounds.midX, y: self.bounds.midY)
        }
    }
    
    private class StateView: UIView {
        
        fileprivate(set) var imageView = UIImageView(frame: .zero)
        fileprivate(set) var titleLabel = UILabel(frame: .zero)
        fileprivate(set) var retryButton = UIButton(frame: .zero)
        
        
        var titleTopMargin: CGFloat?
        var actionButtonTopMargin: CGFloat?
        
        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            loadSubviews()
        }
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            loadSubviews()
        }
        
        func loadSubviews() {
            self.addSubview(imageView)
            titleLabel.font = UIFont.systemFont(ofSize: 20)
            titleLabel.textColor = UIColor(rgb: 0x555555)
            titleLabel.numberOfLines = 3
            titleLabel.lineBreakMode = .byWordWrapping
            titleLabel.textAlignment = .center
            self.addSubview(titleLabel)
            
            retryButton.setTitleColor(UIColor.white, for: .normal)
            retryButton.backgroundColor = UIColor(rgb: 0x28d5b9)
            retryButton.titleLabel?.font = UIFont.systemFont(ofSize: 20)
            retryButton.layer.cornerRadius = 6
            retryButton.layer.masksToBounds = true
            retryButton.tag = PlaceholderView.defaultActionTag
            self.addSubview(retryButton)
                    
            retryButton.setTitle("Common_Retry".localized(), for: .normal)
            retryButton.sizeToFit()
            retryButton.frame.size.width = max(retryButton.frame.width + 16, 70)
            retryButton.frame.size.height = 40
        }
        
        func update(image name: String, title: String, shouldRetry: Bool) {
            imageView.contentMode = .scaleAspectFill
            imageView.image = UIImage(named: name)
            titleLabel.text = title
            retryButton.isHidden = !shouldRetry
            setNeedsLayout()
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            
            imageView.sizeToFit()
            if imageView.bounds.width < 100 && imageView.bounds.width > 0 {
                let imageWidth = imageView.bounds.width, imageHeight = imageView.bounds.height
                // iw / ih = w / h
                imageView.frame.size.width = 100
                imageView.frame.size.height = 100 / imageWidth * imageHeight
            }
            if imageView.bounds.height < 100 && imageView.bounds.height > 0 {
                let imageWidth = imageView.bounds.width, imageHeight = imageView.bounds.height
                imageView.frame.size.height = 100
                imageView.frame.size.width = imageWidth / imageHeight * 100
            }
            
            let titleWidth: CGFloat = self.bounds.width - PlaceholderView.UI.horizontalPadding * 2
            titleLabel.frame.size.width = titleWidth
            titleLabel.frame.size.height = 200
            titleLabel.sizeToFit()
            
            
            let titleTop: CGFloat = titleTopMargin ?? PlaceholderView.UI.titleTop
            let actionButtonTop: CGFloat = actionButtonTopMargin ?? PlaceholderView.UI.actionButtonTop
            
            var totalHeight = imageView.frame.height + titleLabel.frame.height + titleTop
            if !retryButton.isHidden {
                totalHeight += (actionButtonTop + retryButton.frame.height)
            }
            let imageTopMargin: CGFloat = (self.bounds.height - totalHeight) / 2
            imageView.frame = CGRect(x: (self.bounds.width - imageView.frame.width) * 0.5, y: imageTopMargin, width: imageView.frame.width, height: imageView.frame.height)
            titleLabel.frame = CGRect(x: (self.bounds.width - titleWidth) * 0.5, y: imageView.frame.maxY + titleTop, width: titleWidth, height: titleLabel.bounds.height)
            
            if !retryButton.isHidden {
                let buttonWidth: CGFloat = self.bounds.width - 2 * PlaceholderView.UI.horizontalPadding
                retryButton.frame = CGRect(x: (self.bounds.width - buttonWidth) * 0.5, y: titleLabel.frame.maxY + actionButtonTop, width: buttonWidth, height: retryButton.frame.height)
            }
        }
    }
}

extension PlaceholderView.ImageName {
    static let empty = PlaceholderView.ImageName(rawValue: "placeholder_empty")
    static let error = PlaceholderView.ImageName(rawValue: "placeholder_error")
    static let noConnection = PlaceholderView.ImageName(rawValue: "placeholder_no_connection")
}
