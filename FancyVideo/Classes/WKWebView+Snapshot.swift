//
//  WKWebView+Snapshot.swift
//  FancyVideo
//
//  Created by SunJiangting on 2018/3/8.
//  Copyright © 2018年 Samaritan. All rights reserved.
//

import UIKit
import WebKit
import ObjectiveC

public extension WKWebView {
    
    enum WKWebViewScapshotPrivateStatic {
        fileprivate static var isCapturingKey: String = "WKWebViewScapshotPrivateStatic_isCapturingKey"
    }
    
    var isCapturing: Bool {
        get {
            if let capturingValue = objc_getAssociatedObject(self, &WKWebViewScapshotPrivateStatic.isCapturingKey) as? NSNumber {
                return capturingValue.boolValue
            }
            return false
        }
        set(newValue) {
            let capturingValue = NSNumber(value: newValue)
            objc_setAssociatedObject(self, &WKWebViewScapshotPrivateStatic.isCapturingKey, capturingValue, objc_AssociationPolicy.OBJC_ASSOCIATION_COPY_NONATOMIC)
        }
    }
    
    func fv_takeSnapshot(_ completionHandler: @escaping (_ capturedImage: UIImage?) -> Void) {
        
        self.isCapturing = true
        
        let offset = self.scrollView.contentOffset
        
        // Put a fake Cover of View
        guard let snapShotView = snapshotView(afterScreenUpdates: true), let superview = self.superview else {
            completionHandler(nil)
            return
        }
        snapShotView.frame = CGRect(x: self.frame.origin.x, y: self.frame.origin.y, width: snapShotView.frame.size.width, height: snapShotView.frame.size.height)
        superview.addSubview(snapShotView)
        
        if self.frame.size.height < self.scrollView.contentSize.height {
            self.scrollView.contentOffset = CGPoint(x: 0, y: self.scrollView.contentSize.height - self.frame.size.height)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) { [weak self] () -> Void in
            guard let strongSelf = self else {
                return
            }
            strongSelf.scrollView.contentOffset = .zero
            strongSelf.fv_startTakingSnapshot({ [weak strongSelf] (capturedImage) -> Void in
                guard let sStongSelf = strongSelf else { return }
                sStongSelf.scrollView.contentOffset = offset
                snapShotView.removeFromSuperview()
                sStongSelf.isCapturing = false
                completionHandler(capturedImage)
            })
        }
    }
    
    fileprivate func fv_startTakingSnapshot(_ completionHandler:@escaping (_ capturedImage: UIImage?) -> Void) {
        guard let superview = self.superview, let backupIndex = superview.subviews.index(of: self) else {
            completionHandler(nil)
            return
        }
        let containerView = UIView(frame: self.bounds)
        let backupFrame = self.frame
        self.removeFromSuperview()
        containerView.addSubview(self)
        
        let totalSize = self.scrollView.contentSize
        // Divide
        let numberOfPages = Int(ceil(totalSize.height / containerView.bounds.height))
        
        self.frame = CGRect(x: 0, y: 0, width: containerView.bounds.size.width, height: self.scrollView.contentSize.height)
        UIGraphicsBeginImageContextWithOptions(totalSize, false, UIScreen.main.scale)
        fv_takeSnapshotByPage(to: containerView, page: 0, numberOfPages: numberOfPages, drawCallback: { [weak self, weak superview] () -> Void in
            guard let strongSelf = self, let strongSuperview = superview else {
                containerView.removeFromSuperview()
                return
            }
            let capturedImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            // Recover
            strongSelf.removeFromSuperview()
            strongSuperview.insertSubview(strongSelf, at: backupIndex)
            strongSelf.frame = backupFrame
            containerView.removeFromSuperview()
            completionHandler(capturedImage)
        })
    }
    
    fileprivate func fv_takeSnapshotByPage(to targetView: UIView, page: Int, numberOfPages: Int, drawCallback: @escaping () -> Void) {
        // set up split frame of super view
        let splitFrame = CGRect(x: 0, y: CGFloat(page) * targetView.frame.size.height, width: targetView.bounds.size.width, height: targetView.frame.size.height)
        // set up webview frame
        
        self.frame = CGRect(x: 0, y: -(CGFloat(page) * targetView.frame.size.height), width: targetView.bounds.size.width, height: self.scrollView.contentSize.height)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) { [weak self] () -> Void in
            guard let strongSelf = self else {
                targetView.removeFromSuperview()
                return
            }
            targetView.drawHierarchy(in: splitFrame, afterScreenUpdates: true)
            if page < numberOfPages {
                strongSelf.fv_takeSnapshotByPage(to: targetView, page: (page + 1), numberOfPages: numberOfPages, drawCallback: drawCallback)
            }else{
                drawCallback()
            }
        }
    }
}

