//
//  Tracker.swift
//  VEnjoy
//
//  Created by SunJiangting on 2017/1/3.
//  Copyright © 2017年 Suen. All rights reserved.
//

import UIKit
import Amplitude_iOS

func track(_ event: String, properties: [String: Any]? = nil) {
    Tracker.default.track(event, properties: properties)
}

class Tracker {
    
    static let `default` = Tracker()
    
    private let amplitude: Amplitude = {
        let amplitude: Amplitude = Amplitude.instance()
        amplitude.initializeApiKey("3adb816a282fcc097b173a00cb991f97")
        return amplitude
    }()
    
    private let sendQueue = DispatchQueue(label: "com.fancyvideo.event.amplitude", qos: DispatchQoS.background)
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    private init() {
        updateAmplitudeProperties()
    }
    
    func appDidFinishLaunch() {
        track("App Active")
    }
    
    /**
     - parameter event 事件名称
     - parameter properties 发送到第三方统计平台的属性
     */
    func track(_ event: String, properties: [String : Any]? = nil) {
        sendQueue.async {
            var eventProperties = App.localeBasedEventParameters
            eventProperties["Network Access"] = Device.nt.type?.name ?? ""
            eventProperties["Network Radio"] = Device.nt.radio ?? ""
            eventProperties["Network Carrier"] = Device.nt.carrier
            if let p = properties {
                eventProperties += p
            }
            
            self.amplitude.logEvent(event, withEventProperties: eventProperties)
        }
    }
    
    @objc private func handle(userDidLogin notificatin: Notification) {
        updateAmplitudeProperties()
    }
    
    private func updateAmplitudeProperties() {
        sendQueue.async {
            let userProperties = App.eventSessionParameters
            self.amplitude.setUserProperties(userProperties)
        }
    }
}
