//
//  TopTipView.swift
//  FancyVideo
//
//  Created by SunJiangting on 2018/1/31.
//  Copyright © 2018年 Samaritan. All rights reserved.
//

import UIKit
import GiantShoulder

class TopTipView: UIView {

    fileprivate let bkgView = UIImageView()
    fileprivate let textLabel = UILabel(frame: .zero)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func loadSubviews() {
        bkgView.image = UIImage(named: "top_message_bkg")?.resizableImage(withCapInsets: UIEdgeInsets(top: 20, left: 100, bottom: 20, right: 100), resizingMode: .stretch)
        addSubview(bkgView)
        
        textLabel.textColor = UIColor.white
        textLabel.textAlignment = .center
        textLabel.font = UIFont.systemFont(ofSize: 14)
        textLabel.numberOfLines = 0
        addSubview(textLabel)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        bkgView.frame = self.bounds
        textLabel.frame = self.bounds
    }
    
    @discardableResult
    static func showTip(_ tip: String, in view: UIView, duration: TimeInterval = 1.0, offsetY: CGFloat = 0) -> STIndicatorView {
        let indicator = STIndicatorView.show(in: view, animated: false)
        indicator.blurEffectStyle = .none
        indicator.type = .custom
        indicator.cornerRadius = 0
        indicator.minimumSize = .zero
        indicator.backgroundView.isHidden = true
        indicator.alpha = 1.0
        let tipView = TopTipView(frame: CGRect(x: 5, y: offsetY, width: view.bounds.width - 10, height: 40))
        tipView.textLabel.text = tip
        indicator.addSubview(tipView)
        indicator.isUserInteractionEnabled = false
        indicator.hide(animated: false, afterDelay: duration)
        return indicator
    }
}

