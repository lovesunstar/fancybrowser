//
//  ConfigManager.swift
//  VEnjoy
//
//  Created by SunJiangting on 2017/1/3.
//  Copyright © 2017年 Suen. All rights reserved.
//

import UIKit
import GiantShoulder
import Network


final class ConfigManager: NSObject {
    
    static let didChangeNotification = Notification.Name(rawValue: "ConfigDidChangeNotification")
    
    private let persistence: STPersistence
    
    static let shared = ConfigManager()
    
    private(set) var isInReview = true
    
    private var retryTimes: Int = 0
    
    private var isLoading = false
    
    let adConfig: ADConfig
    
    private override init() {
        let persistence = STPersistence(named: "Config")
        let values = persistence.value(forKey: "Values") as? [String: Any]
        if let v = values, let adMob = v["admob_keys"] as? [String: Any], let adConfig = ADConfig(dictionary: adMob) {
            self.adConfig = adConfig
        } else {
            self.adConfig = ADConfig.builtIn
        }
        self.persistence = persistence
        super.init()
        if let v = values, !v.isEmpty {
            updateProperties(v)
        }
    }
    
    @objc func update() {
        if isLoading {
            return
        }
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(update), object: nil)
        if !STIsNetworkConnected() {
            perform(#selector(update), with: nil, afterDelay: 5.0)
            return
        }
        isLoading = true
        Network.request(.config).build()?.fv_response(completion: { (_, _, results: Request.APIResult<[String: Any]>) in
            switch results {
            case .error(_):
                self.isLoading = false
                if self.retryTimes >= 20 {
                    return
                }
                let randomDelay = TimeInterval(arc4random() % 20)
                self.perform(#selector(ConfigManager.update), with: nil, afterDelay: randomDelay)
                self.retryTimes += 1
            case .value(let config):
                self.isLoading = false
                if let dict = config {
                    self.updateProperties(dict)
                    self.save(dict)
                    NotificationCenter.default.post(name: ConfigManager.didChangeNotification, object: self)
                }
                self.retryTimes = 0
            }
        })
    }

    private func updateProperties(_ dict: [String: Any]) {
        self.isInReview = (dict["in_review"] as? NSNumber)?.boolValue ?? false
    }
    
    private func save(_ dict: [String: Any]) {
        persistence.setValue(dict, forKey: "Values")
    }
}


class ADConfig {
    
    static let builtIn = ADConfig(appID: AppKeys.admobAppID, launch: Launch(unitID: AppKeys.admobDefaultUnitID, displayInterval: 15, timeout: 10, display: true), homeBanner: HomeBanner(unitID: AppKeys.admobBannerID, display: true))
    
    class Launch {
        let timeout: TimeInterval
        let unitID: String
        let displayInterval: TimeInterval
        let display: Bool
        
        init(unitID: String, displayInterval: TimeInterval, timeout: TimeInterval = 10, display: Bool = true) {
            self.unitID = unitID
            self.displayInterval = displayInterval
            self.timeout = timeout
            self.display = display
        }
        
        init?(dictionary: [String: Any]) {
            guard let unitID = dictionary["unit_id"] as? String, !unitID.isEmpty else {
                return nil
            }
            self.unitID = unitID
            if let di = (dictionary["display_interval"] as? NSNumber)?.doubleValue {
                self.displayInterval = di
            } else {
                self.displayInterval = 15
            }
            if let ti = (dictionary["timeout_interval"] as? NSNumber)?.doubleValue {
                self.timeout = ti
            } else {
                self.timeout = 10
            }
            display = (dictionary["display"] as? NSNumber)?.boolValue ?? false
        }
    }
    
    class HomeBanner {
        let unitID: String
        let display: Bool
        
        init(unitID: String, display: Bool) {
            self.unitID = unitID
            self.display = display
        }
        
        init?(dictionary: [String: Any]) {
            guard let unitID = dictionary["unit_id"] as? String, !unitID.isEmpty else {
                return nil
            }
            self.unitID = unitID
            if let d = (dictionary["display"] as? NSNumber)?.boolValue {
                self.display = d
            } else {
                self.display = true
            }
        }
    }
    
    let appID: String
    let launch: Launch?
    let homeBanner: HomeBanner?
    
    fileprivate init?(dictionary: [String: Any]) {
        guard let appID = dictionary["app_id"] as? String, !appID.isEmpty else {
            return nil
        }
        self.appID = appID
        if let ld = dictionary["launch"] as? [String: Any] {
            self.launch = Launch(dictionary: ld)
        } else {
            self.launch = nil
        }
        if let hbd = dictionary["home_banner"] as? [String: Any] {
            self.homeBanner = HomeBanner(dictionary: hbd)
        } else {
            self.homeBanner = nil
        }
    }
    
    fileprivate init(appID: String, launch: Launch?, homeBanner: HomeBanner?) {
        self.appID = appID
        self.launch = launch
        self.homeBanner = homeBanner
    }
}
