//
//  HistoryViewController.swift
//  FancyVideo
//
//  Created by SunJiangting on 2018/3/1.
//  Copyright © 2018年 Samaritan. All rights reserved.
//

import UIKit

class HistoryViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    
    typealias DataItem = (date: String, items: [URLStorageItem])

    fileprivate lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.separatorStyle = .none
        tableView.backgroundView = nil
        tableView.backgroundColor = nil
        tableView.tableFooterView = UIView(frame: .zero)
        if #available(iOS 11.0, *) {
            tableView.contentInsetAdjustmentBehavior = .never
        }
        return tableView
    }()
    
    // 反馈 | 评价
    fileprivate lazy var items: [DataItem] = []
    
    fileprivate lazy var placeholderView = PlaceholderView()
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.hidesBottomBarWhenPushed = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "MyHistory".localized()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Clear".localized(), style: .plain, target: self, action: #selector(handleClear))
        
        placeholderView.contentOffset = CGPoint(x: 0, y: -40)
        
        tableView.frame = self.view.bounds
        tableView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        tableView.dataSource = self
        tableView.delegate = self
        self.view.addSubview(tableView)
        tableView.register(Cell.self, forCellReuseIdentifier: "Identifier")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        syncStorage()
    }
    
    @objc private func handleClear() {
        let alertController = UIAlertController(title: "ClearHistoryTitle".localized(), message: nil, preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "OK".localized(), style: .cancel, handler: nil))
        let confirmAction = UIAlertAction(title: "Clear".localized(), style: .destructive) { [weak self] (_) in
            guard let strongSelf = self else {
                return
            }
            LocalURLStorage.history.removeAll()
            strongSelf.syncStorage()
        }
        alertController.addAction(confirmAction)
        present(alertController, animated: true, completion: nil)
    }
    
    private func syncStorage() {
        let sortedItems = LocalURLStorage.history.allItems.sorted(by: {$0.date.timeIntervalSince1970 > $1.date.timeIntervalSince1970})
        // 按照时间分开
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        
        let today = formatter.string(from: Date())
        let yesterday = formatter.string(from: Date(timeIntervalSince1970: Date().timeIntervalSince1970 - 86400))
        let yesterdayBefore = formatter.string(from: Date(timeIntervalSince1970: Date().timeIntervalSince1970 - 86400 * 2))
        
        func name(for date: Date) -> String {
            let result = formatter.string(from: date)
            if result == today {
                return "Today".localized()
            }
            if result == yesterday {
                return "Yesterday".localized()
            }
            if result == yesterdayBefore {
                return "YesterdayBefore".localized()
            }
            return result
        }
        
        var newItems = [DataItem]()
        for item in sortedItems {
            let dateString = name(for: item.date)
            if let idx = newItems.index(where: {$0.0 == dateString}) {
                var items = newItems[idx].1
                items.append(item)
                newItems[idx] = (dateString, items)
            } else {
                newItems.append((dateString, [item]))
            }
        }
        self.items = newItems
        tableView.reloadData()
        updatePlaceholderView()
        self.navigationItem.rightBarButtonItem?.isEnabled = !newItems.isEmpty
    }
    
    private func updatePlaceholderView() {
        if items.isEmpty {
            placeholderView.state = .case(.empty, "HistoryEmpty".localized(), nil)
            placeholderView.frame = tableView.bounds
            tableView.addSubview(placeholderView)
        } else {
            placeholderView.state = .normal
            placeholderView.removeFromSuperview()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
//
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        return nil
//    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return items[section].date
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let url = items[indexPath.section].items[indexPath.row].url
        let webVC = BrowserViewController(url: url)
        self.st_safariController?.show(webVC, animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items[section].items.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Identifier") as! Cell
        cell.item = items[indexPath.section].items[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let item = items[indexPath.section].items[indexPath.row]
            LocalURLStorage.history.remove(item.url)
            syncStorage()
        }
    }
    
    class Cell: TableViewCell {
        
        var item: URLStorageItem? {
            didSet {
                nameLabel.text = item?.name
                urlLabel.text = item?.url.absoluteString
            }
        }
        
        private let nameLabel = UILabel(frame: .zero)
        private let urlLabel = UILabel(frame: .zero)
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            
            nameLabel.textColor = UIColor(rgb: 0x555555)
            nameLabel.font = UIFont.boldSystemFont(ofSize: 16)
            self.contentView.addSubview(nameLabel)
            
            urlLabel.textColor = UIColor(rgb: 0x717171)
            urlLabel.font = UIFont.systemFont(ofSize: 14)
            self.contentView.addSubview(urlLabel)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            nameLabel.frame = CGRect(x: 12, y: 8, width: self.contentView.bounds.width - 24, height: 20)
            urlLabel.frame = CGRect(x: 12, y: 36, width: self.contentView.bounds.width - 24, height: 16)
        }
    }
}
