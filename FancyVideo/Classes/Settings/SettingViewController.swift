//
//  SettingViewController.swift
//  FancyVideo
//
//  Created by SunJiangting on 2018/2/26.
//  Copyright © 2018年 Samaritan. All rights reserved.
//

import UIKit

class SettingViewController: BaseViewController {
    
    fileprivate lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.separatorStyle = .none
        tableView.backgroundView = nil
        tableView.backgroundColor = nil
        tableView.tableFooterView = UIView(frame: .zero)
        if #available(iOS 11.0, *) {
            tableView.contentInsetAdjustmentBehavior = .never
        }
        return tableView
    }()
    
    // 反馈 | 评价
    fileprivate lazy var items: [Item] = []
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "SettingTitle".localized()
        
        var placeAtTopItem = Item(name: "MyPlaceAtTop".localized(), detail: nil, type: .disclosureIndicator, selection: Item.Selection.enabled({ [weak self] (_) in
            guard let strongSelf = self else {
                return
            }
            let vc = PlaceAtTopViewController()
            strongSelf.st_navigationController?.push(viewController: vc, animated: true)
        }))
        placeAtTopItem.isSeparatorHidden = false
        items.append(placeAtTopItem)
        
        var historyItem = Item(name: "MyHistory".localized(), detail: nil, type: .disclosureIndicator, selection: Item.Selection.enabled({ [weak self] (_) in
            guard let strongSelf = self else {
                return
            }
            let vc = HistoryViewController()
            strongSelf.st_navigationController?.push(viewController: vc, animated: true)
        }))
        historyItem.isSeparatorHidden = false
        items.append(historyItem)
        
        var favoriteItem = Item(name: "MyFavorite".localized(), detail: nil, type: .disclosureIndicator, selection: Item.Selection.enabled({ [weak self] (_) in
            guard let strongSelf = self else {
                return
            }
            let vc = FavoriteViewController()
            strongSelf.st_navigationController?.push(viewController: vc, animated: true)
        }))
        favoriteItem.isSeparatorHidden = false
        items.append(favoriteItem)
        
        var versionItem = Item(name: "SettingVersion".localized(), detail: getAppVersion(), type: .detail, selection: .disabled)
        versionItem.isSeparatorHidden = true
        items.append(versionItem)
        
        tableView.frame = self.view.bounds
        tableView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        tableView.dataSource = self
        tableView.delegate = self
        self.view.addSubview(tableView)
        tableView.register(Cell.self, forCellReuseIdentifier: "Identifier")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        updateStatusBar()
    }
    
    override func st_viewDidAppear(_ animated: Bool) {
        updateStatusBar()
    }
}

extension SettingViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Identifier") as! Cell
        cell.item = items[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let item = items[indexPath.row]
        if let selection = item.selection {
            switch selection {
            case .enabled(let handler):
                handler?(item)
            default:
                break
            }
        }
    }
}

fileprivate extension SettingViewController {
    
    fileprivate struct Item {
        
        public enum Selection {
            case disabled
            case enabled(((Item)->Void)?)
        }
        
        fileprivate enum CellType {
            case detail, disclosureIndicator
        }
        
        fileprivate let name: String
        fileprivate let type: CellType
        fileprivate let detail: String?
        fileprivate var selection: Selection?
        var isSeparatorHidden: Bool = false
        
        init(name: String, detail: String? = nil, type: CellType, selection: Selection?) {
            self.name = name
            self.detail = detail
            self.type = type
            self.selection = selection
        }
    }
}

fileprivate extension SettingViewController {
    
    fileprivate class Cell: TableViewCell {
        
        fileprivate var item: Item? {
            didSet {
                
                if let item = item {
                    if let selection = item.selection, case .enabled(_) = selection {
                        self.selectedEnabled = true
                    } else {
                        self.selectedEnabled = false
                    }
                    nameLabel.text = item.name
                    switch item.type {
                    case .detail:
                        detailLabel.text = item.detail
                        detailLabel.isHidden = false
                        accessoryImageView.isHidden = true
                    case .disclosureIndicator:
                        detailLabel.text = nil
                        detailLabel.isHidden = true
                        accessoryImageView.isHidden = false
                    }
                    self.separatorView.isHidden = item.isSeparatorHidden
                } else {
                    nameLabel.text = nil
                    detailLabel.text = nil
                    detailLabel.isHidden = true
                    accessoryImageView.isHidden = true
                    self.separatorView.isHidden = false
                    self.selectedEnabled = false
                }
            }
        }
        
        fileprivate let nameLabel = UILabel(frame: .zero)
        fileprivate let detailLabel = UILabel(frame: .zero)
        fileprivate let accessoryImageView = UIImageView(frame: .zero)
        
        fileprivate override func loadSubviews() {
            super.loadSubviews()
            
            nameLabel.font = UIFont.systemFont(ofSize: 16)
            nameLabel.textColor = UIColor.st_color(rgb: 0x585858)
            self.contentView.addSubview(nameLabel)
            
            detailLabel.font = UIFont.systemFont(ofSize: 14)
            detailLabel.textAlignment = .right
            detailLabel.textColor = UIColor.st_color(rgb: 0x585858)
            detailLabel.isHidden = true
            self.contentView.addSubview(detailLabel)
            
            accessoryImageView.image = UIImage(named: "arrow_right")
            accessoryImageView.isHidden = true
            accessoryImageView.sizeToFit()
            self.contentView.addSubview(accessoryImageView)
            
            self.selectedView.backgroundColor = UIColor.st_color(rgb: 0xDDDDDD)
            separatorViewInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        }
        
        fileprivate override func layoutSubviews() {
            super.layoutSubviews()
            nameLabel.frame = CGRect(x: 30, y: 0, width: self.width - 30 * 2, height: self.height)
            detailLabel.frame = CGRect(x: 30, y: 0, width: self.width - 30 * 2, height: self.height)
            accessoryImageView.frame = CGRect(x: self.width - accessoryImageView.width - 30, y: (self.height - accessoryImageView.height) / 2, width: accessoryImageView.width, height: accessoryImageView.height)
        }
    }
}
