//
//  FavoriteViewController.swift
//  FancyVideo
//
//  Created by SunJiangting on 2018/3/1.
//  Copyright © 2018年 Samaritan. All rights reserved.
//

import UIKit

class FavoriteViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {

    fileprivate lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.separatorStyle = .none
        tableView.backgroundView = nil
        tableView.backgroundColor = nil
        tableView.tableFooterView = UIView(frame: .zero)
        if #available(iOS 11.0, *) {
            tableView.contentInsetAdjustmentBehavior = .never
        }
        return tableView
    }()
    
    // 反馈 | 评价
    fileprivate lazy var items: [URLStorageItem] = []
    
    fileprivate lazy var placeholderView = PlaceholderView()
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.hidesBottomBarWhenPushed = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "MyFavorite".localized()
        
        tableView.frame = self.view.bounds
        tableView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        tableView.dataSource = self
        tableView.delegate = self
        self.view.addSubview(tableView)
        tableView.register(Cell.self, forCellReuseIdentifier: "Identifier")
        placeholderView.contentOffset = CGPoint(x: 0, y: -40)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        syncStorage()
    }
    
    private func syncStorage() {
        items = LocalURLStorage.favorite.allItems
        tableView.reloadData()
        updatePlaceholderView()
    }
    
    private func updatePlaceholderView() {
        if items.isEmpty {
            placeholderView.state = .case(.empty, "FavoriteEmpty".localized(), nil)
            placeholderView.frame = tableView.bounds
            tableView.addSubview(placeholderView)
        } else {
            placeholderView.state = .normal
            placeholderView.removeFromSuperview()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Identifier") as! Cell
        cell.item = items[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let item = items[indexPath.row]
            LocalURLStorage.favorite.remove(item.url)
            syncStorage()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let url = items[indexPath.row].url
        let webVC = BrowserViewController(url: url)
        self.st_safariController?.show(webVC, animated: true)
    }
    
    class Cell: TableViewCell {
        
        var item: URLStorageItem? {
            didSet {
                nameLabel.text = item?.name
                urlLabel.text = item?.url.absoluteString
            }
        }
        
        private let nameLabel = UILabel(frame: .zero)
        private let urlLabel = UILabel(frame: .zero)
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            
            nameLabel.textColor = UIColor(rgb: 0x555555)
            nameLabel.font = UIFont.boldSystemFont(ofSize: 16)
            self.contentView.addSubview(nameLabel)
            
            urlLabel.textColor = UIColor(rgb: 0x717171)
            urlLabel.font = UIFont.systemFont(ofSize: 14)
            self.contentView.addSubview(urlLabel)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            nameLabel.frame = CGRect(x: 12, y: 8, width: self.contentView.bounds.width - 24, height: 20)
            urlLabel.frame = CGRect(x: 12, y: 36, width: self.contentView.bounds.width - 24, height: 16)
        }
    }

}
