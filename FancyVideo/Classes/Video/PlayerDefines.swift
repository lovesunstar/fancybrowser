//
//  PlayerDefines.swift
//  VEnjoy
//
//  Created by SunJiangting on 2016/12/13.
//  Copyright © 2016年 Suen. All rights reserved.
//

import UIKit

protocol VideoEmbeddable: NSObjectProtocol {
    func embed(_ player: UIView)
    func didRemovePlayer()
}

struct Video {
    class Button: UIButton {
        var touchInsets: UIEdgeInsets = .zero
        override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
            if touchInsets == .zero {
                return super.point(inside: point, with: event)
            }
            var bounds = self.bounds
            bounds.origin.x += touchInsets.left
            bounds.origin.y += touchInsets.top
            bounds.size.width -= (touchInsets.left + touchInsets.right)
            bounds.size.height -= (touchInsets.top + touchInsets.bottom)
            return self.isUserInteractionEnabled && self.isEnabled && self.alpha > 0.01 && bounds.contains(point)
        }
    }
    
    struct UI {
        enum PlayStyle: Int {
            case native
            case web
        }
    }
}
