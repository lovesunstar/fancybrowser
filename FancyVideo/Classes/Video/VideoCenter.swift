//
//  VideoCenter.swift
//  NewsMaster
//
//  Created by SunJiangting on 16/1/18.
//  Copyright © 2016年 Suen. All rights reserved.
//

import UIKit
import AVFoundation
import SafariServices

final class VideoCenter: NSObject {
    
    static let shared = VideoCenter()
    
    let player = VideoPlayer(frame: .zero)
    
    var playerInViewController: UIViewController? {
        return player.nextResponder(UIViewController.self)
    }
    
    private(set) var videoURL: URL?
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    private override init() {
        super.init()
    }
    
    func play(_ url: URL, title: String? = nil) {
        if self.videoURL != nil && url == self.videoURL {
            player.play()
            return
        }
        player.stop()
        self.videoURL = url
        player.play(url, title: title, autoPlay: true)
        
        UIApplication.shared.isIdleTimerDisabled = true
    }
    
    func embed(in view: VideoEmbeddable) {
        if let previousEmbeded = player.nextResponder(VideoEmbeddable.self) {
            if let pv = previousEmbeded as? UIView, pv == (view as? UIView) {
                return
            }
            previousEmbeded.didRemovePlayer()
        }
        view.embed(player)
    }
    
    func `continue`() {
        player.play()
    }
    
    func pause() {
        player.pause()
    }
    
    func stop() {
        player.stop()
        self.videoURL = nil
        if let embed = VideoCenter.shared.player.nextResponder(VideoEmbeddable.self) {
            player.removeFromSuperview()
            embed.didRemovePlayer()
        } else {
            player.removeFromSuperview()
        }
        UIApplication.shared.isIdleTimerDisabled = false
    }
    
    var isPlaying: Bool {
        return player.isPlaying
    }
    
    var isTitleVisibleWhenNotFullscreen = true {
        didSet {
            player.isTitleVisibleWhenNotFullscreen = isTitleVisibleWhenNotFullscreen
        }
    }
}
