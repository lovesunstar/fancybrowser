//
//  VideoSettingView.swift
//  BiuTube
//
//  Created by SunJiangting on 2017/4/25.
//  Copyright © 2017年 Suen. All rights reserved.
//

import UIKit

enum VideoPlayerSpeed: Int {
    case slower, slow, normal, fast, faster, fastest
    
    var value: Double {
        switch self {
        case .slower:
            return 0.25
        case .slow:
            return 0.5
        case .normal:
            return 1.0
        case .fast:
            return 1.5
        case .faster:
            return 2.0
        case .fastest:
            return 2.5
        }
    }
    
    var buttonText: String {
        switch self {
        case .slower:
            return "0.25X"
        case .slow:
            return "0.5X"
        case .normal:
            return "1.0X"
        case .fast:
            return "1.5X"
        case .faster:
            return "2.0X"
        case .fastest:
            return "2.5X"
        }
    }
    
    static let availableSpeeds: [VideoPlayerSpeed] = [.slower, .slow, .normal, .fast, .faster]
}

class VideoSettingView: UIView {
    
    fileprivate class SpeedButton: Video.Button {
        
        fileprivate let speed: VideoPlayerSpeed
        
        init(frame: CGRect, speed: VideoPlayerSpeed) {
            self.speed = speed
            super.init(frame: frame)
            
            setTitleShadowColor(UIColor.black.withAlphaComponent(0.7), for: .normal)
            setTitleShadowColor(UIColor.black.withAlphaComponent(0.7), for: .selected)
            
            self.titleLabel?.shadowColor = UIColor.black.withAlphaComponent(0.7)
            self.titleLabel?.shadowOffset = CGSize(width: 0, height: 1)
            self.titleLabel?.layer.shadowRadius = 2
            
            self.touchInsets = UIEdgeInsets(top: -20, left: 0, bottom: -20, right: 0)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
    }
    
    enum Results {
        case cancelled
        case changed
    }
    
    struct UI {
        static let itemSize = CGSize(width: 80, height: 30)
    }
    
    var dismissHandler: ((VideoSettingView, Results)->Void)?
    
    let speedView = UIView(frame: .zero)
    let gradientLayer = CAGradientLayer()
    
    private var speedButtons: [SpeedButton] = []
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.layer.addSublayer(gradientLayer)
        gradientLayer.colors = [UIColor.clear.cgColor, UIColor.black.withAlphaComponent(0.5).cgColor]
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 0, y: 1)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        addGestureRecognizer(tapGesture)

        addSubview(speedView)
        
        VideoPlayerSpeed.availableSpeeds.forEach { (speed) in
            let speedButton = SpeedButton(frame: .zero, speed: speed)
            speedButton.setTitle(speed.buttonText, for: .normal)
            speedView.addSubview(speedButton)
            speedButton.addTarget(self, action: #selector(handleSpeedChanged(_:)), for: .touchUpInside)
            speedButton.layer.cornerRadius = UI.itemSize.height * 0.5
            speedButtons.append(speedButton)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc private func handleTap(_ tap: UITapGestureRecognizer) {
        cancel()
    }
    
    private func cancel() {
        removeFromSuperview()
        dismissHandler?(self, .cancelled)
    }
    
    @objc private func handleSpeedChanged(_ button: SpeedButton) {
        self.speed = button.speed
        removeFromSuperview()
        dismissHandler?(self, .changed)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        gradientLayer.frame = self.bounds
        let speedViewWidth = UI.itemSize.width * CGFloat(speedButtons.count)
        let speedViewHeight = UI.itemSize.height * 2
        speedView.frame = CGRect(x: (self.bounds.width - speedViewWidth) * 0.5, y: (self.bounds.height - speedViewHeight) * 0.5, width: speedViewWidth, height: speedViewHeight)
        for (idx, button) in speedButtons.enumerated() {
            button.frame = CGRect(x: CGFloat(idx) * UI.itemSize.width, y: (speedViewHeight - UI.itemSize.height) * 0.5 , width: UI.itemSize.width, height: UI.itemSize.height)
        }
    }
    
    var speed: VideoPlayerSpeed = .normal {
        didSet {
            speedButtons.forEach({
                $0.setTitleColor(UIColor.white, for: .normal)
                $0.layer.borderWidth = 0
                $0.layer.borderColor = nil
            })
            guard let selectedButton = speedButtons.first(where: { $0.speed == speed} ) else {
                return
            }
            selectedButton.setTitleColor(UIColor(rgb: 0xE44C47), for: .normal)
            selectedButton.layer.borderColor = UIColor(rgb: 0xE44C47).cgColor
            selectedButton.layer.borderWidth = 2
        }
    }
}
