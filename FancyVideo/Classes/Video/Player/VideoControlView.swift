//
//  VideoControlView.swift
//  YTVideo
//
//  Created by SunJiangting on 2017/4/7.
//  Copyright © 2016年 Suen. All rights reserved.
//

import UIKit
import MediaPlayer

final class VideoControlView: UIView {
    
    enum State: Equatable {
        case loading
        case play, stalled ,paused, end
        case error(Error)
        
        var canHideControlView: Bool {
            switch self {
            case .play, .paused, .end:
                return true
            default:
                return false
            }
        }
        
        var isPaused: Bool {
            switch self {
            case .paused:
                return true
            default:
                return false
            }
        }
        
        var showsMoreButton: Bool {
            switch self {
            case .play, .paused:
                return true
            default:
                return false
            }
        }
        
        var isGestureEnabled: Bool {
            switch self {
            case .play, .paused, .end, .stalled:
                return true
            default:
                return false
            }
        }
        
        public static func ==(lhs: State, rhs: State) -> Bool {
            switch (lhs, rhs) {
            case (.loading, .loading):
                return true
            case (.play, .play):
                return true
            case (.stalled, .stalled):
                return true
            case (.paused, .paused):
                return true
            case (.end, .end):
                return true
            case (.error(_), .error(_)):
                return true
            default:
                return false
            }
        }
        
    }
    
    var duration: TimeInterval = 0 {
        didSet {
            miniSlider.maximumValue = CGFloat(duration)
            controlBar.duration = duration
        }
    }
    
    var time: TimeInterval = 0 {
        didSet {
            miniSlider.value = CGFloat(time)
            controlBar.time = time
        }
    }
    
    var bufferedTime: TimeInterval = 0 {
        didSet {
            controlBar.bufferedTime = bufferedTime
            miniSlider.bufferedValue = CGFloat(bufferedTime)
        }
    }
    
    var title: String? = nil {
        didSet {
            self.titleLabel.text = title
            setNeedsLayout()
        }
    }
    
    var isBackButtonHidden: Bool = true {
        didSet {
            backButton.isHidden = isBackButtonHidden
            setNeedsLayout()
        }
    }
    
    private lazy var topView = UIView(frame: .zero)
    private lazy var titleBkgView = UIImageView(frame: .zero)
    
    let titleLabel = UILabel(frame: .zero)
    
    let backButton = Video.Button(frame: .zero)
    
    let controlBar = Toolbar(frame: .zero)
    
    var isActionButtonHidden: Bool = false
    
    var isTitleViewHidden: Bool {
        return self.isHidden || topView.isHidden
    }
    
    let playButton = UIButton(frame: .zero)
    let pauseButton = UIButton(frame: .zero)
    let replayButton = UIButton(frame: .zero)
    var retryButton: UIButton {
        return errorView.actionButton
    }
    
    fileprivate let errorView = VideoErrorView(frame: .zero)
    fileprivate let firstFrameLoadingView = DotLoadingView(frame: .zero)
    
    let miniSlider = MiniSlider(frame: .zero)
    
    let playIndicator = UIActivityIndicatorView(activityIndicatorStyle: .white)
    
    let volumePanGesture = UIPanGestureRecognizer()
    let progressPanGesture = UIPanGestureRecognizer()
    
    let trackingGestures: [UIGestureRecognizer]
    
    let tapGesture = UITapGestureRecognizer()
    
    private var volumePanStartPoint = CGPoint.zero
    private var turningPanStartPoint = CGPoint.zero
    private var startVolume: CGFloat = 0
    private var panGestureTracking = false
    
    private let systemVolumeView: MPVolumeView = {
        let volumeValue = MPVolumeView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        return volumeValue
    }()
    
    private func systemVolumeSlider() -> UISlider? {
        for view in systemVolumeView.subviews {
            if let slider = view as? UISlider {
                return slider
            }
        }
        return nil
    }
    private func systemVolumeValue() -> CGFloat {
        return CGFloat(systemVolumeSlider()?.value ?? 0.0)
    }
    
    private func updateSystemVolume(_ newValue: CGFloat) {
        guard let slider = systemVolumeSlider() else {
            return
        }
        slider.setValue(Float(newValue), animated: false)
        slider.sendActions(for: .valueChanged)
    }
    
    private let progressIndicator = ProgressIndicator(frame: CGRect(x: 0, y: 0, width: 70, height: 35), duration: 0)
    private let volumeIndicator = VolumeIndicator(frame: .zero)
    
    private var interrupted = false
    
    var isFullscreen: Bool = false {
        didSet {
            setNeedsLayout()
            if isFullscreen {
                DispatchQueue.main.asyncAfter(delay: 0.1) {
                    self.showMoreGuideIfNeeded()
                }
            }
        }
    }
    
    private(set) var moreButton = Video.Button(frame: .zero)
    
    var isMinimumState: Bool = false {
        didSet {
            if isMinimumState {
                [playButton, pauseButton, controlBar, topView].forEach({$0.alpha = 0})
            } else {
                [playButton, pauseButton, controlBar, topView].forEach({$0.alpha = 1})
            }
        }
    }
    
    var state: State = .paused {
        didSet {
            let center = CGPoint(x: self.bounds.midX, y: self.bounds.midY)
            switch state {
            case .loading:
                errorView.removeFromSuperview()
                playButton.removeFromSuperview()
                pauseButton.removeFromSuperview()
                replayButton.removeFromSuperview()
                playIndicator.stopAnimating()
                playIndicator.removeFromSuperview()
                moreButton.removeFromSuperview()
                
                addSubview(firstFrameLoadingView)
                firstFrameLoadingView.startAnimating()
                controlBar.slider.isEnabled = false
                bringSubview(toFront: topView)
                bringSubview(toFront: controlBar)
            case .play:
                errorView.removeFromSuperview()
                firstFrameLoadingView.stopAnimating()
                firstFrameLoadingView.removeFromSuperview()
                pauseButton.removeFromSuperview()
                replayButton.removeFromSuperview()
                playIndicator.removeFromSuperview()
                playIndicator.stopAnimating()
                playButton.center = center
                addSubview(playButton)
                topView.addSubview(moreButton)
                controlBar.slider.isEnabled = true
                if isFullscreen {
                    showMoreGuideIfNeeded()
                }
            case .paused:
                errorView.removeFromSuperview()
                firstFrameLoadingView.stopAnimating()
                firstFrameLoadingView.removeFromSuperview()
                playButton.removeFromSuperview()
                replayButton.removeFromSuperview()
                playIndicator.removeFromSuperview()
                playIndicator.stopAnimating()
                pauseButton.center = center
                addSubview(pauseButton)
                topView.addSubview(moreButton)
                controlBar.slider.isEnabled = true
                if isFullscreen {
                    showMoreGuideIfNeeded()
                }
            case .end:
                errorView.removeFromSuperview()
                firstFrameLoadingView.stopAnimating()
                firstFrameLoadingView.removeFromSuperview()
                playButton.removeFromSuperview()
                pauseButton.removeFromSuperview()
                playIndicator.removeFromSuperview()
                playIndicator.stopAnimating()
                replayButton.center = center
                addSubview(replayButton)
                moreButton.removeFromSuperview()
                controlBar.slider.isEnabled = true
            case .stalled:
                errorView.removeFromSuperview()
                firstFrameLoadingView.stopAnimating()
                firstFrameLoadingView.removeFromSuperview()
                playButton.removeFromSuperview()
                pauseButton.removeFromSuperview()
                replayButton.removeFromSuperview()
                playIndicator.startAnimating()
                playIndicator.center = center
                addSubview(playIndicator)
                topView.addSubview(moreButton)
                controlBar.slider.isEnabled = true
            case .error(let error):
                firstFrameLoadingView.stopAnimating()
                firstFrameLoadingView.removeFromSuperview()
                playButton.removeFromSuperview()
                pauseButton.removeFromSuperview()
                replayButton.removeFromSuperview()
                playIndicator.stopAnimating()
                playIndicator.removeFromSuperview()
                moreButton.removeFromSuperview()
                errorView.update(message: error.localizedDescription, buttonText: "Retry".localized())
                addSubview(errorView)
                
                bringSubview(toFront: topView)
                bringSubview(toFront: controlBar)
                
                controlBar.slider.isEnabled = false
            }
        }
    }
    
    override init(frame: CGRect) {
        
        trackingGestures = [progressPanGesture, volumePanGesture]
        
        super.init(frame: frame)
        
        trackingGestures.forEach({ $0.isEnabled = false })
        addSubview(topView)
        
        backButton.isHidden = true
        backButton.touchInsets = UIEdgeInsetsMake(-20, -20, -20, -20)
        backButton.setImage(UIImage(named: "video_fullscreen_back"), for: .normal)
        backButton.imageView?.contentMode = .scaleAspectFit
        topView.addSubview(backButton)
        
        titleBkgView.image = UIImage(named: "video_cell_title_bkg")
        titleBkgView.contentMode = .scaleToFill
        topView.addSubview(titleBkgView)
        
        titleLabel.textColor = UIColor(rgb: 0xffffff)
        titleLabel.numberOfLines = 2
        titleLabel.font = UIFont.systemFont(ofSize: 18)
        let shadowWidth = 1.0 / max(1.0, UIScreen.main.scale)
        titleLabel.layer.shadowOffset = CGSize(width: shadowWidth, height: shadowWidth)
        titleLabel.layer.shadowRadius = shadowWidth
        titleLabel.layer.shadowColor = UIColor.black.withAlphaComponent(0.6).cgColor
        titleLabel.layer.shadowOpacity = 1.0
        titleBkgView.addSubview(titleLabel)
        topView.bringSubview(toFront: backButton)
        
        moreButton.setImage(UIImage(named: "video_fullscreen_more"), for: .normal)
        moreButton.touchInsets = UIEdgeInsets(top: -20, left: -20, bottom: -20, right: -20)
        moreButton.imageView?.contentMode = .scaleAspectFit
        topView.addSubview(moreButton)
        
        controlBar.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        addSubview(controlBar)
        
        progressIndicator.isHidden = true
        addSubview(progressIndicator)
        
        volumeIndicator.isHidden = true
        addSubview(volumeIndicator)
        
        controlBar.slider.addTarget(self, action: #selector(handleSlide(_:)), for: .valueChanged)
        
        let actionFrame = CGRect(x: 0, y: 0, width: 44, height: 44)
        
        playButton.frame = actionFrame
        playButton.setImage(UIImage(named: "video_play"), for: .normal)
        
        pauseButton.frame = actionFrame
        pauseButton.setImage(UIImage(named: "video_pause"), for: .normal)
        
        replayButton.frame = actionFrame
        replayButton.setImage(UIImage(named: "video_replay"), for: .normal)
        
        playIndicator.sizeToFit()
        playIndicator.color = UIColor(rgb: 0xE44C47)
        
        firstFrameLoadingView.sizeToFit()
        
        tapGesture.addTarget(self, action: #selector(handleTapGesture(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        addGestureRecognizer(tapGesture)
        
        progressPanGesture.delegate = self
        progressPanGesture.addTarget(self, action: #selector(handleProgressGesture(_:)))
        addGestureRecognizer(progressPanGesture)
        
        volumePanGesture.delegate = self
        volumePanGesture.addTarget(self, action: #selector(handleVolumeGesture(_:)))
        addGestureRecognizer(volumePanGesture)
        
        addSubview(miniSlider)
        
        state = .paused
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleVolumeChange(_:)), name: Notification.Name("AVSystemController_SystemVolumeDidChangeNotification"), object: nil)
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError() }
    
    @available(iOS 11.0, *)
    override func safeAreaInsetsDidChange() {
        super.safeAreaInsetsDidChange()
        setNeedsLayout()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        var safeAreaInsets: UIEdgeInsets
        if #available(iOS 11.0, *) {
            if Device.isIPhoneX {
                safeAreaInsets = UIEdgeInsets(top: 20, left: 34, bottom: isFullscreen ? 20 : 0, right: 34)
            } else {
                safeAreaInsets = UIEdgeInsets(top: 20, left: 10, bottom: 0, right: 15)
            }
        } else {
            safeAreaInsets = UIEdgeInsets(top: 20, left: 10, bottom: 0, right: 15)
        }
        
        let titleLeft: CGFloat
        
        if backButton.isHidden {
            backButton.frame = .zero
            titleLeft = safeAreaInsets.left
        } else {
            backButton.frame = CGRect(x: safeAreaInsets.left, y: 0, width: 12, height: 20)
            titleLeft = backButton.frame.maxX + 5
        }
        
        moreButton.isHidden = !isFullscreen
        let titleWidth: CGFloat
        
        if moreButton.isHidden {
            moreButton.frame = .zero
            titleWidth = self.bounds.width - safeAreaInsets.right - titleLeft
        } else {
            moreButton.frame = CGRect(x: self.bounds.width - 22 - safeAreaInsets.right, y: 20, width: 12, height: 24)
            titleWidth = moreButton.frame.minX - titleLeft - 10
        }

        titleLabel.frame = CGRect(x: titleLeft , y: 0, width: titleWidth, height: 80)
        titleLabel.sizeToFit()
        titleLabel.frame = CGRect(x: titleLeft, y: isFullscreen ? (safeAreaInsets.top) : 10, width: titleWidth, height: titleLabel.frame.height)
        
        // Layout
        if titleLabel.size.height > 0 {
            // button / more
            backButton.centerY = titleLabel.centerY
            moreButton.centerY = titleLabel.centerY
            topView.frame = CGRect(x: 0, y: 0, width: self.bounds.width, height: titleLabel.frame.maxY + (isFullscreen ? (20) : 10))
        } else {
            backButton.frame = CGRect(x: safeAreaInsets.left, y: safeAreaInsets.top, width: 12, height: 20)
            moreButton.frame = CGRect(x: self.bounds.width - 22 - safeAreaInsets.right, y: safeAreaInsets.top, width: 12, height: 24)
            topView.frame = CGRect(x: 0, y: 0, width: self.bounds.width, height: 60)
        }
        titleBkgView.frame = topView.bounds
        
        controlBar.frame = CGRect(x: 0, y: self.bounds.height - 40 - safeAreaInsets.bottom, width: self.bounds.width, height: 40 + safeAreaInsets.bottom)
        controlBar.update()
        let center = CGPoint(x: self.bounds.midX, y: self.bounds.midY)
        
        playButton.center = center
        pauseButton.center = center
        replayButton.center = center
        playIndicator.center = center
        
        var miniSliderSafeArea: UIEdgeInsets = .zero
        if isFullscreen && Device.isIPhoneX {
            if #available(iOS 11.0, *) {
                miniSliderSafeArea.left = 34
                miniSliderSafeArea.right = 34
            }
        }
        miniSlider.frame = CGRect(x: miniSliderSafeArea.left, y: self.bounds.height - 1.5, width: self.bounds.width - miniSliderSafeArea.left - miniSliderSafeArea.right, height: 1.5)
        miniSlider.update()
        
        progressIndicator.center = CGPoint(x: self.frame.width * 0.5, y: self.frame.height * 0.5)
        volumeIndicator.frame = CGRect(x: 24, y: (self.bounds.height - 80) / 2, width: 30, height: self.height / 3)
        
        errorView.frame = self.bounds
        firstFrameLoadingView.center = center
    }
    
    @objc private func handleSlide(_ slider: UISlider) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(hideControls), object: nil)
        perform(#selector(hideControls), with: nil, afterDelay: 5, inModes: [RunLoopMode.defaultRunLoopMode])
    }
    
    @objc private func hideControls() {
        if state.canHideControlView {
            hide()
        }
    }
    
    private func showMoreGuideIfNeeded() {
        guard isFullscreen && !isMinimumState, state.showsMoreButton, let moreSuperview = moreButton.superview else {
            return
        }
        if UIGuideManager.shared.hasShow(.videoMoreAction) {
            return
        }
        UIGuideManager.shared.setShow(true, for: .videoMoreAction)
        let guideView = TextArrowGuideView(direction: .up, text: "GuideTipVideoSpeed".localized())
        var pointCenter = moreSuperview.convert(moreButton.center, to: self)
        pointCenter.y += moreButton.frame.height * 0.5 + 8
        guideView.show(in: self, center: pointCenter)
    }
    
    func flash(for timeInterval: TimeInterval = 5) {
        interrupted = false
        show()
        perform(#selector(hideControls), with: nil, afterDelay: timeInterval, inModes: [RunLoopMode.defaultRunLoopMode])
    }
    
    func show() {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(hideControls), object: nil)
        isActionButtonHidden = false
        [topView, controlBar, playButton, pauseButton, replayButton, playIndicator].forEach({ $0.isHidden = false })
        miniSlider.isHidden = !isActionButtonHidden
        updateStatusBar()
        showMoreGuideIfNeeded()
    }
    
    func hide() {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(hideControls), object: nil)
        if controlBar.slider.isTracking {
            // 如果正在拖动， 则先不隐藏了
            interrupted = true
            return
        }
        interrupted = false
        isActionButtonHidden = true
        [topView, controlBar, playButton, pauseButton, replayButton, playIndicator].forEach({ $0.isHidden = true })
        miniSlider.value = CGFloat(time)
        miniSlider.isHidden = !isActionButtonHidden
        updateStatusBar()
    }
    
    func cancelFlash() {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(hideControls), object: nil)
    }
    
    @objc private func handleTapGesture(_ tap: UIGestureRecognizer) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(hideControls), object: nil)
        if !state.canHideControlView {
            show()
            return
        }
        if state.isPaused {
            if controlBar.isHidden {
                flash()
            } else {
                hideControls()
            }
        } else {
            if controlBar.isHidden {
                show()
            } else {
                hide()
            }
        }
    }
    
    @objc private func handleProgressGesture(_ gesture: UIPanGestureRecognizer) {
        switch gesture.state {
        case .began:
            panGestureTracking = true
            progressIndicator.isHidden = false
            progressIndicator.value = 0
            turningPanStartPoint = gesture.location(in: self)
            if !isActionButtonHidden {
                [playButton, pauseButton, replayButton].forEach({ $0.isHidden = true })
            }
        case .changed:
            handleProgressChange(with: gesture)
        case .ended, .cancelled:
            panGestureTracking = false
            progressIndicator.isHidden = true
            volumeIndicator.isHidden = true
            turningPanStartPoint = .zero
            notifyProgressPanGesture()
            [playButton, pauseButton, replayButton].forEach({ $0.isHidden = self.isActionButtonHidden })
        case _:
            break
        }
    }
    
    private func handleProgressChange(with gesture: UIPanGestureRecognizer) {
        let transitionX = gesture.location(in: self).x - turningPanStartPoint.x
        var targetValue: Double = 0.25 * Double(transitionX)
        targetValue = min(duration - time, max(-time, targetValue))
        progressIndicator.value = targetValue
    }
    
    private func notifyProgressPanGesture() {
        miniSlider.value += CGFloat(progressIndicator.value)
        controlBar.slider.value += CGFloat(progressIndicator.value)
        controlBar.slider.sendActions(for: .valueChanged)
    }
    
    @objc private func handleVolumeGesture(_ gesture: UIPanGestureRecognizer) {
        switch gesture.state {
        case .began:
            panGestureTracking = true
            volumePanStartPoint = gesture.location(in: self)
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(hideVolumeIndicator), object: nil)
            volumeIndicator.isHidden = false
            startVolume = systemVolumeValue()
            volumeIndicator.value = systemVolumeValue()
        case .changed:
            handleVolumeChange(with: gesture)
        case .ended, .cancelled:
            panGestureTracking = false
            progressIndicator.isHidden = true
            volumeIndicator.isHidden = true
        case _:
            break
        }
    }

    private func handleVolumeChange(with gesture: UIPanGestureRecognizer) {
        let transitionY = gesture.location(in: self).y - volumePanStartPoint.y
        let delta = -(1.0 / max(self.height / 2, 1)) * transitionY
        updateSystemVolume(startVolume + delta)
    }
    
    @objc private func hideVolumeIndicator() {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(hideVolumeIndicator), object: nil)
        volumeIndicator.isHidden = true
    }
    
    @objc private func handleVolumeChange(_ notification: Notification) {
        if let volumeValue = notification.userInfo?["AVSystemController_AudioVolumeNotificationParameter"] as? CGFloat {
            volumeIndicator.value = volumeValue
        } else {
            volumeIndicator.value = systemVolumeValue()
        }
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(hideVolumeIndicator), object: nil)
        if !panGestureTracking {
            if volumePanGesture.isEnabled {
                volumeIndicator.isHidden = false
                perform(#selector(hideVolumeIndicator), with: nil, afterDelay: 1.0)
            }
        }
    }
    
    func update() {
        miniSlider.setNeedsLayout()
        controlBar.setNeedsLayout()
        
        miniSlider.update()
        controlBar.update()
    }
}


extension VideoControlView: UIGestureRecognizerDelegate {
    
    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        guard let panGesture = gestureRecognizer as? UIPanGestureRecognizer, gestureRecognizer == progressPanGesture || gestureRecognizer == volumePanGesture else {
            return super.gestureRecognizerShouldBegin(gestureRecognizer)
        }
        guard state.isGestureEnabled else {
            return false
        }
        let velocity = panGesture.velocity(in: self)
        if abs(velocity.y) > abs(velocity.x) {
            return panGesture == volumePanGesture && volumePanGesture.isEnabled
        } else {
            return panGesture == progressPanGesture && progressPanGesture.isEnabled
        }
    }
}
