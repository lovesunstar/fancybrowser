//
//  VideoPlayerView.swift
//  YTPlayer
//
//  Created by SunJiangting on 16/3/7.
//  Copyright © 2016年 Suen. All rights reserved.
//

import UIKit
import AVFoundation

infix operator ≈: ComparisonPrecedence
infix operator !≈: ComparisonPrecedence

func ≈(lhs: Double, rhs: Double) -> Bool {
    return abs(lhs - rhs) < 0.0001
}

func ≈(lhs: Float, rhs: Float) -> Bool {
    return abs(lhs - rhs) < 0.0001
}

func !≈(lhs: Double, rhs: Double) -> Bool {
    return abs(lhs - rhs) >= 0.0001
}

func !≈(lhs: Float, rhs: Float) -> Bool {
    return abs(lhs - rhs) >= 0.0001
}

struct VideoLoadState: OptionSet {
    let rawValue: Int
    init(rawValue: Int) {
        self.rawValue = rawValue
    }
    public static let playthroughOK = VideoLoadState(rawValue: 1 << 0)
    public static let stalled = VideoLoadState(rawValue: 1 << 1)
}

enum VideoPlaybackState {
    case stopped, playing, paused, seeking
}

enum VideoFinishReason {
    case playbackEnded
    case playbackError(Error?)
    case userExited
}

protocol VideoPlayerViewDelegate: NSObjectProtocol {
    
    func didReadyToPlay(_ playerView: VideoPlayerView)
    
    func videoPlayerView(_ playerView: VideoPlayerView, didUpdateBufferedProgress progress: CGFloat)
    func videoPlayerView(_ playerView: VideoPlayerView, didUpdatePlayableTime time: TimeInterval)
    func videoPlayerView(_ playerView: VideoPlayerView, didPlayToTime time: TimeInterval)
    
    func videoPlayerView(_ playerView: VideoPlayerView, didChangeLoadState newState: VideoLoadState)
    func videoPlayerView(_ playerView: VideoPlayerView, didChangePlaybackState newState: VideoPlaybackState)
    
    func videoPlayerView(_ playerView: VideoPlayerView, didFinishPlayingWithReason reason: VideoFinishReason)
}

final class VideoPlayerView: UIView {

    fileprivate static let playerObserveKeyPaths: [String] = [
        #keyPath(AVPlayer.rate),
        #keyPath(AVPlayer.currentItem),
        #keyPath(AVPlayer.timeControlStatus)
    ]

    fileprivate static let playerItemObserveKeyPaths: [String] = [
        #keyPath(AVPlayerItem.status),
        #keyPath(AVPlayerItem.playbackLikelyToKeepUp),
        #keyPath(AVPlayerItem.loadedTimeRanges),
        #keyPath(AVPlayerItem.playbackBufferEmpty),
        #keyPath(AVPlayerItem.isPlaybackBufferFull)
    ]
    
    fileprivate let maximumHighWaterMarkInMilliSeconds: CGFloat = 15000
    
    private(set) var contentURL: URL?
    weak var delegate: VideoPlayerViewDelegate?
    
    fileprivate(set) var maximumPlayableTime: TimeInterval = 0
    fileprivate(set) var loadState: VideoLoadState = .stalled
    fileprivate(set) var playbackState: VideoPlaybackState = .stopped
    
    fileprivate(set) var bufferingProgress: CGFloat = 0
    fileprivate(set) var duration: TimeInterval = 0
    
    private var periodicTimeObserver: Any?
    private var observerContext: Int = 0
    fileprivate var model = Model()
    
    fileprivate var player = AVPlayer() {
        didSet {
            removeObserver(for: oldValue)
            addObserver(for: player)
            if #available(iOS 10.0, *) {
                player.automaticallyWaitsToMinimizeStalling = false
            }
        }
    }
    
    override class var layerClass: AnyClass {
        return AVPlayerLayer.self
    }
    
    override var contentMode: UIViewContentMode {
        didSet {
            let supportedModes: [UIViewContentMode] = [.scaleAspectFill, .scaleAspectFit, .scaleToFill, .center]
            switch contentMode {
            case (let mode) where supportedModes.contains(mode):
                break
            case _:
                if supportedModes.contains(oldValue) {
                    contentMode = oldValue
                } else {
                    contentMode = .scaleAspectFill
                }
            }
        }
    }
    
    fileprivate var playerItem: AVPlayerItem? {
        didSet {
            if let item = oldValue {
                removeObserver(for: item)
            }
            if let item = playerItem {
                addObserver(for: item)
            }
        }
    }
    
    deinit {
        if let item = playerItem {
            removeObserver(for: item)
        }
        removeObserver(for: player)
    }
    
    convenience override init(frame: CGRect) {
        self.init(frame: frame, contentURL: nil)
    }
    
    init(frame: CGRect, contentURL: URL?) {
        super.init(frame: frame)
        if let playerLayer = self.layer as? AVPlayerLayer {
            playerLayer.player = player
        }
        if #available(iOS 10.0, *) {
            player.automaticallyWaitsToMinimizeStalling = false
        }
        self.contentMode = .scaleAspectFill
        self.backgroundColor = UIColor.black
        addObserver(for: player)
        
        self.contentURL = contentURL
        if let URL = contentURL {
            playerItem = AVPlayerItem(url: URL as URL)
            player.replaceCurrentItem(with: playerItem)
        }
        if let item = playerItem {
            addObserver(for: item)
        }
        NotificationCenter.default.addObserver(self, selector: #selector(handleBackground(_:)), name: .UIApplicationDidEnterBackground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleActive(_:)), name: .UIApplicationDidBecomeActive, object: nil)
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError() }
    
    fileprivate var rateValue: Float = 0
    var rate: Float {
        set {
            if #available(iOS 10.0, *) {
                player.playImmediately(atRate: newValue)
            }
            player.rate = newValue
            rateValue = newValue
        }
        get {
            return rateValue
        }
    }
    
    func replace(contentURL: URL?) {
        if contentURL == self.contentURL {
            return
        }
        rateValue = 0
        model = Model()
        if let newURL = contentURL {
            let isPlayingBefore = isPlaying
            player.pause()
            playerItem = AVPlayerItem(url: newURL)
            player.replaceCurrentItem(with: playerItem)
            if isPlayingBefore {
                play()
            }
        } else {
            playerItem = nil
            player.replaceCurrentItem(with: nil)
        }
        self.contentURL = contentURL
    }
    
    // MARK: - KVO Callback
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if context == &observerContext {
            if let playerItem = object as? AVPlayerItem {
                if keyPath == #keyPath(AVPlayerItem.status) {
                    handleStatusChanged(playerItem)
                } else if keyPath == #keyPath(AVPlayerItem.playbackLikelyToKeepUp) {
                    handlePlaybackLikelyToKeepUpChanged(playerItem)
                } else if keyPath == #keyPath(AVPlayerItem.loadedTimeRanges) {
                    handleLoadedTimeRangesChanged(playerItem)
                } else if keyPath == #keyPath(AVPlayerItem.playbackBufferEmpty) {
                    handlePlaybackBufferEmptyChanged(playerItem)
                } else if keyPath == #keyPath(AVPlayerItem.isPlaybackBufferFull) {
                    handleIsPlaybackBufferFullChanged(playerItem)
                }
            } else if let player = object as? AVPlayer {
                if keyPath == #keyPath(AVPlayer.rate) {
                    handleRateChanged(player)
                } else if keyPath == #keyPath(AVPlayer.currentItem) {
                    handleCurrentItemChanged(player)
                } else if keyPath == #keyPath(AVPlayer.timeControlStatus) {
                    handleTimeStatusChanged(player)
                }
            }
        } else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }
    
    private func addObserver(for player: AVPlayer) {
        if player.st_observingByVideoPlayer {
            return
        }
        VideoPlayerView.playerObserveKeyPaths.forEach {
            player.addObserver(self, forKeyPath: $0, options: [.new, .old], context: &observerContext)
        }
        periodicTimeObserver = player.addPeriodicTimeObserver(forInterval: CMTime(seconds: 1, preferredTimescale: 1), queue: DispatchQueue.main) { [weak self] (time) -> Void in
            guard let strongSelf = self else {
                return
            }
            strongSelf.didPlay(to: strongSelf.currentTime)
        }
        player.st_observingByVideoPlayer = true
    }
    
    private func removeObserver(for player: AVPlayer) {
        if player.st_observingByVideoPlayer {
            if let timeObserver = periodicTimeObserver {
                player.removeTimeObserver(timeObserver)
            }
            VideoPlayerView.playerObserveKeyPaths.forEach {
                player.removeObserver(self, forKeyPath: $0)
            }
            player.st_observingByVideoPlayer = false
        }
    }
    
    private func addObserver(for playerItem: AVPlayerItem) {
        if playerItem.st_observingByVideoPlayer {
            return
        }
        let options: NSKeyValueObservingOptions = [.old, .new]
        VideoPlayerView.playerItemObserveKeyPaths.forEach {
            playerItem.addObserver(self, forKeyPath: $0, options: options, context: &observerContext)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleDidPlayToEndTime(_:)), name: .AVPlayerItemDidPlayToEndTime, object: playerItem)
        NotificationCenter.default.addObserver(self, selector: #selector(handleFailedToEndTime(_:)), name: .AVPlayerItemFailedToPlayToEndTime, object: playerItem)
        
        playerItem.st_observingByVideoPlayer = true
    }
    
    private func removeObserver(for playerItem: AVPlayerItem) {
        if !playerItem.st_observingByVideoPlayer {
            return
        }
        playerItem.cancelPendingSeeks()
        VideoPlayerView.playerItemObserveKeyPaths.forEach {
            playerItem.removeObserver(self, forKeyPath: $0)
        }
        
        NotificationCenter.default.removeObserver(self, name: .AVPlayerItemDidPlayToEndTime, object: playerItem)
        NotificationCenter.default.removeObserver(self, name: .AVPlayerItemFailedToPlayToEndTime, object: playerItem)
        
        playerItem.st_observingByVideoPlayer = false
    }
    
    private func didPlay(to time: TimeInterval) {
        delegate?.videoPlayerView(self, didPlayToTime: time)
    }
    
    fileprivate var currentLoadState: VideoLoadState {
        guard let playerItem = player.currentItem else {
            return VideoLoadState(rawValue: 0)
        }
        if !model.isPrepareToPlay {
            return VideoLoadState(rawValue: 0)
        }
        if model.isSeeking {
            return .stalled
        }
        if #available(iOS 10.0, *) {
            if player.timeControlStatus == .waitingToPlayAtSpecifiedRate {
                return .stalled
            }
        }
        if playerItem.isPlaybackLikelyToKeepUp {
            return .playthroughOK
        }
        if playerItem.isPlaybackBufferEmpty {
            if player.currentTime().isValid && (player.currentTime().seconds ≈ duration) && (duration > 0) {
                return .playthroughOK
            }
            return .stalled
        }
        if (player.rate !≈ 0 && rateValue ≈ 0) || (rateValue !≈ 0 && rateValue == player.rate) || playerItem.isPlaybackBufferFull {
            return .playthroughOK
        }
        if player.rate ≈ 0 {
            return .stalled
        }
        return VideoLoadState(rawValue: 0)
    }
    
    fileprivate var currentPlaybackState: VideoPlaybackState {
        if !model.isPrepareToPlay {
            return .stopped
        }
        if model.isFinished {
            return .stopped
        }
        if isPlaying {
            return .playing
        }
        if model.isBuffering && !model.isPausedByUser {
            return .playing
        }
        return .paused
    }
    
    fileprivate class Model: NSObject {
        
        fileprivate var isFinished = false
        
        fileprivate var isSeeking = false
        fileprivate var seekingTime: TimeInterval = 0
        
        fileprivate var isPausedByUser: Bool = false
        
        fileprivate var isPrepareToPlay = false
        
        fileprivate var isBuffering = false
        
        fileprivate var bufferedTimes: Int = 0
        
        fileprivate static let maximumAllowsBufferedTimes: Int = 15
    }
}

// MARK: - Application State
extension VideoPlayerView {
    
    @objc fileprivate func handleBackground(_ notification: Notification) {
        if let playerLayer = self.layer as? AVPlayerLayer {
            playerLayer.player = nil
        }
    }
    
    @objc fileprivate func handleActive(_ notification: Notification) {
        if let playerLayer = self.layer as? AVPlayerLayer {
            playerLayer.player = player
        }
    }
}

// MARK: - Play Actions
extension VideoPlayerView {
    
    func replay() {
        model.isFinished = false
        model.isPausedByUser = false
        model.isBuffering = false
        model.bufferedTimes = 0
        player.seek(to: kCMTimeZero)
        play()
    }
    
    func play() {
        if model.isFinished {
            player.seek(to: kCMTimeZero)
            model.isFinished = false
        }
        model.isBuffering = false
        model.bufferedTimes = 0
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(playAfterBufferTime), object: nil)
        model.isPausedByUser = false
        if rate !≈ 0 {
            if #available(iOS 10.0, *) {
                player.playImmediately(atRate: rate)
            }
            player.rate = rate
        } else {
            player.play()
        }
        
        if let url = self.contentURL {
            NotificationCenter.default.post(name: .videoPlayerPlay, object: nil, userInfo: ["url": url])
        } else {
            NotificationCenter.default.post(name: .videoPlayerPlay, object: nil, userInfo: nil)
        }
    }
    
    func pause() {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(playAfterBufferTime), object: nil)
        model.isPausedByUser = true
        model.isBuffering = false
        player.pause()
        
        if let url = self.contentURL {
            NotificationCenter.default.post(name: .videoPlayerPause, object: nil, userInfo: ["url": url])
        } else {
            NotificationCenter.default.post(name: .videoPlayerPause, object: nil, userInfo: nil)
        }
    }
    
    private func reset() {
        let oldContentURL = self.contentURL
        replace(contentURL: nil)
        if let playerLayer = self.layer as? AVPlayerLayer {
            player = AVPlayer()
            playerLayer.player = player
        }
        model.isSeeking = false
        model.isPausedByUser = false
        model.isBuffering = false
        model.bufferedTimes = 0
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(playAfterBufferTime), object: nil)
        if let url = oldContentURL {
            NotificationCenter.default.post(name: .videoPlayerDidFinishPlay, object: nil, userInfo: ["url": url])
        }
    }
    
    func stop() {
        reset()
        if !model.isFinished {
            model.isFinished = true
            delegate?.videoPlayerView(self, didFinishPlayingWithReason: .userExited)
        }
    }
    
    var isReady: Bool {
        return model.isPrepareToPlay
    }
    
    var isPlaying: Bool {
        return (player.rate !≈ 0)
    }
    
    func seek(to time: TimeInterval, autoplayWhenFinish autoplay: Bool = false) {
        guard let item = player.currentItem, item.status == .readyToPlay else { return }
        model.isSeeking = true
        model.seekingTime = time
        changeLoadState()
        if isPlaying {
            player.pause()
        }
        let timeScale = Int32(NSEC_PER_SEC)
        let time = CMTime(seconds: time, preferredTimescale: timeScale)
        player.seek(to: time, toleranceBefore: CMTime(seconds: 0.5, preferredTimescale: timeScale), toleranceAfter: CMTime(seconds: 0.5, preferredTimescale: timeScale)) { [weak self] (finished) -> Void in
            guard let strongSelf = self else {
                return
            }
            if finished {
                DispatchQueue.main.async {
                    let stateModel = strongSelf.model
                    stateModel.isSeeking = false
                    if !stateModel.isPausedByUser || autoplay {
                        stateModel.isFinished = false
                        if strongSelf.rate !≈ 0 {
                            if #available(iOS 10.0, *) {
                                strongSelf.player.playImmediately(atRate: strongSelf.rate)
                            }
                            strongSelf.player.rate = strongSelf.rate
                        } else {
                            strongSelf.player.play()
                        }
                    }
                    strongSelf.changeLoadState()
                    strongSelf.changePlaybackState()
                }
            }
        }
    }
    
    var currentTime: TimeInterval {
        if model.isSeeking {
            return model.seekingTime
        }
        if player.currentTime().isValid {
            return player.currentTime().seconds
        }
        return model.seekingTime
    }
}

// MARK: - Play End Notifications
fileprivate extension VideoPlayerView {
    
    @objc fileprivate func handleDidPlayToEndTime(_ notification: Notification) {
        if model.isFinished {
            return
        }
        model.isFinished = true
        DispatchQueue.main.async {
            self.changePlaybackState()
            self.changeLoadState()
            self.delegate?.videoPlayerView(self, didFinishPlayingWithReason: .playbackEnded)
            if let url = self.contentURL {
                NotificationCenter.default.post(name: .videoPlayerDidFinishPlay, object: nil, userInfo: ["url": url])
            } else {
                NotificationCenter.default.post(name: .videoPlayerDidFinishPlay, object: nil, userInfo: nil)
            }
        }
    }
    
    @objc fileprivate func handleFailedToEndTime(_ notification: Notification) {
        DispatchQueue.main.async {
            self.didTriggerError()
            self.delegate?.videoPlayerView(self, didFinishPlayingWithReason: .playbackError(self.playerItem?.error))
            if let url = self.contentURL {
                NotificationCenter.default.post(name: .videoPlayerDidFinishPlay, object: nil, userInfo: ["url": url])
            } else {
                NotificationCenter.default.post(name: .videoPlayerDidFinishPlay, object: nil, userInfo: nil)
            }
        }
    }
}

// MARK: - KVO Observer Handler Send By Player
fileprivate extension VideoPlayerView {
    
    fileprivate func handleRateChanged(_ player: AVPlayer) {
        if !model.isSeeking {
            changeLoadState()
            changePlaybackState()
        }
    }
    
    fileprivate func handleCurrentItemChanged(_ player: AVPlayer) {
        if let _ = player.currentItem {
            changePlaybackState()
            changeLoadState()
        } else {
            // PlayerChangeToNull
        }
    }
    
    fileprivate func handleTimeStatusChanged(_ player: AVPlayer) {
        changeLoadState()
    }
    
}

// MARK: - KVO Observer Handler Send By PlayerItem
fileprivate extension VideoPlayerView {
    
    fileprivate func handleStatusChanged(_ playerItem: AVPlayerItem) {
        if playerItem.status == .readyToPlay {
            duration = max(0, playerItem.duration.seconds)
            if !model.isPrepareToPlay {
                model.isPrepareToPlay = true
                model.isBuffering = true
                delegate?.didReadyToPlay(self)
                changeLoadState()
                changePlaybackState()
            }
        } else {
            if !model.isFinished {
                didTriggerError()
            }
            self.delegate?.videoPlayerView(self, didFinishPlayingWithReason: .playbackError(playerItem.error))
            if let url = self.contentURL {
                NotificationCenter.default.post(name: .videoPlayerDidFinishPlay, object: nil, userInfo: ["url": url])
            } else {
                NotificationCenter.default.post(name: .videoPlayerDidFinishPlay, object: nil, userInfo: nil)
            }
        }
    }
    
    fileprivate func handlePlaybackLikelyToKeepUpChanged(_ playerItem: AVPlayerItem) {
        changeLoadState()
        if playerItem.isPlaybackLikelyToKeepUp {
            model.bufferedTimes = 0
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(playAfterBufferTime), object: nil)
        }
        if !model.isSeeking && !model.isPausedByUser && player.rate ≈ 0.0 {
            if playerItem.isPlaybackLikelyToKeepUp {
                if rate !≈ 0 {
                    if #available(iOS 10.0, *) {
                        player.playImmediately(atRate: rate)
                    }
                    player.rate = rate
                } else {
                    player.play()
                }
            }
        }
    }
    
    fileprivate func handlePlaybackBufferEmptyChanged(_ playerItem: AVPlayerItem) {
        if playerItem.isPlaybackBufferEmpty {
            // Buffer 1 Second
            model.isBuffering = true
            changeLoadState()
            player.pause()
            model.bufferedTimes = 0
            perform(#selector(playAfterBufferTime), with: nil, afterDelay: 1)
        } else {
            changeLoadState()
        }
    }
    
    @objc fileprivate func playAfterBufferTime() {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(playAfterBufferTime), object: nil)
        guard let playerItem = playerItem, !playerItem.isPlaybackLikelyToKeepUp else {
            model.isBuffering = false
            model.bufferedTimes = 0
            return
        }
        if model.bufferedTimes < Model.maximumAllowsBufferedTimes {
            model.bufferedTimes += 1
            perform(#selector(playAfterBufferTime), with: nil, afterDelay: 1)
        } else {
            model.isBuffering = false
            changePlaybackState()
        }
    }
    
    fileprivate func handleIsPlaybackBufferFullChanged(_ playerItem: AVPlayerItem) {
        changeLoadState()
        if !model.isSeeking && !model.isPausedByUser && player.rate ≈ 0.0 {
            if playerItem.isPlaybackBufferFull {
                if rate !≈ 0 {
                    if #available(iOS 10.0, *) {
                        player.playImmediately(atRate: rate)
                    }
                    player.rate = rate
                } else {
                    player.play()
                }
                NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(playAfterBufferTime), object: nil)
            }
        }
    }
    
    fileprivate func handleLoadedTimeRangesChanged(_ playerItem: AVPlayerItem) {
        var maximumPlayableTime: TimeInterval = 0
        if playerItem.status == .readyToPlay {
            let loadedTimeRanges = playerItem.loadedTimeRanges
            let currentTime = playerItem.currentTime()
            for timeRange in loadedTimeRanges {
                let range = timeRange.timeRangeValue
                if range.containsTime(currentTime) {
                    maximumPlayableTime = range.end.seconds
                    break
                }
            }
        } else {
            maximumPlayableTime = 0
        }
        self.maximumPlayableTime = maximumPlayableTime
        delegate?.videoPlayerView(self, didUpdatePlayableTime: maximumPlayableTime)
        updateBufferingProgress()
    }
    
}

// MARK: - notify
fileprivate extension VideoPlayerView {
    
    func changeLoadState() {
        let currentState = currentLoadState
        if loadState != currentState {
            loadState = currentState
            if currentState == .playthroughOK && (rateValue !≈ 0 && player.rate != rateValue) {
                if let status = playerItem?.status, status == .readyToPlay {
                    delegate?.videoPlayerView(self, didChangeLoadState: currentState)
                }
                if #available(iOS 10.0, *) {
                    player.playImmediately(atRate: rate)
                }
                player.rate = rate
                return
            }
            if let status = playerItem?.status, status == .readyToPlay {
                delegate?.videoPlayerView(self, didChangeLoadState: currentState)
            }
        }
    }
    
    func changePlaybackState() {
        let currentState = currentPlaybackState
        if playbackState != currentState {
            playbackState = currentState
            delegate?.videoPlayerView(self, didChangePlaybackState: currentState)
            if currentState == .playing && (player.rate !≈ rateValue && rateValue !≈ 0) {
                if #available(iOS 10.0, *) {
                    player.playImmediately(atRate: rate)
                }
                player.rate = rate
            }
        }
    }
    
    func didTriggerError() {
        model.isFinished = true
        model.isPausedByUser = false
        self.changePlaybackState()
        self.changeLoadState()
    }
    
    func updateBufferingProgress() {
        let playbackTime = currentTime
        let playbackTimeInMilliSeconds = Int(playbackTime * 1000)
        let durationInMilliSeconds = Int(duration * 1000)
        let maximumPlayableTimeInMillsSeconds = Int(maximumPlayableTime * 1000)
        let bufferedPlayableDurationInMilliSeconds = maximumPlayableTimeInMillsSeconds - playbackTimeInMilliSeconds
        if bufferedPlayableDurationInMilliSeconds > 0 {
            self.bufferingProgress = (CGFloat(bufferedPlayableDurationInMilliSeconds) * 100) / maximumHighWaterMarkInMilliSeconds
            delegate?.videoPlayerView(self, didUpdateBufferedProgress: bufferingProgress)
            if self.bufferingProgress >= 100 || playbackTimeInMilliSeconds + Int(maximumHighWaterMarkInMilliSeconds) >= durationInMilliSeconds {
                if !isPlaying && !model.isPausedByUser && !model.isSeeking {
                    DispatchQueue.main.async {
                        self.player.play()
                    }
                }
            }
        }
    }
}

// MARK: - OBJC Properties Binding
fileprivate extension AVPlayer {
    
    private struct STVideoPlayerItemExtension {
        static var isObservingByVideoPlayer = "st_isObservingByVideoPlayerKey"
    }
    
    var st_observingByVideoPlayer: Bool {
        get {
            return (objc_getAssociatedObject(self, &STVideoPlayerItemExtension.isObservingByVideoPlayer) as? Bool) ?? false
        }
        set {
            objc_setAssociatedObject(self, &STVideoPlayerItemExtension.isObservingByVideoPlayer, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_COPY)
        }
    }
    
}

fileprivate extension AVPlayerItem {
    
    fileprivate struct VideoPlayerItemExtension {
        static var isObservingByVideoPlayer = "st_isObservingByVideoPlayerKey"
    }
    
    var st_observingByVideoPlayer: Bool {
        get {
            return (objc_getAssociatedObject(self, &VideoPlayerItemExtension.isObservingByVideoPlayer) as? Bool) ?? false
        }
        set {
            objc_setAssociatedObject(self, &VideoPlayerItemExtension.isObservingByVideoPlayer, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_COPY)
        }
    }
}
