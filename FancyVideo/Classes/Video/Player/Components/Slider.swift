//
//  Slider.swift
//  STQPlayer
//
//  Created by SunJiangting on 16/7/13.
//  Copyright © 2016年 Suen. All rights reserved.
//

import UIKit

final class MiniSlider: UIView {
    
    fileprivate var pvalue: CGFloat = 0
    
    var value: CGFloat {
        set {
            update(value: newValue)
            pvalue = newValue
        }
        get {
            if thumbView.frame.isEmpty {
                return 0
            }
            return (thumbView.frame.origin.x / (self.bounds.width - thumbView.frame.size.width)) * CGFloat(maximumValue)
        }
    }
    
    var bufferedValue: CGFloat = 0 {
        didSet {
            updateBufferedValue()
        }
    }
    
    var maximumValue: CGFloat = 0 {
        didSet {
            update(value: value)
            updateBufferedValue()
        }
    }
    
    private let backgroundView = UIView(frame: .zero)
    private let watchedView = UIView(frame: .zero)
    private let bufferedView = UIView(frame: .zero)
    
    private let thumbView = UIView(frame: .zero)
    
    override var frame: CGRect {
        didSet {
            updateSubviewFrames()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadSubviews()
        updateSubviewFrames()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func loadSubviews() {
        backgroundView.backgroundColor = UIColor.white.withAlphaComponent(0.3)
        addSubview(backgroundView)
        
        bufferedView.backgroundColor = UIColor(rgb: 0xCCCCCC)
        addSubview(bufferedView)
        
        watchedView.backgroundColor = UIColor(rgb: 0xf85959)
        addSubview(watchedView)
    }
    
    private func updateSubviewFrames() {
        updateBufferedValue()
        update(value: value)
        
        backgroundView.frame = CGRect(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height)
    }
    
    private func updateBufferedValue() {
        let valuePercent = min(1, bufferedValue / max(0.01, maximumValue))
        bufferedView.frame = CGRect(x: 0, y: 0, width: valuePercent * self.bounds.width, height: self.bounds.height)
    }
    
    private func update(value: CGFloat) {
        let valuePercent = min(1, value / max(0.01, maximumValue))
        watchedView.frame = CGRect(x: 0, y: 0, width: valuePercent * self.bounds.width, height: self.bounds.height)
    }
    
    func update() {
        updateSubviewFrames()
    }
}

final class Slider: UIControl, UIGestureRecognizerDelegate {
    
    struct UI {
        static let progressHeight: CGFloat = 3
        static let thumbLength: CGFloat = 14
        static let progressHorizontalPadding = thumbLength / 2
    }
    
    fileprivate var pvalue: CGFloat = 0
    
    var value: CGFloat {
        set {
            update(value: newValue)
            pvalue = newValue
        }
        get {
            if thumbView.frame.isEmpty {
                return pvalue
            }
            return (thumbView.frame.origin.x / (self.bounds.width - thumbView.frame.size.width)) * CGFloat(maximumValue)
        }
    }
    
    var bufferedValue: CGFloat = 0 {
        didSet {
            updateBufferedValue()
        }
    }
    
    var maximumValue: CGFloat = 0 {
        didSet {
            update(value: pvalue)
            updateBufferedValue()
        }
    }
    
    override var isEnabled: Bool {
        didSet {
            panGestureRecognizer.isEnabled = isEnabled
        }
    }
    
    override var isTracking: Bool {
        return trackingThumb
    }
    
    var valueDidUpdateCallback: ((CGFloat) -> Void)?
    
    private let panGestureRecognizer = UIPanGestureRecognizer()
    private let backgroundView = UIView(frame: .zero)
    private let watchedView = UIView(frame: .zero)
    private let bufferedView = UIView(frame: .zero)
    
    private let thumbView = UIView(frame: .zero)
    
    override var frame: CGRect {
        didSet {
            updateSubviewFrames()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadSubviews()
        updateSubviewFrames()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func loadSubviews() {
        backgroundView.backgroundColor = UIColor.white.withAlphaComponent(0.3)
        backgroundView.layer.cornerRadius = UI.progressHeight / 2.0
        backgroundView.layer.masksToBounds = true
        addSubview(backgroundView)
        
        bufferedView.backgroundColor = UIColor(rgb: 0xCCCCCC)
        bufferedView.layer.cornerRadius = UI.progressHeight / 2.0
        bufferedView.layer.masksToBounds = true
        addSubview(bufferedView)
        
        watchedView.backgroundColor = UIColor(rgb: 0xf85959)
        watchedView.layer.cornerRadius = UI.progressHeight / 2.0
        watchedView.layer.masksToBounds = true
        addSubview(watchedView)
        
        thumbView.backgroundColor = UIColor.white
        thumbView.layer.cornerRadius = UI.thumbLength / 2.0
        thumbView.layer.masksToBounds = true
        addSubview(thumbView)
        
        panGestureRecognizer.delegate = self
        panGestureRecognizer.addTarget(self, action: #selector(handlePan(_:)))
        addGestureRecognizer(panGestureRecognizer)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        addGestureRecognizer(tapGesture)
    }
    
    @objc private func handleTap(_ tapGesture: UITapGestureRecognizer) {
        let touchPoint = tapGesture.location(in: self)
        let centerX = touchPoint.x
        var thumbFrame = thumbView.frame
        thumbFrame.origin.x = centerX - thumbView.frame.width * 0.5
        if thumbFrame.origin.x < 0 {
            thumbFrame.origin.x = 0
        }
        if thumbFrame.origin.x + thumbFrame.size.width > self.bounds.width {
            thumbFrame.origin.x = (self.bounds.width - thumbFrame.size.width)
        }
        let value = max(0, min(maximumValue, thumbFrame.origin.x / (self.bounds.width - thumbFrame.size.width) * maximumValue))
        update(value: value)
        valueDidUpdateCallback?(value)
        sendActions(for: .valueChanged)
    }
    
    @objc private func handlePan(_ panGesture: UIPanGestureRecognizer) {
        let touchPoint = panGesture.location(in: self)
        switch panGesture.state {
        case .began:
            previousTouchPoint = touchPoint
            trackingThumb = true
        case .changed:
            let delta = touchPoint.x - previousTouchPoint.x
            var frame = thumbView.frame
            frame.origin.x = frame.origin.x + delta
            if frame.origin.x < 0 {
                frame.origin.x = 0
            }
            if frame.origin.x + frame.size.width > self.bounds.width {
                frame.origin.x = (self.bounds.width - frame.size.width)
            }
            let value = max(0, min(maximumValue, frame.origin.x / (self.bounds.width - frame.size.width) * maximumValue))
            update(value: value)
            valueDidUpdateCallback?(value)
            previousTouchPoint = touchPoint
        case .ended, .failed, .cancelled:
            previousTouchPoint = .zero
            trackingThumb = false
            sendActions(for: .valueChanged)
        case _:
            trackingThumb = false
        }
    }
    
    private var previousTouchPoint = CGPoint.zero
    private var trackingThumb: Bool = false
    
    private func updateSubviewFrames() {
        updateBufferedValue()
        update(value: pvalue)
        
        let progressViewWidth = self.bounds.width - UI.progressHorizontalPadding * 2
        backgroundView.frame = CGRect(x: UI.progressHorizontalPadding, y: (self.bounds.height - UI.progressHeight) / 2, width: progressViewWidth, height: UI.progressHeight)
    }
    
    private func updateBufferedValue() {
        let valuePercent = min(1, bufferedValue / max(0.01, maximumValue))
        let progressViewWidth = self.bounds.width - UI.progressHorizontalPadding * 2
        bufferedView.frame = CGRect(x: UI.progressHorizontalPadding, y: (self.bounds.height - UI.progressHeight) / 2, width: valuePercent * progressViewWidth, height: UI.progressHeight)
    }
    
    private func update(value: CGFloat) {
        let valuePercent = min(1, value / max(0.01, maximumValue))
        let progressViewWidth = (self.bounds.width - UI.progressHorizontalPadding * 2)
        thumbView.frame = CGRect(x: (valuePercent * (self.bounds.width - UI.thumbLength)), y: (self.bounds.height - UI.thumbLength) / 2, width: UI.thumbLength, height: UI.thumbLength)
        watchedView.frame = CGRect(x: UI.progressHorizontalPadding, y: (self.bounds.height - UI.progressHeight) / 2, width: valuePercent * progressViewWidth, height: UI.progressHeight)
    }
    
    func update() {
        updateSubviewFrames()
        update(value: pvalue)
    }
    
    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer == self.panGestureRecognizer {
            let touchPoint = gestureRecognizer.location(in: self)
            return thumbView.frame.insetBy(dx: -20, dy: -20).contains(touchPoint)
        }
        return true
    }
}
    
