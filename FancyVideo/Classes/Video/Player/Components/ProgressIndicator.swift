//
//  ProgressIndicator.swift
//  STQPlayer
//
//  Created by SunJiangting on 16/7/13.
//  Copyright © 2016年 Suen. All rights reserved.
//

import UIKit

final class ProgressIndicator: UIView {
    
    private let arrowView = UIImageView(frame: .zero)
    private let timeLabel = UILabel(frame: .zero)
    
    var startValue: TimeInterval = 0
    
    var value: TimeInterval = 0 {
        didSet {
            updateValue()
        }
    }
    
    init(frame: CGRect, duration: TimeInterval) {
        super.init(frame: frame)
        loadSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func loadSubviews() {
        self.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        self.layer.cornerRadius = 3
        self.layer.masksToBounds = true
        
        arrowView.image = UIImage(named: "video_fineturning_forward")
        addSubview(arrowView)
        
        timeLabel.textColor = UIColor.white
        timeLabel.textAlignment = .left
        timeLabel.font = UIFont.systemFont(ofSize: 12)
        addSubview(timeLabel)
    }
    
    private func updateValue() {
        let imageName = value > 0 ? "video_fineturning_forward" : "video_fineturning_backward"
        arrowView.image = UIImage(named: imageName)
        
        let directionString = value > 0 ? "+":"-", secondString = "\(abs(Int(value)))"
        let displayString = "\(directionString) \(secondString)"
        let attributedText = NSMutableAttributedString(string: displayString)
        let color = UIColor(rgb: 0xf85959)
        attributedText.setAttributes([NSAttributedStringKey.foregroundColor: color], range: NSMakeRange(directionString.utf16.count, displayString.utf16.count - directionString.utf16.count))
        self.timeLabel.attributedText = attributedText
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        arrowView.frame = CGRect(x: 10, y: 14, width: 12, height: 9)
        timeLabel.frame = CGRect(x: 30, y: 10, width: self.width - 35, height: 15)
    }
}
