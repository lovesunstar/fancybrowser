//
//  VolumeIndicator.swift
//  STQPlayer
//
//  Created by SunJiangting on 16/7/13.
//  Copyright © 2016年 Suen. All rights reserved.
//

import UIKit
import GiantShoulder

final class VolumeIndicator: UIView {
    
    private let volumeView = UIImageView(frame: .zero)
    private var startValue: TimeInterval = 0
    
    private let backgroundView = UIView(frame: .zero)
    private let foregroundView = UIView(frame: .zero)
    
    
    var value: CGFloat = 0 {
        didSet {
            setNeedsLayout()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func loadSubviews() {
        self.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        self.layer.cornerRadius = 3
        self.layer.masksToBounds = true
        
        backgroundView.backgroundColor = UIColor.white.withAlphaComponent(0.4)
        backgroundView.layer.cornerRadius = ComponentsDefines.UI.progressHeight / 2.0
        backgroundView.layer.masksToBounds = true
        addSubview(backgroundView)
        
        foregroundView.backgroundColor = UIColor(rgb: 0xf85959)
        foregroundView.layer.cornerRadius = ComponentsDefines.UI.progressHeight / 2.0
        foregroundView.layer.masksToBounds = true
        addSubview(foregroundView)
        
        volumeView.image = UIImage(named: "video_volume")
        addSubview(volumeView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        foregroundView.height = (self.bounds.height - 27) * value
        foregroundView.maxY = self.bounds.height - 19
        foregroundView.width = 2
        foregroundView.centerX = self.inCenterX
        
        backgroundView.frame = CGRect(x: (self.bounds.width - 2) / 2, y: 8, width: 2, height: self.bounds.height - 27)
        volumeView.frame = CGRect(x: (self.bounds.width - 10) / 2, y: self.bounds.height - 15, width: 10, height: 8)
    }
}
