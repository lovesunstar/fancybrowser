//
//  ComponentsDefines.swift
//  YTVideo
//
//  Created by SunJiangting on 2017/4/7.
//  Copyright © 2016年 Suen. All rights reserved.
//

import UIKit

class ComponentsDefines: NSObject {
    struct UI {
        static let progressHeight: CGFloat = 3
        static let thumbLength: CGFloat = 14
        static let progressHorizontalPadding = thumbLength / 2
    }
}
