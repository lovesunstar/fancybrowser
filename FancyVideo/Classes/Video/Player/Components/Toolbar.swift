//
//  Toolbar.swift
//  STQPlayer
//
//  Created by SunJiangting on 16/7/13.
//  Copyright © 2016年 Suen. All rights reserved.
//

import UIKit
import GiantShoulder

func formatVideo(timeInterval: TimeInterval, couldBeZero: Bool = true) -> String {
    if timeInterval > 0 || couldBeZero {
        let intDuration = Int(timeInterval)
        let minutes = Int(intDuration / 60)
        let seconds = Int(intDuration % 60)
        return NSString(format: "%02i:%02i", minutes, seconds) as String
    } else {
        return "--:--"
    }
}

final class Toolbar: UIView {
    
    struct UI {
        fileprivate static let horizontalPadding: CGFloat = 5
        fileprivate static let playButtonWidth: CGFloat = 30
        fileprivate static let playButtonHeight: CGFloat = 30
        fileprivate static let magnifyWidth: CGFloat = 30
    }
    
    
    var duration: TimeInterval = 0 {
        didSet {
            slider.maximumValue = CGFloat(duration)
            durationLabel.text = formatVideo(timeInterval: duration, couldBeZero: false)
        }
    }
    
    var time: TimeInterval = 0 {
        didSet {
            if !slider.isTracking {
                slider.value = CGFloat(time)
                timeLabel.text = formatVideo(timeInterval: time)
            }
        }
    }
    
    var bufferedTime: TimeInterval = 0 {
        didSet {
            slider.bufferedValue = CGFloat(bufferedTime)
        }
    }
    
    let timeLabel = UILabel(frame: .zero)
    let slider = Slider(frame: .zero)
    let durationLabel = UILabel(frame: .zero)
    let magnifyButton = Video.Button(frame: .zero)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let textFont = UIFont.systemFont(ofSize: 11)
        let textWidth = ("000:00" as NSString).size(with: textFont, constrainedTo: CGSize(width: 999, height: 20), paragraphStyle: nil).width
        let textColor = UIColor.white
        
        timeLabel.textColor = textColor
        timeLabel.font = textFont
        timeLabel.textAlignment = .center
        timeLabel.text = formatVideo(timeInterval: 0, couldBeZero: false)
        timeLabel.width = textWidth
        addSubview(timeLabel)
        
        slider.valueDidUpdateCallback = { [weak self] newValue in
            self?.timeLabel.text = formatVideo(timeInterval: TimeInterval(newValue))
        }
        addSubview(slider)
        
        durationLabel.textColor = textColor
        durationLabel.font = textFont
        durationLabel.textAlignment = .center
        durationLabel.text = formatVideo(timeInterval: 0, couldBeZero: false)
        durationLabel.width = textWidth
        addSubview(durationLabel)
        
        magnifyButton.touchInsets = UIEdgeInsets(top: -20, left: -10, bottom: -20, right: -20)
        magnifyButton.setImage(UIImage(named: "video_magnify"), for: .normal)
        addSubview(magnifyButton)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let isFullscreen = nextResponder(VideoControlView.self)?.isFullscreen ?? false
        let supportSafeArea = isFullscreen && Device.isIPhoneX
        let timeLeft: CGFloat = (supportSafeArea) ? 20 : 5
        let magnifyRight: CGFloat = supportSafeArea ? 20 : 5
        
        let height: CGFloat = 40
        let top: CGFloat = 0
        timeLabel.frame = CGRect(x: timeLeft, y: top, width: timeLabel.width, height: height)

        magnifyButton.frame = CGRect(x: self.width - UI.magnifyWidth - magnifyRight, y: top, width: UI.magnifyWidth, height: height)
        durationLabel.frame = CGRect(x: magnifyButton.minX - UI.horizontalPadding - durationLabel.width, y: top, width: durationLabel.width, height: height)
        let sliderLeft = timeLabel.maxX + UI.horizontalPadding
        slider.frame = CGRect(x: sliderLeft, y: top, width: durationLabel.minX - 2 * UI.horizontalPadding - sliderLeft, height: height)
        slider.update()
    }
    
    func update() {
        slider.update()
    }
}
