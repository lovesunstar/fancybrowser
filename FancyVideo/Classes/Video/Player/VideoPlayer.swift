//
//  VideoPlayer.swift
//  YTVideo
//
//  Created by SunJiangting on 2017/4/7.
//  Copyright © 2016年 Suen. All rights reserved.
//

import UIKit
import SafariServices
import GiantShoulder

extension Notification.Name {
    static let videoPlayerDidFinishPlay = Notification.Name(rawValue: "FVVideoPlayerDidFinishPlay")
    static let videoPlayerDidFailToPlay = Notification.Name(rawValue: "FVVideoPlayerDidFailToPlay")
    static let videoPlayerPlay = Notification.Name(rawValue: "FVVideoPlayerPlay")
    static let videoPlayerPause = Notification.Name(rawValue: "FVVideoPlayerPause")
}

final class VideoPlayer: UIView, VideoPlayerViewDelegate {
    
    enum LandscapeOrientation {
        case left
        case right
        
        var rotationAngle: CGFloat {
            switch self {
            case .left:
                return -CGFloat.pi * 0.5
            case .right:
                return CGFloat.pi * 0.5
            }
        }
        
        var deviceOrientation: UIDeviceOrientation {
            switch self {
            case .left:
                return UIDeviceOrientation.landscapeRight
            case .right:
                return UIDeviceOrientation.landscapeLeft
            }
        }
        
        var statusBarOrientation: UIInterfaceOrientation {
            switch self {
            case .left:
                return UIInterfaceOrientation.landscapeLeft
            case .right:
                return UIInterfaceOrientation.landscapeRight
            }
        }
    }
    
    fileprivate let transitionView = UIView(frame: .zero)
    
    let playerView = VideoPlayerView(frame: .zero, contentURL: nil)
    let controlView = VideoControlView(frame: .zero)
    let settingView = VideoSettingView(frame: .zero)
    
    fileprivate(set) var isFullscreen = false
    
    var hidesContolForMinimumView: Bool = false {
        didSet {
            controlView.isMinimumState = hidesContolForMinimumView
        }
    }
    
    fileprivate var isEnded = false
    
    fileprivate var isRotating = false
    fileprivate var landscapeOrientation: LandscapeOrientation?
    
    private var speed = VideoPlayerSpeed.normal
    
    var isTitleVisibleWhenNotFullscreen = true {
        didSet {
            if !isFullscreen {
                controlView.titleLabel.isHidden = !isTitleVisibleWhenNotFullscreen
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.black
        
        transitionView.frame = self.bounds
        addSubview(transitionView)
        
        playerView.delegate = self
        playerView.frame = transitionView.bounds
        playerView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        transitionView.addSubview(playerView)
        
        controlView.controlBar.slider.addTarget(self, action: #selector(handleSlide(_:)), for: .valueChanged)
        controlView.controlBar.magnifyButton.addTarget(self, action: #selector(handleMagnify(_:)), for: .touchUpInside)
        controlView.frame = transitionView.bounds
        controlView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        controlView.playButton.addTarget(self, action: #selector(handlePlay(_:)), for: .touchUpInside)
        controlView.pauseButton.addTarget(self, action: #selector(handlePause(_:)), for: .touchUpInside)
        controlView.replayButton.addTarget(self, action: #selector(handleReplay(_:)), for: .touchUpInside)
        controlView.backButton.addTarget(self, action: #selector(handleBack(_:)), for: .touchUpInside)
        controlView.moreButton.addTarget(self, action: #selector(handleMore(_:)), for: .touchUpInside)
        controlView.retryButton.addTarget(self, action: #selector(handleRetry(_:)), for: .touchUpInside)
        transitionView.addSubview(controlView)
        
        settingView.dismissHandler = { [weak self] (view, results) in
            guard let strongSelf = self else { return }
            if strongSelf.playerView.loadState == .stalled || strongSelf.playerView.playbackState == .paused || strongSelf.playerView.playbackState == .stopped || strongSelf.playerView.playbackState == .seeking {
                strongSelf.controlView.show()
            } else {
                strongSelf.controlView.flash()
            }
            if results == .cancelled {
                return
            }
            if view.speed != strongSelf.speed {
                strongSelf.speed = view.speed
                strongSelf.playerView.rate = Float(view.speed.value)
            }
        }
        settingView.speed = .normal
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleRotateNotification(_:)), name: .UIDeviceOrientationDidChange, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleEnterForegroundNotification(_:)), name: .UIApplicationWillEnterForeground, object: nil)
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError() }
    
    func play(_ url: URL, title: String? = nil, autoPlay: Bool = true) {
        if !self.isFullscreen {
            self.controlView.titleLabel.isHidden = !self.isTitleVisibleWhenNotFullscreen
        }
        self.speed = .normal
        reset()
        replace(contentURL: url)
        controlView.title = title
        if autoPlay {
            play()
        }
        controlView.state = .loading
    }
    
    func play() {
        playerView.play()
        playerView.rate = Float(self.speed.value)
    }
    
    func pause() {
        playerView.pause()
    }
    
    func stop() {
        clear()
    }
    
    private func clear() {
        self.speed = .normal
        isEnded = false
        playerView.stop()
        settingView.removeFromSuperview()
    }
    
    var isPlaying: Bool {
        return playerView.isPlaying
    }
    
    var isReady: Bool {
        return playerView.isReady
    }

    private func resetControl() {
        controlView.bufferedTime = 0
        controlView.duration = -1
        controlView.time = 0
        controlView.state = .stalled
        settingView.removeFromSuperview()
    }
    
    @objc private func handleSlide(_ slider: Slider) {
        if playerView.duration != 0 && !slider.isTracking {
            let targetTime = TimeInterval(slider.value)
            playerView.seek(to: targetTime, autoplayWhenFinish: isPlaying)
        }
        controlView.miniSlider.value = slider.value
    }
    
    func reset() {
        self.speed = .normal
        clear()
        resetControl()
    }
    
    func replace(contentURL: URL?) {
        self.speed = .normal
        playerView.replace(contentURL: contentURL)
        resetControl()
    }
    
    @objc private func handlePlay(_ button: UIButton) {
        self.play()
        controlView.flash()
        controlView.state = .paused
        isEnded = false
    }
    
    @objc private func handlePause(_ button: UIButton) {
        self.pause()
        controlView.cancelFlash()
        controlView.state = .play
    }
    
    @objc private func handleReplay(_ button: UIButton) {
        playerView.replay()
        controlView.state = .paused
        controlView.flash()
        isEnded = false
    }
    
    func setFullscreen(_ fullscreen: Bool, animated: Bool) {
        if controlView.state.isPaused {
            controlView.flash(for: 10)
        } else {
            controlView.cancelFlash()
            controlView.show()
            settingView.removeFromSuperview()
        }
        if !self.isFullscreen && fullscreen {
            guard let w = UIApplication.shared.delegate?.window, let window = w, transitionView.superview != window else {
                return
            }
            let targetFrame = convert(transitionView.frame, to: window)
            transitionView.removeFromSuperview()
            transitionView.frame = targetFrame
            window.addSubview(transitionView)
            self.isFullscreen = true
            controlView.isBackButtonHidden = false
            controlView.controlBar.magnifyButton.setImage(UIImage(named: "video_shrink"), for: .normal)
            controlView.isFullscreen = fullscreen
            setLandscape(orientation: .right, animated: animated, completion: { (finished) in
                self.controlView.titleLabel.isHidden = false
                self.controlView.trackingGestures.forEach({ $0.isEnabled = true })
                updateStatusBar()
            })
            if let vc = topmostViewController() as? BaseViewController {
                vc.isHomeIndicatorHidden = true
            }
        } else if self.isFullscreen && !fullscreen {
            UIApplication.shared.beginIgnoringInteractionEvents()
            guard let superview = transitionView.superview, superview != self else {
                UIApplication.shared.endIgnoringInteractionEvents()
                UIDevice.current.endGeneratingDeviceOrientationNotifications()
                return
            }
            let backgroundView = UIView(frame: superview.bounds)
            backgroundView.backgroundColor = UIColor.black
            superview.addSubview(backgroundView)
            superview.bringSubview(toFront: transitionView)
            let completionHandler: ((Bool)->Void) = { finished in
                self.landscapeOrientation = nil
                self.transitionView.transform = .identity
                self.transitionView.frame = self.bounds
                self.addSubview(self.transitionView)
                backgroundView.removeFromSuperview()
                UIApplication.shared.endIgnoringInteractionEvents()
                updateStatusBar()
                self.controlView.titleLabel.isHidden = !self.isTitleVisibleWhenNotFullscreen
                self.controlView.trackingGestures.forEach({ $0.isEnabled = false })
                STSetStatusBarOrientation(.portrait, false)
                UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
                UIDevice.current.endGeneratingDeviceOrientationNotifications()
            }
            let selfBoundsInWindow = self.superview?.convert(self.frame, to: superview) ?? self.bounds
            if animated {
                UIView.animate(withDuration: 0.3, delay: 0, options: .layoutSubviews, animations: {
                    self.transitionView.transform = .identity
                    self.transitionView.frame = selfBoundsInWindow
                }, completion: completionHandler)
            } else {
                completionHandler(true)
            }
            STSetStatusBarOrientation(.portrait, true)
            UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
            self.isFullscreen = false
            controlView.isBackButtonHidden = true
            controlView.controlBar.magnifyButton.setImage(UIImage(named: "video_magnify"), for: .normal)
            controlView.isFullscreen = fullscreen
            if let vc = topmostViewController() as? BaseViewController {
                vc.isHomeIndicatorHidden = false
            }
            settingView.removeFromSuperview()
        }
    }
    
    @objc private func handleRotateNotification(_ notification: Notification) {
        let deviceOrientation = UIDevice.current.orientation
        switch deviceOrientation {
        case .landscapeLeft:
            setLandscape(orientation: .right, animated: true, completion: nil)
        case .landscapeRight:
            setLandscape(orientation: .left, animated: true, completion: nil)
        default:
            break
        }
    }
    
    @objc private func handleEnterForegroundNotification(_ notification: Notification) {
        guard isFullscreen, !isRotating, let orientation = landscapeOrientation else {
            return
        }
        STSetStatusBarOrientation(orientation.statusBarOrientation, false)
//        UIDevice.current.setValue(orientation.deviceOrientation.rawValue, forKey: "orientation")
    }
    
    private func setLandscape(orientation: LandscapeOrientation, animated: Bool, completion:((Bool)->Void)?) {
        if isRotating || !isFullscreen || orientation == landscapeOrientation {
            completion?(false)
            return
        }
        UIApplication.shared.beginIgnoringInteractionEvents()
        guard let w = UIApplication.shared.delegate?.window, let window = w, transitionView.superview == window else {
            UIApplication.shared.endIgnoringInteractionEvents()
            return
        }
        isRotating = true
        let backgroundView = UIView(frame: window.bounds)
        backgroundView.backgroundColor = UIColor.black
        window.insertSubview(backgroundView, belowSubview: transitionView)
        let radian = orientation.rotationAngle
        let completionHandler: ((Bool)->Void) = { finished in
            self.transitionView.bounds = CGRect(x: 0, y: 0, width: window.frame.height, height: window.frame.size.width)
            self.transitionView.center = CGPoint(x: window.bounds.width / 2, y: window.bounds.height / 2)
            self.transitionView.transform = CGAffineTransform(rotationAngle: radian)
            backgroundView.removeFromSuperview()
            UIApplication.shared.endIgnoringInteractionEvents()
            STSetStatusBarOrientation(orientation.statusBarOrientation, false)
            UIDevice.current.setValue(orientation.deviceOrientation.rawValue, forKey: "orientation")
            UIDevice.current.beginGeneratingDeviceOrientationNotifications()
            self.isRotating = false
            completion?(finished)
        }
        if animated {
            UIView.animate(withDuration: 0.3, delay: 0, options: .layoutSubviews, animations: {
                self.transitionView.bounds = CGRect(x: 0, y: 0, width: window.frame.height, height: window.frame.size.width)
                self.transitionView.center = CGPoint(x: window.bounds.width / 2, y: window.bounds.height / 2)
                self.transitionView.transform = CGAffineTransform(rotationAngle: radian)
            }, completion: completionHandler)
        } else {
            completionHandler(true)
        }
        self.landscapeOrientation = orientation
        STSetStatusBarOrientation(orientation.statusBarOrientation, animated)
        UIDevice.current.setValue(orientation.deviceOrientation.rawValue, forKey: "orientation")
    }
    
    @objc private func handleMagnify(_ button: UIButton) {
        setFullscreen(!isFullscreen, animated: true)
    }
    
    @objc private func handleBack(_ button: UIButton) {
        setFullscreen(false, animated: true)
    }
    
    @objc private func handleMore(_ button: UIButton) {
        transitionView.addSubview(settingView)
        settingView.speed = self.speed
        controlView.cancelFlash()
        controlView.hide()
    }
    
    @objc private func handleRetry(_ button: UIButton) {
        guard let oldURL = playerView.contentURL else {
            return
        }
        self.speed = .normal
        playerView.stop()
        replace(contentURL: oldURL)
        play()
        controlView.state = .loading
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if transitionView.superview == self {
            transitionView.frame = self.bounds
        }
        settingView.frame = transitionView.bounds
    }
    
    // MARK: - VideoPlayerViewDelegate
    func didReadyToPlay(_ playerView: VideoPlayerView) {
        isEnded = false
        bringSubview(toFront: controlView)
        controlView.time = 0
        controlView.bufferedTime = 0
        controlView.duration = playerView.duration
        controlView.state = .stalled
    }
    
    func videoPlayerView(_ playerView: VideoPlayerView, didPlayToTime time: TimeInterval) {
        if (controlView.state == VideoControlView.State.stalled) && !playerView.loadState.contains(.stalled) {
            controlView.state = .paused
            controlView.flash(for: 10)
        }
        controlView.time = time
    }
    
    func videoPlayerView(_ playerView: VideoPlayerView, didUpdatePlayableTime time: TimeInterval) {
        controlView.bufferedTime = time
    }
    
    func videoPlayerView(_ playerView: VideoPlayerView, didUpdateBufferedProgress progress: CGFloat) {
        
    }
    
    func videoPlayerView(_ playerView: VideoPlayerView, didChangeLoadState newState: VideoLoadState) {
        if newState.contains(.stalled) {
            controlView.state = .stalled
            if settingView.superview == nil {
                controlView.show()
            }
        } else {
            switch playerView.playbackState {
            case .playing:
                controlView.state = .paused
                if settingView.superview == nil {
                    controlView.flash()
                }
            case .paused:
                if isEnded {
                    controlView.state = .end
                    return
                }
                controlView.state = .play
                controlView.cancelFlash()
                if settingView.superview == nil {
                    controlView.show()
                }
            case .stopped:
                controlView.state = .play
                settingView.removeFromSuperview()
                controlView.show()
            default:
                break
            }
        }
    }
    
    func videoPlayerView(_ playerView: VideoPlayerView, didChangePlaybackState newState: VideoPlaybackState) {
        if playerView.loadState.contains(.stalled) {
            controlView.state = .stalled
            if settingView.superview == nil {
                controlView.show()
            }
            return
        }
        switch newState {
        case .playing:
            controlView.state = .paused
        case .paused:
            controlView.state = .play
            controlView.cancelFlash()
            if settingView.superview == nil {
                controlView.show()
            }
        case .stopped:
            controlView.state = .play
            settingView.removeFromSuperview()
        default:
            break
        }
    }
    
    func videoPlayerView(_ playerView: VideoPlayerView, didFinishPlayingWithReason reason: VideoFinishReason) {
        // 用户主动点击导致播放结束的先不管
        isEnded = true
        switch reason {
        case .userExited:
            return
        case .playbackEnded:
            settingView.removeFromSuperview()
            controlView.show()
            controlView.state = .end
            return
        case .playbackError(let error):
            settingView.removeFromSuperview()
            controlView.state = .error(error ?? NSError.makeError("VideoCannotPlayNow".localized()))
        }
    }
    
}
