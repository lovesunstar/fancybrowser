//
//  VideoErrorView.swift
//  FancyVideo
//
//  Created by SunJiangting on 2018/2/2.
//  Copyright © 2018年 Samaritan. All rights reserved.
//

import UIKit

class VideoErrorView: UIView {

    enum UI {
        fileprivate static let textActionMargin: CGFloat = 5
        fileprivate static let actionLineMargin: CGFloat = -6
        fileprivate static let lineExtraWidth: CGFloat = 5
    }
    
    fileprivate let textLabel = UILabel(frame: .zero)
    let actionButton = UIButton(frame: .zero)
    fileprivate let actionLineView = UIView(frame: .zero)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.black
        textLabel.textAlignment = .center
        textLabel.textColor = UIColor.white
        textLabel.numberOfLines = 0
        textLabel.font = UIFont.systemFont(ofSize: 13)
        addSubview(textLabel)
        
        actionButton.setTitleColor(UIColor.white, for: .normal)
        actionButton.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        addSubview(actionButton)
        
        actionLineView.backgroundColor = UIColor.white
        addSubview(actionLineView)
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError() }
    
    func update(message: String, buttonText: String?) {
        textLabel.text = message
        actionButton.setTitle(buttonText, for: .normal)
        setNeedsLayout()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        textLabel.frame = CGRect(x: 0, y: 0, width: self.bounds.width - 40, height: 9999)
        textLabel.sizeToFit()
        let textWidth = textLabel.frame.width, textHeight = textLabel.frame.height
        
        let buttonWidth: CGFloat, buttonHeight: CGFloat
        if actionButton.title(for: .normal)?.isEmpty ?? true {
            buttonWidth = 0
            buttonHeight = 0
        } else {
            actionButton.sizeToFit()
            buttonWidth = actionButton.frame.width
            buttonHeight = actionButton.frame.height
        }
        
        if buttonHeight > 0 {
            let totalHeight = (buttonHeight + UI.textActionMargin + textHeight)
            let topMargin = (self.bounds.height - totalHeight) / 2
            textLabel.frame = CGRect(x: (self.bounds.width - textWidth) / 2, y: topMargin, width: textWidth, height: textHeight)
            actionButton.frame = CGRect(x: (self.bounds.width - buttonWidth) / 2, y: textLabel.frame.maxY + UI.textActionMargin, width: buttonWidth, height: buttonHeight)
            actionLineView.frame = CGRect(x: (self.bounds.width - buttonWidth - UI.lineExtraWidth) / 2, y: actionButton.frame.maxY + UI.actionLineMargin, width: buttonWidth + UI.lineExtraWidth, height: 1.0 / max(UIScreen.main.scale, 1))
            actionLineView.isHidden = false
        } else {
            textLabel.frame = CGRect(x: (self.bounds.width - textWidth) / 2, y: (self.bounds.height - textHeight) / 2, width: textWidth, height: textHeight)
            actionLineView.isHidden = true
        }
    }

}
