//
//  InterstitialADManager.swift
//  VEnjoy
//
//  Created by SunJiangting on 16/10/8.
//  Copyright © 2016年 Suen. All rights reserved.
//

import UIKit
import GoogleMobileAds

struct InterstitialAD {
    
    enum RequestError: Error {
        case timedOut
        case failed
    }
    
    enum Response {
        case success(GADInterstitial)
        case failure(RequestError)
    }
    
    enum WillEndDisplayingReason {
        case failedToPresent, dismissScreen
    }
    
    static let defaultTimeout: TimeInterval = 10
    
    private static var loaders = [Loader]()
    
    static func request(_ config: ADConfig.Launch, completionHandler:((Response)->Void)?) -> Request {
        let adRequest = Request(config: config)
        let adLoader = Loader(adRequest) { (loader, response) in
            if let idx = self.loaders.index(of: loader) {
                self.loaders.remove(at: idx)
            }
            completionHandler?(response)
        }
        loaders.append(adLoader)
        return adRequest
    }
    
    class Request {
        fileprivate let config: ADConfig.Launch
        fileprivate init(config: ADConfig.Launch) {
            self.config = config
        }
        
        var displayCompletionHandler: ((GADInterstitial, WillEndDisplayingReason)->Void)?
        var willEndDisplayHandler: (()->Void)?
    }
    
    fileprivate class Loader: NSObject, GADInterstitialDelegate {
        
        private let timeout: TimeInterval
        private let loadCompletionHandler: ((Loader, Response)->Void)
        
        fileprivate let interstitial: GADInterstitial
        private let request: Request
        fileprivate init(_ request: Request,
                         completionHandler:@escaping ((Loader, Response)->Void)) {
            self.request = request
            self.timeout = request.config.timeout
            self.interstitial = GADInterstitial(adUnitID: request.config.unitID)
            self.loadCompletionHandler = completionHandler
            super.init()
            interstitial.delegate = self
            interstitial.ve_adLoader = self
            let adRequest = GADRequest()
            if App.isDebug {
                if Device.isSimulator {
                    adRequest.testDevices = [kGADSimulatorID]
                } else {
                    adRequest.testDevices = ["0785945b6784e62c085cdc49c98f7f72"]
                }
            }
            interstitial.load(adRequest)
            perform(#selector(handleTimeout), with: nil, afterDelay: timeout, inModes: [.commonModes])
        }
        
        @objc private func handleTimeout() {
            interstitial.delegate = nil
            interstitial.ve_adLoader = nil
            // Cancel
            loadCompletionHandler(self, .failure(.timedOut))
        }
        
        fileprivate func interstitialDidReceiveAd(_ ad: GADInterstitial) {
            loadCompletionHandler(self, .success(interstitial))
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(handleTimeout), object: nil)
        }
        
        fileprivate func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
            loadCompletionHandler(self, .failure(.failed))
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(handleTimeout), object: nil)
        }
        
        fileprivate func interstitialWillPresentScreen(_ ad: GADInterstitial) {
            
        }
        
        fileprivate func interstitialDidFail(toPresentScreen ad: GADInterstitial) {
            interstitial.ve_adLoader = nil
            request.displayCompletionHandler?(interstitial, .failedToPresent)
        }
        
        fileprivate func interstitialWillDismissScreen(_ ad: GADInterstitial) {
            request.willEndDisplayHandler?()
        }
        
        fileprivate func interstitialDidDismissScreen(_ ad: GADInterstitial) {
            interstitial.ve_adLoader = nil
            request.displayCompletionHandler?(interstitial, .dismissScreen)
        }
        
        fileprivate func interstitialWillLeaveApplication(_ ad: GADInterstitial) {
            interstitial.ve_adLoader = nil
            request.displayCompletionHandler?(interstitial, .dismissScreen)
        }
    }
}

extension GADInterstitial {
    
    fileprivate struct VEPrivateStatic {
        static var loader = "VEADManagerLoader"
    }
    
    fileprivate var ve_adLoader: InterstitialAD.Loader? {
        get {
            return objc_getAssociatedObject(self, &VEPrivateStatic.loader) as? InterstitialAD.Loader
        }
        set {
            objc_setAssociatedObject(self, &VEPrivateStatic.loader, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
}
