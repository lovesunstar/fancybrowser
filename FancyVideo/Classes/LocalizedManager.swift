//
//  LocalizedManager.swift
//  FancyVideo
//
//  Created by SunJiangting on 16/10/8.
//  Copyright © 2016年 Suen. All rights reserved.
//

import UIKit

enum Language {
    case zhCN
    
    fileprivate var localeFileName: String {
        switch self {
        case .zhCN:
            return "zh-Hans"
        }
    }
}

class LocalizedManager {

    static let shared = LocalizedManager()
    private var languages = [Language : [String : String]]()
    private(set) var language: Language = .zhCN
    
    private init() {
        
    }
    
    func localizedString(forKey key: String) -> String {
        if let properties = languages[language] {
            return properties[key] ?? key
        }
        
        let fileName = language.localeFileName
        let stringName = fileName + ".lproj/Localized"
        if let stringPath = Bundle.main.path(forResource: stringName, ofType: "strings") ?? Bundle.main.path(forResource: "zh-lproj/Localized", ofType: "strings") {
            if let dict = NSDictionary(contentsOfFile: stringPath) as? [String : String] {
                languages[language] = dict
                return dict[key] ?? key
            }
        }
        return NSLocalizedString(key, comment: "")
    }
}

extension String {
    
    func localized(_ arguments: [Any] = []) -> String {
        var results: String = LocalizedManager.shared.localizedString(forKey: self)
        for (idx, obj) in arguments.enumerated() {
            let localizableIndex = (idx + 1)
            results = results.replacingOccurrences(of: "{\(localizableIndex)}", with: "\(obj)")
        }
        return results
    }
    
    mutating func localize(_ arguments: [Any] = []) {
        self = self.localized(arguments)
    }
}
