//
//  ImagePreviewViewController.swift
//  TestImageScrollView
//
//  Created by SunJiangting on 2018/3/9.
//  Copyright © 2018年 M&ETimes. All rights reserved.
//

import UIKit

class ImagePreviewViewController: BaseViewController {
    
    enum UI {
        static let hideNavigationBarDelay: TimeInterval = 3.0
    }
    
    private lazy var shareButton = FancyVideo.Button()
    private lazy var scrollView = ImageScrollView()
    
    private let image: UIImage
    
    init(image: UIImage) {
        self.image = image
        super.init(nibName: nil, bundle: nil)
        self.hidesBottomBarWhenPushed = true
        self.automaticallyAdjustsScrollViewInsets = false
        self.extendedLayoutIncludesOpaqueBars = true
        self.statusBarStyle = .lightContent
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.st_navigationBar?.barTintColor = UIColor(rgb: 0x555555).withAlphaComponent(0.3)
        self.st_navigationBar?.blurEffect = UIBlurEffect(style: .dark)
        self.st_navigationBar?.separatorView.removeFromSuperview()
        
        let backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 44))
        backButton.addTarget(self, action: #selector(handleNavigationBack), for: .touchUpInside)
        backButton.setImage(UIImage(named: "navigation_dismiss_white"), for: .normal)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 11, left: 15, bottom: 11, right: 25)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
        
        let shareButton = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 44))
        shareButton.addTarget(self, action: #selector(handleNavigationShare), for: .touchUpInside)
        shareButton.setImage(UIImage(named: "navigation_share_white"), for: .normal)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: shareButton)
        
        scrollView.frame = self.view.bounds
        scrollView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        scrollView.image = image
        self.view.addSubview(scrollView)
        scrollView.tapGestureRecognizer.addTarget(self, action: #selector(handleScrollViewTap))
        perform(#selector(handleScrollViewTap), with: nil, afterDelay: UI.hideNavigationBarDelay)
    }
    
    @objc private func handleScrollViewTap() {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(handleScrollViewTap), object: nil)
        if self.st_navigationBarHidden {
            st_setNavigationBarHidden(false, animated: false)
            self.st_maximumInteractivePopEdgeDistance = 30
        } else {
            st_setNavigationBarHidden(true, animated: false)
            self.st_maximumInteractivePopEdgeDistance = 0
        }
    }
    
    @objc private func handleNavigationShare() {
        let activityVC = UIActivityViewController(activityItems: [image], applicationActivities: nil)
        present(activityVC, animated: true)
    }
    
    override func st_didPop(animated: Bool) {
        super.st_didPop(animated: animated)
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(handleScrollViewTap), object: nil)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if #available(iOS 11.0, *) {
            scrollView.contentInset = self.view.safeAreaInsets
        }
    }
    
    @available(iOS 11.0, *)
    override func viewSafeAreaInsetsDidChange() {
        super.viewSafeAreaInsetsDidChange()
        self.view.setNeedsLayout()
    }
    
    @objc private func handleNavigationBack() {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(handleScrollViewTap), object: nil)
        self.st_navigationController?.dismiss(animated: true, completion: nil)
    }

}
