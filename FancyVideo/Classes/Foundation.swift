//
//  Foundation.swift
//  FancyVideo
//
//  Created by SunJiangting on 16/10/8.
//  Copyright © 2016年 Suen. All rights reserved.
//

import UIKit
import Foundation
import CoreGraphics
import GiantShoulder
import Network
import AdSupport


extension CGRect {
    
    func map(_ transform: (CGRect) -> CGRect) -> CGRect {
        return transform(self)
    }
    
    func wide(by: CGFloat) -> CGRect {
        var frame = self
        frame.size.width += by
        return frame
    }
    
    func higher(by: CGFloat) -> CGRect {
        var frame = self
        frame.size.height += by
        return frame
    }
    
    func moveLeft(by: CGFloat) -> CGRect {
        var frame = self
        frame.origin.x -= by
        return frame
    }
    
    func moveRight(by: CGFloat) -> CGRect {
        var frame = self
        frame.origin.x += by
        return frame
    }
    
    func moveUp(by: CGFloat) -> CGRect {
        var frame = self
        frame.origin.y -= by
        return frame
    }
    
    func moveDown(by: CGFloat) -> CGRect {
        var frame = self
        frame.origin.y += by
        return frame
    }
}

func appropriate<T>(size iphone4: T, iphone5: T? = nil, iphone6: T? = nil, iphone6p: T? = nil)->T {
    let height = max(UIScreen.main.bounds.height, UIScreen.main.bounds.width)
    if height >= 736 {
        return (iphone6p ?? iphone6) ?? iphone4
    }
    if height >= 667 {
        return (iphone6 ?? iphone6p) ?? iphone4
    }
    if height >= 536 {
        return iphone5 ?? iphone4
    }
    return iphone4
}

extension DispatchQueue {
    
    public func asyncAfter(delay: TimeInterval, execute work: @escaping @convention(block) () -> Swift.Void) {
        asyncAfter(deadline: .now() + .milliseconds(Int(delay * 1000)), execute: work)
    }
}

func getAppVersion() -> String {
    return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String ?? "1.0"
}

func - (size0: CGSize, size1: CGSize) -> CGSize {
    return CGSize(width: size0.width - size1.width, height: size0.height - size1.height)
}

func - (size: CGSize, edgeInsets: UIEdgeInsets) -> CGSize {
    return CGSize(width: size.width - (edgeInsets.left + edgeInsets.right), height: size.height - (edgeInsets.top + edgeInsets.bottom))
}

extension CGRect {
    
    var x: CGFloat {
        return self.origin.x
    }
    var y: CGFloat {
        return self.origin.y
    }
}

extension UIEdgeInsets {
    
    var width: CGFloat {
        return self.left + self.right
    }
    
    var height: CGFloat {
        return self.top + self.bottom
    }
}

extension String {
    
    func repeatBy(_ count: UInt) -> String {
        var oldValue = self
        for _ in 0..<count {
            oldValue += self
        }
        return oldValue
    }
}

extension Array {
    func slices(_ maxSplit: UInt) -> [[Element]] {
        let count = self.count
        var slices = [[Element]]()
        var slice = [Element]()
        for (idx, obj) in self.enumerated() {
            let i: Int = idx + 1
            slice.append(obj)
            if maxSplit == 0 || i % Int(maxSplit) == 0 || i == count {
                slices.append(slice)
                slice = []
            }
        }
        return slices
    }
}


extension Array {
    func distinct(using block: ((Element, Element)->Bool)) -> [Element] {
        var results = [Element]()
        for lhs in self {
            var contains = false
            for rhs in results {
                if block(lhs, rhs) {
                    contains = true
                    break
                }
            }
            if !contains {
                results.append(lhs)
            }
        }
        return results
    }
}

extension Array where Element : Equatable {
    func distinct() -> [Element] {
        return self.distinct(using: ==)
    }
}


extension Array where Element : Equatable {
    mutating func removeElement(_ element: Element) {
        if let index = self.index(of: element) {
            self.remove(at: index)
        }
    }
}

extension Array where Element : Equatable {
    mutating func replace(_ element: Element, with newElement: Element) {
        if let index = self.index(of: element) {
            self.replaceSubrange(Range(uncheckedBounds: (index,index + 1)), with: [newElement])
        }
    }
}


extension Array {
    /*
     Randomly shuffles the array in-place
     This is the Fisher-Yates algorithm, also known as the Knuth shuffle.
     Time complexity: O(n)
     */
    public mutating func shuffle() {
        for i in stride(from: (count - 1), through: 1, by: -1) {
            let j = random(i + 1)
            if i != j {
                self.swapAt(i, j)
            }
        }
    }
}


func -<T: Equatable>(lhs: [T], rhs: [T]) -> [T] {
    var results = [T]()
    for item in lhs {
        if !rhs.contains(item) {
            results.append(item)
        }
    }
    return results
}


func |<T: Equatable>(lhs: [T], rhs: [T]) -> [T] {
    var results = lhs
    for item in rhs {
        if !lhs.contains(item) {
            results.append(item)
        }
    }
    return results
}

func &<T: Equatable>(lhs: [T], rhs: [T]) -> [T] {
    var results = [T]()
    for item in rhs {
        if lhs.contains(item) {
            results.append(item)
        }
    }
    return results
}

extension Dictionary {
    
    func mapAll<K, V>(_ transform: (Key, Value) throws -> (K?, V?)) rethrows -> [K : V] {
        var results = [K : V]()
        for (k, v) in self {
            let transformedValue = try transform(k, v)
            if let newK = transformedValue.0, let newV = transformedValue.1  {
                results[newK] = newV
            }
        }
        return results
    }
    
    func filterAll(_ transform: (Key, Value) throws -> Bool) rethrows -> [Key : Value] {
        var results = [Key : Value]()
        for (k, v) in self {
            if try transform(k, v) {
                results[k] = v
            }
        }
        return results
    }
}

extension Array {
    
    func mapAll<T>(_ transform: (Element) throws -> T?) rethrows -> [T] {
        var results = [T]()
        for e in self {
            let newElement = try transform(e)
            if let newE = newElement {
                results.append(newE)
            }
        }
        return results
    }
}

extension Array where Element: Equatable {
    
    func hasPrefix(_ rhs: [Element]) -> Bool {
        if self.count == 0 && rhs.count == 0 {
            return true
        }
        if self.count < rhs.count {
            return false
        }
        if rhs.count == 0 {
            return true
        }
        for (idx, obj) in rhs.enumerated() {
            if self[idx] != obj {
                return false
            }
        }
        return true
    }
}

func ==<KeyType, ValueType: Equatable>(dict1: Dictionary<KeyType, ValueType>, dict2: Dictionary<KeyType, ValueType>) -> Bool {
    for (k, v) in dict1 {
        if v != dict2[k] {
            return false
        }
    }
    return true
}

func +=<KeyType, ValueType>(dict1: inout Dictionary<KeyType, ValueType>, dict2: Dictionary<KeyType, ValueType>?) {
    if let dict = dict2 {
        for (k, v) in dict {
            dict1.updateValue(v, forKey: k)
        }
    }
}

func +<KeyType, ValueType>(dict1: [KeyType : ValueType], dict2: [KeyType : ValueType]?) -> [KeyType : ValueType] {
    var results = dict1
    if let dict = dict2 {
        for (k, v) in dict {
            results.updateValue(v, forKey: k)
        }
    }
    return results
}

func -<KeyType, ValueType>(dict1: [KeyType : ValueType], dict2: [KeyType : ValueType]?) -> [KeyType : ValueType] {
    var results = dict1
    if let dict = dict2 {
        for (k, _) in dict {
            results.removeValue(forKey: k)
        }
    }
    return results
}

func -=<KeyType, ValueType>(dict1: inout Dictionary<KeyType, ValueType>, dict2: Dictionary<KeyType, ValueType>?) {
    if let dict = dict2 {
        for (k, _) in dict {
            dict1.removeValue(forKey: k)
        }
    }
}


func +=(lhs: inout String, rhs: String?) {
    if let rhs = rhs {
        lhs += rhs
    }
}

func *=(lhs: inout String, rhs: Int) {
    for _ in 0..<rhs {
        lhs += lhs
    }
}

func +(lhs: String, rhs: String?) -> String {
    if let rhs = rhs {
        return lhs + rhs
    }
    return lhs
}

extension Dictionary {
    
    var jsonString: String? {
        if let data = try? JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions()) {
            return String(data: data, encoding: .utf8)
        }
        return nil
    }
    
    func jsonString(with options: JSONSerialization.WritingOptions = JSONSerialization.WritingOptions()) -> String? {
        if let data = try? JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions()) {
            return String(data: data, encoding: .utf8)
        }
        return nil
    }
}

extension String {
    
    func size(with attributes: [NSAttributedStringKey: Any], constrainedTo size: CGSize, numberOfLines: Int = 0) -> CGSize {
        let title = self
        guard !title.isEmpty else {
            return .zero
        }
        var newAttributes = attributes
        if !(newAttributes[NSAttributedStringKey.font] is UIFont) {
            newAttributes[NSAttributedStringKey.font] = UIFont.systemFont(ofSize: 14)
        }
        let font = newAttributes[NSAttributedStringKey.font]! as! UIFont
        var lineSpacing: CGFloat = 0, fontHeight: CGFloat = font.lineHeight
        let pageStyle = newAttributes[NSAttributedStringKey.paragraphStyle] as? NSParagraphStyle
        if let style = pageStyle {
            lineSpacing = style.lineSpacing
            if style.maximumLineHeight > fontHeight {
                fontHeight = style.maximumLineHeight
            }
        }
        let lineHeight = lineSpacing + fontHeight
        let constrainedSize = CGSize(width: size.width, height: (numberOfLines == 0) ? size.height : lineHeight * CGFloat(numberOfLines))
        let size = (title as NSString).boundingRect(with: constrainedSize, options: [.usesFontLeading, .usesLineFragmentOrigin], attributes: newAttributes, context: nil).size
        return CGSize(width: ceil(size.width) + 2, height: ceil(size.height) + 5)
    }
    
    
    func size(with font: UIFont, constrainedTo size: CGSize, numberOfLines: Int = 0) -> CGSize {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineBreakMode = .byCharWrapping
        let size = self.size(with: [NSAttributedStringKey.font: font, NSAttributedStringKey.paragraphStyle: paragraphStyle], constrainedTo: size, numberOfLines: numberOfLines)
        return size
    }
}

public func random(_ n: Int) -> Int {
    return Int(arc4random_uniform(UInt32(n)))
}

extension URL {
    var isBlankURL: Bool {
        return absoluteString.lowercased().hasPrefix("about:blank")
    }
    
    var webViewCanOpen: Bool {
        guard let scheme = self.scheme?.lowercased(), let host = self.host?.lowercased() else {
            return false
        }
        if host.contains("itunes.apple.com") {
            return false
        }
        if scheme.hasPrefix("http") || scheme.hasPrefix("ftp") || scheme.hasPrefix("file") {
            return true
        }
        return false
    }
}

func makeFancyURL(_ text: String) -> URL? {
    guard !text.isEmpty else { return nil }
    func isValid(_ urlString: String) -> Bool {
        if urlString.contains(" ") || urlString.isEmpty {
            return false
        }
        if let dataDetector = try? NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue) {
            let nsURLString = urlString as NSString
            if let checkingResults = dataDetector.firstMatch(in: nsURLString as String, options: NSRegularExpression.MatchingOptions.reportCompletion, range: NSMakeRange(0, nsURLString.length)) {
                if checkingResults.resultType == .link && checkingResults.range.length == nsURLString.length {
                    return true
                }
            }
            return false
        }
        return true
    }
    
    if isValid(text) {
        if text.lowercased().hasPrefix("about:") {
            return URL(string: text)
        }
        var newURLString = text
        if !newURLString.contains("://") {
            newURLString = "http://" + text
        }
        if let url = URL(string: newURLString) {
            if ["http", "https"].contains(url.scheme?.lowercased() ?? "") {
                // Open WebView
                return url
            }
        }
        return nil
    } else {
        // Search
        let query = (text as NSString).st_stringByURLEncoded()
        let urlString = "https://www.baidu.com/s?ie=UTF-8&wd=\(query)"
        return URL(string: urlString)
    }
}

extension NSError {
    static func makeError(_ description: String) -> Error {
        return NSError(domain: "com.fancy.Error", code: 1001, userInfo: [NSLocalizedDescriptionKey: description])
    }
}


extension Network {
    static func request(_ urlString :URLString) -> RequestBuilder {
        return Network.request(urlString.rawValue)
    }
}

extension Error {
    var displayDescription: String? {
        return (self as DescribableError).description
    }
}

protocol DescribableError: Error {
    var description: String { get }
}

extension NSError: DescribableError {
    
    static var displayDescriptionKey: String {
        return  "errorDescription"
    }
    
    convenience init(domain: String, code: Int, errorDescription: String) {
        self.init(domain: domain, code: code, userInfo: [NSError.displayDescriptionKey: errorDescription])
    }
    
    open override var description: String {
        if let desc = userInfo[NSError.displayDescriptionKey] as? String, !desc.isEmpty {
            return desc
        }
        return self.localizedFailureReason ?? self.localizedDescription
    }
}

extension Request {
    
    enum ServerErrorCode: Int {
        case userDidNotLogin = 401
    }
    
    struct TransferError: Error, DescribableError {
        let description: String
        fileprivate init(_ description: String) {
            self.description = description
        }
    }
    
    struct ResponseError: Error, DescribableError {
        let code: Int
        let description: String
        let userInfo: [String: Any]?
        
        fileprivate init(_ code: Int, _ description: String, _ userInfo: [String: Any]? = nil) {
            self.code = code
            self.description = description
            self.userInfo = userInfo
        }
    }
    
    enum APIResult<T> {
        // let result
        case value(T?)
        // let msg, let userInfo
        case error(DescribableError)
    }
    
    private func isConnectError(_ error: NSError) -> Bool {
        let codes: [URLError.Code] = [.timedOut, .cannotFindHost, .cannotConnectToHost, .dnsLookupFailed, .networkConnectionLost, .notConnectedToInternet]
        return codes.map({ $0.rawValue }).contains(error.code)
    }
    
    @discardableResult func fv_response<T>(completion: ((URLRequest?, HTTPURLResponse?, APIResult<T>) -> Void)?)
        -> Self {
            return responseJSON { (req, resp, value, error) in
                if let error = error, self.isConnectError(error as NSError)  {
                    if STIsNetworkConnected() {
                        completion?(req, resp, .error(TransferError("NetworkError".localized())))
                    } else {
                        completion?(req, resp, .error(TransferError("NoNetworkError".localized())))
                    }
                } else if error != nil {
                    completion?(req, resp, .error(TransferError("ServerError".localized())))
                } else {
                    if let value = value as? [String: Any] {
                        let errno = (value["err_no"] as? NSNumber)?.intValue ?? 0
                        if errno == 0 {
                            completion?(req, resp, .value(value["results"] as? T))
                        } else {
                            let msg = (value["err_msg"] as? String) ?? "ServerError".localized()
                            completion?(req, resp, .error(ResponseError(errno, msg)))
                        }
                    } else {
                        completion?(req, resp, .error(TransferError("ServerError".localized())))
                    }
                }
            }
    }
}


func parseVideoURL(_ parameters: [String: Any]) -> URL? {
    let videoURL: URL?
    if let videoInfo = parameters["videoInfo"] as? [String: Any], let videoURLString = videoInfo["videoUrl"] as? String  {
        if !videoURLString.isEmpty, let url = URL(string: videoURLString) {
            videoURL = url
        } else {
            videoURL = nil
        }
    } else if let videoURLString = parameters["videoInfo"] as? String {
        if !videoURLString.isEmpty, let url = URL(string: videoURLString) {
            videoURL = url
        } else {
            videoURL = nil
        }
    } else if let videoURLString = parameters["videoUrl"] as? String {
        if !videoURLString.isEmpty, let url = URL(string: videoURLString) {
            videoURL = url
        } else {
            videoURL = nil
        }
    } else {
        videoURL = nil
    }
    return videoURL
}
