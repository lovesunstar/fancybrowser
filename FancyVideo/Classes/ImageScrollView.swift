//
//  ImageScrollView.swift
//  TestImageScrollView
//
//  Created by SunJiangting on 2018/3/9.
//  Copyright © 2018年 M&ETimes. All rights reserved.
//

import UIKit

final class ImageScrollView: UIView {
    
    private let scrollView = UIScrollView()
    
    private let imageView = UIImageView()
    
    let tapGestureRecognizer = UITapGestureRecognizer()
    
    var contentInset: UIEdgeInsets = .zero {
        didSet {
            scrollView.contentInset = contentInset
            zoomToFit()
            maximumZoomScaleToFit()
        }
    }
    
    var image: UIImage? {
        didSet {
            imageView.image = image
            zoomToFit()
            maximumZoomScaleToFit()
            locationToFit()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadSubviews()
    }
    
    private func loadSubviews() {
        self.backgroundColor = UIColor.black
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.minimumZoomScale = 1.0
        scrollView.delaysContentTouches = false
        scrollView.alwaysBounceVertical = true
        scrollView.alwaysBounceHorizontal = true
        scrollView.delegate = self
        if #available(iOS 11.0, *) {
            scrollView.contentInsetAdjustmentBehavior = .never
        }
        addSubview(scrollView)
        
        imageView.contentMode = .scaleAspectFit
        scrollView.addSubview(imageView)
        
        let doubleTapGesture = UITapGestureRecognizer(target: self, action: #selector(handleDoubleTap(_:)))
        doubleTapGesture.numberOfTapsRequired = 2
        doubleTapGesture.numberOfTouchesRequired = 1
        scrollView.addGestureRecognizer(doubleTapGesture)
        
        tapGestureRecognizer.numberOfTapsRequired = 1
        tapGestureRecognizer.numberOfTouchesRequired = 1
        scrollView.addGestureRecognizer(tapGestureRecognizer)
        tapGestureRecognizer.require(toFail: doubleTapGesture)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        scrollView.frame = self.bounds
        zoomToFit()
        maximumZoomScaleToFit()
        locationToFit()
    }
    
    private func zoomToFit() {
        guard let image = self.image else {
            scrollView.zoomScale = 1.0
            return
        }
        scrollView.zoomScale = 1.0
        let size = scrollView.bounds.size
        let targetSize = preferredSize(for: image)
        let imageViewFrame = CGRect(x: (size.width - targetSize.width) * 0.5, y: (size.height - targetSize.height) * 0.5, width: targetSize.width, height: targetSize.height)
        imageView.frame = imageViewFrame
    }
    
    private func preferredSize(for image: UIImage) -> CGSize {
        let width = scrollView.bounds.width - (contentInset.left + contentInset.right), height = (scrollView.bounds.height - (contentInset.top + contentInset.bottom))
        let imageWidth = image.size.width, imageHeight = image.size.height
        guard width > 0, height > 0, imageWidth > 0, imageHeight > 0 else {
            return .zero
        }
        if (imageWidth <= width && imageHeight <= height) {
            return CGSize(width: imageWidth, height: imageHeight)
        }
        let imageRatio = imageWidth / imageHeight, viewRatio = width / height
        // iw / ih = pw/ph -> pw = ph * ir; ph = pw / ir
        if imageRatio > viewRatio {
            return CGSize(width: width, height: width / imageRatio)
        } else {
            return CGSize(width: height * imageRatio, height: height)
        }
    }

    private func maximumZoomScaleToFit() {
        guard let image = image else {
            scrollView.maximumZoomScale = 1.0
            return
        }
        let viewSize = scrollView.bounds.size
        let imageSize = image.size
        let imageViewSize = preferredSize(for: image)
        let imageViewWidth = imageViewSize.width, imageViewHeight = imageViewSize.height
        let imageWidth = imageSize.width, imageHeight = imageSize.height
        guard imageWidth > 0, imageHeight > 0, imageViewWidth > 0, imageViewHeight > 0 else {
            scrollView.maximumZoomScale = 1.0
            return
        }
        var scale: CGFloat = 1.0
        let imageRatio = imageWidth / imageHeight, viewRatio = viewSize.width / viewSize.height
        if imageRatio > viewRatio {
            scale = (imageHeight * 0.5 > viewSize.height) ? (imageHeight / (imageViewHeight * 2)) : (viewSize.height / imageViewHeight)
        } else {
            scale = (imageWidth * 0.5 > viewSize.width) ? (imageWidth / (2 * imageViewWidth)) : (viewSize.width / imageViewWidth)
        }
        scrollView.maximumZoomScale = max(3.0, scale)
    }
    
    private func locationToFit() {
        let visibleSize = CGSize(width: scrollView.bounds.width - scrollView.contentInset.left - scrollView.contentInset.right, height: scrollView.bounds.height - scrollView.contentInset.top - scrollView.contentInset.bottom)
        let contentSize = scrollView.contentSize
        let centerX = max(contentSize.width, visibleSize.width) * 0.5
        let centerY = max(contentSize.height, visibleSize.height) * 0.5
        imageView.center = CGPoint(x: centerX, y: centerY)
    }
    
    private func zoom(to location: CGPoint) {
        let rect: CGRect
        if scrollView.zoomScale == scrollView.maximumZoomScale {
            rect = scrollView.bounds
        } else {
            rect = zoomRect(for: scrollView.maximumZoomScale, center: location)
        }
        scrollView.zoom(to: rect, animated: true)
    }
    
    private func zoomRect(for scale: CGFloat, center: CGPoint) -> CGRect {
        guard scale != 0 else {
            return scrollView.bounds
        }
        let width = scrollView.bounds.width, height = scrollView.bounds.height - (contentInset.top + contentInset.bottom)
        var zoomRect: CGRect = .zero
        zoomRect.origin.x = center.x - (width / scale) * 0.5
        zoomRect.origin.y = center.y - (height / scale) * 0.5
        zoomRect.size.width = width / scale
        zoomRect.size.height = height / scale
        return zoomRect
    }

    @objc private func handleDoubleTap(_ gesture: UITapGestureRecognizer) {
        zoom(to: gesture.location(in: self))
    }
}

extension ImageScrollView: UIScrollViewDelegate {
    
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        locationToFit()
    }
    
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        scrollView.setZoomScale(scale, animated: false)
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    
}
