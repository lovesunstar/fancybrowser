//
//  OpenInSafariActivity.swift
//  FancyVideo
//
//  Created by SunJiangting on 2018/3/13.
//  Copyright © 2018年 Samaritan. All rights reserved.
//

import UIKit

class OpenInSafariActivity: UIActivity {

    private let url: URL
    
    init(url: URL) {
        self.url = url
        super.init()
    }
    
    override var activityTitle: String? {
        return "OpenInSafari".localized()
    }
    
    override var activityType: UIActivityType? {
        return UIActivityType(rawValue: "com.fancy.activity.openInSafari")
    }
    
    override var activityImage: UIImage? {
        return UIImage(named: "safari_activity")
    }
    
    override func perform() {
        UIApplication.shared.open(url, options: [:]) { (_) in
        
        }
        activityDidFinish(true)
    }
    
    override class var activityCategory: UIActivityCategory {
        return .share
    }
    
    override func canPerform(withActivityItems activityItems: [Any]) -> Bool {
        guard let delegate = UIApplication.shared.delegate, let delegateWindow = delegate.window, let _ = delegateWindow else {
            return false
        }
        return true
    }
}
