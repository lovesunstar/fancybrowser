//
//  SafariViewController.swift
//  FancyVideo
//
//  Created by SunJiangting on 2018/5/4.
//  Copyright © 2018年 Samaritan. All rights reserved.
//

import UIKit

protocol SafariZoomable: NSObjectProtocol {
    var isSafariZoomed: Bool { get set}
}

class SafariViewController: UIViewController {
    
    private lazy var rootViewController = MainTabBarController()
    
    private var dataSource = [UIViewController]()
    
    private lazy var pageController = SCSafariPageController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(rgb: 0x16151d)
        // Do any additional setup after loading the view.
        dataSource.insert(rootViewController, at: 0)
        
        pageController.dataSource = self
        pageController.delegate = self
        
        addChildViewController(pageController)
        pageController.view.frame = self.view.bounds
        pageController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.addSubview(pageController.view)
        pageController.didMove(toParentViewController: self)
    }
    
    func expose(animated: Bool) {
        pageController.zoomOut(animated: animated, completion: nil)
    }
    
    func show(_ viewController: UIViewController, animated: Bool, at index: Int? = nil) {
        // 第 0 位置被固定了, 无法插入, 从第一个位置开始插入
        var validIndex = ((index ?? Int(pageController.currentPage)) + 1)
        if validIndex > (dataSource.count) {
            validIndex = dataSource.count
        }
        dataSource.insert(viewController, at: validIndex)
        UIApplication.shared.beginIgnoringInteractionEvents()
        pageController.insertPages(at: IndexSet(integer: Int(validIndex)), animated: animated) { () -> Void in
            if animated {
                self.pageController.zoomOut(animated: true, completion: {
                  self.pageController.zoomIntoPage(at: UInt(validIndex), animated: true, completion: nil)
                    UIApplication.shared.endIgnoringInteractionEvents()
                })
            } else {
                self.pageController.navigateToPage(at: UInt(validIndex), animated: false, completion: nil)
                UIApplication.shared.endIgnoringInteractionEvents()
            }
        }
    }
    
    func close(_ viewController: UIViewController, animated: Bool) {
        guard let idx = dataSource.index(of: viewController) else {
            return
        }
        dataSource.remove(at: idx)
        pageController.deletePages(at: IndexSet(integer: idx), animated: animated, completion: nil)
    }
    
    var visibleViewController: UIViewController? {
        if pageController.isZoomedOut {
            return self
        }
        let vcIdx = pageController.currentPage
        if vcIdx >= dataSource.count {
            return self
        }
        return dataSource[Int(vcIdx)]
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        guard let visibleVC = visibleViewController, visibleVC != self else {
            return .default
        }
        return visibleVC.preferredStatusBarStyle
    }
    
    @available(iOS 11.0, *)
    override func prefersHomeIndicatorAutoHidden() -> Bool {
        guard let visibleVC = visibleViewController, visibleVC != self else {
            return false
        }
        return visibleVC.prefersHomeIndicatorAutoHidden()
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
}

extension SafariViewController: SCSafariPageControllerDataSource {
    
    func numberOfPages(in pageController: SCSafariPageController!) -> UInt {
        return UInt(dataSource.count)
    }
    
    func pageController(_ pageController: SCSafariPageController!, viewControllerForPageAt index: UInt) -> UIViewController! {
        return dataSource[Int(index)]
    }
}

extension SafariViewController: SCSafariPageControllerDelegate {
    
    func pageController(_ pageController: SCSafariPageController!, willDeletePageAt pageIndex: UInt) {
        if pageIndex < dataSource.count {
            dataSource.remove(at: Int(pageIndex))
        }
    }
    
    func pageController(_ pageController: SCSafariPageController!, didDeletePageAt pageIndex: UInt) {
        if dataSource.count == 1 {
            pageController.zoomIntoPage(at: 0, animated: true, completion: nil)
        }
    }
    
    func pageController(_ pageController: SCSafariPageController!, willZoomInAnimated animated: Bool) {
        dataSource.forEach { (vc) in
            (vc as? SafariZoomable)?.isSafariZoomed = false
            vc.view.setNeedsLayout()
        }
    }
    
    func pageController(_ pageController: SCSafariPageController!, willZoomOutAnimated animated: Bool) {
        dataSource.forEach { (vc) in
            (vc as? SafariZoomable)?.isSafariZoomed = true
            vc.view.setNeedsLayout()
        }
    }
}

extension UIViewController {
    var st_safariController: SafariViewController? {
        guard let pvc = self.sc_pageController else {
            return nextResponder(SafariViewController.self)
        }
        return pvc.nextResponder(SafariViewController.self)
    }
}
