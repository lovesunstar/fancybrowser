//
//  PlaceAtTopActivity.swift
//  FancyVideo
//
//  Created by SunJiangting on 2018/4/24.
//  Copyright © 2018年 Samaritan. All rights reserved.
//

import UIKit

class PlaceAtTopActivity: UIActivity {
    
    let handler: (PlaceAtTopActivity) -> Void
    
    init(handler: @escaping (PlaceAtTopActivity) -> Void) {
        self.handler = handler
        super.init()
    }
    
    override var activityTitle: String? {
        return "PlaceAtTop".localized()
    }
    
    override var activityType: UIActivityType? {
        return UIActivityType(rawValue: "com.fancy.activity.placeAtTop")
    }
    
    override var activityImage: UIImage? {
        return UIImage(named: "place_to_top_activity")
    }
    
    override func perform() {
        handler(self)
        activityDidFinish(true)
    }
    
    override class var activityCategory: UIActivityCategory {
        return .share
    }
    
    override func canPerform(withActivityItems activityItems: [Any]) -> Bool {
        guard let delegate = UIApplication.shared.delegate, let delegateWindow = delegate.window, let _ = delegateWindow else {
            return false
        }
        return true
    }
}

