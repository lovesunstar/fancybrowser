//
//  BaseNavigationController.swift
//  FancyVideo
//
//  Created by SunJiangting on 2018/3/6.
//  Copyright © 2018年 Samaritan. All rights reserved.
//

import UIKit
import GiantShoulder

class BaseNavigationController: STNavigationController {

    @available(iOS 11.0, *)
    override func prefersHomeIndicatorAutoHidden() -> Bool {
        return visibleViewController?.prefersHomeIndicatorAutoHidden() ?? false
    }

    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var prefersStatusBarHidden: Bool {
        return visibleViewController?.prefersStatusBarHidden ?? false
    }
}
