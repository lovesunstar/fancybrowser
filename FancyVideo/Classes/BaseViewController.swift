//
//  BaseViewController.swift
//  FancyVideo
//
//  Created by SunJiangting on 2017/6/9.
//  Copyright © 2017年 Samaritan. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController, SafariZoomable {
    
    var statusBarStyle: UIStatusBarStyle = .default
    var temporaryStatusBarStyle: UIStatusBarStyle? = nil
    
    private var backBarButtonItem: UIBarButtonItem?
    private var observerContext = 0
    private var isObservingNavigationItem = false
    
    var isSafariZoomed: Bool = false
    
    var isHomeIndicatorHidden = false {
        didSet {
            if #available(iOS 11.0, *) {
                setNeedsUpdateOfHomeIndicatorAutoHidden()
            }
        }
    }
    
    deinit {
        if isObservingNavigationItem {
            self.navigationItem.removeObserver(self, forKeyPath: #keyPath(UINavigationItem.hidesBackButton))
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.st_navigationBar?.separatorView.backgroundColor = UIColor.st_color(rgb: 0xDDDDDD)
        self.view.backgroundColor = UIColor(rgb: 0xf5f6f7)
        self.navigationItem.addObserver(self, forKeyPath: #keyPath(UINavigationItem.hidesBackButton), options: .new, context: &observerContext)
        isObservingNavigationItem = true
        createNavigationBackButtonIfNeeded()
        self.view.backgroundColor = UIColor.white
        self.st_maximumInteractivePopEdgeDistance = self.view.bounds.width * 0.5
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if context == &observerContext {
            guard let keyPath = keyPath, let object = object as? NSObject else {
                return
            }
            if object == self.navigationItem && keyPath == #keyPath(UINavigationItem.hidesBackButton) {
                if self.navigationItem.hidesBackButton && self.navigationItem.leftBarButtonItem == self.backBarButtonItem {
                    self.navigationItem.leftBarButtonItem = nil
                } else if !self.navigationItem.hidesBackButton && self.backBarButtonItem == nil {
                    createNavigationBackButtonIfNeeded()
                }
            }
        } else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }
    
    private func createNavigationBackButtonIfNeeded() {
        if self.navigationItem.hidesBackButton {
            return
        }
        if let nav = self.st_navigationController, let vcs = nav.viewControllers , vcs.count != 1 && self.navigationItem.leftBarButtonItem == nil {
            let backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 44))
            backButton.setImage(UIImage(named: "navigation_back"), for: .normal)
            backButton.imageEdgeInsets = UIEdgeInsetsMake(11, 15, 11, 0)
            backButton.contentHorizontalAlignment = .left
            backButton.addTarget(self, action: #selector(handleBackAction), for: .touchUpInside)
            self.backBarButtonItem = UIBarButtonItem(customView: backButton)
            self.navigationItem.leftBarButtonItem = self.backBarButtonItem
        }
    }
    
    @objc private func handleBackAction() {
        let _ = self.st_navigationController?.popViewController(animated: true)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override func st_viewWillAppear(_ animated: Bool) {
        super.st_viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return temporaryStatusBarStyle ?? statusBarStyle
    }
    
    override func prefersHomeIndicatorAutoHidden() -> Bool {
        return isHomeIndicatorHidden
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
}
