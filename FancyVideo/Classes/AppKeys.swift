//
//  AppKeys.swift
//  VEnjoy
//
//  Created by SunJiangting on 16/10/8.
//  Copyright © 2016年 Suen. All rights reserved.
//

import UIKit

class AppKeys {
    
    static let admobAppID = "ca-app-pub-4216054279520142~9147699030"
    static let admobDefaultUnitID = "ca-app-pub-4216054279520142/3580511734"
    static let admobBannerID = "ca-app-pub-4216054279520142/1337292083"
    static let appStoreID = "1198427263"
    
}
