//
//  AppDelegate.swift
//  FancyVideo
//
//  Created by SunJiangting on 2017/6/8.
//  Copyright © 2017年 Samaritan. All rights reserved.
//

import UIKit
import AVFoundation
import GiantShoulder
import Network
import GoogleMobileAds

enum LaunchADConfiguration {
    static let adWindowAppearLevel: CGFloat = UIWindowLevelStatusBar + 1
    static let adWindowDisppearLevel: CGFloat = UIWindowLevelNormal
    
    static let launchPreviousDateKey: String = "com.charles.launch.previousLaunchDate"
}

private let launchScreenViewController = UIStoryboard(name: "Launch", bundle: nil).instantiateInitialViewController() ?? UIViewController()

extension Notification.Name {
    static let willDisplayLaunchAD = Notification.Name(rawValue: "com.charles.willDisplayLaunchAD")
    static let didDismissLaunchAD = Notification.Name(rawValue: "com.charles.didDismissLaunchAD")
}


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    private var isDisplayingLaunchAD = false
    
    private lazy var adWindow = LaunchADWindow()
    
    private var beginDate: Date?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        Network.client = NetworkClient.self
        ConfigManager.shared.update()
        GADMobileAds.configure(withApplicationID: ConfigManager.shared.adConfig.appID)
        Tracker.default.appDidFinishLaunch()
        application.isStatusBarHidden = false
        STNavigationBar.appearance().separatorColor = UIColor(rgb: 0xDDDDDD)
        STNavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor.rawValue: UIColor(rgb: 0x555555)]
        let window = UIWindow(frame: UIScreen.main.bounds)
        self.window = window
        
        window.windowLevel = UIWindowLevelNormal
        window.rootViewController = SafariViewController()
        window.makeKeyAndVisible()
        
        if #available(iOS 10.0, *) {
            try? AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: [.allowBluetooth, .defaultToSpeaker, .allowBluetoothA2DP, .allowAirPlay])
        } else {
            // Fallback on earlier versions
            try? AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: [.allowBluetooth, .defaultToSpeaker])
        }
        beginDate = Date()
        if !Device.isSimulator {
            showLaunchAD(true)
        }
        hitNetwork()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        trackAppStay()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        beginDate = Date()
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        trackAppStay()
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        guard let host = url.host, let components = URLComponents(url: url, resolvingAgainstBaseURL: false) else {
            return false
        }
        if host == "website" {
            guard let urlString = components.queryItems?.filter({$0.name == "url"}).first?.value, let newURL = URL(string: urlString) else {
                return false
            }
            (self.window?.rootViewController as? SafariViewController)?.show(BrowserViewController(url: newURL), animated: true)
        }
        return true
    }
    
    private func trackAppStay() {
        guard let date = self.beginDate else {
            return
        }
        let duration = Date().timeIntervalSince(date)
        track("App Stay", properties: ["Stay Time": duration])
        beginDate = nil
    }
    
    private func showLaunchAD(_ force: Bool) {
        if !STIsNetworkConnected() {
            return
        }
        guard let launchAD = ConfigManager.shared.adConfig.launch else {
            return
        }
        if !launchAD.display {
            return
        }
        if isDisplayingLaunchAD {
            return
        }
        let previousADShowTimestamp = UserDefaults.standard.value(forKey: LaunchADConfiguration.launchPreviousDateKey) as? TimeInterval ?? 0
        if (Date().timeIntervalSince1970 - previousADShowTimestamp) < launchAD.displayInterval {
            return
        }
        isDisplayingLaunchAD = true
        
        let snapshotTag = 1001
        
        NotificationCenter.default.post(name: .willDisplayLaunchAD, object: nil)
        let window = self.window!
        
        self.adWindow.windowLevel = LaunchADConfiguration.adWindowAppearLevel
        self.adWindow.isHidden = false
        if let view = self.adWindow.vc.view.viewWithTag(snapshotTag) {
            view.removeFromSuperview()
        }
        track("AD Request", properties: ["Type": "Launch"])
        let request = InterstitialAD.request(launchAD) { (response) in
            switch response {
            case .success(let interstitial):
                interstitial.present(fromRootViewController: self.adWindow.rvc)
                track("AD Show", properties: ["Type": "Launch"])
                track("AD Request Success", properties: ["Type": "Launch"])
                UserDefaults.standard.setValue(Date().timeIntervalSince1970, forKey: LaunchADConfiguration.launchPreviousDateKey)
                UserDefaults.standard.synchronize()
            case .failure(_):
                self.adWindow.isHidden = true
                self.isDisplayingLaunchAD = false
                self.adWindow.windowLevel = UIWindowLevelNormal
                NotificationCenter.default.post(name: .didDismissLaunchAD, object: nil)
                track("AD Request Failure", properties: ["Type": "Launch"])
            }
        }
        request.willEndDisplayHandler = { () in
            if let view = window.snapshotView(afterScreenUpdates: false) {
                view.tag = snapshotTag
                view.frame = self.adWindow.vc.view.bounds
                self.adWindow.vc.view.addSubview(view)
            }
            self.adWindow.windowLevel = UIWindowLevelNormal
        }
        request.displayCompletionHandler = { (_, _) in
            if let view = self.adWindow.vc.view.viewWithTag(snapshotTag) {
                view.removeFromSuperview()
            }
            self.adWindow.isHidden = true
            self.isDisplayingLaunchAD = false
            NotificationCenter.default.post(name: .didDismissLaunchAD, object: nil)
        }
    }
    
    private func hitNetwork() {
        URLSession.shared.dataTask(with: URL(string: "https://www.apple.com/")!) { (_, _, _) in
            
        }.resume()
    }
    
    private class LaunchADWindow: UIWindow {
        
        fileprivate let vc = launchScreenViewController
        fileprivate let rvc = UIViewController()
        
        init() {
            super.init(frame: UIScreen.main.bounds)
            self.rootViewController = rvc
            rvc.view.addSubview(vc.view)
            vc.view.frame = rvc.view.bounds
            vc.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
}

