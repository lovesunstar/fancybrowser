//
//  SCSafariPageWrapperViewController.m
//  SCSafariPageController
//
//  Created by Stefan Ceriu on 6/30/15.
//  Copyright (c) 2015 Stefan Ceriu. All rights reserved.
//

#import "SCSafariPageWrapperViewController.h"

@interface SCSafariPageWrapperViewControllerView : UIView

@end

@interface SCSafariPageWrapperViewController () <UIScrollViewDelegate, UIGestureRecognizerDelegate>

@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, strong) UIViewController *childViewController;

@property (nonatomic, strong) UITapGestureRecognizer *tapToZoomOutGesture;

@end

@implementation SCSafariPageWrapperViewController

- (instancetype)initWithViewController:(UIViewController *)viewController {
	if(self = [super init]) {
		_childViewController = viewController;
        _couldRemoveOnSwipe = YES;
	}
	
	return self;
}

- (void)loadView {
    self.view = [[SCSafariPageWrapperViewControllerView alloc] init];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColor.clearColor;
    self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.view.frame = self.childViewController.view.frame;
    
    UITapGestureRecognizer *tapToZoomOutGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleBackgoundTapped)];
    tapToZoomOutGesture.numberOfTouchesRequired = 1;
    tapToZoomOutGesture.numberOfTapsRequired = 1;
    tapToZoomOutGesture.delegate = self;
    [self.view addGestureRecognizer:tapToZoomOutGesture];
    self.tapToZoomOutGesture = tapToZoomOutGesture;
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    self.scrollView.delegate = self;
    self.scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.backgroundColor = UIColor.clearColor;
    self.scrollView.clipsToBounds = NO;
    self.scrollView.scrollEnabled = self.scrollEnabled && self.couldRemoveOnSwipe;
    self.scrollView.pagingEnabled = YES;
    self.scrollView.directionalLockEnabled = YES;
    [self.view addSubview:self.scrollView];
	
	[self addChildViewController:self.childViewController];
	[self.scrollView addSubview:self.childViewController.view];
    self.childViewController.view.frame = self.scrollView.bounds;
    self.childViewController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	[self.childViewController didMoveToParentViewController:self];
}

- (void)handleBackgoundTapped {
    if ([self.delegate respondsToSelector:@selector(safariPageWrapperViewControllerDidTapped:)]) {
        [self.delegate safariPageWrapperViewControllerDidTapped:self];
    }
}

- (void)viewWillLayoutSubviews {
	[super viewWillLayoutSubviews];
    self.scrollView.contentSize = CGSizeMake(CGRectGetWidth(self.view.bounds) * 2.0f, 0.0f);
}

- (void)setScrollEnabled:(BOOL)scrollEnabled {
	if(_scrollEnabled == scrollEnabled) {
		return;
	}
	_scrollEnabled = scrollEnabled;
    self.scrollView.scrollEnabled = scrollEnabled && self.couldRemoveOnSwipe;
}

- (void)setCouldRemoveOnSwipe:(BOOL)couldRemoveOnSwipe {
    _couldRemoveOnSwipe = couldRemoveOnSwipe;
    if (![self isViewLoaded]) {
        return;
    }
    self.scrollView.scrollEnabled = _scrollEnabled && couldRemoveOnSwipe;
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
	CGFloat percentage = scrollView.contentOffset.x / CGRectGetWidth(self.scrollView.bounds);
	self.scrollPercentage = percentage;
	
	if([self.delegate respondsToSelector:@selector(safariPageWrapperViewController:didScrollToPercentage:)]) {
		[self.delegate safariPageWrapperViewController:self didScrollToPercentage:percentage];
	}
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView
					 withVelocity:(CGPoint)velocity
			  targetContentOffset:(inout CGPoint *)targetContentOffset {
	CGFloat percentage = targetContentOffset->x /  CGRectGetWidth(self.scrollView.bounds);
	if([self.delegate respondsToSelector:@selector(safariPageWrapperViewControllerDidEndDragging:atPercentage:)]) {
		[self.delegate safariPageWrapperViewControllerDidEndDragging:self atPercentage:percentage];
	}
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if (gestureRecognizer == self.tapToZoomOutGesture) {
        return !self.childViewController.view.userInteractionEnabled;
    }
    return YES;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return [self.childViewController preferredStatusBarStyle];
}

- (BOOL)prefersHomeIndicatorAutoHidden {
    return [self.childViewController prefersHomeIndicatorAutoHidden];
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

@end

@implementation SCSafariPageWrapperViewControllerView

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
	UIView *hitView = [super hitTest:point withEvent:event];
	
	if([hitView isEqual:self]) {
		return nil;
	}
	return hitView;
}

@end
