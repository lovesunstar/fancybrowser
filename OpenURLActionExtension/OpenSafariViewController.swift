//
//  OpenSafariViewController.swift
//  OpenURLActionExtension
//
//  Created by SunJiangting on 2018/3/10.
//  Copyright © 2018年 Samaritan. All rights reserved.
//

import UIKit
import ObjectiveC
import MobileCoreServices

extension UIResponder {
    func fvae_nextResponder<T>(_: T.Type) -> T? {
        var nextResponder:UIResponder? = self
        while (nextResponder != nil) && !(nextResponder is T) {
            nextResponder = nextResponder?.next
        }
        if nextResponder is T {
            return nextResponder as? T
        }
        return nil
    }
}

class OpenSafariViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        preferredContentSize = .zero
        modalPresentationStyle = .custom
        self.view.backgroundColor = UIColor.clear
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        handleRequest()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func beginRequest(with context: NSExtensionContext) {
        super.beginRequest(with: context)
    }
    
    private func handleRequest() {
        guard let context = self.extensionContext else {
            return
        }
        guard let inputItems = context.inputItems as? [NSExtensionItem] else {
            context.cancelRequest(withError: NSError(domain: "com.fancybrowser.errorDomain", code: 1001, userInfo: [NSExtensionItemsAndErrorsKey: []]))
            return
        }
        let attachments = inputItems.compactMap({$0.attachments as? [NSItemProvider]}).flatMap({$0})
        let urlProviders = attachments.filter({ $0.hasItemConformingToTypeIdentifier(String(kUTTypeURL)) })
        guard let urlProvider = urlProviders.first else {
            context.cancelRequest(withError: NSError(domain: "com.fancybrowser.errorDomain", code: 1001, userInfo: [NSExtensionItemsAndErrorsKey: inputItems]))
            return
        }
        urlProvider.loadItem(forTypeIdentifier: String(kUTTypeURL), options: nil) { [weak self] (obj, error) in
            guard let strongSelf = self else {
                return
            }
            let characters = CharacterSet(charactersIn:"`#%^{}\"[]|\\<>//").inverted
            guard let url = (obj as? URL), let urlStringInQuery = url.absoluteString
                .addingPercentEncoding(withAllowedCharacters: characters), let newURL = URL(string: "fancybrowser://website?url=\(urlStringInQuery)") else {
                    context.completeRequest(returningItems: nil, completionHandler: nil)
                    return
            }
            strongSelf.openURL(newURL)
        }
    }

    private func openURL(_ url: URL) {
        guard let application = self.fvae_nextResponder(UIApplication.self) else {
            self.extensionContext?.completeRequest(returningItems: nil, completionHandler: nil)
            return
        }
        application.fvae_open(url)
        self.extensionContext?.completeRequest(returningItems: nil, completionHandler: nil)
    }
}
