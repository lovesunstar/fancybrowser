//
//  NSObject+PerformSelector.h
//  OpenURLActionExtension
//
//  Created by SunJiangting on 2018/3/10.
//  Copyright © 2018年 Samaritan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSObject (PerformSelector)

- (nullable id)fvae_performSelector:(nonnull SEL)aSelector withObjects:(nullable id)object, ...;

@end

@interface UIApplication(OpenURL)

- (void)fvae_openURL:(nonnull NSURL *)newURL;

@end
