//
//  NSObject+PerformSelector.m
//  OpenURLActionExtension
//
//  Created by SunJiangting on 2018/3/10.
//  Copyright © 2018年 Samaritan. All rights reserved.
//

#import "NSObject+PerformSelector.h"

NSValue *FVAECreateValueFromPrimitivePointer(void *pointer, const char *objCType) {
    // CASE marcro inspired by https://www.mikeash.com/pyblog/friday-qa-2013-02-08-lets-build-key-value-coding.html
#define CASE(ctype)                                                                                                                                  \
if (strcmp(objCType, @encode(ctype)) == 0) {                                                                                                     \
return @((*(ctype *)pointer));                                                                                                               \
}
    CASE(BOOL);
    CASE(char);
    CASE(unsigned char);
    CASE(short);
    CASE(unsigned short);
    CASE(int);
    CASE(unsigned int);
    CASE(long);
    CASE(unsigned long);
    CASE(long long);
    CASE(unsigned long long);
    CASE(float);
    CASE(double);
#undef CASE
    @try {
        return [NSValue valueWithBytes:pointer objCType:objCType];
    }
    @catch (NSException *exception) {
    }
    return nil;
}

@implementation NSObject (PerformSelector)

/// 注明： 如果返回值为基本类型，struct除外，其余都转换为NSNumber。 如果返回值是struct。则转为NSValue
- (id)fvae_performSelector:(SEL)aSelector withObjects:(id)object, ... {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    NSMutableArray *parameters = [NSMutableArray arrayWithCapacity:2];
    if (object) {
        [parameters addObject:object];
        va_list arglist;
        va_start(arglist, object);
        id arg;
        while ((arg = va_arg(arglist, id))) {
            if (arg) {
                [parameters addObject:arg];
            }
        }
        va_end(arglist);
    }
    if (![self respondsToSelector:aSelector]) {
        return nil;
    }
    NSMethodSignature *methodSignature = [self methodSignatureForSelector:aSelector];
    if (!methodSignature) {
        return nil;
    }
    NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:methodSignature];
    invocation.selector = aSelector;
    NSUInteger numberOfArguments = methodSignature.numberOfArguments;
    if (numberOfArguments > 2) {
        for (int i = 2; i < numberOfArguments; i++) {
            NSInteger idx = i - 2;
            id parameter = (parameters.count > idx) ? parameters[idx] : nil;
            const char *argumentType = [methodSignature getArgumentTypeAtIndex:i];
            if (strcmp(argumentType, @encode(id)) == 0 || strcmp(argumentType, @encode(Class)) == 0) {
                [invocation setArgument:&parameter atIndex:i];
            } else if (![parameter isKindOfClass:[NSNumber class]]) {
                [invocation setArgument:&parameter atIndex:i];
            } else {
                NSNumber *value = parameter;
                BOOL hasProcessed = NO;
#define CASE(type, selectorPart) \
if (!hasProcessed && strcmp(argumentType, @encode(type)) == 0) { \
type primitiveValue = [value selectorPart ## Value]; \
[invocation setArgument:&primitiveValue atIndex:i]; \
hasProcessed = YES; \
}
                CASE(char, char);
                CASE(unsigned char, unsignedChar);
                CASE(short, short);
                CASE(unsigned short, unsignedShort);
                CASE(int, int);
                CASE(unsigned int, unsignedInt);
                CASE(long, long);
                CASE(unsigned long, unsignedLong);
                CASE(long long, longLong);
                CASE(unsigned long long, unsignedLongLong);
                CASE(float, float);
                CASE(double, double);
                CASE(BOOL, bool);
                CASE(NSInteger, integer);
                CASE(NSUInteger, unsignedInteger);
#undef CASE
                if (!hasProcessed) {
                    [invocation setArgument:&parameter atIndex:i];
                }
            }
        }
    }
    [invocation invokeWithTarget:self];
    const char *type = methodSignature.methodReturnType;
    if (!strcmp(type, @encode(void)) || methodSignature.methodReturnLength == 0) {
        return nil;
    }
    id returnValue;
    if (!strcmp(type, @encode(id))) {
        [invocation getReturnValue:&returnValue];
        return returnValue;
    }
    //    NSNumber, 基本类型都转换位NSNumber
    void *buffer = (void *)malloc(methodSignature.methodReturnLength);
    [invocation getReturnValue:buffer];
    returnValue = FVAECreateValueFromPrimitivePointer(buffer, type);
    free(buffer);
    return returnValue;
#pragma clang diagnostic pop
}


@end

@implementation UIApplication(OpenURL)

- (void)fvae_openURL:(nonnull NSURL *)newURL {
    SEL selector = NSSelectorFromString(@"openURL:options:completionHandler:");
    [self fvae_performSelector:selector withObjects:newURL, @{}, nil, nil];
}
@end
