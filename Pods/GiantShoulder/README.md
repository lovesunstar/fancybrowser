# GiantShoulder

[![CI Status](http://img.shields.io/travis/lovesunstar@sina.com/GiantShoulder.svg?style=flat)](https://travis-ci.org/lovesunstar@sina.com/GiantShoulder)
[![Version](https://img.shields.io/cocoapods/v/GiantShoulder.svg?style=flat)](http://cocoapods.org/pods/GiantShoulder)
[![License](https://img.shields.io/cocoapods/l/GiantShoulder.svg?style=flat)](http://cocoapods.org/pods/GiantShoulder)
[![Platform](https://img.shields.io/cocoapods/p/GiantShoulder.svg?style=flat)](http://cocoapods.org/pods/GiantShoulder)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

GiantShoulder is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "GiantShoulder"
```

## Author

lovesunstar@sina.com, sunjiangting@imusics.net

## License

GiantShoulder is available under the MIT license. See the LICENSE file for more info.
