//
//  STStringTokenizer.h
//  GiantShoulder
//
//  Created by SunJiangting on 14-5-12.
//  Copyright (c) 2014年 SunJiangting. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/// <mark type=text name="name:systemFont" size="size:16" color="color:black" style="bold/itaic/plain:plain" underline="single:none"
/// strikethrough="single/thick:none">Text</mark>
/// :后面的为 default value.
/// to be continue
/// <mark type=image width=123 height=123 placeholder=123.png>123.png</mark>
@interface STStringTokenizer : NSObject

+ (nonnull NSAttributedString *)attributedStringWithMarkedString:(nonnull NSString *)markedText NS_SWIFT_NAME(attributedString(with:));
/// 分词
+ (nonnull NSDictionary *)dictionaryWithMarkedString:(nonnull NSString *)markedString NS_SWIFT_NAME(dictionary(with:));

@end

@interface UILabel (STStringTokenizer)

- (void)setMarkedText:(nonnull NSString *)markedText;

@end

@interface NSString (STStringTokenizer)

- (nonnull NSString *)st_stringByTrimingQuotation;

@end
