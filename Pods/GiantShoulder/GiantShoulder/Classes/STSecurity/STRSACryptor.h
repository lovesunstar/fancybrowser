//
//  STRSACryptor.h
//  GiantShoulder
//
//  Created by SunJiangting on 14-9-19.
//  Copyright (c) 2014年 SunJiangting. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Security/Security.h>

/**
 * @abstract 根据格式，从Data中读取公钥/私钥
 *
 * @param data      密钥的data
 * @return 密钥(autorelease)
 */
extern SecKeyRef __nullable STSecPublicKeyFromDERData(NSData *__nonnull data);
/**
 * @abstract 根据格式，从Data中读取公钥/私钥
 *
 * @param base64String 密钥经过base64编码之后的数据
 * @return 密钥(autorelease)
 */
extern SecKeyRef __nullable STSecPublicKeyFromDERBase64String(NSString *__nonnull base64String);

extern SecKeyRef __nullable STSecPublicKeyFromPEMData(NSData *__nonnull data);
extern SecKeyRef __nullable STSecPublicKeyFromPEMBase64String(NSString *__nonnull base64String);

extern SecKeyRef __nullable STSecPrivateKeyFromPEMData(NSData *__nonnull data);
extern SecKeyRef __nullable STSecPrivateKeyFromPEMBase64String(NSString *__nonnull base64String);

/// 目前只支持有密码的P12文件读取，如果没有密码的，iOS库本身不支持
extern SecKeyRef __nullable STSecPrivateKeyFromP12Data(NSData *__nonnull data, NSString *__nonnull password);

/// 比较两个key是否相同
extern BOOL STSecKeyEqualToSecKey(SecKeyRef __nonnull key1, SecKeyRef __nonnull key2);

@interface STRSACryptor : NSObject

- (nonnull instancetype)initWithPublicSecKey:(nonnull SecKeyRef)publicSecKey privateSecKey:(nonnull SecKeyRef)privateSecKey;

@property(nonatomic, readonly) NSInteger padding;

/**
 * @abstract 使用私钥对data进行签名
 *
 * @param   data 需要签名的数据
 * @return  返回签名后的数据
 */
- (nullable NSData *)signData:(nonnull NSData *)data;

/**
 * @abstract 使用公钥验证签名是否有效
 *
 * @param   signature  使用私钥签名之后的数据
 * @param   signedData 原始 未被签名的数据
 * @return  是否验证通过
 */
- (BOOL)verifySignature:(nonnull NSData *)signature signedData:(nonnull NSData *)signedData;

/// 使用公钥加密/私钥解密 data : 需要加/解密的数据
- (nullable NSData *)encryptData:(nonnull NSData *)data;
- (nullable NSData *)decryptData:(nonnull NSData *)data;

@end
extern NSString *__nonnull const STSecAttrApplicationTag;
