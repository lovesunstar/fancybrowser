//
//  STAESCryptor.h
//  GiantShoulder
//
//  Created by SunJiangting on 14-9-19.
//  Copyright (c) 2014年 SunJiangting. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_OPTIONS(NSInteger, STAESCryptorOptions){
    STAESCryptorOptionsKeySize128   = 1 << 0, // default
    STAESCryptorOptionsKeySize192   = 1 << 1,
    STAESCryptorOptionsKeySize256   = 1 << 2,

    /// 如果需要加密的数据 不是整Key对应的size的整数倍，补全方式
    STAESCryptorOptionsNoPadding    = 1 << 8,
    STAESCryptorOptionsZerosPadding = 1 << 9, // 补0
    STAESCryptorOptionsPKCS5Padding = 1 << 10,
    STAESCryptorOptionsPKCS7Padding = STAESCryptorOptionsPKCS5Padding, // default

    // 加密模式
    STAESCryptorOptionsCBCMode      = 1 << 15, // default
    STAESCryptorOptionsECBMode      = 1 << 16,
};

@interface STAESCryptor : NSObject

- (nonnull instancetype)initWithKey:(nonnull NSString *)key;

- (nonnull instancetype)initWithKey:(nonnull NSString *)key options:(STAESCryptorOptions)options;

/**
 * @abstract 创建 AES
 *
 * @param key 密钥
 * @param options  default STAESCryptorOptionsKeySize128 | STAESCryptorOptionsPKCS7Padding | STAESCryptorOptionsCBCMode
 * @param iv       初始向量，默认为空，ECB模式下，这个参数无用
 */
- (nonnull instancetype)initWithKey:(nonnull NSString *)key options:(STAESCryptorOptions)options iv:(nullable NSString *)iv;

@property(nonnull, nonatomic, copy, readonly) NSString *key;
/// 初始向量, 默认为空。会根据 加密方式128,192,256 截取keysize。如果不足，则0填充
@property(nullable, nonatomic, copy, readonly) NSString *iv;

@property(nonatomic, readonly) STAESCryptorOptions options;

- (nullable NSData *)encryptData:(nonnull NSData *)data;
- (nullable NSData *)decryptData:(nonnull NSData *)data;

@end

@interface NSData (STAESCryptor)

/// default options  STAESCryptorOptionsPKCS7Padding | STAESCryptorOptionsCBCMode
- (nullable NSData *)AES128EncryptedDataWithKey:(nonnull NSString *)key;
- (nullable NSData *)AES192EncryptedDataWithKey:(nonnull NSString *)key;
- (nullable NSData *)AES256EncryptedDataWithKey:(nonnull NSString *)key;

- (nullable NSData *)decryptAES128DataWithKey:(nonnull NSString *)key;
- (nullable NSData *)decryptAES192DataWithKey:(nonnull NSString *)key;
- (nullable NSData *)decryptAES256DataWithKey:(nonnull NSString *)key;

- (nullable NSData *)AESEncryptedDataWithKey:(nonnull NSString *)key options:(STAESCryptorOptions)options;
- (nullable NSData *)decryptAESWithKey:(nonnull NSString *)key options:(STAESCryptorOptions)options;

@end
