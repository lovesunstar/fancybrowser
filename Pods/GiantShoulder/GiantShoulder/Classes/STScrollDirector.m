//
//  STScrollDirector.m
//  GiantShoulder
//
//  Created by SunJiangting on 14-5-10.
//  Copyright (c) 2014年 SunJiangting. All rights reserved.
//

#import "STScrollDirector.h"
#import "STResourceManager.h"

@interface STAccessoryView ()

@property(nonnull, nonatomic, strong) UIImageView *imageView;

@property(nonnull, nonatomic, strong) UILabel *textLabel;

@property(nonnull, nonatomic, strong) UIButton *reloadButton;

@end

@implementation STAccessoryView

- (instancetype)initWithFrame:(CGRect)frame {
    if (frame.size.width < STAccessoryViewMinimumSize.width) {
        frame.size.width = STAccessoryViewMinimumSize.width;
    }
    if (frame.size.height < STAccessoryViewMinimumSize.height) {
        frame.size.height = STAccessoryViewMinimumSize.height;
    }
    self = [super initWithFrame:frame];
    if (self) {
        CGFloat width = CGRectGetWidth(frame), height = CGRectGetHeight(frame);
        CGFloat leftMargin = (width - STAccessoryViewMinimumSize.width) / 2, topMargin = (height - STAccessoryViewMinimumSize.height) / 2;

        UIView *contentView =
            [[UIView alloc] initWithFrame:CGRectMake(leftMargin, topMargin, STAccessoryViewMinimumSize.width, STAccessoryViewMinimumSize.height)];
        contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        [self addSubview:contentView];

        self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(77, 10, 45, 45)];
        self.imageView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        [contentView addSubview:self.imageView];

        self.textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 70, 200, 20)];
        self.textLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        self.textLabel.backgroundColor = [UIColor clearColor];
        self.textLabel.textAlignment = NSTextAlignmentCenter;
        [contentView addSubview:self.textLabel];
    }
    return self;
}

@end

CGSize const STAccessoryViewMinimumSize = {200, 100};

/// 可以为scrollView添加下拉刷新和分页加载更多。
@interface STScrollDirector : NSObject
/// 主要提供set方法。。
@property(nullable, nonatomic, strong) UIScrollView *scrollView;

@property(null_resettable, nonatomic, strong) STRefreshControl *refreshControl;
@property(null_resettable, nonatomic, strong) STPaginationControl *paginationControl;

@end

@interface STScrollDirector (STDefaultControl)

/// @see STRefreshControlState @see STPaginationControlState
- (void)setTitle:(NSString *__nullable)title forState:(STScrollDirectorState)state;
- (nullable NSString *)titleForState:(NSInteger)state;

@end

@interface STScrollDirector ()

@property(nonatomic, strong) NSMutableDictionary *titleDictionary;

@end

@implementation STScrollDirector

- (void)dealloc {
    self.scrollView = nil;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.titleDictionary = [NSMutableDictionary dictionaryWithCapacity:5];
    }
    return self;
}

- (STPaginationControl *)paginationControl {
    if (!_paginationControl) {
        STDefaultPaginationControl *paginatinControl = [[STDefaultPaginationControl alloc] init];
        if ([self titleForState:STScrollDirectorStatePaginationNormal]) {
            [paginatinControl setTitle:[self titleForState:STScrollDirectorStatePaginationNormal] forState:STPaginationControlStateNormal];
            ;
        }
        if ([self titleForState:STScrollDirectorStatePaginationLoading]) {
            [paginatinControl setTitle:[self titleForState:STScrollDirectorStatePaginationLoading] forState:STPaginationControlStateLoading];
        }
        if ([self titleForState:STScrollDirectorStatePaginationFailed]) {
            [paginatinControl setTitle:[self titleForState:STScrollDirectorStatePaginationFailed] forState:STPaginationControlStateFailed];
        }
        if ([self titleForState:STScrollDirectorStatePaginationReachedEnd]) {
            [paginatinControl setTitle:[self titleForState:STScrollDirectorStatePaginationReachedEnd] forState:STPaginationControlStateReachedEnd];
        }
        _paginationControl = paginatinControl;
    }
    return _paginationControl;
}

- (STRefreshControl *)refreshControl {
    if (!_refreshControl) {
        STDefaultRefreshControl *refreshControl = [[STDefaultRefreshControl alloc] init];
        if ([self titleForState:STScrollDirectorStateRefreshNormal]) {
            [refreshControl setTitle:[self titleForState:STScrollDirectorStateRefreshNormal] forState:STRefreshControlStateNormal];
        }
        if ([self titleForState:STScrollDirectorStateRefreshReachedThreshold]) {
            [refreshControl setTitle:[self titleForState:STScrollDirectorStateRefreshReachedThreshold]
                            forState:STRefreshControlStateReachedThreshold];
        }
        if ([self titleForState:STScrollDirectorStateRefreshLoading]) {
            [refreshControl setTitle:[self titleForState:STScrollDirectorStateRefreshLoading] forState:STRefreshControlStateLoading];
        }
        _refreshControl = refreshControl;
    }
    if (!_refreshControl.superview && self.scrollView) {
        [self.scrollView addSubview:_refreshControl];
    }
    return _refreshControl;
}

- (void)setScrollView:(UIScrollView *)scrollView {
    if (_scrollView) {
        [_refreshControl removeFromSuperview];
        [_paginationControl removeFromSuperview];
    }
    if (_refreshControl) {
        [scrollView addSubview:_refreshControl];
    }
    _scrollView = scrollView;
}

@end

@implementation STScrollDirector (STDefaultControl)

- (NSString *)titleForState:(NSInteger)state {
    NSString *key = [NSString stringWithFormat:@"%ld", (long)state];
    return [self.titleDictionary valueForKey:key];
}

- (void)setTitle:(NSString *)title forState:(STScrollDirectorState)state {
    NSString *key = [NSString stringWithFormat:@"%ld", (long)state];
    [self.titleDictionary setValue:title forKey:key];
    if (state < 10) {
        if ([_refreshControl isKindOfClass:[STDefaultRefreshControl class]]) {
            [((STDefaultRefreshControl *)_refreshControl)setTitle:title forState:state - 1];
        }
    } else {
        if ([_paginationControl isKindOfClass:[STDefaultPaginationControl class]]) {
            [((STDefaultPaginationControl *)_paginationControl)setTitle:title forState:state - 11];
        }
    }
}

@end

@implementation UIScrollView (STScrollDirector)

const char *STScrollRefreshControlKey = "STScrollRefreshControlKey";
- (void)st_setRefreshControl:(STRefreshControl *)st_refreshControl {
    UIView *oldRefreshControl = objc_getAssociatedObject(self, STScrollRefreshControlKey);
    if ([oldRefreshControl isKindOfClass:[UIView class]]) {
        [oldRefreshControl removeFromSuperview];
    }
    if ([st_refreshControl isKindOfClass:[STRefreshControl class]]) {
        [self addSubview:st_refreshControl];
    } else {
        st_refreshControl = nil;
    }
    objc_setAssociatedObject(self, STScrollRefreshControlKey, st_refreshControl, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (STRefreshControl *)st_refreshControl {
    STRefreshControl *refreshControl = objc_getAssociatedObject(self, STScrollRefreshControlKey);
    if (![refreshControl isKindOfClass:[STRefreshControl class]]) {
        refreshControl = [[STDefaultRefreshControl alloc] init];
        [self st_setRefreshControl:refreshControl];
    }
    return refreshControl;
}

const char *STScrollPaginationControlKey = "STScrollPaginationControlKey";
- (void)st_setPaginationControl:(STPaginationControl *)st_paginationControl {
    UIView *oldRefreshControl = objc_getAssociatedObject(self, STScrollPaginationControlKey);
    if ([oldRefreshControl isKindOfClass:[UIView class]]) {
        [oldRefreshControl removeFromSuperview];
    }
    if ([st_paginationControl isKindOfClass:[STPaginationControl class]]) {
        objc_setAssociatedObject(self, STScrollPaginationControlKey, st_paginationControl, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
}

- (STPaginationControl *)st_paginationControl {
    STPaginationControl *paginationControl = objc_getAssociatedObject(self, STScrollPaginationControlKey);
    if (![paginationControl isKindOfClass:[STPaginationControl class]]) {
        paginationControl = [[STDefaultPaginationControl alloc] init];
        [self st_setPaginationControl:paginationControl];
    }
    return paginationControl;
}

@end
