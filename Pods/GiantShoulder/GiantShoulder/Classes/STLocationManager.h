//
//  STLocationManager.h
//  GiantShoulder
//
//  Created by SunJiangting on 14-5-13.
//  Copyright (c) 2014年 SunJiangting. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

typedef void (^STLocationHandler)(CLLocation * __nullable location, NSError * __nullable error);

typedef void (^STGeoReverseHandler)(CLPlacemark * __nullable placemark, NSError * __nullable error);

@interface STLocationManager : NSObject
#if FOUNDATION_SWIFT_SDK_EPOCH_AT_LEAST(8)
@property(class, nonatomic, readonly, nonnull) STLocationManager *sharedManager;
#else
+ (nonnull instancetype)sharedManager;
#endif
- (void)requestWhenInUseAuthorization;
- (void)requestAlwaysAuthorization;

- (void)obtainLocationWithHandler:(nullable STLocationHandler)handler;

- (void)obtainLocationWithGeoHandler:(nullable STGeoReverseHandler)geoReverseHandler;
/// first step. obtain location then reverse geocode
- (void)obtainLocationWithHandler:(nullable STLocationHandler)handler reverseGeocodeHandler:(nullable STGeoReverseHandler)geoReverseHandler;

- (void)reverseLocation:(nonnull CLLocation *)location reverseHandler:(nonnull STGeoReverseHandler)geoReverseHandler;

/// default nil, if has been located once, it will be the lastest location
@property(nullable, nonatomic, strong) CLLocation *location;
@property(nullable, nonatomic, strong) CLPlacemark *placemark;
@property(nullable, nonatomic, strong) NSError *error;

@end

extern NSString * __nonnull const STLocationDidObtainNotification;
extern NSString * __nonnull const STLocationDidReverseNotification;
