//
//  STPersistence.h
//  GiantShoulder
//
//  Created by SunJiangting on 13-12-8.
//  Copyright (c) 2013年 SunJiangting. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GiantShoulder/Foundation+GiantShoulder.h>

typedef NS_ENUM(NSInteger, STPersistenceDirectory) {
    STPersistenceDirectoryDocument,  // document 目录
    STPersistenceDirectoryLibiary,   // Libiary  目录
    STPersistenceDirectoryCache,     // Cache 目录
    STPersistenceDirectoryTemporary, // 临时目录
};

ST_ASSUME_NONNULL_BEGIN

extern NSString *STPersistDocumentDirectory(void);
extern NSString *STPersistLibiaryDirectory(void);
extern NSString *STPersistCacheDirectory(void);
extern NSString *STPersistTemporyDirectory(void);

@interface STPersistence : NSObject

- (instancetype)initWithDirectory:(STPersistenceDirectory)directory
                          subpath:(STNULLABLE NSString *)subpath;

- (void)setValue:(STNULLABLE id)value forKey:(NSString *)key;

- (STNULLABLE id)valueForKey:(NSString *)key;
- (BOOL)containsValueForKey:(NSString *)key;

- (STNONNULL NSString *)cacheDirectory;
- (NSString *)cachedPathForKey:(NSString *)key;

@end

/// 通过以下方法创建的都会都会根据name来创建name.plist.相同name会被存储到单独的文件中
@interface STPersistence (STFileBased)
#if FOUNDATION_SWIFT_SDK_EPOCH_AT_LEAST(8)
@property(class, nonatomic, readonly, nonnull) STPersistence *standardPersistence;
#else
+ (instancetype)standardPersistence;
#endif
+ (instancetype)persistenceNamed:(STNULLABLE NSString *)name;

@end


@interface STPersistence (STPersistCreation)

#if FOUNDATION_SWIFT_SDK_EPOCH_AT_LEAST(8)
@property(class, nonatomic, readonly, nonnull) STPersistence *documentPersistence;
#else
+ (instancetype)documentPersistence;
#endif

#if FOUNDATION_SWIFT_SDK_EPOCH_AT_LEAST(8)
@property(class, nonatomic, readonly, nonnull) STPersistence *libiaryPersistence;
#else
+ (instancetype)libiaryPersistence;
#endif

#if FOUNDATION_SWIFT_SDK_EPOCH_AT_LEAST(8)
@property(class, nonatomic, readonly, nonnull) STPersistence *temporaryPersistence;
#else
+ (instancetype)tempoaryPersistence;
#endif

#if FOUNDATION_SWIFT_SDK_EPOCH_AT_LEAST(8)
@property(class, nonatomic, readonly, nonnull) STPersistence *cachePersistence;
#else
+ (instancetype)cachePersistence;
#endif
+ (instancetype)documentPersistenceWithSubpath:(STNULLABLE NSString *)subpath NS_SWIFT_NAME(document(with:));
+ (instancetype)libiaryPersistenceWithSubpath:(STNULLABLE NSString *)subpath NS_SWIFT_NAME(libiary(with:));
+ (instancetype)cachePersistenceWithSubpath:(STNULLABLE NSString *)subpath NS_SWIFT_NAME(cache(with:));
+ (instancetype)temporaryPersistenceWithSubpath:(STNULLABLE NSString *)subpath NS_SWIFT_NAME(temporary(with:));

@end

@interface STPersistence (STPersistenceClean)
- (unsigned long long)cachedSize;
- (void)calculateCacheSizeWithCompletionHandler:(void(^)(unsigned long long))completionHandler NS_SWIFT_NAME(calculateCacheSize(_:));
- (void)calculateCacheSizeInQueue:(STNULLABLE dispatch_queue_t)backgroundQueue completionHandler:(void(^)(unsigned long long))completionHandler NS_SWIFT_NAME(calculateCacheSize(in:completion:));
/// 这个对 persistenceNamed的无效， persistenceNamed的需要使用reset/removeAllCachedValues来清空
- (void)removeCachedValuesBeforeDate:(STNULLABLE NSDate *)date NS_SWIFT_NAME(removeCachedValues(before:));
/// 
- (void)removeAllCachedValues;

@end
ST_ASSUME_NONNULL_END
