//
//  STLabel.h
//  GiantShoulder
//
//  Created by SunJiangting on 13-10-26.
//  Copyright (c) 2013年 SunJiangting. All rights reserved.
//

#import <GiantShoulder/STDefines.h>
#import <UIKit/UIKit.h>

/**
 * @abstract 为Label添加垂直对齐方式和内容间距
 */
@interface STLabel : UILabel
/// 垂直方向上的对其方式
@property(nonatomic) STVerticalAlignment verticalAlignment;
/// 内容间距
@property(nonatomic) UIEdgeInsets contentInsets;
@end
