//
//  STResourceManager.h
//  GiantShoulder
//
//  Created by SunJiangting on 14-5-11.
//  Copyright (c) 2014年 SunJiangting. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/// 目前只支持以下列表中的资源ID
@interface STResourceManager : NSObject
/// 根据ResourceID 生成image。
+ (nullable UIImage *)imageWithResourceID:(nonnull NSString *)resourceID;

@end

/// 下拉可以刷新，箭头
extern NSString *__nonnull const STImageResourceRefreshControlArrowID;
extern NSString *__nonnull const STImageResourceAccessoryDataZeroID;
/// 导航返回按钮
extern NSString *__nonnull const STImageResourceNavigationItemBackID;
/// 相册选中效果
extern NSString *__nonnull const STImageResourceImagePickerSelectedID;
extern NSString *__nonnull const STImageResourceImagePickerLockedID;

extern NSString *__nonnull const STImageResourcePlaceholderID;
extern NSString *__nonnull const STImageResourceSaveToAlbumID;
extern NSString *__nonnull const STImageResourceNavigationBarID;
