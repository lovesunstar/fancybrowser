//
//  Foundation+STKit.h
//  GiantShoulder
//
//  Created by SunJiangting on 13-10-5.
//  Copyright (c) 2013年 SunJiangting. All rights reserved.
//

#import <GiantShoulder/STDefines.h>
#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <objc/runtime.h>

ST_ASSUME_NONNULL_BEGIN
ST_EXTERN void STExchangeSelectors(Class _Nonnull aClass, SEL _Nonnull oldSelector, SEL _Nonnull newSelector);

ST_EXTERN void STClassAddMethod(Class _Nonnull aClass, SEL _Nonnull selector, Method _Nonnull method);

/// 是否是某个类的子类
ST_EXTERN BOOL STClassIsKindOfClass(Class _class, Class parentClass);
/// 根据基本类型+值，转换成NSValue， 不包含CGRect等等
ST_EXTERN NSValue *STCreateValueFromPrimitivePointer(void *pointer, const char *objCType);
/// 某个类是否响应某个类方法
ST_EXTERN BOOL STClassRespondsToSelector(Class class, SEL aSelector);

ST_EXTERN void STPrintClassMethods(Class cls);
ST_EXTERN void STPrintClassProperties(Class cls);

#pragma mark - STRadian Degree
ST_EXTERN CGFloat STRadianToDegree(CGFloat radian);

ST_EXTERN CGFloat STDegreeToRadian(CGFloat degree);

ST_EXTERN NSString *STTemporaryDirectory(void);
ST_EXTERN NSString *STDocumentDirectory(void);
ST_EXTERN NSString *STLibiaryDirectory(void);
ST_EXTERN NSString *STCacheDirectory(void);

ST_EXTERN BOOL STIsIPhoneX(void);

ST_EXTERN void STSetStatusBarOrientation(UIInterfaceOrientation orientation, BOOL animated);

#pragma mark - BitOffset
ST_EXTERN BOOL STGetBitOffset(NSInteger value, NSInteger offset);

ST_EXTERN NSInteger STSetBitOffset(NSInteger value, NSInteger bit, BOOL t);

ST_EXTERN NSInteger STCleanBitOffset(NSInteger value, NSInteger bit);
ST_ASSUME_NONNULL_END

typedef void (^ STObserveHandler) (id __nullable obj, NSString * __nullable keyPath, NSDictionary *__nullable change);

@interface NSObject (STKit)
/**
 * @brief 给全局变量赋值/读取
 *
 * @param value  全局变量的新值
 * @param varName  全局变量名称.PS:属性的话记得加下划线 _property
 *
 * @discussion 如果是基本类型的var的话需要将value转换成 void *
 *
 */
- (void)st_setValue:(id __nullable)value forVar:(NSString *__nonnull)varName;
- (nullable id)st_valueForVar:(NSString *__nonnull)varName;
/**
 * @brief 该类是否响应某个selector的类方法
 *
 * @param aSelector 类方法名称。
 *
 * @attention 不要和对象的respondsToSelector:搞混了，这个是对于某个Class的
 */
+ (BOOL)st_classRespondsToSelector:(SEL __nonnull)aSelector;

@end

@interface NSObject (STPerformSelector)
/// 注明： 如果返回值为基本类型，struct除外，其余都转换为NSNumber。 如果返回值是struct。则转为NSValue,
/// 如果selector不存在，则直接返回nil, 如果参数不足，则nil填充。如果selector参数类型为基本类型，则可以直接传NSNumber
- (nullable id)st_performSelector:(SEL __nonnull)aSelector withObjects:(id __nullable)object, ... __attribute__((sentinel(0, 1)));

@end

@interface NSObject (STKeyValueObserver)

/**
 * @brief 添加自动释放的 KVO
 *
 * @attention 不要和对象的respondsToSelector:搞混了，这个是对于某个Class的
 */
- (void)st_observeKeyPath:(NSString * __nonnull)keyPath usingBlock:(STObserveHandler __nonnull)block;
- (void)st_observeKeyPath:(NSString * __nonnull)keyPath options:(NSKeyValueObservingOptions)options usingBlock:(STObserveHandler __nonnull)block;

- (void)st_removeObserveBlocksForKeyPath:(NSString * __nonnull)keyPath;
- (void)st_removeAllObserveBlocks;

@end

@interface NSString (STKit)
/// 是否包含子字符串
- (BOOL)st_contains:(NSString *__nonnull)substring;
/// 过滤空格
- (nonnull NSString *)st_stringByTrimingWhitespace;
/// substring的range
- (nullable NSArray<NSString *> *)st_rangesOfString:(NSString *__nonnull)string;
/// 使用正则表达式将字符串分割，array中不包含正则表达式
- (nullable NSArray<NSString *> *)st_componentsSeparatedByRegex:(NSString *__nonnull)regex;
/// ranges substring's range
- (nullable NSArray<NSString *> *)st_componentsSeparatedByRegex:(NSString *__nonnull)regex ranges:(NSArray *__nonnull*__nullable)ranges;
/// ranges 表示正则表达式的区间。 里面为字符串，使用 NSRangeFromString可以直接解析
- (nullable NSArray<NSString *> *)st_componentsSeparatedByRegex:(NSString *__nonnull)regex regexRanges:(NSArray *__nonnull*__nullable)ranges;
- (nullable NSArray<NSString *> *)st_componentsSeparatedByRegex:(NSString *__nonnull)regex ranges:(NSArray *__nonnull*__nullable)ranges checkingResults:(NSArray *__nonnull*__nullable)checkingResults;
- (nonnull NSString *)st_stringByAddingHTMLEscapes;
- (nonnull NSString *)st_stringByReplacingHTMLEscapes;
- (nullable NSData *)st_UTF8EncodedData;
/// md5 加密
- (nonnull NSString *)st_md5String;
- (nonnull NSString *)st_sha1String;

@end

@interface NSData (STKit)

+ (nullable NSData *)st_dataWithBase64EncodedString:(NSString *__nonnull)base64String;

- (nonnull NSString *)st_base64String;

- (nullable NSString *)st_UTF8String;

- (nonnull NSString *)st_md5String;

- (nonnull NSString *)st_hexString;

@end

typedef NS_ENUM(NSInteger, STBookSeekDirection) {
    STBookSeekDirectionForward = 1,
    STBookSeekDirectionReverse = 2,
};

/// @require <CoreText/CoreText.h>
@interface NSString (STPagination)

- (nonnull NSString *)st_reversedString;

@end

@class UIFont;
@interface NSString (STDrawSize)

- (CGFloat)st_heightWithFont:(UIFont *__nonnull)font constrainedToWidth:(CGFloat)size;

@end

#pragma mark - NSNotificationOnMainThread
@interface NSNotificationCenter (STPostOnMainThread)

- (void)st_postNotificationOnMainThread:(NSString *__nonnull)aName;
- (void)st_postNotificationOnMainThreadWithName:(NSString *__nonnull)aName object:(id __nullable)anObject;
- (void)st_postNotificationOnMainThreadWithName:(NSString *__nonnull)aName object:(id __nullable)anObject userInfo:(NSDictionary * __nullable)aUserInfo;

@end

/**
 * @abstract timer fired时候的回调
 *
 * @param    timer 所创建的timer
 * @param    invalidate  是否invalidate当前的timer
 */
typedef void(^STTimerFiredHandler) (NSTimer * __nonnull timer, BOOL *__nonnull invalidate);
/// 使用block的形式，简化timer的使用
@interface NSTimer (STBlock)
+ (nonnull NSTimer *)st_timerWithTimeInterval:(NSTimeInterval)timeInterval firedHandler:(STTimerFiredHandler __nonnull)handler;
+ (nonnull NSTimer *)st_scheduledTimerWithTimeInterval:(NSTimeInterval)timeInterval firedHandler:(STTimerFiredHandler __nonnull)handler;
- (nonnull instancetype)st_initWithFireDate:(NSDate *__nonnull)date interval:(NSTimeInterval)interval  firedHandler:(STTimerFiredHandler __nonnull)handler;

@end


#pragma mark - NSDateComponents
@interface NSDate (STKit)

@property(readonly) NSInteger year;
@property(readonly) NSInteger month;
@property(readonly) NSInteger day;

@property(readonly) NSInteger hour;
@property(readonly) NSInteger minute;
@property(readonly) NSInteger second;

@property(nonnull, readonly) NSDateComponents *components;

/// MS MillSecond
+ (nonnull NSDate *)st_dateWithMSTimeIntervalSince1970:(NSTimeInterval)millisecond;

+ (nonnull NSString *)st_dateWithTimeIntervalSince1970:(NSTimeInterval)millisecond format:(NSString * __nonnull)format;

+ (nonnull NSString *)st_dateWithMSTimeIntervalSince1970:(NSTimeInterval)millisecond format:(NSString * __nonnull)format;
+ (nonnull NSString *)st_timestampFormattedStringWithTimeIntervalSince1970:(NSTimeInterval)millisecond;

- (nonnull NSString *)st_timestampFormattedString;

@end

#pragma mark - STJSON

@interface NSData (STKitJSON)

- (nullable id)JSONValue;

@end

@interface NSString (STKitJSON)

- (nullable id)JSONValue;

@end

@interface NSDictionary (STKitJSON)

+ (nullable id)dictionaryWithJSONString:(NSString *_Nonnull)JSONString;
+ (nullable id)dictionaryWithJSONData:(NSData *_Nonnull)JSONData;
- (nullable NSString *)JSONString;

@end

@interface NSArray (STKitJSON)

+ (nullable id)arrayWithJSONString:(NSString *_Nonnull)JSONString;
+ (nullable id)arrayWithJSONData:(NSData *_Nonnull)JSONData;
- (nullable NSString *)JSONString;

@end

#pragma mark - STSecureAccessor

@interface NSArray (STSecure)

- (nullable id)st_objectAtIndex:(NSUInteger)index;

@end

@interface NSDictionary (STSecure)

- (NSInteger)st_integerValueForKey:(NSString *_Nonnull)key;
- (long long)st_longLongValueForKey:(NSString *_Nonnull)key;
- (int)st_intValueForKey:(NSString *_Nonnull)key;
- (double)st_doubleValueForKey:(NSString *_Nonnull)key;
- (BOOL)st_boolValueForKey:(NSString *_Nonnull)key;

- (nullable NSArray *)st_arrayValueForKey:(NSString *_Nonnull)key;
- (nullable NSDictionary *)st_dictionaryValueForKey:(NSString *_Nonnull)key;
- (nullable NSString *)st_stringValueForKey:(NSString *_Nonnull)key;
- (nullable NSString *)st_stringAtValueForKey:(NSString *_Nonnull)key;

@end

@interface NSArray (STClass)

- (BOOL)st_containsClass:(Class _Nonnull)aClass;
- (NSUInteger)st_indexOfClass:(Class _Nonnull)aClass;
- (NSUInteger)st_firstIndexOfClass:(Class _Nonnull)aClass;
- (NSUInteger)st_lastIndexOfClass:(Class _Nonnull)aClass;

- (nullable id)st_firstObjectOfClass:(Class _Nonnull)aClass;
- (nullable id)st_lastObjectOfClass:(Class _Nonnull)aClass;

@end

@interface NSDictionary (STURLQuery)
/// connector is between key and value, separator are between each record
/// eg. @{@"name":@"suen", @"age":@(24)} will be convert to name=suen&age=24 (connector=,separator&)
- (nonnull NSString *)st_compontentsJoinedByConnector:(NSString *_Nonnull)connector separator:(NSString *_Nonnull)separator;
/// URL
- (nonnull NSString *)st_compontentsJoinedUsingURLStyle;

+ (nonnull instancetype)st_dictionaryWithURLQuery:(NSString *_Nonnull)URLQuery;

@end

@interface NSString (STNetwork)
- (nonnull NSString *)st_stringByURLEncoded;
- (nonnull NSString *)st_stringByURLDecoded;
@end

extern NSString *__nonnull STGetMachineID(void);
extern NSString *__nonnull STKitGetVersion(void);
