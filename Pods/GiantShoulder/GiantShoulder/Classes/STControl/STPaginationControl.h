//
//  STPaginationControl.h
//  GiantShoulder
//
//  Created by SunJiangting on 14-9-17.
//  Copyright (c) 2014年 SunJiangting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GiantShoulder/UIKit+GiantShoulder.h>
#import <GiantShoulder/STControlDefines.h>

/// 分页空间，列表滑动到最底部时，请求加载下一页。加载更多
@interface STPaginationControl : UIControl
/// 距离底部多少开始触发分页操作
@property(nonatomic, assign) CGFloat threshold;
/// 是否正在加载更多
@property(nonatomic, readonly, getter=isLoading) BOOL loading;

@property(nonatomic, assign) STPaginationControlState paginationState;

- (void)paginationControlDidChangedToState:(STPaginationControlState)controlState;

/// 尝试去触发LoadMore（如果条件允许的话）
- (void)paginationTest;

@end

@interface STDefaultPaginationControl : STPaginationControl

@property(nonnull, nonatomic, strong, readonly) UIButton *reloadButton;
@property(nonnull, nonatomic, strong, readonly) UILabel *titleLabel;
@property(nonnull, nonatomic, strong, readonly) UIActivityIndicatorView *indicatorView;

- (void)setTitle:(nullable NSString *)title forState:(STPaginationControlState)state;
- (nullable NSString *)titleForState:(STPaginationControlState)state;

@end

extern CGSize const STPaginationControlSize;
