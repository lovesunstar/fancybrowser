//
//  STRefreshControl.h
//  GiantShoulder
//
//  Created by SunJiangting on 14-9-17.
//  Copyright (c) 2014年 SunJiangting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GiantShoulder/STControlDefines.h>

/// 刷新控件
@interface STRefreshControl : UIControl

/// 当scrollView.contentOffset 超过这个值时，会触发刷新事件
@property(nonatomic) CGFloat threshold;

/// 是否正在刷新
@property(nonatomic, getter=isRefreshing) BOOL refreshing;

- (void)beginRefreshing;

- (void)endRefreshing;

- (void)notifyResultsWithView:(nonnull UIView *)notifyView;

/// 下拉刷新组件当前状态
@property(nonatomic, readonly) STRefreshControlState refreshControlState;

/// these are callback functions, do not call this method directly, if you want
/// to custom your refreshcontrol, you should override them to fit different
/// state.
- (void)scrollViewDidChangeContentOffset:(CGPoint)contentOffset;
- (void)refreshControlWillChangedToState:(STRefreshControlState)refreshControlState;
- (void)refreshControlDidChangedToState:(STRefreshControlState)refreshControlState;

@property(nonatomic) NSTimeInterval animationDuration;
/// 最短的加载时间
@property(nonatomic) NSTimeInterval minimumLoadingDuration;

@end

@interface STDefaultRefreshControl : STRefreshControl
// 内部的控件
@property(nonnull, nonatomic, strong, readonly) UILabel      *refreshStatusLabel;
@property(nonnull, nonatomic, strong, readonly) UIImageView  *arrowImageView;

@property(nonnull, nonatomic, strong, readonly) UIActivityIndicatorView *indicatorView;

- (void)setTitle:(nullable NSString *)title forState:(STRefreshControlState)state;
- (nonnull NSString *)titleForState:(STRefreshControlState)state;

@end

extern CGSize const STRefreshControlSize;
