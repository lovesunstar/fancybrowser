//
//  STTrashManager.h
//  GiantShoulder
//
//  Created by SunJiangting on 15/10/12.
//  Copyright © 2015年 SunJiangting. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GiantShoulder/STDefines.h>

@interface STTrashManager : NSObject
#if FOUNDATION_SWIFT_SDK_EPOCH_AT_LEAST(8)
@property(class, nonatomic, readonly, nonnull) STTrashManager *sharedManager;
#else
+ (nonnull instancetype)sharedManager;
#endif

@property(nonatomic) BOOL tryEmptyTrashWhenEnterBackground;
/// 移到废纸篓
- (BOOL)trashItemAtPath:(nonnull NSString *)path resultingItemPath:(NSString *__nullable * __nullable)outResultingPath error:(NSError *__nullable * __nullable)error;
/// 清空废纸篓
- (void)emptyTrashWithCompletionHandler:(void (^ __nullable)(BOOL finished))completionHandler NS_SWIFT_NAME(emptyTrash(_:));

/// caclulate all trash size synchronized, will block current thread
@property(nonatomic, readonly) unsigned long long trashSize;

- (void)calculateTrashSizeWithCompletionHandler:(void(^ __nullable)(unsigned long long size))completionHandler NS_SWIFT_NAME(calculateTrashSize(_:));

#if FOUNDATION_SWIFT_SDK_EPOCH_AT_LEAST(8)
@property(nonatomic) BOOL isEmptying;
#else
- (BOOL)isEmptying;
#endif

- (void)cancel;

@end
