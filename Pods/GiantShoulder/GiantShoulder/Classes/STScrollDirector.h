//
//  STScrollDirector.h
//  GiantShoulder
//
//  Created by SunJiangting on 14-5-10.
//  Copyright (c) 2014年 SunJiangting. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <GiantShoulder/STRefreshControl.h>
#import <GiantShoulder/STPaginationControl.h>

@interface STAccessoryView : UIView

@property(nonnull, nonatomic, strong, readonly) UIImageView *imageView;

@property(nonnull, nonatomic, strong, readonly) UILabel *textLabel;

@property(nonnull, nonatomic, strong, readonly) UIButton *reloadButton;

@end

extern CGSize const STAccessoryViewMinimumSize;

@interface UIScrollView (STScrollDirector)

@property(null_resettable, nonatomic, strong, setter=st_setRefreshControl:, getter=st_refreshControl) STRefreshControl *st_refreshControl;
@property(null_resettable, nonatomic, strong, setter=st_setPaginationControl:, getter=st_paginationControl) STPaginationControl *st_paginationControl;

@end
