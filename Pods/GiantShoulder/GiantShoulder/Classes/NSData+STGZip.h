//
//  NSData+STGZip.h
//  GiantShoulder
//
//  Created by SunJiangting on 15-3-26.
//  Copyright (c) 2015年 SunJiangting. All rights reserved.
//

#import <Foundation/Foundation.h>

// 需要引入 zib
@interface NSData (STGZip)

- (nullable NSData *)st_gzipCompressedDataWithError:(NSError * __nullable __autoreleasing * __nullable)error;

- (nullable NSData *)st_gzipDecompressedDataWithError:(NSError * __nullable __autoreleasing * __nullable)error;

@end

extern NSInteger const STGZipChunkSize;
extern NSInteger const STGZipDefaultMemoryLevel;
extern NSInteger const STGZipDefaultWindowBits;

extern NSString *__nonnull const STGZipErrorDomain;
