//
//  STTextView.h
//  GiantShoulder
//
//  Created by SunJiangting on 14-1-7.
//  Copyright (c) 2014年 SunJiangting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GiantShoulder/STDefines.h>
#import <GiantShoulder/STLabel.h>
/// 提供了有placeHolder的文本输入框
@interface STTextView : UITextView

@property(nonnull, nonatomic, strong, readonly) STLabel *placeholderLabel;
@property(nullable, nonatomic, copy) NSString *placeholder;

@end
