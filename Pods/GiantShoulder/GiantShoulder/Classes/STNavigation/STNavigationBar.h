//
//  STNavigationBar.h
//  GiantShoulder
//
//  Created by SunJiangting on 14-2-18.
//  Copyright (c) 2014年 SunJiangting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GiantShoulder/STDefines.h>

@interface STNavigationBar : UIView <UIAppearance, UIAppearanceContainer>

@property(nullable, nonatomic, copy) NSString    *title;
@property(nullable, nonatomic, strong) UIView    *leftBarView;
@property(nullable, nonatomic, strong) UIView    *titleView;
@property(nullable, nonatomic, strong) UIView    *rightBarView;
@property(nonnull, nonatomic, strong, readonly) UIView *contentView;
@property(nullable, nonatomic, strong) UIVisualEffect *blurEffect;

@property(nonnull, nonatomic, strong, readonly) UIView *transitionView;

@property(nonnull, nonatomic, strong, readonly) UIView *separatorView;

@property(nullable, nonatomic, copy) UIColor          *barTintColor UI_APPEARANCE_SELECTOR;
@property(nullable, nonatomic, copy) NSDictionary<NSString *,id>   *titleTextAttributes UI_APPEARANCE_SELECTOR;
@property(nullable, nonatomic, copy) UIColor *separatorColor UI_APPEARANCE_SELECTOR;

@end

typedef NS_ENUM(NSInteger, STBarButtonCustomItem) {
    STBarButtonCustomItemBack,
    STBarButtonCustomItemDismiss,
    STBarButtonCustomItemMore,
};

@interface UIBarButtonItem (STKit)

+ (nonnull instancetype)backBarButtonItemWithTarget:(nullable id)target action:(nullable SEL)action;
- (nonnull instancetype)initWithBarButtonCustomItem:(STBarButtonCustomItem)customItem target:(nullable id)target action:(nullable SEL)action;

- (nonnull instancetype)initWithTitle:(nullable NSString *)title target:(nullable id)target action:(nullable SEL)action;

- (nonnull instancetype)initWithTitle:(nullable NSString *)title tintColor:(nullable UIColor *)tintColor target:(nullable id)target action:(nullable SEL)action;

- (nullable UIView *)st_customView;

@end
