//
//  STImageCache.h
//  GiantShoulder
//
//  Created by SunJiangting on 13-12-17.
//  Copyright (c) 2013年 SunJiangting. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef long long STIdentifier;

extern NSString *__nonnull STImageCacheDirectory(void);

extern STIdentifier STImageCacheBeginContext(void);
extern void STImageCachePushContext(STIdentifier contextId);
extern void STImageCachePopContext(STIdentifier contextId);

@class UIImage;
@interface STImageCache : NSObject
/// 将图片缓存到文件
/**
 * @abstract 将图片缓存到文件，读取时需要通过key。图片会存储在Document下的某一目录
 *
 * @param     image     需要缓存的图片
 * @param     key       一般为图片的URL
 */
+ (void)cacheImage:(nullable UIImage *)image forKey:(nonnull NSString *)key;
+ (void)removeCacheImageForKey:(nonnull NSString *)key;

+ (void)removeMemoryCacheForKey:(nonnull NSString *)key;
+ (BOOL)hasMemoryCacheForKey:(nonnull NSString *)key;


+ (void)removeCachedImagesBeforeDate:(nullable NSDate *)date
                   completionHandler:(void(^__nullable)(void))completionHandler;

/**
 * @abstract 得到该key的图片缓存目录
 */
+ (nonnull NSString *)cachedPathForKey:(nonnull NSString *)key;
/// 是否有缓存该图片
+ (BOOL)hasCachedImageForKey:(nonnull NSString *)key;
+ (nullable UIImage *)cachedImageForKey:(nonnull NSString *)key;

+ (void)cacheData:(nullable NSData *)data forKey:(nonnull NSString *)key;
+ (nullable NSData *)cachedDataForKey:(nonnull NSString *)key;

+ (unsigned long long)cacheSize;
/// default backgroundQueue / DISPATCH_QUEUE_PRIORITY_BACKGROUND
+ (void)calculateCacheSizeWithCompletionHandler:(void(^__nonnull)(CGFloat))completionHandler;
+ (void)calculateCacheSizeInQueue:(dispatch_queue_t __nullable)backgroundQueue completionHandler:(void(^__nonnull)(CGFloat))completionHandler;

@end
