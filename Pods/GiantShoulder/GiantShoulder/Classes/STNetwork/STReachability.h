//
//  STReachability.h
//  GiantShoulder
//
//  Created by SunJiangting on 13-12-7.
//  Copyright (c) 2013年 SunJiangting. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SystemConfiguration/SCNetworkReachability.h>

typedef NS_ENUM(NSInteger, STNetworkStatus) {
    STNetworkStatusReachNone, // 网络不通
    STNetworkStatusReachWIFI, // WIFI
    STNetworkStatusReachWWAN, // GPRS,E等
};

/**
 * @abstract 此类可以获取网络的一些状况
 */
@interface STReachability : NSObject

#if FOUNDATION_SWIFT_SDK_EPOCH_AT_LEAST(8)
@property(nonatomic, strong, readonly, nonnull) STReachability *defaultReachability;
#else
+ (nonnull instancetype)defaultReachability;
#endif
/// 是否能够ping通host
+ (nonnull instancetype)reachabilityWithHost:(nonnull NSString *)host;

/// designed initializer
- (nonnull instancetype)initWithHost:(nonnull NSString *)host;

@property(nonatomic, readonly) STNetworkStatus reachabilityStatus;
@property(nonatomic, readonly) SCNetworkReachabilityFlags reachabilityFlags;

@end

@interface STReachability (STNotification)

/**
 * @abstract 开启/关闭网络状态改变的通知 @see
 *STReachabilityDidChangedNotification
 *
 * @discussion 该通知肯定在主线程发送。
 */
- (BOOL)startNotification;
- (void)stopNotification;

@end

@interface STReachability (STAccessor)

- (BOOL)reachable;
- (BOOL)reachWWAN;
- (BOOL)reachWIFI;

@end

extern BOOL STIsNetworkConnected(void);
extern BOOL STIsWIFIConnected(void);

extern NSNotificationName __nonnull const STReachabilityDidChangedNotification;
