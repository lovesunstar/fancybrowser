//
//  STTabBarController.m
//  GiantShoulder
//
//  Created by SunJiangting on 14-2-13.
//  Copyright (c) 2014年 SunJiangting. All rights reserved.
//

#import "STTabBarController.h"
#import "STTabBar.h"
#import "STTabBarItem.h"
#import "STNavigationController.h"
#import <objc/runtime.h>

@interface STTabBarController () <STTabBarDelegate> {
}

@property(nonatomic, strong) UIView *transitionView;
@property(nonatomic, strong) STTabBar *tabBar;
@property(nonatomic, strong) NSArray *tabBarItems;

- (void)updateVisibleChildController;

@end

@implementation STTabBarController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _selectedIndex = -1;
        _tabBarHeight = STCustomTabBarHeight;
        _automaticallyAdjustTabBarSafeArea = YES;
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        _selectedIndex = -1;
        _tabBarHeight = STCustomTabBarHeight;
        _automaticallyAdjustTabBarSafeArea = YES;
    }
    return self;
}

#pragma mark - TabBarViewController
- (void)setViewControllers:(NSArray *)viewControllers {
    [self setViewControllers:viewControllers animated:NO];
}

- (void)setViewControllers:(NSArray *)viewControllers animated:(BOOL)animated {
    [self willChangeValueForKey:@"viewControllers"];
    [_viewControllers enumerateObjectsUsingBlock:^(UIViewController *obj, NSUInteger idx, BOOL *stop) {
        SEL selector = NSSelectorFromString(@"st_setTabBarController:");
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        if ([obj respondsToSelector:selector]) {
            [obj performSelector:selector withObject:nil];
        }
#pragma clang diagnostic pop
        [obj willMoveToParentViewController:nil];
        [obj removeFromParentViewController];
    }];
    NSMutableArray *items = [NSMutableArray arrayWithCapacity:5];
    [viewControllers enumerateObjectsUsingBlock:^(UIViewController *obj, NSUInteger idx, BOOL *stop) {
        [self addChildViewController:obj];
        [obj didMoveToParentViewController:self];
        SEL selector = NSSelectorFromString(@"st_setTabBarController:");
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        if ([obj respondsToSelector:selector]) {
            [obj performSelector:selector withObject:self];
        }
#pragma clang diagnostic pop
        STTabBarItem *tabBarItem = obj.st_tabBarItem;
        [items addObject:tabBarItem];
    }];
    _viewControllers = viewControllers;
    self.tabBarItems = items;
    [self updateVisibleChildController];
    [self didChangeValueForKey:@"viewControllers"];
}

- (void)loadView {
    [super loadView];
    self.view.backgroundColor = [UIColor clearColor];
    
    self.tabBar.frame = CGRectMake(0, CGRectGetHeight(self.view.bounds) - [self _displayTabBarHeight], CGRectGetWidth(self.view.bounds), [self _displayTabBarHeight]);
    [self.view addSubview:self.tabBar];
    
    self.transitionView = [[UIView alloc] initWithFrame:self.view.bounds];
    self.transitionView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.transitionView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.transitionView];
    [self.view bringSubviewToFront:self.tabBar];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self updateVisibleChildController];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewSafeAreaInsetsDidChange {
    [super viewSafeAreaInsetsDidChange];
    // Layout TabBar
    [self _layoutTabBar];
}

- (void)setAutomaticallyAdjustTabBarSafeArea:(BOOL)automaticallyAdjustTabBarSafeArea {
    _automaticallyAdjustTabBarSafeArea = automaticallyAdjustTabBarSafeArea;
    [self _layoutTabBar];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self _layoutTabBar];
    self.transitionView.frame = self.view.bounds;
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self _layoutTabBar];
    self.transitionView.frame = self.view.bounds;
}

#pragma mark - SelectedViewController
- (void)setSelectedViewController:(UIViewController *)selectedViewController {
    if ([self.delegate respondsToSelector:@selector(tabBarController:shouldSelectViewController:)] &&
        ![self.delegate tabBarController:self shouldSelectViewController:selectedViewController]) {
        return;
    }
    
    if (_selectedViewController == selectedViewController) {
        SEL selector = NSSelectorFromString(@"popToRootViewControllerAnimated:");
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        if ([selectedViewController respondsToSelector:selector]) {
            [(STNavigationController *)selectedViewController popToRootViewControllerAnimated:YES];
        }
#pragma clang diagnostic pop
    } else {
        UIViewController *fromController = self.selectedViewController;
        UIViewController *toController = selectedViewController;
        [self privateSetSelectedController:toController];
        if (![self.view isDescendantOfView:self.tabBar]) {
            [self.tabBar removeFromSuperview];
            [self.view addSubview:self.tabBar];
            [self updateTabBarFrameWithTopViewController:selectedViewController];
        }
        toController.view.frame = self.transitionView.bounds;
        toController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self.transitionView addSubview:toController.view];
        [fromController.view removeFromSuperview];
        [self setNeedsStatusBarAppearanceUpdate];
    }
    if ([self.delegate respondsToSelector:@selector(tabBarController:didSelectViewController:)]) {
        [self.delegate performSelector:@selector(tabBarController:didSelectViewController:) withObject:self withObject:selectedViewController];
    }
}

- (void)setSelectedIndex:(NSUInteger)selectedIndex {
    if (selectedIndex >= self.viewControllers.count) {
        return;
    }
    self.selectedViewController = [self.viewControllers objectAtIndex:selectedIndex];
}

#pragma mark - AMTabBarDelegate
- (void)tabBar:(STTabBar *)tabbar didSelectItem:(STTabBarItem *)item {
    self.selectedIndex = [self.tabBarItems indexOfObject:item];
}

#pragma mark - Private Method
- (void)privateSetSelectedController:(UIViewController *)controller {
    _selectedViewController = controller;
    _selectedIndex = [self.viewControllers indexOfObject:_selectedViewController];
    self.tabBar.selectedItem = _selectedViewController.st_tabBarItem;
}

- (void)updateTabBarFrameWithTopViewController:(UIViewController *)topViewController {
    if (!topViewController.hidesBottomBarWhenPushed) {
        self.tabBar.frame = CGRectMake(0, CGRectGetHeight(self.view.bounds) - [self _displayTabBarHeight], CGRectGetWidth(self.view.bounds), [self _displayTabBarHeight]);
    } else {
        self.tabBar.frame = CGRectMake(-CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds) - [self _displayTabBarHeight],
                                       CGRectGetWidth(self.view.bounds), [self _displayTabBarHeight]);
    }
}

- (void)updateVisibleChildController {
    if (self.viewControllers.count == 0) {
        return;
    }
    if (!_selectedViewController) {
        _selectedViewController = [self.viewControllers objectAtIndex:0];
        _selectedIndex = 0;
    }
    /// 如果view没有加载先不用管，view加载了以后迟早还会掉用这个方法
    if (!self.isViewLoaded) {
        return;
    }
    if (![self.transitionView isDescendantOfView:self.selectedViewController.view]) {
        [self.transitionView removeAllSubviews];
        [self.selectedViewController.view removeFromSuperview];
        UIView *view = self.selectedViewController.view;
        view.frame = self.transitionView.bounds;
        [self.transitionView addSubview:view];
    }
    if (self.tabBarItems) {
        [self.tabBar setItems:self.tabBarItems];
    }
    self.tabBar.selectedItem = self.selectedViewController.st_tabBarItem;
    [self updateTabBarFrameWithTopViewController:self.selectedViewController];
    
    [self setNeedsStatusBarAppearanceUpdate];
}

- (STTabBar *)tabBar {
    if (!_tabBar) {
        CGSize screenSize = [UIScreen mainScreen].bounds.size;
        _tabBar = [[STTabBar alloc] initWithFrame:CGRectMake(0, screenSize.height - [self _displayTabBarHeight], screenSize.width, [self _displayTabBarHeight])];
        _tabBar.delegate = self;
    }
    return _tabBar;
}

- (void)setBadgeValue:(NSString *)badgeValue forIndex:(NSInteger)index {
    [_tabBar setBadgeValue:badgeValue forIndex:index];
}
- (NSString *)badgeValueForIndex:(NSInteger)index {
    return [_tabBar badgeValueForIndex:index];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return self.selectedViewController.preferredStatusBarStyle;
}

#pragma mark - Rotate
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self.selectedViewController willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    [self.selectedViewController didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}

#pragma mark - AutoRotate
- (BOOL)shouldAutorotate {
    return [self.selectedViewController shouldAutorotate];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return [self.selectedViewController supportedInterfaceOrientations];
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return [self.selectedViewController preferredInterfaceOrientationForPresentation];
}

- (void)_layoutTabBar {
    CGFloat safeBottom = 0;
    if (_automaticallyAdjustTabBarSafeArea) {
        if (@available(iOS 11.0, *)) {
            safeBottom = self.view.safeAreaInsets.bottom;
        }
    }
    self.tabBar.frame = CGRectMake(0, CGRectGetHeight(self.view.bounds) - [self _displayTabBarHeight], CGRectGetWidth(self.view.bounds), [self _displayTabBarHeight]);
}

- (CGFloat)_displayTabBarHeight {
    CGFloat safeBottom = 0;
    if (_automaticallyAdjustTabBarSafeArea) {
        if (@available(iOS 11.0, *)) {
            safeBottom = self.view.safeAreaInsets.bottom;
        }
    }
    return self.tabBarHeight + safeBottom;
}

@end

static NSString *const STCustomTabBarItemKey = @"STCustomTabBarItemKey";
static NSString *const STCustomTabBarControllerKey = @"STCustomTabBarControllerKey";
@implementation UIViewController (STTabBarControllerItem)

- (STTabBarItem *)st_tabBarItem {
    STTabBarItem *tabBarItem = objc_getAssociatedObject(self, (__bridge const void *)(STCustomTabBarItemKey));
    if (!tabBarItem) {
        tabBarItem = [[STTabBarItem alloc] initWithTitle:nil image:nil selectedImage:nil];
        [self st_setTabBarItem:tabBarItem];
    }
    return tabBarItem;
}

- (void)st_setTabBarItem:(STTabBarItem *)customTabBarItem {
    objc_setAssociatedObject(self, (__bridge const void *)(STCustomTabBarItemKey), customTabBarItem, OBJC_ASSOCIATION_RETAIN);
}

- (STTabBarController *)st_tabBarController {
    STTabBarController *tabBarController = objc_getAssociatedObject(self, (__bridge const void *)(STCustomTabBarControllerKey));
    if (!tabBarController) {
        if (self.navigationController) {
            return self.navigationController.st_tabBarController;
        } else {
            return self.st_navigationController.st_tabBarController;
        }
    }
    return tabBarController;
}

- (void)st_setTabBarController:(STTabBarController *)customTabBarController {
    objc_setAssociatedObject(self, (__bridge const void *)(STCustomTabBarControllerKey), customTabBarController, OBJC_ASSOCIATION_ASSIGN);
}

@end

const CGFloat STCustomTabBarHeight = 49;

