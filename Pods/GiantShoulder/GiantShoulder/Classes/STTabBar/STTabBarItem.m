//
//  STTabBarItem.m
//  GiantShoulder
//
//  Created by SunJiangting on 14-2-13.
//  Copyright (c) 2014年 SunJiangting. All rights reserved.
//

#import "STTabBarItem.h"
#import "STTabBar.h"

@implementation STTabBarItem

- (instancetype)initWithTitle:(NSString *)title image:(UIImage *)image selectedImage:(UIImage *)selectedImage {
    self = [super init];
    if (self) {
        self.title = title;
        self.image = image;
        self.selectedImage = selectedImage;
        
        NSDictionary *attributes = [STTabBar appearance].titleTextAttributes;
        if ([[attributes valueForKey:NSForegroundColorAttributeName] isKindOfClass:[UIColor class]]) {
            self.titleColor = [attributes valueForKey:NSForegroundColorAttributeName];
        }
        if ([[attributes valueForKey:NSFontAttributeName] isKindOfClass:[UIColor class]]) {
            self.titleFont = [attributes valueForKey:NSFontAttributeName];
        }
        NSDictionary *selectedAttributes = [STTabBar appearance].selectedTitleTextAttributes;
        if ([[selectedAttributes valueForKey:NSForegroundColorAttributeName] isKindOfClass:[UIColor class]]) {
            self.selectedTitleColor = [selectedAttributes valueForKey:NSForegroundColorAttributeName];
        }
    }
    return self;
}

- (void)setBadgeValue:(NSString *)badgeValue {
    _badgeValue = badgeValue;
    if ([self.itemView respondsToSelector:@selector(setBadgeValue:)]) {
        [self.itemView performSelector:@selector(setBadgeValue:) withObject:badgeValue];
    }
}

@end
