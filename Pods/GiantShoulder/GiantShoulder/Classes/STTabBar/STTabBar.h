//
//  STTabBar.h
//  GiantShoulder
//
//  Created by SunJiangting on 14-2-13.
//  Copyright (c) 2014年 SunJiangting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GiantShoulder/STDefines.h>

@protocol STTabBarDelegate;
@class STTabBarItem;
/// UITabBar
@interface STTabBar : UIView <UIAppearance, UIAppearanceContainer>

@property(nullable, nonatomic, assign) id<STTabBarDelegate> delegate; // weak reference. default is nil
@property(nullable, nonatomic, copy) NSArray *items;                  // get/set visible UITabBarItems. default is nil. changes not animated. shown in order
@property(nullable, nonatomic, assign) STTabBarItem *selectedItem;    // will show feedback based on mode. default is nil
/*
 The behavior of tintColor for bars has changed on iOS 7.0. It no longer affects the bar's background
 and behaves as described for the tintColor property added to UIView.
 To tint the bar's background, please use -barTintColor.
 */
@property(nullable, nonatomic, retain) UIColor *barTintColor UI_APPEARANCE_SELECTOR;         // default is nil
@property(nullable, nonatomic, copy) NSDictionary<NSString *,id>   *titleTextAttributes UI_APPEARANCE_SELECTOR;
@property(nullable, nonatomic, copy) NSDictionary<NSString *,id>   *selectedTitleTextAttributes UI_APPEARANCE_SELECTOR;
/* selectedImageTintColor will be applied to the gradient image used when creating the
 selected image. Default is nil and will result in the system bright blue for selected
 tab item images. If you wish to also customize the unselected image appearance, you must
 use -setFinishedSelectedImage:finishedUnselectedImage: on individual tab bar items.
 */
@property(nullable, nonatomic, retain) UIColor *selectedImageTintColor;

/* The background image will be tiled to fit, even if it was not created via the UIImage resizableImage methods.
 */
@property(nullable, nonatomic, retain) UIImage *backgroundImage;

@property(nonatomic, getter=isTranslucent) BOOL translucent;

/// default NO, if YES you need to custom add subview
@property(nonatomic, assign) BOOL customizable;

- (void)setBadgeValue:(nullable NSString *)badgeValue forIndex:(NSInteger)index;
- (nullable NSString *)badgeValueForIndex:(NSInteger)index;

@property(nonnull, nonatomic, strong, readonly) UIView *separatorView;

@end

@protocol STTabBarDelegate <NSObject>
@optional

- (void)tabBar:(nonnull STTabBar *)tabBar didSelectItem:(nonnull STTabBarItem *)item; // called when a new view is selected by the user (but not programatically)
@end
